package net.ihe.gazelle.test;

import java.util.List;

import net.ihe.gazelle.gen.common.Concept;
import net.ihe.gazelle.gen.common.SVSConsumer;

public class GetSVS {
	
	public static void main(String[] args) {
		SVSConsumer sc = new SVSConsumer(){
			protected String getSVSRepositoryUrl()
			{
				return "http://gazelle.ihe.net/SVSSimulator/rest/RetrieveValueSetForSimulator";
			}
		};
		List<Concept> cc = sc.getConceptsListFromValueSet("1.3.6.1.4.1.12559.11.10.1.3.1.42.5", null);
		System.out.println("---");
		for (Concept concept : cc) {
			if (!concept.getDisplayName().contains("icd-10")){
				System.out.println(concept.getCode() + ";" + concept.getDisplayName());
			}
		}
	}

}
