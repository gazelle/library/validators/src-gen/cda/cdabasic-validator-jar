package net.ihe.gazelle.test;

import java.io.File;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Warning;

public class TesCDA {
	
	public static void main(String[] args) {
		DetailedResult doc = CommonUtil
				.getDetailedResultFromFile("/media/aboufahj/RZOUGOV/evsclient2/opt/EVSClient_prod/validatedObjects/CDA/validatedCDA_8592.xml");
		int i = 0;
		for (Object ss : doc.getMDAValidation().getWarningOrErrorOrNote()) {

			if (ss instanceof net.ihe.gazelle.validation.Error) {
				Error ss1 = (Error) ss;
				System.out.println(ss1.getTest() + "#" + ss1.getDescription());
				i++;
			}

			if (ss instanceof net.ihe.gazelle.validation.Info) {
				Info ss1 = (Info) ss;
				System.out.println(ss1.getTest() + "#" + ss1.getDescription());
				i++;
			}

			if (ss instanceof net.ihe.gazelle.validation.Warning) {
				Warning ss1 = (Warning) ss;
				System.out.println(ss1.getTest() + "#" + ss1.getDescription());
				i++;
			}
		}
		System.out.println(i + " Problem(s)");
	}
	
	public static int getNumberErrors(File file){
		DetailedResult doc = CommonUtil
				.getDetailedResultFromFile(file.getAbsolutePath());
		int i = 0;
		for (Object ss : doc.getMDAValidation().getWarningOrErrorOrNote()) {

			if (ss instanceof net.ihe.gazelle.validation.Error) {
				Error ss1 = (Error) ss;
				System.out.println(ss1.getTest() + "#" + ss1.getDescription());
				i++;
			}

		}
		return i;
	}

	public static int getNumberWarn(File file) {
		DetailedResult doc = CommonUtil
				.getDetailedResultFromFile(file.getAbsolutePath());
		int i = 0;
		for (Object ss : doc.getMDAValidation().getWarningOrErrorOrNote()) {

			if (ss instanceof net.ihe.gazelle.validation.Warning) {
				Warning ss1 = (Warning) ss;
				System.out.println(ss1.getTest() + "#" + ss1.getDescription());
				i++;
			}

		}
		return i;
	}
	
	public static double generateIndicatorWarn(File file) {
		DetailedResult doc = CommonUtil
				.getDetailedResultFromFile(file.getAbsolutePath());
		double res = -1;
		if (doc != null){
			if (doc.getMDAValidation() != null && doc.getMDAValidation().getWarningOrErrorOrNote().size()>0){
				res = 0;
				for (Object not : doc.getMDAValidation().getWarningOrErrorOrNote()) {
					if (not instanceof net.ihe.gazelle.validation.Warning){
						net.ihe.gazelle.validation.Warning warn = (net.ihe.gazelle.validation.Warning)not;
						Double dd = GenerateStatistics.mapWarn.get(warn.getTest());
						res = res + (dd!=null?dd:0);
					}
				}
			}
		}
		return res;
	}
	

}
