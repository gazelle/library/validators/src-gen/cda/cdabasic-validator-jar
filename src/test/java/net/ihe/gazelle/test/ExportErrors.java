package net.ihe.gazelle.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.xml.bind.JAXBException;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;

public class ExportErrors {
	
	public static void main(String[] args) throws IOException, JAXBException {
		File file = new File("/home/aboufahj/art-decor/pap/sample_ccdas-master");
		Map<String, List<ValidationResult>> maperrors = new TreeMap<String, List<ValidationResult>>();
		List<ValidationResult> listValidation = null;
		for (File fis : file.listFiles()) {
			if (fis.isDirectory()){
				String standardName = fis.getName();
				listValidation = new ArrayList<ValidationResult>();
				getListValidations(fis, listValidation);
				maperrors.put(standardName, listValidation);
			}
		}
		
		for (File fis : file.listFiles()) {
			if (fis.isDirectory()){
				String standardName = fis.getName();
				List<ValidationResult> lvr = maperrors.get(standardName);
				generateListEF(fis, lvr);
			}
		}
		
		System.out.println(maperrors);
		System.out.println("----------------------");
		for (String string : maperrors.keySet()) {
			System.out.println(string + ";" + getMoyenneError(maperrors.get(string)) + ";" + 
					getMoyenneWarn(maperrors.get(string)) + ";" + 
					getMoyenneIndicatorWarn(maperrors.get(string)));
		}
	}
	
	private static Double getMoyenneIndicatorWarn(List<ValidationResult> li) {
		double d = 0.0;
		for (ValidationResult pi : li) {
			d = d + pi.getIndicatorWarn();
		}
		return d/li.size();
	}

	static Double getMoyenneError(List<ValidationResult> li){
		double d = 0.0;
		for (ValidationResult pi : li) {
			d = d + pi.getNumberError();
		}
		return d/li.size();
	}
	
	static Double getMoyenneWarn(List<ValidationResult> li){
		double d = 0.0;
		for (ValidationResult pi : li) {
			d = d + pi.getNumberWarn();
		}
		return d/li.size();
	}

	private static void getListValidations(File file, List<ValidationResult> listValidation) throws IOException {
		if (file.isFile() && file.getName().endsWith("xml")){
			String content = readDoc(file.getAbsolutePath());
			if (content.contains("ClinicalDocument")){
				int err = TesCDA.getNumberErrors(file);
				int warr = TesCDA.getNumberWarn(file);
				double indw = TesCDA.generateIndicatorWarn(file);
				ValidationResult vr = new ValidationResult();
				vr.setNumberError(err);
				vr.setNumberWarn(warr);
				vr.setIndicatorWarn(indw);
				listValidation.add(vr);
			}
		}
		else if (file.isDirectory()){
			for (File fis : file.listFiles()) {
				getListValidations(fis, listValidation);
			}
		}
	}
	
	public static String readDoc(String name) throws IOException {
		BufferedReader scanner = new BufferedReader(new FileReader(name));
		StringBuilder res = new StringBuilder();
		try {
			String line = scanner.readLine();
			while (line != null) {
				res.append(line + "\n");
				line = scanner.readLine();
			}
		} finally {
			scanner.close();
		}
		return res.toString();
	}
	
	private static void generateListEF(File file, List<ValidationResult> lvr) throws IOException, JAXBException {
		if (file.isFile() && file.getName().endsWith("rich")){
			ValidationResult var = ValidationResult.getValidationResult(lvr, file.getAbsolutePath().replace(".rich", ".xml"));
			DetailedResult doc = CommonUtil
					.getDetailedResultFromFile(file.getAbsolutePath());
			if (var == null) System.out.println(file.getAbsolutePath());
			var.setNumberEF(generateEF(doc));
		}
		else if (file.isDirectory()){
			for (File fis : file.listFiles()) {
				//generateStatisticsForReach(fis, lvr);
			}
		}
	}
	
	private static int generateEF(DetailedResult doc) {
		int res = -1;
		Set<String> lerrk = new HashSet<String>();
		if (doc != null){
			if (doc.getMDAValidation() != null && doc.getMDAValidation().getWarningOrErrorOrNote().size()>0){
				for (Object not : doc.getMDAValidation().getWarningOrErrorOrNote()) {
					if (not instanceof net.ihe.gazelle.validation.Note){
						lerrk.add(((net.ihe.gazelle.validation.Note)not).getIdentifiant());
					}
				}
				res = lerrk.size();
			}
		}
		return res;
	}

}
