package net.ihe.gazelle.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import net.ihe.gazelle.validation.DetailedResult;

public class GenerateStatisticsValidIHE {
	
	static int num = 0;
	
	public static int totalCDA = 417;
	public static int totalDTY = 172;
	public static int totalNBL = 268;
	
	public static Map<String, Double> mapWarn = new HashMap<String, Double>();
	
	static{
		mapWarn.put("rmim068", 0.5);
		mapWarn.put("rmim098", 0.25);
		mapWarn.put("rmim064", 0.25);
		mapWarn.put("rmim114", 0.75);
		mapWarn.put("rmim115", 0.75);
		mapWarn.put("rmim070", 0.5);
		mapWarn.put("rmim102", 0.25);
		mapWarn.put("rmim052", 0.25);
		mapWarn.put("cdadt002", 0.5);
		mapWarn.put("rmim108", 0.5);
		mapWarn.put("rmim112_1", 0.25);
		mapWarn.put("rmim112_2", 0.25);
		mapWarn.put("rmim112_3", 0.25);
	}
	
	public static void main(String[] args) throws IOException, JAXBException {
		File resFile = new File("/media/aboufahj/RZOUGOV/resevsclient4/opt/EVSClient_prod/validatedObjects/CDA/");
//		File richFile = new File("/media/aboufahj/RZOUGOV/richevsclient2/");
		List<ValidationResult> lvr = new ArrayList<ValidationResult>();
		for (File fis : resFile.listFiles()) {
			try{
				generateStatistics(fis, lvr);
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
//		for (File fis : richFile.listFiles()) {
//			generateStatisticsForReach(fis, lvr);
//		}
		String res = "";
		for (ValidationResult validationResult : lvr) {
			res = res + validationResult.getAbsolutePath() + ";" + 
					validationResult.getNumberError() + ";" + 
					validationResult.getNumberErrorA() + ";" + 
					validationResult.getNumberWarn() + ";" + 
					validationResult.getNumberWarna() + ";" + 
					validationResult.getIndicatorWarn() + ";" + 
					validationResult.getIndicatorWarnA() + ";" + 
					validationResult.getNumberEF() + ";" + 
					Double.valueOf(validationResult.getNumberEF())/(totalCDA) + "\n";
		}
		FileWriter fw = new FileWriter(new File("test_res_ValidIHE_final.csv"));
		fw.write(res);
		fw.close();
	}

	private static void generateStatistics(File file, List<ValidationResult> lvr) throws IOException, JAXBException {
		if (file.isFile() && file.getName().endsWith("val") &&
				Arrays.asList(ValidIHE.validIHEFile).contains(file.getName().split("\\.")[0] + ".xml")){
			System.out.println(num++);
			ValidationResult var = new ValidationResult();
			DetailedResult doc = getDetailedResult(new FileInputStream(file));
			var.setAbsolutePath(file.getAbsolutePath().replace(".val", ".xml"));
			var.setNumberError(generateNumberError(doc));
			var.setNumberErrorA(generateNumberErrorA(doc));
			var.setNumberWarn(generateNumberWarn(doc));
			var.setNumberWarna(generateNumberWarnA(doc));
			var.setIndicatorWarn(generateIndicatorWarn(doc));
			var.setIndicatorWarnA(generateIndicatorWarnA(doc));
			lvr.add(var);
		}
		else if (file.isDirectory()){
			for (File fis : file.listFiles()) {
				generateStatistics(fis, lvr);
			}
		}
	}
	
	private static double generateIndicatorWarnA(DetailedResult doc) {
		double res = -1;
		Set<String> lerrk = new HashSet<String>();
		if (doc != null){
			if (doc.getMDAValidation() != null && doc.getMDAValidation().getWarningOrErrorOrNote().size()>0){
				for (Object not : doc.getMDAValidation().getWarningOrErrorOrNote()) {
					if (not instanceof net.ihe.gazelle.validation.Warning){
						lerrk.add(((net.ihe.gazelle.validation.Warning)not).getTest());
					}
				}
				res = 0;
				for (String warn : lerrk) {
					Double dd = mapWarn.get(warn);
					res = res + (dd!=null?dd:0);
				}
				return res;
			}
		}
		return res;
	}
	
	private static double generateIndicatorWarn(DetailedResult doc) {
		double res = -1;
		if (doc != null){
			if (doc.getMDAValidation() != null && doc.getMDAValidation().getWarningOrErrorOrNote().size()>0){
				res = 0;
				for (Object not : doc.getMDAValidation().getWarningOrErrorOrNote()) {
					if (not instanceof net.ihe.gazelle.validation.Warning){
						net.ihe.gazelle.validation.Warning warn = (net.ihe.gazelle.validation.Warning)not;
						Double dd = mapWarn.get(warn.getTest());
						res = res + (dd!=null?dd:0);
					}
				}
			}
		}
		return res;
	}
	
	private static void generateStatisticsForReach(File file, List<ValidationResult> lvr) throws IOException, JAXBException {
		if (file.isFile() && file.getName().endsWith("rich")){
			ValidationResult var = ValidationResult.getValidationResult(lvr, file.getAbsolutePath().replace(".rich", ".xml"));
			DetailedResult doc = getDetailedResult(new FileInputStream(file));
			if (var == null) {
				System.out.println(file.getAbsolutePath());
				return;
			}
			var.setNumberEF(generateEF(doc));
		}
		else if (file.isDirectory()){
			for (File fis : file.listFiles()) {
				generateStatisticsForReach(fis, lvr);
			}
		}
	}
	
	private static int generateEF(DetailedResult doc) {
		int res = -1;
		Set<String> lerrk = new HashSet<String>();
		if (doc != null){
			if (doc.getMDAValidation() != null && doc.getMDAValidation().getWarningOrErrorOrNote().size()>0){
				for (Object not : doc.getMDAValidation().getWarningOrErrorOrNote()) {
					if (not instanceof net.ihe.gazelle.validation.Note){
						lerrk.add(((net.ihe.gazelle.validation.Note)not).getIdentifiant());
					}
				}
				res = lerrk.size();
			}
		}
		return res;
	}

	private static int generateNumberWarnA(DetailedResult doc) {
		int res = -1;
		Set<String> lerrk = new HashSet<String>();
		if (doc != null){
			if (doc.getMDAValidation() != null && doc.getMDAValidation().getWarningOrErrorOrNote().size()>0){
				for (Object not : doc.getMDAValidation().getWarningOrErrorOrNote()) {
					if (not instanceof net.ihe.gazelle.validation.Warning){
						lerrk.add(((net.ihe.gazelle.validation.Warning)not).getIdentifiant());
					}
				}
				res = lerrk.size();
			}
		}
		return res;
	}
	
	private static int generateNumberWarn(DetailedResult doc) {
		int res = -1;
		if (doc != null){
			if (doc.getMDAValidation() != null && doc.getMDAValidation().getWarningOrErrorOrNote().size()>0){
				res = 0;
				for (Object not : doc.getMDAValidation().getWarningOrErrorOrNote()) {
					if (not instanceof net.ihe.gazelle.validation.Warning){
						res++;
					}
				}
			}
		}
		return res;
	}

	private static int generateNumberErrorA(DetailedResult doc) {
		int res = -1;
		Set<String> lerrk = new HashSet<String>();
		if (doc != null){
			if (doc.getMDAValidation() != null && doc.getMDAValidation().getWarningOrErrorOrNote().size()>0){
				for (Object not : doc.getMDAValidation().getWarningOrErrorOrNote()) {
					if (not instanceof net.ihe.gazelle.validation.Error){
						lerrk.add(((net.ihe.gazelle.validation.Error)not).getIdentifiant());
					}
				}
				res = lerrk.size();
			}
		}
		return res;
	}

	private static int generateNumberError(DetailedResult doc) {
		int res = -1;
		if (doc != null){
			if (doc.getMDAValidation() != null && doc.getMDAValidation().getWarningOrErrorOrNote().size()>0){
				res = 0;
				for (Object not : doc.getMDAValidation().getWarningOrErrorOrNote()) {
					if (not instanceof net.ihe.gazelle.validation.Error){
						res++;
					}
				}
			}
		}
		return res;
	}

	public static DetailedResult getDetailedResult(InputStream is) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance(DetailedResult.class);
		Unmarshaller m = jc.createUnmarshaller();
		DetailedResult res = (DetailedResult) m.unmarshal(is);
		return res;
	}
	
	public static String readDoc(String name) throws IOException {
		BufferedReader scanner = new BufferedReader(new FileReader(name));
		StringBuilder res = new StringBuilder();
		try {
			String line = scanner.readLine();
			while (line != null) {
				res.append(line + "\n");
				line = scanner.readLine();
			}
		} finally {
			scanner.close();
		}
		return res.toString();
	}

}
