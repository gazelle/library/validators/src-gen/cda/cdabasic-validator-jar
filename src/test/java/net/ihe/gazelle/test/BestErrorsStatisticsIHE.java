package net.ihe.gazelle.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Notification;

public class BestErrorsStatisticsIHE {
	
	static int num = 0;
	
	public static void main(String[] args) throws IOException, JAXBException {
		File resFile = new File("/home/aboufahj/Téléchargements/CH/RES");
		Map<String, Integer> lvr = new TreeMap<String, Integer>();
		Map<String, Integer> lvrtotal = new TreeMap<String, Integer>();
		for (File fis : resFile.listFiles()) {
			generateStatistics(fis, lvr, lvrtotal);
		}
		for (String string : lvrtotal.keySet()) {
			System.out.println(string + ";" + lvrtotal.get(string) + ";" + (lvr.get(string)!=null?lvr.get(string):0));
		}
	}

	private static void generateStatistics(File file, Map<String, Integer> lvr, Map<String, Integer> lvrtotal) throws IOException, JAXBException {
		if (file.isFile() && file.getName().endsWith("val")){
			System.out.println(num++);
			DetailedResult doc = null;
			try{
				doc = getDetailedResult(new FileInputStream(file));
			}
			catch(Exception e){
				e.printStackTrace();
				return;
			}
			if (doc.getMDAValidation() != null){
				for (Object obj : doc.getMDAValidation().getWarningOrErrorOrNote()) {
					if (obj instanceof Error){
						Error err = (Error)obj;
						 List<Assertion> identifier = err.getAssertions();
						 for (Assertion assertion : identifier) {
							 if (!lvr.containsKey(assertion.getAssertionId())){
									lvr.put(assertion.getAssertionId(), 1);
								}
								else{
									lvr.put(assertion.getAssertionId(), lvr.get(assertion.getAssertionId())+1);
								}
						}
					}
					Notification not = (Notification)obj;
					List<Assertion> identifier = not.getAssertions();
					for (Assertion assertion : identifier) {
						if (!lvrtotal.containsKey(assertion.getAssertionId())){
							lvrtotal.put(assertion.getAssertionId(), 1);
						}
						else{
							lvrtotal.put(assertion.getAssertionId(), lvrtotal.get(assertion.getAssertionId())+1);
						}
					}
				}
			}
		}
		else if (file.isDirectory()){
			for (File fis : file.listFiles()) {
				generateStatistics(fis, lvr, lvrtotal);
			}
		}
	}
	
	

	public static DetailedResult getDetailedResult(InputStream is) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance(DetailedResult.class);
		Unmarshaller m = jc.createUnmarshaller();
		DetailedResult res = (DetailedResult) m.unmarshal(is);
		return res;
	}
	
	public static String readDoc(String name) throws IOException {
		BufferedReader scanner = new BufferedReader(new FileReader(name));
		StringBuilder res = new StringBuilder();
		try {
			String line = scanner.readLine();
			while (line != null) {
				res.append(line + "\n");
				line = scanner.readLine();
			}
		} finally {
			scanner.close();
		}
		return res.toString();
	}

}
