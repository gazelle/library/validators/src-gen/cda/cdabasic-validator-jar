package net.ihe.gazelle.test;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;

public class GenerateValidationStats {
	
	static int a = 0;
	
	public static void main(String[] args) throws IOException, JAXBException {
		File file = new File("/home/aboufahj/Téléchargements/CH/projects");
		if (file.isDirectory()){
			getListValidations(file, "/home/aboufahj/Téléchargements/CH/projects", 
					"/home/aboufahj/Téléchargements/CH/RES");
		}
	}
	
	public static void main2(String[] args) throws IOException, JAXBException {
		File file = new File("/home/aboufahj/ECQM");
		if (file.isDirectory()){
			getListValidations(file, "/home/aboufahj/ECQM", "/home/aboufahj/ECQMRES");
		}
	}
	
	public static void main33(String[] args) throws IOException, JAXBException {
		File file = new File("/media/aboufahj/RZOUGOV/evsclient2/");
		if (file.isDirectory()){
			try{
				getListValidations(file, "/media/aboufahj/RZOUGOV/evsclient2/", "/media/aboufahj/RZOUGOV/resevsclient4/");
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	public static void main4(String[] args) throws IOException, JAXBException {
		File file = new File("/home/aboufahj/Documents/ihic/NI");
		if (file.isDirectory()){
			getListValidations(file, "/home/aboufahj/Documents/ihic/NI", "/home/aboufahj/Documents/ihic/RES");
		}
	}
	
	private static void getListValidations(File file, String parentPath, String resPAth) throws IOException, JAXBException {
		if (file.isFile() && file.getName().endsWith("xml")){
			String content = readDoc(file.getAbsolutePath());
			if (content.contains("ClinicalDocument")){
				try {
					DetailedResult doc = CommonUtil.getDetailedResultFromFile(file.getAbsolutePath());
					if (containBigProblem(doc)){
						throw new Exception();
					}
					System.out.println(a++);
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					printDetailedResult(doc, baos);
					String abspath = file.getAbsolutePath().replace(parentPath, resPAth).replace(".xml", ".val");
					File fileval = new File(abspath);
					if(fileval.exists()) return;
					fileval.getParentFile().mkdirs();
					FileOutputStream fos = new FileOutputStream(fileval);
					fos.write(baos.toByteArray());
					fos.flush();
					fos.close();
				}
				catch(Exception e){
					System.err.println(file.getAbsolutePath());
					e.printStackTrace();
				}
			}
		}
		else if (file.isDirectory()){
			for (File fis : file.listFiles()) {
				getListValidations(fis, parentPath, resPAth);
			}
		}
	}
	
	private static boolean containBigProblem(DetailedResult doc) {
		if (doc.getMDAValidation() != null && doc.getMDAValidation().getWarningOrErrorOrNote() != null){
			for (Object obj : doc.getMDAValidation().getWarningOrErrorOrNote()) {
				Notification not = (Notification)obj;
				if (not.getTest().equals("val")){
					return true;
				}
			}
		}
		return false;
	}

	public static void printDetailedResult(DetailedResult xmi, OutputStream os) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance(DetailedResult.class);
		Marshaller m = jc.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		m.marshal(xmi, os);
	}
	
	public static String readDoc(String name) throws IOException {
		BufferedReader scanner = new BufferedReader(new FileReader(name));
		StringBuilder res = new StringBuilder();
		try {
			String line = scanner.readLine();
			while (line != null) {
				res.append(line + "\n");
				line = scanner.readLine();
			}
		} finally {
			scanner.close();
		}
		return res.toString();
	}

}
