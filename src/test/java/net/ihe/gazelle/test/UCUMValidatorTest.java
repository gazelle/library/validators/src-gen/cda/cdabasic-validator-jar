package net.ihe.gazelle.test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.fhir.ucum.UcumEssenceService;
import org.fhir.ucum.UcumException;
import org.fhir.ucum.UcumService;
import org.junit.Test;

import net.ihe.gazelle.basic.validation.UCUMValidator;
import net.ihe.gazelle.script.FileReadWrite;

import static org.junit.Assert.*;

public class UCUMValidatorTest {

	@Test
	public void testUCUMValidatorFhirService() throws UcumException {
		UcumService ucumService = new UcumEssenceService("src/test/resources/ucum-essence.xml");
		String OK = ucumService.validate("[IU]");
		String KO = ucumService.validate("WrongValue");
		assertNull(OK);
		assertNotNull(KO);
	}


	@Test
	public void testValidateUCUM() {
		assertTrue(UCUMValidator.validateUCUM("m"));
		assertTrue(UCUMValidator.validateUCUM("m2"));
		assertTrue(UCUMValidator.validateUCUM("N/m2"));
		assertTrue(UCUMValidator.validateUCUM("m/s"));
		assertTrue(UCUMValidator.validateUCUM("mg"));
		assertTrue(UCUMValidator.validateUCUM("d"));
		assertTrue(UCUMValidator.validateUCUM("1"));
		assertTrue(UCUMValidator.validateUCUM("{tablet}"));
		assertFalse(UCUMValidator.validateUCUM("kk"));
	}
	
	@Test
	public void testValidateEpsosUcum() throws Exception {
		String ucums = FileReadWrite.readDoc("src/test/resources/1.3.6.1.4.1.12559.11.10.1.3.1.42.16-2013-06-03T000000.xml");
		Pattern pat = Pattern.compile("code=\"(.*?)\"");
		Matcher mat = pat.matcher(ucums);
		while (mat.find()) {
			String unit = mat.group(1);
			if (!UCUMValidator.validateUCUM(unit)) {
				System.out.println("unit not valid:" + unit);
			}
			assertTrue(UCUMValidator.validateUCUM(unit));
		}
	}
	
	@Test
	public void testValidateDecorUcum() throws Exception {
		String ucums = FileReadWrite.readDoc("src/test/resources/DECOR-ucum.xml");
		Pattern pat = Pattern.compile("code=\"(.*?)\"");
		Matcher mat = pat.matcher(ucums);
		while (mat.find()) {
			String unit = mat.group(1);
			if (!UCUMValidator.validateUCUM(unit)) {
				System.out.println("unit not valid:" + unit);
			}
			assertTrue(UCUMValidator.validateUCUM(unit));
		}
	}
	
	@Test
	public void testValidateEYEUcum() throws Exception {
		String ucums = FileReadWrite.readDoc("src/test/resources/2.16.840.1.113883.1.11.12839.xml");
		Pattern pat = Pattern.compile("code=\"(.*?)\"");
		Matcher mat = pat.matcher(ucums);
		while (mat.find()) {
			String unit = mat.group(1);
			if (!UCUMValidator.validateUCUM(unit)) {
				System.out.println("unit not valid:" + unit);
			}
			//assertTrue(UCUMValidator.validateUCUM(unit));
		}
	}
	
	@Test
	public void testUnitsAreComparable() throws Exception {
		assertTrue(UCUMValidator.unitsAreComparable("m", "cm"));
		assertTrue(UCUMValidator.unitsAreComparable("kg", "mg"));
		assertTrue(UCUMValidator.unitsAreComparable("d", "s"));
		assertFalse(UCUMValidator.unitsAreComparable("d", "m"));
		assertFalse(UCUMValidator.unitsAreComparable(null, "m"));
		assertFalse(UCUMValidator.unitsAreComparable("d", null));
		assertFalse(UCUMValidator.unitsAreComparable(null, null));
		assertFalse(UCUMValidator.unitsAreComparable(" ", ""));
	}
	
	@Test
	public void testconvertToUnit() throws Exception {
		Double dd = UCUMValidator.convertToUnit(3.3, "m", "cm");
		assertTrue(dd == 330);
	}
	
	@Test
	public void testconvertToUnit2() throws Exception {
		Double dd = UCUMValidator.convertToUnit(3.0, "kg", "g");
		assertTrue(dd == 3000);
	}
	
	@Test
	public void testconvertToUnit3() throws Exception {
		Double dd = UCUMValidator.convertToUnit(3.0, "kg", "cm");
		assertNull(dd);
		dd = UCUMValidator.convertToUnit(3.0, "kg", "");
		assertNull(dd);
		dd = UCUMValidator.convertToUnit(3.0, "kg", null);
		assertNull(dd);
		dd = UCUMValidator.convertToUnit(3.0, null, "cm");
		assertNull(dd);
	}
	
	@Test
	public void testValue1IsLowerOrEqualThanValue2() throws Exception {
		assertTrue(UCUMValidator.value1IsLowerOrEqualThanValue2("1", "m", "2", "m"));
		assertTrue(UCUMValidator.value1IsLowerOrEqualThanValue2("1", "m", "1", "m"));
		assertTrue(UCUMValidator.value1IsLowerOrEqualThanValue2("1", "m", "1000", "mm"));
		assertTrue(UCUMValidator.value1IsLowerOrEqualThanValue2("1", "m", "200", "cm"));
		assertTrue(UCUMValidator.value1IsLowerOrEqualThanValue2("0.1", "km", "200", "m"));
		assertFalse(UCUMValidator.value1IsLowerOrEqualThanValue2("10", "kg", "10", "g"));
		assertFalse(UCUMValidator.value1IsLowerOrEqualThanValue2("10", "kg", "10", "m"));
		assertTrue(UCUMValidator.value1IsLowerOrEqualThanValue2("", "m", "", "cm"));
		assertTrue(UCUMValidator.value1IsLowerOrEqualThanValue2("1", "m", "s", "cm"));
		assertTrue(UCUMValidator.value1IsLowerOrEqualThanValue2(null, "m", "1", "cm"));
		assertFalse(UCUMValidator.value1IsLowerOrEqualThanValue2("1", null, "1", "cm"));
		assertTrue(UCUMValidator.value1IsLowerOrEqualThanValue2("1", "m", null, "cm"));
		assertFalse(UCUMValidator.value1IsLowerOrEqualThanValue2("1", "m", "1", null));
		assertTrue(UCUMValidator.value1IsLowerOrEqualThanValue2(null, "m", null, "cm"));
		assertTrue(UCUMValidator.value1IsLowerOrEqualThanValue2("1", "Pa", "2", "N/m2"));
	}

}
