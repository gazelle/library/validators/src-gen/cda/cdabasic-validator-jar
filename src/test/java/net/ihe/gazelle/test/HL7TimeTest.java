package net.ihe.gazelle.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.basic.validation.HL7Time;

public class HL7TimeTest {

	@Test
	public void testParse1() {
		String ss = "200509121930+0200";
		assertTrue(HL7Time.parse(ss).toString().equals("Mon Sep 12 19:30:00 CEST 2005"));
		String ss2 = "20081201121500+0100";
		assertTrue(HL7Time.parse(ss2).toString().equals("Mon Dec 01 12:15:00 CET 2008"));
	}
	@Test
	public void testParse2() {
		String ss = "20050912";
		assertTrue(HL7Time.parse(ss).toString().equals("Mon Sep 12 00:00:00 CEST 2005"));
		String ss2 = "200801";
		assertTrue(HL7Time.parse(ss2).toString().equals("Tue Jan 01 00:00:00 CET 2008"));
	}
	
	@Test
	public void testParse3() {
		System.out.println(HL7Time.parse(null));
		assertTrue(HL7Time.parse(null) == null);
		assertTrue(HL7Time.parse("eee") == null);
	}

}
