package net.ihe.gazelle.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Error;

public class BestErrorsStatisticsNI {
	
	static int num = 0;
	
	public static void main(String[] args) throws IOException, JAXBException {
		File resFile = new File("/home/aboufahj/Documents/ihic/RES/");
		Map<String, Integer> lvr = new TreeMap<String, Integer>();
		for (File fis : resFile.listFiles()) {
			generateStatistics(fis, lvr);
		}
		for (String string : lvr.keySet()) {
			System.out.println(string + ";" + lvr.get(string));
		}
	}

	private static void generateStatistics(File file, Map<String, Integer> lvr) throws IOException, JAXBException {
		if (file.isFile() && file.getName().endsWith("val")){
			System.out.println(num++);
			DetailedResult doc = getDetailedResult(new FileInputStream(file));
			if (doc.getMDAValidation() != null){
				for (Object obj : doc.getMDAValidation().getWarningOrErrorOrNote()) {
					if (obj instanceof Error){
						Error err = (Error)obj;
						 List<Assertion> identifier = err.getAssertions();
						 for (Assertion assertion : identifier) {
							 if (!lvr.containsKey(assertion.getAssertionId())){
									lvr.put(assertion.getAssertionId(), 1);
								}
								else{
									lvr.put(assertion.getAssertionId(), lvr.get(assertion.getAssertionId())+1);
								}
						}
					}
				}
			}
		}
		else if (file.isDirectory()){
			for (File fis : file.listFiles()) {
				generateStatistics(fis, lvr);
			}
		}
	}
	
	

	public static DetailedResult getDetailedResult(InputStream is) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance(DetailedResult.class);
		Unmarshaller m = jc.createUnmarshaller();
		DetailedResult res = (DetailedResult) m.unmarshal(is);
		return res;
	}
	
	public static String readDoc(String name) throws IOException {
		BufferedReader scanner = new BufferedReader(new FileReader(name));
		StringBuilder res = new StringBuilder();
		try {
			String line = scanner.readLine();
			while (line != null) {
				res.append(line + "\n");
				line = scanner.readLine();
			}
		} finally {
			scanner.close();
		}
		return res.toString();
	}

}
