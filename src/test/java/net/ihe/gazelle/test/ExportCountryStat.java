package net.ihe.gazelle.test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExportCountryStat {
	
	static int ncdarich = 417;
	
	static List<String> lcountry = new ArrayList<String>();
	
	static{
		lcountry.add("AT");
		lcountry.add("CA");
		lcountry.add("CH");
		lcountry.add("DE");
		lcountry.add("FI");
		lcountry.add("FR");
		lcountry.add("IT");
		lcountry.add("IT");
		lcountry.add("NL");
		lcountry.add("CZ");
		lcountry.add("US");
	}
	
	public static void main(String[] args) throws IOException {
		List<ValidationResult> lvr = generateValidationResult(
				"/home/aboufahj/workspaceTopcased/cdabasic-validator-jar/test_res_NI_final.csv");
		Map<String, List<ValidationResult>> mapCountry = new HashMap<String, List<ValidationResult>>();
		for (String cont : lcountry) {
			mapCountry.put(cont, new ArrayList<ValidationResult>());
		}
		for (ValidationResult validationResult : lvr) {
			for (String cont : lcountry) {
				if (validationResult.getAbsolutePath().contains("/home/aboufahj/Documents/ihic/RES/" + cont + "/")){
					mapCountry.get(cont).add(validationResult);
					break;
				}
			}
		}
		exportInformations(mapCountry);
	}
	
	private static void exportInformations(Map<String, List<ValidationResult>> mapCountry) {
		for (String string : mapCountry.keySet()) {
			String res = string + ";" + getMoyenneError(mapCountry.get(string)) + ";" + 
					getMoyenneWarn(mapCountry.get(string)) + ";" + 
					getMoyenneIndicatorRichness(mapCountry.get(string)) + ";" + 
					getMoyenneIndicatorConformance(mapCountry.get(string)) + ";" ;
			System.out.println(res.replace(".", ","));
		}
		
	}

	private static double getMoyenneIndicatorConformance(List<ValidationResult> li) {
		double d = 0.0;
		for (ValidationResult pi : li) {
			double conf = 1/(pi.getNumberError() + pi.getIndicatorWarn() + 1);
			d = d + conf;
		}
		return d/li.size();
	}

	private static double getMoyenneIndicatorRichness(List<ValidationResult> li) {
		double d = 0.0;
		for (ValidationResult pi : li) {
			d = d + (Double.valueOf(pi.getNumberEF())/ncdarich);
		}
		return d/li.size();
	}

	static Double getMoyenneError(List<ValidationResult> li){
		double d = 0.0;
		for (ValidationResult pi : li) {
			d = d + pi.getNumberError();
		}
		return d/li.size();
	}
	
	static Double getMoyenneWarn(List<ValidationResult> li){
		double d = 0.0;
		for (ValidationResult pi : li) {
			d = d + pi.getNumberWarn();
		}
		return d/li.size();
	}

	private static List<ValidationResult> generateValidationResult(String string) throws IOException {
		List<ValidationResult> res = new ArrayList<ValidationResult>();
		String content = readDoc(string);
		String[] ss = content.split("\n");
		for (String string2 : ss) {
			String[] kk = string2.split(";");
			ValidationResult vr = new ValidationResult();
			vr.setAbsolutePath(kk[0]);
			vr.setNumberError(Integer.valueOf(kk[1]));
			vr.setNumberErrorA(Integer.valueOf(kk[2]));
			vr.setNumberWarn(Integer.valueOf(kk[3]));
			vr.setNumberWarna(Integer.valueOf(kk[4]));
			vr.setIndicatorWarn(Double.valueOf(kk[5]));
			vr.setIndicatorWarnA(Double.valueOf(kk[6]));
			vr.setNumberEF(Integer.valueOf(kk[7]));
			res.add(vr);
		}
		return res;
	}
	
	public static String readDoc(String name) throws IOException {
		BufferedReader scanner = new BufferedReader(new FileReader(name));
		StringBuilder res = new StringBuilder();
		try {
			String line = scanner.readLine();
			while (line != null) {
				res.append(line + "\n");
				line = scanner.readLine();
			}
		} finally {
			scanner.close();
		}
		return res.toString();
	}
	
	
	
	

}
