package net.ihe.gazelle.test;

import java.io.File;
import java.util.List;

public class ValidationResult {
	
	private int numberError;
	
	private int numberErrorA;
	
	private int numberWarn;
	
	private int numberWarna;
	
	private int numberEF;
	
	private double indicatorWarn;
	
	private double indicatorWarnA;
	
	private String absolutePath;

	public int getNumberError() {
		return numberError;
	}

	public void setNumberError(int numberError) {
		this.numberError = numberError;
	}

	public int getNumberErrorA() {
		return numberErrorA;
	}

	public void setNumberErrorA(int numberErrorA) {
		this.numberErrorA = numberErrorA;
	}

	public int getNumberWarn() {
		return numberWarn;
	}

	public void setNumberWarn(int numberWarn) {
		this.numberWarn = numberWarn;
	}

	public int getNumberWarna() {
		return numberWarna;
	}

	public void setNumberWarna(int numberWarna) {
		this.numberWarna = numberWarna;
	}

	public int getNumberEF() {
		return numberEF;
	}

	public void setNumberEF(int numberEF) {
		this.numberEF = numberEF;
	}

	public String getAbsolutePath() {
		return absolutePath;
	}

	public void setAbsolutePath(String absolutePath) {
		this.absolutePath = absolutePath;
	}
	
	public static ValidationResult getValidationResult(List<ValidationResult> lvr, String path){
		for (ValidationResult validationResult : lvr) {
			File fil = new File(path);
			if (validationResult.getAbsolutePath() != null && validationResult.getAbsolutePath().contains(fil.getName())){
				return validationResult;
			}
		}
		return null;
	}

	public double getIndicatorWarn() {
		return indicatorWarn;
	}

	public void setIndicatorWarn(double indicatorWarn) {
		this.indicatorWarn = indicatorWarn;
	}

	public double getIndicatorWarnA() {
		return indicatorWarnA;
	}

	public void setIndicatorWarnA(double indicatorWarnA) {
		this.indicatorWarnA = indicatorWarnA;
	}

}
