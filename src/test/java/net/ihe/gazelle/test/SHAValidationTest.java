package net.ihe.gazelle.test;

import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import net.ihe.gazelle.basic.validation.SHAValidation;

public class SHAValidationTest {

	@Test
	public void testDecodeB64() throws IOException {
		InputStream is = SHAValidation.decodeB64("aGVsbG8=", null);
		assertTrue(is instanceof ByteArrayInputStream);
		OutputStream out = new ByteArrayOutputStream();
		IOUtils.copy(is,out);
		assertTrue(out.toString().equals("hello"));
	}
	
	@Test
	public void testDecodeB64_2() throws IOException {
		InputStream is = SHAValidation.decodeB64(null, null);
		assertTrue(is == null);
	}

	@Test
	public void testCalcStringString() {
		String ss = "Hola word";
		String calc = SHAValidation.calc(ss, "SHA-1");
		assertTrue(calc.equals("886211e8d01db261e042711301cb5edec0f3cd0d"));
		calc = SHAValidation.calc(ss, "SHA-256");
		assertTrue(calc.equals("86164972d8255fff430eb5a21703e2a056b306db993f4ab953080439293d91cf"));
	}
	
	@Test
	public void testCalcStringString2() {
		String ss = null;
		String calc = SHAValidation.calc(ss, "SHA-1");
		assertTrue(calc == null);
	}

	@Test
	public void testCalcInputStreamString() {
		String ss = "Hola word";
		String calc = SHAValidation.calc(new ByteArrayInputStream(ss.getBytes()), "SHA-1");
		assertTrue(calc.equals("886211e8d01db261e042711301cb5edec0f3cd0d"));
		calc = SHAValidation.calc(new ByteArrayInputStream(ss.getBytes()), "SHA-256");
		assertTrue(calc.equals("86164972d8255fff430eb5a21703e2a056b306db993f4ab953080439293d91cf"));
	}

}
