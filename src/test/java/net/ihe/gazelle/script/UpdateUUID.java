package net.ihe.gazelle.script;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * the aim is to capitalize the uuid used in a CDA document
 * @author Abderrazek Boufahja
 *
 */
public class UpdateUUID {
	
	public static void main(String[] args) throws IOException {
		String file = "/home/aboufahj/Téléchargements/170.314b2_AmbulatoryTOC.xml";
		String file2 = "/home/aboufahj/Téléchargements/170.314b2_AmbulatoryTOC_fixed.xml";
		updateUUIDs(file, file2);
	}

	private static void updateUUIDs(String in, String out) throws IOException {
		String content = FileReadWrite.readDoc(in);
		String res = content;
		String regex = "(root|id)=\"([0-9a-zA-Z]{8}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{12})\"";
		Pattern pat = Pattern.compile(regex, Pattern.MULTILINE|Pattern.DOTALL);
		Matcher mat = pat.matcher(content);
		while (mat.find()) {
			res = res.replace(mat.group(), mat.group(1) + "=\"" +  mat.group(2).toUpperCase() + "\"");
			System.out.println(mat.group(1) + "=\"" +  mat.group(2).toUpperCase() + "\"");
		}
		FileReadWrite.printDoc(res, out);
	}

}
