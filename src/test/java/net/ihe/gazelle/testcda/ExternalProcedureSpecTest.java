package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class ExternalProcedureSpecTest {
	@Test
	// ExternalProcedure SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-045, RIM-001)
	public void test_rmim045_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ExternalProcedureSpec_rmim045_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ExternalProcedureSpec-rmim045_1");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ExternalProcedureSpec_rmim045_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ExternalProcedureSpec-rmim045_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ExternalProcedureSpec_rmim045_1_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-ExternalProcedureSpec-rmim045_1");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The ids elements of ExternalProcedure SHALL be distinct (RMIM-045, RIM-002)
	public void test_rmim045_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ExternalProcedureSpec_rmim045_2_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ExternalProcedureSpec-rmim045_2");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ExternalProcedureSpec_rmim045_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ExternalProcedureSpec-rmim045_2");
		Assert.assertTrue(nn != null);//
	}


}		
