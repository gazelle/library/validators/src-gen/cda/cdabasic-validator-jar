package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class OrganizationSpecTest {
	@Test
	// Organization SHALL NOT have id element with nullFlavor, if there are other ids (RMIM-094, RIM-020)
	public void test_rmim094_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/OrganizationSpec_rmim094_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-OrganizationSpec-rmim094_1");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/OrganizationSpec_rmim094_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-OrganizationSpec-rmim094_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/OrganizationSpec_rmim094_1_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-OrganizationSpec-rmim094_1");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The ids elements of Organization SHALL be distinct (RMIM-094, RIM-021)
	public void test_rmim094_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/OrganizationSpec_rmim094_2_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-OrganizationSpec-rmim094_2");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/OrganizationSpec_rmim094_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-OrganizationSpec-rmim094_2");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// Organization SHALL NOT have name element with nullFlavor, if there are other name (RMIM-094, RIM-022)
	public void test_rmim094_3() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/OrganizationSpec_rmim094_3_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-OrganizationSpec-rmim094_3");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/OrganizationSpec_rmim094_3_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-OrganizationSpec-rmim094_3");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/OrganizationSpec_rmim094_3_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-OrganizationSpec-rmim094_3");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The name elements of Organizarion SHALL be distinct (RMIM-094, RIM-023)
	public void test_rmim094_4() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/OrganizationSpec_rmim094_4_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-OrganizationSpec-rmim094_4");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/OrganizationSpec_rmim094_4_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-OrganizationSpec-rmim094_4");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// Organization SHALL NOT have telecom element with nullFlavor,
	// if there are other telecom elements which are not null (RMIM-094, RIM-024)
	public void test_rmim094_5() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/OrganizationSpec_rmim094_5_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-OrganizationSpec-rmim094_5");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/OrganizationSpec_rmim094_5_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-OrganizationSpec-rmim094_5");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/OrganizationSpec_rmim094_5_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-OrganizationSpec-rmim094_5");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The telecom elements of Entity SHALL be distinct (RMIM-094, RIM-025)
	public void test_rmim094_6() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/OrganizationSpec_rmim094_6_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-OrganizationSpec-rmim094_6");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/OrganizationSpec_rmim094_6_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-OrganizationSpec-rmim094_6");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// Organization SHALL NOT have addr element with nullFlavor, if there are other (RMIM-095)
	public void test_rmim095() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/OrganizationSpec_rmim095_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-OrganizationSpec-rmim095");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/OrganizationSpec_rmim095_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-OrganizationSpec-rmim095");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/OrganizationSpec_rmim095_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-OrganizationSpec-rmim095");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The addr elements of Organization SHALL be distinct (RMIM-096)
	public void test_rmim096() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/OrganizationSpec_rmim096_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-OrganizationSpec-rmim096");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/OrganizationSpec_rmim096_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-OrganizationSpec-rmim096");
		Assert.assertTrue(nn != null);//
	}


}		
