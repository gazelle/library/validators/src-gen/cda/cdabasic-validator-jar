package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class AssociatedEntitySpecTest {
	@Test
	// AssociatedEntity SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-083, RIM-011)
	public void test_rmim083_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssociatedEntitySpec_rmim083_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-AssociatedEntitySpec-rmim083_1");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssociatedEntitySpec_rmim083_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-AssociatedEntitySpec-rmim083_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssociatedEntitySpec_rmim083_1_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-AssociatedEntitySpec-rmim083_1");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The ids elements of AssociatedEntity SHALL be distinct (RMIM-083, RIM-012)
	public void test_rmim083_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssociatedEntitySpec_rmim083_2_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-AssociatedEntitySpec-rmim083_2");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssociatedEntitySpec_rmim083_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-AssociatedEntitySpec-rmim083_2");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// AssociatedEntity SHALL NOT have addr element with nullFlavor, if there are other addr (RMIM-083, RIM-016)
	public void test_rmim083_3() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssociatedEntitySpec_rmim083_3_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-AssociatedEntitySpec-rmim083_3");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssociatedEntitySpec_rmim083_3_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-AssociatedEntitySpec-rmim083_3");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssociatedEntitySpec_rmim083_3_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-AssociatedEntitySpec-rmim083_3");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The addr elements of AssociatedEntity SHALL be distinct  (RMIM-083, RIM-017)
	public void test_rmim083_4() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssociatedEntitySpec_rmim083_4_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-AssociatedEntitySpec-rmim083_4");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssociatedEntitySpec_rmim083_4_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-AssociatedEntitySpec-rmim083_4");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// AssociatedEntity SHALL NOT have telecom element with nullFlavor, if there are other  (RMIM-083, RIM-018)
	public void test_rmim083_5() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssociatedEntitySpec_rmim083_5_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-AssociatedEntitySpec-rmim083_5");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssociatedEntitySpec_rmim083_5_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-AssociatedEntitySpec-rmim083_5");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssociatedEntitySpec_rmim083_5_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-AssociatedEntitySpec-rmim083_5");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The telecom elements of AssociatedEntity SHALL be distinct  (RMIM-083, RIM-019)
	public void test_rmim083_6() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssociatedEntitySpec_rmim083_6_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-AssociatedEntitySpec-rmim083_6");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssociatedEntitySpec_rmim083_6_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-AssociatedEntitySpec-rmim083_6");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// If associatedEntity has not a null of flavor and its enclosing participant has not a null of flavor, 
	//it SHALL have an associatedPerson, or a scopingOrganization (RMIM-084)
	public void test_rmim084() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssociatedEntitySpec_rmim084_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-AssociatedEntitySpec-rmim084");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssociatedEntitySpec_rmim084_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-AssociatedEntitySpec-rmim084");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssociatedEntitySpec_rmim084_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-AssociatedEntitySpec-rmim084");
		Assert.assertTrue(nn != null);
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssociatedEntitySpec_rmim084_OK_3.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-AssociatedEntitySpec-rmim084");
		Assert.assertTrue(nn != null);
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssociatedEntitySpec_rmim084_OK_4.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-AssociatedEntitySpec-rmim084");
		Assert.assertTrue(nn != null);
	}

}		
