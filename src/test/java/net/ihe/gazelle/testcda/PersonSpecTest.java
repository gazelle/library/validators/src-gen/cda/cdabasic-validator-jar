package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class PersonSpecTest {
	@Test
	// Person SHALL NOT have name element with nullFlavor, if there are other name (RMIM-097, RIM-022)
	public void test_rmim097_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PersonSpec_rmim097_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-PersonSpec-rmim097_1");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PersonSpec_rmim097_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-PersonSpec-rmim097_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PersonSpec_rmim097_1_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-PersonSpec-rmim097_1");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The name elements of Person SHALL be distinct (RMIM-097, RIM-023)
	public void test_rmim097_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PersonSpec_rmim097_2_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-PersonSpec-rmim097_2");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PersonSpec_rmim097_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-PersonSpec-rmim097_2");
		Assert.assertTrue(nn != null);//
	}


}		
