package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class RelatedEntitySpecTest {
	@Test
	// RelatedEntity SHALL NOT have addr element with nullFlavor, if there are other addr (RMIM-082, RIM-016)
	public void test_rmim082_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RelatedEntitySpec_rmim082_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-RelatedEntitySpec-rmim082_1");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RelatedEntitySpec_rmim082_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-RelatedEntitySpec-rmim082_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RelatedEntitySpec_rmim082_1_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-RelatedEntitySpec-rmim082_1");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The addr elements of RelatedEntity SHALL be distinct (RMIM-082, RIM-017)
	public void test_rmim082_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RelatedEntitySpec_rmim082_2_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-RelatedEntitySpec-rmim082_2");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RelatedEntitySpec_rmim082_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-RelatedEntitySpec-rmim082_2");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// RelatedEntity SHALL NOT have telecom element with nullFlavor, if there are other telecom elements which are not null (RMIM-082, RIM-018)
	public void test_rmim082_3() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RelatedEntitySpec_rmim082_3_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-RelatedEntitySpec-rmim082_3");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RelatedEntitySpec_rmim082_3_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-RelatedEntitySpec-rmim082_3");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RelatedEntitySpec_rmim082_3_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-RelatedEntitySpec-rmim082_3");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The telecom elements of RelatedEntity SHALL be distinct (RMIM-082, RIM-019)
	public void test_rmim082_4() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RelatedEntitySpec_rmim082_4_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-RelatedEntitySpec-rmim082_4");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RelatedEntitySpec_rmim082_4_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-RelatedEntitySpec-rmim082_4");
		Assert.assertTrue(nn != null);//
	}


}		
