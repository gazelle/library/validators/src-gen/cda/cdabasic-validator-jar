package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class SpecimenRoleSpecTest {
	@Test
	// SpecimenRole SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-091, RIM-011)
	public void test_rmim091_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SpecimenRoleSpec_rmim091_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-SpecimenRoleSpec-rmim091_1");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SpecimenRoleSpec_rmim091_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-SpecimenRoleSpec-rmim091_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SpecimenRoleSpec_rmim091_1_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-SpecimenRoleSpec-rmim091_1");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The ids elements of SpecimenRole SHALL be distinct (RMIM-091, RIM-012)
	public void test_rmim091_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SpecimenRoleSpec_rmim091_2_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-SpecimenRoleSpec-rmim091_2");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SpecimenRoleSpec_rmim091_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-SpecimenRoleSpec-rmim091_2");
		Assert.assertTrue(nn != null);//
	}


}		
