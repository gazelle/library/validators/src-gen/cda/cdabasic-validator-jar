package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class RelatedSubjectSpecTest {
	@Test
	// The addr elements of RelatedSubject SHALL be distinct (RMIM-087, RIM-016)
	public void test_rmim087_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RelatedSubjectSpec_rmim087_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-RelatedSubjectSpec-rmim087_1");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RelatedSubjectSpec_rmim087_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-RelatedSubjectSpec-rmim087_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RelatedSubjectSpec_rmim087_1_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-RelatedSubjectSpec-rmim087_1");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The addr elements of RelatedSubject SHALL be distinct (RMIM-087, RIM-017)
	public void test_rmim087_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RelatedSubjectSpec_rmim087_2_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-RelatedSubjectSpec-rmim087_2");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RelatedSubjectSpec_rmim087_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-RelatedSubjectSpec-rmim087_2");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// RelatedSubject SHALL NOT have telecom element with nullFlavor, if there are other telecom elements which are not null (RMIM-087, RIM-018)
	public void test_rmim087_3() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RelatedSubjectSpec_rmim087_3_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-RelatedSubjectSpec-rmim087_3");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RelatedSubjectSpec_rmim087_3_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-RelatedSubjectSpec-rmim087_3");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RelatedSubjectSpec_rmim087_3_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-RelatedSubjectSpec-rmim087_3");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The telecom elements of RelatedSubject SHALL be distinct (RMIM-087, RIM-019)
	public void test_rmim087_4() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RelatedSubjectSpec_rmim087_4_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-RelatedSubjectSpec-rmim087_4");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RelatedSubjectSpec_rmim087_4_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-RelatedSubjectSpec-rmim087_4");
		Assert.assertTrue(nn != null);//
	}


}		
