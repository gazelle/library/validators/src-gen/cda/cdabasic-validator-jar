package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class OrganizationPartOfSpecTest {
	@Test
	// OrganizationPartOf SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-071, RIM-011)
	public void test_rmim071_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/OrganizationPartOfSpec_rmim071_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-OrganizationPartOfSpec-rmim071_1");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/OrganizationPartOfSpec_rmim071_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-OrganizationPartOfSpec-rmim071_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/OrganizationPartOfSpec_rmim071_1_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-OrganizationPartOfSpec-rmim071_1");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The ids elements of OrganizationPartOf SHALL be distinct (RMIM-071, RIM-012)
	public void test_rmim071_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/OrganizationPartOfSpec_rmim071_2_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-OrganizationPartOfSpec-rmim071_2");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/OrganizationPartOfSpec_rmim071_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-OrganizationPartOfSpec-rmim071_2");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// The statusCode if presents SHALL be from the valueSet RoleStatus (RMIM-071, RIM-013)
	public void test_rmim071_3() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/OrganizationPartOfSpec_rmim071_3_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-OrganizationPartOfSpec-rmim071_3");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/OrganizationPartOfSpec_rmim071_3_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-OrganizationPartOfSpec-rmim071_3");
		Assert.assertTrue(nn != null);//
	}


}		
