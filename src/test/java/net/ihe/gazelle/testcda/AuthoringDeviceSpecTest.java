package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class AuthoringDeviceSpecTest {
	@Test
	// asMaintainedEntity SHOULD NOT be used (RMIM-098)
	public void test_rmim098() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AuthoringDeviceSpec_rmim098_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-AuthoringDeviceSpec-rmim098");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "WARNING", "cdabasic-AuthoringDeviceSpec-rmim098");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AuthoringDeviceSpec_rmim098_KO.xml");
		nn = CommonUtil.selectNotification(res, "WARNING", "cdabasic-AuthoringDeviceSpec-rmim098");
		Assert.assertTrue(nn != null);//
	}


}		
