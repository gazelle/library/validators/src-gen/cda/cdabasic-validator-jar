package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class SubstanceAdministrationSpecTest {
	@Test
	// SubstanceAdministration SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-032, RIM-001)
	public void test_rmim032_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SubstanceAdministrationSpec_rmim032_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-SubstanceAdministrationSpec-rmim032_1");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SubstanceAdministrationSpec_rmim032_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-SubstanceAdministrationSpec-rmim032_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SubstanceAdministrationSpec_rmim032_1_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-SubstanceAdministrationSpec-rmim032_1");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The ids elements of SubstanceAdministration SHALL be distinct (RMIM-032, RIM-002)
	public void test_rmim032_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SubstanceAdministrationSpec_rmim032_2_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-SubstanceAdministrationSpec-rmim032_2");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SubstanceAdministrationSpec_rmim032_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-SubstanceAdministrationSpec-rmim032_2");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// The statusCode if presents SHALL be from the valueSet ActStatus (RMIM-032, RIM-003)
	public void test_rmim032_3() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SubstanceAdministrationSpec_rmim032_3_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-SubstanceAdministrationSpec-rmim032_3");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SubstanceAdministrationSpec_rmim032_3_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-SubstanceAdministrationSpec-rmim032_3");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// The approachSiteCode elements of SubstanceAdministration SHALL be distinct (RMIM-033)
	public void test_rmim033() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SubstanceAdministrationSpec_rmim033_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-SubstanceAdministrationSpec-rmim033");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SubstanceAdministrationSpec_rmim033_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-SubstanceAdministrationSpec-rmim033");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// SubstanceAdministration SHALL NOT have approachSiteCode element with nullFlavor, 
	//if there are other approachSiteCode elements which are not null (RMIM-034)
	public void test_rmim034() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SubstanceAdministrationSpec_rmim034_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-SubstanceAdministrationSpec-rmim034");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SubstanceAdministrationSpec_rmim034_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-SubstanceAdministrationSpec-rmim034");
		Assert.assertTrue(nn != null);//
	
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SubstanceAdministrationSpec_rmim034_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-SubstanceAdministrationSpec-rmim034");
		Assert.assertTrue(nn != null);
	}


}		
