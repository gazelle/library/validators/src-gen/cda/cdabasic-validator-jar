package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class StructuredBodySpecTest {
	@Test
	// The languageCode shall be from the valueSet HumanLanguage (RMIM-049)
	public void test_rmim049() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/StructuredBodySpec_rmim049_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-StructuredBodySpec-rmim049");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/StructuredBodySpec_rmim049_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-StructuredBodySpec-rmim049");
		Assert.assertTrue(nn != null);//
	}


}		
