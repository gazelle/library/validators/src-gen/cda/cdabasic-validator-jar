package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class ManufacturedProductSpecTest {
	@Test
	// ManufacturedProduct SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-089, RIM-011)
	public void test_rmim089_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ManufacturedProductSpec_rmim089_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ManufacturedProductSpec-rmim089_1");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ManufacturedProductSpec-rmim089_1");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ManufacturedProductSpec_rmim089_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ManufacturedProductSpec-rmim089_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ManufacturedProductSpec_rmim089_1_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-ManufacturedProductSpec-rmim089_1");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The ids elements of ManufacturedProduct SHALL be distinct (RMIM-089, RIM-012)
	public void test_rmim089_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ManufacturedProductSpec_rmim089_2_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ManufacturedProductSpec-rmim089_2");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ManufacturedProductSpec-rmim089_2");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ManufacturedProductSpec_rmim089_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ManufacturedProductSpec-rmim089_2");
		Assert.assertTrue(nn != null);//
	}

}		
