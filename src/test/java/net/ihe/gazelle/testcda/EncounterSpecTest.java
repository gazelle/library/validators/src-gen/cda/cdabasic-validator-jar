package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class EncounterSpecTest {
	@Test
	// Encounter SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-003, RIM-001)
	public void test_rmim003_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EncounterSpec_rmim003_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-EncounterSpec-rmim003_1");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EncounterSpec_rmim003_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-EncounterSpec-rmim003_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EncounterSpec_rmim003_1_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-EncounterSpec-rmim003_1");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The ids elements of Encounter SHALL be distinct (RMIM-003, RIM-002)
	public void test_rmim003_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EncounterSpec_rmim003_2_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-EncounterSpec-rmim003_2");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EncounterSpec_rmim003_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-EncounterSpec-rmim003_2");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// The statusCode if presents SHALL be from the valueSet ActStatus (RMIM-003, RIM-003)
	public void test_rmim003_3() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EncounterSpec_rmim003_3_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-EncounterSpec-rmim003_3");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EncounterSpec_rmim003_3_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-EncounterSpec-rmim003_3");
		Assert.assertTrue(nn != null);//
	}


}		
