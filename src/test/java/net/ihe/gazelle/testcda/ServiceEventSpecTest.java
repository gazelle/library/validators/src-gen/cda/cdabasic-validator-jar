package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class ServiceEventSpecTest {
	@Test
	// ServiceEvent SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-057, RIM-001)
	public void test_rmim057_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ServiceEventSpec_rmim057_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ServiceEventSpec-rmim057_1");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ServiceEventSpec_rmim057_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ServiceEventSpec-rmim057_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ServiceEventSpec_rmim057_1_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-ServiceEventSpec-rmim057_1");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The ids elements of ServiceEvent SHALL be distinct (RMIM-057, RIM-002)
	public void test_rmim057_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ServiceEventSpec_rmim057_2_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ServiceEventSpec-rmim057_2");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ServiceEventSpec_rmim057_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ServiceEventSpec-rmim057_2");
		Assert.assertTrue(nn != null);//
	}


}		
