package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class AuthenticatorSpecTest {
	@Test
	// The signatureCode elements of Authenticator SHALL be from the valueSet ParticipationSignature (RMIM-067, RIM-010)
	public void test_rmim067_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AuthenticatorSpec_rmim067_1_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-AuthenticatorSpec-rmim067_1");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AuthenticatorSpec_rmim067_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-AuthenticatorSpec-rmim067_1");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// The authenticator.signatureCode elements SHOULD NOT have the value X (RMIM-068)
	public void test_rmim068() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AuthenticatorSpec_rmim068_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-AuthenticatorSpec-rmim068");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AuthenticatorSpec_rmim068_KO.xml");
		nn = CommonUtil.selectNotification(res, "WARNING", "cdabasic-AuthenticatorSpec-rmim068");
		Assert.assertTrue(nn != null);//
	}


}		
