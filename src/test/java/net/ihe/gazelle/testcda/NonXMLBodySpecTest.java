package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class NonXMLBodySpecTest {
	@Test
	// The languageCode shall be from the valueSet HumanLanguage (RMIM-047)
	public void test_rmim047() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/NonXMLBodySpec_rmim047_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-NonXMLBodySpec-rmim047");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/NonXMLBodySpec_rmim047_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-NonXMLBodySpec-rmim047");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// The content of text element SHALL be in other form than XML form (RMIM-048)
	public void test_rmim048() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/NonXMLBodySpec_rmim048_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-NonXMLBodySpec-rmim048");
		Assert.assertTrue(nn != null);
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/NonXMLBodySpec_rmim048_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-NonXMLBodySpec-rmim048");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/NonXMLBodySpec_rmim048_KO_1.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-NonXMLBodySpec-rmim048");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/NonXMLBodySpec_rmim048_KO_2.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-NonXMLBodySpec-rmim048");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/NonXMLBodySpec_rmim048_KO_3.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-NonXMLBodySpec-rmim048");
		Assert.assertTrue(nn != null);//
	}


}		
