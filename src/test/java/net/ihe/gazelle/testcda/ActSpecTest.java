package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class ActSpecTest {
	@Test
	// Act SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-001, RIM-001)
	public void test_rmim001_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ActSpec_rmim001_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ActSpec-rmim001_1");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ActSpec-rmim001_1");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ActSpec_rmim001_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ActSpec-rmim001_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ActSpec_rmim001_1_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-ActSpec-rmim001_1");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// The ids elements of Act SHALL be distinct (RMIM-001, RIM-002)
	public void test_rmim001_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ActSpec_rmim001_2_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ActSpec-rmim001_2");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ActSpec-rmim001_2");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ActSpec_rmim001_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ActSpec-rmim001_2");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ActSpec_rmim001_2_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-ActSpec-rmim001_2");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// The statusCode if presents SHALL be from the valueSet ActStatus (RMIM-001, RIM-003)
	public void test_rmim001_3() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ActSpec_rmim001_3_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ActSpec-rmim001_3");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ActSpec-rmim001_3");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ActSpec_rmim001_3_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ActSpec-rmim001_3");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// The languageCode shall be from the valueSet HumanLanguage (RMIM-002)
	public void test_rmim002() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ActSpec_rmim002_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ActSpec-rmim002");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ActSpec-rmim002");
		Assert.assertFalse(nn != null);
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ActSpec_rmim002_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ActSpec-rmim002");
		Assert.assertTrue(nn != null);//
	}


}		
