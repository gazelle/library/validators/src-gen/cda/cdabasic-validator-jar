package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class ExternalActSpecTest {
	@Test
	// ExternalAct SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-043, RIM-001)
	public void test_rmim043_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ExternalActSpec_rmim043_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ExternalActSpec-rmim043_1");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ExternalActSpec_rmim043_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ExternalActSpec-rmim043_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ExternalActSpec_rmim043_1_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-ExternalActSpec-rmim043_1");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The ids elements of ExternalAct SHALL be distinct (RMIM-043, RIM-002)
	public void test_rmim043_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ExternalActSpec_rmim043_2_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ExternalActSpec-rmim043_2");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ExternalActSpec_rmim043_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ExternalActSpec-rmim043_2");
		Assert.assertTrue(nn != null);//
	}


}		
