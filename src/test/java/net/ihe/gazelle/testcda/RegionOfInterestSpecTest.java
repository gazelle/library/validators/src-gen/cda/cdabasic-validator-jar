package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class RegionOfInterestSpecTest {
	@Test
	// RegionOfInterest SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-023, RIM-001)
	public void test_rmim023_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RegionOfInterestSpec_rmim023_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-RegionOfInterestSpec-rmim023_1");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-RegionOfInterestSpec-rmim023_1");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RegionOfInterestSpec_rmim023_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-RegionOfInterestSpec-rmim023_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RegionOfInterestSpec_rmim023_1_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-RegionOfInterestSpec-rmim023_1");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The ids elements of RegionOfInterest SHALL be distinct (RMIM-023, RIM-002)
	public void test_rmim023_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RegionOfInterestSpec_rmim023_2_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-RegionOfInterestSpec-rmim023_2");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-RegionOfInterestSpec-rmim023_2");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RegionOfInterestSpec_rmim023_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-RegionOfInterestSpec-rmim023_2");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// A RegionOfInterest SHALL have an entryRelationship[typeCode="SUBJ"]/observationMedia, 
	//or it SHALL be included into an observationMedia/entryRelationship[@typeCode="SUBJ" and @inversionInd="true"], 
	//or it SHALL include an external Observation into reference[typeCode="SUBJ"]/ExternalObservation (RMIM-024)
	public void test_rmim024() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RegionOfInterestSpec_rmim024_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-RegionOfInterestSpec-rmim024");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-RegionOfInterestSpec-rmim024");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RegionOfInterestSpec_rmim024_KO_1.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-RegionOfInterestSpec-rmim024");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RegionOfInterestSpec_rmim024_KO_2.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-RegionOfInterestSpec-rmim024");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RegionOfInterestSpec_rmim024_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-RegionOfInterestSpec-rmim024");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-RegionOfInterestSpec-rmim024");
		Assert.assertFalse(nn != null);
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RegionOfInterestSpec_rmim024_OK_3.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-RegionOfInterestSpec-rmim024");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-RegionOfInterestSpec-rmim024");
		Assert.assertFalse(nn != null);
	}

	@Test
	// A RegionOfInterest must reference exactly one ObservationMedia or one ExternalObservation (RMIM-025)
	public void test_rmim025() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RegionOfInterestSpec_rmim025_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-RegionOfInterestSpec-rmim025");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-RegionOfInterestSpec-rmim025");
		Assert.assertFalse(nn != null);
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RegionOfInterestSpec_rmim025_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-RegionOfInterestSpec-rmim025");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-RegionOfInterestSpec-rmim025");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RegionOfInterestSpec_rmim025_KO_1.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-RegionOfInterestSpec-rmim025");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RegionOfInterestSpec_rmim025_KO_2.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-RegionOfInterestSpec-rmim025");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// The code of RegionOfInterest if present SHALL be from ROIOverlayShape valueSet (RMIM-028)
	public void test_rmim028() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RegionOfInterestSpec_rmim028_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-RegionOfInterestSpec-rmim028");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-RegionOfInterestSpec-rmim028");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RegionOfInterestSpec_rmim028_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-RegionOfInterestSpec-rmim028");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// The value list SHALL contain a pair number of values (RMIM-029)
	public void test_rmim029() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RegionOfInterestSpec_rmim029_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-RegionOfInterestSpec-rmim029");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-RegionOfInterestSpec-rmim029");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RegionOfInterestSpec_rmim029_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-RegionOfInterestSpec-rmim029");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// If the code has the value CIRCLE, the value list SHALL have 4 values (RMIM-030)
	public void test_rmim030() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RegionOfInterestSpec_rmim030_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-RegionOfInterestSpec-rmim030");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-RegionOfInterestSpec-rmim030");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RegionOfInterestSpec_rmim030_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-RegionOfInterestSpec-rmim030");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// If the code has the value ELLIPSE, the value list SHALL have 8 values (RMIM-031)
	public void test_rmim031() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RegionOfInterestSpec_rmim031_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-RegionOfInterestSpec-rmim031");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-RegionOfInterestSpec-rmim031");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RegionOfInterestSpec_rmim031_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-RegionOfInterestSpec-rmim031");
		Assert.assertTrue(nn != null);//
	}


}		
