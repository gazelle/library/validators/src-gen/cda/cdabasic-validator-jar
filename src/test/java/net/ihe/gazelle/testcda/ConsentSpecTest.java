package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class ConsentSpecTest {
	@Test
	// Consent SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-059, RIM-001)
	public void test_rmim059_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ConsentSpec_rmim059_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ConsentSpec-rmim059_1");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ConsentSpec_rmim059_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ConsentSpec-rmim059_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ConsentSpec_rmim059_1_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-ConsentSpec-rmim059_1");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The ids elements of Consent SHALL be distinct (RMIM-059, RIM-002)
	public void test_rmim059_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ConsentSpec_rmim059_2_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ConsentSpec-rmim059_2");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ConsentSpec_rmim059_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ConsentSpec-rmim059_2");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// The statusCode if presents SHALL be from the valueSet ActStatus (RMIM-059, RIM-003)
	public void test_rmim059_3() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ConsentSpec_rmim059_3_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ConsentSpec-rmim059_3");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ConsentSpec_rmim059_3_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ConsentSpec-rmim059_3");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// consent.statusCode SHALL have the value 'completed' (or nothing) (RMIM-060)
	public void test_rmim060() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ConsentSpec_rmim060_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ConsentSpec-rmim060");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ConsentSpec_rmim060_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ConsentSpec-rmim060");
		Assert.assertTrue(nn != null);//
	}


}		
