package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class AssignedEntitySpecTest {
	@Test
	// AssignedEntity SHALL NOT have id element with nullFlavor, if there are other ids element (RMIM-072, RIM-011)
	public void test_rmim072_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssignedEntitySpec_rmim072_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-AssignedEntitySpec-rmim072_1");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssignedEntitySpec_rmim072_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-AssignedEntitySpec-rmim072_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssignedEntitySpec_rmim072_1_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-AssignedEntitySpec-rmim072_1");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The ids elements of AssignedEntity SHALL be distinct (RMIM-072, RIM-012)
	public void test_rmim072_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssignedEntitySpec_rmim072_2_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-AssignedEntitySpec-rmim072_2");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssignedEntitySpec_rmim072_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-AssignedEntitySpec-rmim072_2");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// AssignedEntity SHALL NOT have addr element with nullFlavor, if there are other addr (RMIM-072, RIM-016)
	public void test_rmim072_3() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssignedEntitySpec_rmim072_3_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-AssignedEntitySpec-rmim072_3");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssignedEntitySpec_rmim072_3_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-AssignedEntitySpec-rmim072_3");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssignedEntitySpec_rmim072_3_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-AssignedEntitySpec-rmim072_3");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The addr elements of AssignedEntity SHALL be distinct (RMIM-072, RIM-017)
	public void test_rmim072_4() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssignedEntitySpec_rmim072_4_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-AssignedEntitySpec-rmim072_4");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssignedEntitySpec_rmim072_4_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-AssignedEntitySpec-rmim072_4");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// If assignedEntity has not a null of flavor and its enclosing participation has not a null of flavor, 
	//it SHALL have an assignedPerson or a representedOrganization(RMIM-073)
	public void test_rmim073() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssignedEntitySpec_rmim073_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-AssignedEntitySpec-rmim073");
		Assert.assertTrue(nn != null);

		// Without assignedPerson
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssignedEntitySpec_rmim073_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-AssignedEntitySpec-rmim073");
		Assert.assertTrue(nn != null);
		
		// Without representedOrganization
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssignedEntitySpec_rmim073_OK_3.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-AssignedEntitySpec-rmim073");
		Assert.assertTrue(nn != null);
		
		// Without assignedPerson and representedOrganization
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssignedEntitySpec_rmim073_KO_1.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-AssignedEntitySpec-rmim073");
		Assert.assertTrue(nn != null);//
		
		// Authenticator is Nullflavor
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssignedEntitySpec_rmim073_OK_4.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-AssignedEntitySpec-rmim073");
		Assert.assertTrue(nn != null);
		
		// LegalAuthenticator is Nullflavor
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssignedEntitySpec_rmim073_OK_5.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-AssignedEntitySpec-rmim073");
		Assert.assertTrue(nn != null);
		
	}


}		
