package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class ObservationSpecTest {
	@Test
	// Observation SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-004, RIM-001)
	public void test_rmim004_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationSpec_rmim004_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ObservationSpec-rmim004_1");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ObservationSpec-rmim004_1");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationSpec_rmim004_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ObservationSpec-rmim004_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationSpec_rmim004_1_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-ObservationSpec-rmim004_1");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The ids elements of Observation SHALL be distinct (RMIM-004, RIM-002)
	public void test_rmim004_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationSpec_rmim004_2_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ObservationSpec-rmim004_2");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ObservationSpec-rmim004_2");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationSpec_rmim004_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ObservationSpec-rmim004_2");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// The statusCode if presents SHALL be from the valueSet ActStatus (RMIM-004, RIM-003)
	public void test_rmim004_3() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationSpec_rmim004_3_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ObservationSpec-rmim004_3");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ObservationSpec-rmim004_3");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationSpec_rmim004_3_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ObservationSpec-rmim004_3");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// Observation SHALL NOT have interpretationCode element with nullFlavor, 
	//if there are other interpretationCode elements which are not null (RMIM-004, RIM-004)
	public void test_rmim004_4() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationSpec_rmim004_4_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ObservationSpec-rmim004_4");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ObservationSpec-rmim004_4");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationSpec_rmim004_4_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ObservationSpec-rmim004_4");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationSpec_rmim004_4_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-ObservationSpec-rmim004_4");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The interpretationCode elements of Act SHALL be distinct (RMIM-004, RIM-005)
	public void test_rmim004_5() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationSpec_rmim004_5_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ObservationSpec-rmim004_5");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ObservationSpec-rmim004_5");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationSpec_rmim004_5_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ObservationSpec-rmim004_5");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// Observation SHALL NOT have methodCode element with nullFlavor, 
	//if there are other methodCode elements which are not null   (RMIM-004, RIM-006)
	public void test_rmim004_6() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationSpec_rmim004_6_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ObservationSpec-rmim004_6");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ObservationSpec-rmim004_6");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationSpec_rmim004_6_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ObservationSpec-rmim004_6");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationSpec_rmim004_6_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-ObservationSpec-rmim004_6");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The methodCode elements of Act SHALL be distinct  (RMIM-004, RIM-007)
	public void test_rmim004_7() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationSpec_rmim004_7_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ObservationSpec-rmim004_7");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ObservationSpec-rmim004_7");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationSpec_rmim004_7_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ObservationSpec-rmim004_7");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// Observation SHALL NOT have targetSiteCode element with nullFlavor, if there are other targetSiteCode elements which are not null
	//  (RMIM-004, RIM-008)
	public void test_rmim004_8() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationSpec_rmim004_8_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ObservationSpec-rmim004_8");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ObservationSpec-rmim004_8");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationSpec_rmim004_8_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ObservationSpec-rmim004_8");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationSpec_rmim004_8_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-ObservationSpec-rmim004_8");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The targetSiteCode elements of Act SHALL be distinct  (RMIM-004, RIM-009)
	public void test_rmim004_9() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationSpec_rmim004_9_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ObservationSpec-rmim004_9");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ObservationSpec-rmim004_9");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationSpec_rmim004_9_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ObservationSpec-rmim004_9");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// The languageCode shall be from the valueSet HumanLanguage (RMIM-005)
	public void test_rmim005() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationSpec_rmim005_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ObservationSpec-rmim005");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ObservationSpec-rmim005");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationSpec_rmim005_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ObservationSpec-rmim005");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// Numeric values SHALL NOT be communicated in Observation/value as a simple String (RMIM-009)
	public void test_rmim009() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationSpec_rmim009_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ObservationSpec-rmim009");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ObservationSpec-rmim009");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationSpec_rmim009_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ObservationSpec-rmim009");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// CS datatype SHALL NOT be used in value element, because only datatypes with (RMIM-010)
	public void test_rmim010() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationSpec_rmim010_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ObservationSpec-rmim010");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ObservationSpec-rmim010");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationSpec_rmim010_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ObservationSpec-rmim010");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// interpretationCode values SHALL be from the valueSet ObservationInterpretation (RMIM-011)
	public void test_rmim011() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationSpec_rmim011_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ObservationSpec-rmim011");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ObservationSpec-rmim011");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationSpec_rmim011_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ObservationSpec-rmim011");
		Assert.assertTrue(nn != null);//
	}


}		
