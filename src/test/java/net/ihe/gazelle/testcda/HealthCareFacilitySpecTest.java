package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class HealthCareFacilitySpecTest {
	@Test
	// HealthCareFacility SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-085, RIM-011)
	public void test_rmim085_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/HealthCareFacilitySpec_rmim085_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-HealthCareFacilitySpec-rmim085_1");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/HealthCareFacilitySpec_rmim085_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-HealthCareFacilitySpec-rmim085_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/HealthCareFacilitySpec_rmim085_1_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-HealthCareFacilitySpec-rmim085_1");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The ids elements of HealthCareFacility SHALL be distinct (RMIM-085, RIM-012)
	public void test_rmim085_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/HealthCareFacilitySpec_rmim085_2_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-HealthCareFacilitySpec-rmim085_2");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/HealthCareFacilitySpec_rmim085_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-HealthCareFacilitySpec-rmim085_2");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// If healthCareFacility has not a null of flavor and its enclosing location has not a null of flavor, 
	//it SHALL have a location or a serviceProviderOrganization (RMIM-086)
	public void test_rmim086() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/HealthCareFacilitySpec_rmim086_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-HealthCareFacilitySpec-rmim086");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/HealthCareFacilitySpec_rmim086_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-HealthCareFacilitySpec-rmim086");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/HealthCareFacilitySpec_rmim086_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-HealthCareFacilitySpec-rmim086");
		Assert.assertTrue(nn != null);
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/HealthCareFacilitySpec_rmim086_OK_3.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-HealthCareFacilitySpec-rmim086");
		Assert.assertTrue(nn != null);
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/HealthCareFacilitySpec_rmim086_OK_4.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-HealthCareFacilitySpec-rmim086");
		Assert.assertTrue(nn != null);
	}


}		
