package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class EntrySpecTest {
	@Test
	// When entry@typeCode=DRIV, the section containing the entry SHALL contain a text element (RMIM-066)
	public void test_rmim066() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EntrySpec_rmim066_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-EntrySpec-rmim066");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EntrySpec_rmim066_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-EntrySpec-rmim066");
		Assert.assertTrue(nn != null);//
	}


}		
