package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class CriterionSpecTest {
	@Test
	// Numeric values SHALL NOT be communicated in Criterion/value as a simple String (RMIM-041)
	public void test_rmim041() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CriterionSpec_rmim041_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-CriterionSpec-rmim041");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-CriterionSpec-rmim041");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CriterionSpec_rmim041_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-CriterionSpec-rmim041");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CriterionSpec_rmim041_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-CriterionSpec-rmim041");
		Assert.assertTrue(nn != null);
	}

	@Test
	// CS datatype SHALL NOT be used in value element, because only datatypes with codeSystem shall be used (RMIM-042)
	public void test_rmim042() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CriterionSpec_rmim042_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-CriterionSpec-rmim042");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-CriterionSpec-rmim042");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CriterionSpec_rmim042_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-CriterionSpec-rmim042");
		Assert.assertTrue(nn != null);//
	}


}		
