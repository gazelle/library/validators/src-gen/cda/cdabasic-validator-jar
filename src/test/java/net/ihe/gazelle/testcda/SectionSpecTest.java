package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class SectionSpecTest {
	@Test
	// The languageCode shall be from the valueSet HumanLanguage (RMIM-050)
	public void test_rmim050() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SectionSpec_rmim050_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-SectionSpec-rmim050");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SectionSpec_rmim050_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-SectionSpec-rmim050");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// Section.text SHALL be specified, as it is a required element (RMIM-052)
	public void test_rmim052() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SectionSpec_rmim052_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-SectionSpec-rmim052");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SectionSpec_rmim052_KO.xml");
		nn = CommonUtil.selectNotification(res, "WARNING", "cdabasic-SectionSpec-rmim052");
		Assert.assertTrue(nn != null);//
	}


}		
