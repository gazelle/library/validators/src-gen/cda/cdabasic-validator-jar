package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class AssignedAuthorSpecTest {
	@Test
	// AssignedAuthor SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-077, RIM-011)
	public void test_rmim077_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssignedAuthorSpec_rmim077_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-AssignedAuthorSpec-rmim077_1");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssignedAuthorSpec_rmim077_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-AssignedAuthorSpec-rmim077_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssignedAuthorSpec_rmim077_1_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-AssignedAuthorSpec-rmim077_1");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// The ids elements of AssignedAuthor SHALL be distinct (RMIM-077, RIM-012)
	public void test_rmim077_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssignedAuthorSpec_rmim077_2_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-AssignedAuthorSpec-rmim077_2");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssignedAuthorSpec_rmim077_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-AssignedAuthorSpec-rmim077_2");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssignedAuthorSpec_rmim077_2_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-AssignedAuthorSpec-rmim077_2");
		Assert.assertTrue(nn != null);
	}

	@Test
	// AssignedAuthor SHALL NOT have addr element with nullFlavor, if there are other addr (RMIM-077, RIM-016)
	public void test_rmim077_3() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssignedAuthorSpec_rmim077_3_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-AssignedAuthorSpec-rmim077_3");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssignedAuthorSpec_rmim077_3_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-AssignedAuthorSpec-rmim077_3");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssignedAuthorSpec_rmim077_3_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-AssignedAuthorSpec-rmim077_3");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The addr elements of AssignedAuthor SHALL be distinct (RMIM-077, RIM-017)
	public void test_rmim077_4() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssignedAuthorSpec_rmim077_4_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-AssignedAuthorSpec-rmim077_4");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AssignedAuthorSpec_rmim077_4_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-AssignedAuthorSpec-rmim077_4");
		Assert.assertTrue(nn != null);//
	}


}		
