package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class LegalAuthenticatorSpecTest {
	@Test
	// The signatureCode elements of LegalAuthenticator SHALL be from the valueSet ParticipationSignature (RMIM-069, RIM-010)
	public void test_rmim069_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/LegalAuthenticatorSpec_rmim069_1_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-LegalAuthenticatorSpec-rmim069_1");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/LegalAuthenticatorSpec_rmim069_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-LegalAuthenticatorSpec-rmim069_1");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// The legalAuthenticator.signatureCode elements SHOULD NOT have the value X (RMIM-070)
	public void test_rmim070() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/LegalAuthenticatorSpec_rmim070_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-LegalAuthenticatorSpec-rmim070");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/LegalAuthenticatorSpec_rmim070_KO.xml");
		nn = CommonUtil.selectNotification(res, "WARNING", "cdabasic-LegalAuthenticatorSpec-rmim070");
		Assert.assertTrue(nn != null);//
	}


}		
