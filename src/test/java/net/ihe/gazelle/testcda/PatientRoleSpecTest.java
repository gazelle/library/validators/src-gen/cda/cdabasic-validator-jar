package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class PatientRoleSpecTest {
	@Test
	// PatientRole SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-079, RIM-011)
	public void test_rmim079_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PatientRoleSpec_rmim079_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-PatientRoleSpec-rmim079_1");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PatientRoleSpec_rmim079_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-PatientRoleSpec-rmim079_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PatientRoleSpec_rmim079_1_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-PatientRoleSpec-rmim079_1");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The ids elements of PatientRole SHALL be distinct (RMIM-079, RIM-012)
	public void test_rmim079_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PatientRoleSpec_rmim079_2_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-PatientRoleSpec-rmim079_2");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PatientRoleSpec_rmim079_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-PatientRoleSpec-rmim079_2");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// PatientRole SHALL NOT have addr element with nullFlavor, if there are other addr (RMIM-079, RIM-016)
	public void test_rmim079_3() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PatientRoleSpec_rmim079_3_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-PatientRoleSpec-rmim079_3");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PatientRoleSpec_rmim079_3_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-PatientRoleSpec-rmim079_3");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PatientRoleSpec_rmim079_3_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-PatientRoleSpec-rmim079_3");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The addr elements of PatientRole SHALL be distinct (RMIM-079, RIM-017)
	public void test_rmim079_4() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PatientRoleSpec_rmim079_4_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-PatientRoleSpec-rmim079_4");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PatientRoleSpec_rmim079_4_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-PatientRoleSpec-rmim079_4");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// PatientRole SHALL NOT have telecom element with nullFlavor, if there are other telecom elements which are not null (RMIM-079, RIM-018)
	public void test_rmim079_5() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PatientRoleSpec_rmim079_5_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-PatientRoleSpec-rmim079_5");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PatientRoleSpec_rmim079_5_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-PatientRoleSpec-rmim079_5");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PatientRoleSpec_rmim079_5_OK_2.xml");
		nn= CommonUtil.selectNotification(res, "note", "cdabasic-PatientRoleSpec-rmim079_5");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The telecom elements of PatientRole SHALL be distinct (RMIM-079, RIM-019)
	public void test_rmim079_6() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PatientRoleSpec_rmim079_6_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-PatientRoleSpec-rmim079_6");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PatientRoleSpec_rmim079_6_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-PatientRoleSpec-rmim079_6");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// If patientRole has not a null of flavor and its enclosing recordTarget has not a null of flavor,
	// it SHALL have a patient entity, or a providerOrganization (RMIM-080)
	public void test_rmim080() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PatientRoleSpec_rmim080_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-PatientRoleSpec-rmim080");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PatientRoleSpec_rmim080_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-PatientRoleSpec-rmim080");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PatientRoleSpec_rmim080_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-PatientRoleSpec-rmim080");
		Assert.assertTrue(nn != null);
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PatientRoleSpec_rmim080_OK_3.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-PatientRoleSpec-rmim080");
		Assert.assertTrue(nn != null);
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PatientRoleSpec_rmim080_OK_4.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-PatientRoleSpec-rmim080");
		Assert.assertTrue(nn != null);
	}


}		
