package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class ParentDocumentSpecTest {
	@Test
	// ParentDocument SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-053, RIM-001)
	public void test_rmim053_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ParentDocumentSpec_rmim053_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ParentDocumentSpec-rmim053_1");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ParentDocumentSpec_rmim053_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ParentDocumentSpec-rmim053_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ParentDocumentSpec_rmim053_1_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-ParentDocumentSpec-rmim053_1");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The ids elements of ParentDocument SHALL be distinct (RMIM-053, RIM-002)
	public void test_rmim053_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ParentDocumentSpec_rmim053_2_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ParentDocumentSpec-rmim053_2");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ParentDocumentSpec_rmim053_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ParentDocumentSpec-rmim053_2");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// ParentDocument/text.BIN element SHALL NOT be used (RMIM-054)
	public void test_rmim054() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ParentDocumentSpec_rmim054_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ParentDocumentSpec-rmim054");
		Assert.assertTrue(nn != null);
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ParentDocumentSpec_rmim054_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-ParentDocumentSpec-rmim054");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ParentDocumentSpec_rmim054_KO_1.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ParentDocumentSpec-rmim054");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ParentDocumentSpec_rmim054_KO_2.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ParentDocumentSpec-rmim054");
		Assert.assertTrue(nn != null);//
	}


}		
