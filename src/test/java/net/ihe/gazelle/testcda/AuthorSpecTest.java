package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class AuthorSpecTest {
	@Test
	// If assignedAuthor has not a null of flavor and its enclosing author has not a null of flavor, 
	//it SHALL have an assignedAuthoringDevice or assignedPerson or a representedOrganization element (RMIM-078)
	public void test_rmim078() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AuthorSpec_rmim078_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-AuthorSpec-rmim078");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AuthorSpec_rmim078_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-AuthorSpec-rmim078");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AuthorSpec_rmim078_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-AuthorSpec-rmim078");
		Assert.assertTrue(nn != null);
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AuthorSpec_rmim078_OK_3.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-AuthorSpec-rmim078");
		Assert.assertTrue(nn != null);
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AuthorSpec_rmim078_OK_4.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-AuthorSpec-rmim078");
		Assert.assertTrue(nn != null);
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/AuthorSpec_rmim078_OK_5.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-AuthorSpec-rmim078");
		Assert.assertTrue(nn != null);
	}


}		
