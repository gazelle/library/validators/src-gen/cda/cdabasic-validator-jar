package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class ProcedureSpecTest {
	@Test
	// Procedure SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-014, RIM-001)
	public void test_rmim014_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ProcedureSpec_rmim014_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ProcedureSpec-rmim014_1");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ProcedureSpec_rmim014_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ProcedureSpec-rmim014_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ProcedureSpec_rmim014_1_OK_2.xml");
		nn= CommonUtil.selectNotification(res, "note", "cdabasic-ProcedureSpec-rmim014_1");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The ids elements of Procedure SHALL be distinct (RMIM-014, RIM-002)
	public void test_rmim014_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ProcedureSpec_rmim014_2_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ProcedureSpec-rmim014_2");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ProcedureSpec_rmim014_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ProcedureSpec-rmim014_2");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// The statusCode if presents SHALL be from the valueSet ActStatus (RMIM-014, RIM-003)
	public void test_rmim014_3() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ProcedureSpec_rmim014_3_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ProcedureSpec-rmim014_3");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ProcedureSpec_rmim014_3_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ProcedureSpec-rmim014_3");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// The languageCode shall be from the valueSet HumanLanguage (RMIM-015)
	public void test_rmim015() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ProcedureSpec_rmim015_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ProcedureSpec-rmim015");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ProcedureSpec_rmim015_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ProcedureSpec-rmim015");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// Procedure SHALL NOT have methodCode element with nullFlavor, 
	//if there are other methodCode elements which are not null (RMIM-017)
	public void test_rmim017() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ProcedureSpec_rmim017_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ProcedureSpec-rmim017");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ProcedureSpec_rmim017_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ProcedureSpec-rmim017");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ProcedureSpec_rmim017_OK_2.xml");
		nn= CommonUtil.selectNotification(res, "note", "cdabasic-ProcedureSpec-rmim017");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The methodCode elements of Procedure SHALL be distinct (RMIM-018)
	public void test_rmim018() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ProcedureSpec_rmim018_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ProcedureSpec-rmim018");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ProcedureSpec_rmim018_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ProcedureSpec-rmim018");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// Procedure SHALL NOT have approachSiteCode element with nullFlavor, 
	//if there are other approachSiteCode elements which are not null (RMIM-019)
	public void test_rmim019() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ProcedureSpec_rmim019_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ProcedureSpec-rmim019");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ProcedureSpec_rmim019_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ProcedureSpec-rmim019");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ProcedureSpec_rmim019_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-ProcedureSpec-rmim019");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The approachSiteCode elements of Procedure SHALL be distinct (RMIM-020)
	public void test_rmim020() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ProcedureSpec_rmim020_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ProcedureSpec-rmim020");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ProcedureSpec_rmim020_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ProcedureSpec-rmim020");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// Procedure SHALL NOT have targetSiteCode element with nullFlavor, 
	//if there are other targetSiteCode elements which are not null (RMIM-021)
	public void test_rmim021() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ProcedureSpec_rmim021_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ProcedureSpec-rmim021");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ProcedureSpec_rmim021_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ProcedureSpec-rmim021");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ProcedureSpec_rmim021_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-ProcedureSpec-rmim021");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The targetSiteCode elements of Procedure SHALL be distinct (RMIM-022)
	public void test_rmim022() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ProcedureSpec_rmim022_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ProcedureSpec-rmim022");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ProcedureSpec_rmim022_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ProcedureSpec-rmim022");
		Assert.assertTrue(nn != null);//
	}


}		
