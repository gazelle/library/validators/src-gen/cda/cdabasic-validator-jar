package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class ClinicalDocumentSpecTest {
	@Test
	// if regionOfInterest is target of a renderMultimedia then it shall have exactly one ObservationMedia and not an ExternalObservation (RMIM-026)
	public void test_rmim026() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ClinicalDocumentSpec_rmim026_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ClinicalDocumentSpec-rmim026");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ClinicalDocumentSpec-rmim026");
		Assert.assertFalse(nn != null);
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ClinicalDocumentSpec_rmim026_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ClinicalDocumentSpec-rmim026");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// The Section.id element is unique into the CDA Document (RMIM-051)
	public void test_rmim051() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ClinicalDocumentSpec_rmim051_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ClinicalDocumentSpec-rmim051");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ClinicalDocumentSpec-rmim051");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ClinicalDocumentSpec_rmim051_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ClinicalDocumentSpec-rmim051");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// ParentDocument/id SHALL be different from the id of ClinicalDocument/id, 
	//when the typeCode=RPLC or typeCode=APND (RMIM-055)
	public void test_rmim055() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ClinicalDocumentSpec_rmim055_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ClinicalDocumentSpec-rmim055");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ClinicalDocumentSpec-rmim055");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ClinicalDocumentSpec_rmim055_KO_1.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ClinicalDocumentSpec-rmim055");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ClinicalDocumentSpec_rmim055_KO_2.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ClinicalDocumentSpec-rmim055");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ClinicalDocumentSpec_rmim055_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-ClinicalDocumentSpec-rmim055");
		Assert.assertTrue(nn != null);
	}

	@Test
	// All documents in a chain of replacement SHALL have the same setId (RMIM-056)
	public void test_rmim056_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ClinicalDocumentSpec_rmim056_1_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ClinicalDocumentSpec-rmim056_1");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ClinicalDocumentSpec-rmim056_1");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ClinicalDocumentSpec_rmim056_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ClinicalDocumentSpec-rmim056_1");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// All documents in a chain of replacement SHALL have a versionNumber incrementing (RMIM-056)
	public void test_rmim056_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ClinicalDocumentSpec_rmim056_2_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ClinicalDocumentSpec-rmim056_2");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ClinicalDocumentSpec-rmim056_2");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ClinicalDocumentSpec_rmim056_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ClinicalDocumentSpec-rmim056_2");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ClinicalDocumentSpec_rmim056_2_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-ClinicalDocumentSpec-rmim056_2");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The languageCode shall be from the valueSet HumanLanguage (RMIM-063)
	public void test_rmim063() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ClinicalDocumentSpec_rmim063_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ClinicalDocumentSpec-rmim063");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ClinicalDocumentSpec-rmim063");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ClinicalDocumentSpec_rmim063_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ClinicalDocumentSpec-rmim063");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// copyTime SHOULD NOT be used (4.2.1.9, [1]) (RMIM-064)
	public void test_rmim064() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ClinicalDocumentSpec_rmim064_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ClinicalDocumentSpec-rmim064");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "WARNING", "cdabasic-ClinicalDocumentSpec-rmim064");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ClinicalDocumentSpec_rmim064_KO.xml");
		nn = CommonUtil.selectNotification(res, "WARNING", "cdabasic-ClinicalDocumentSpec-rmim064");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// Only these combination of relatedDocument MAY be present : (APND), (RPLC), (XFR), (XFRM, RPLC), (XFRM, APND) (RMIM-065)
	public void test_rmim065() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ClinicalDocumentSpec_rmim065_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ClinicalDocumentSpec-rmim065");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ClinicalDocumentSpec-rmim065");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ClinicalDocumentSpec_rmim065_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ClinicalDocumentSpec-rmim065");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// renderMultimedia@referencedObject SHALL have a value of ObservationMultimedia or RegionOfInterest within the same document (RMIM-109)
	public void test_rmim109() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ClinicalDocumentSpec_rmim109_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ClinicalDocumentSpec-rmim109");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ClinicalDocumentSpec-rmim109");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ClinicalDocumentSpec_rmim109_KO_1.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ClinicalDocumentSpec-rmim109");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ClinicalDocumentSpec_rmim109_KO_2.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ClinicalDocumentSpec-rmim109");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// renderMultimedia SHALL refer to a single observationMedia, or to multiple regionOfInterest (RMIM-110)
	public void test_rmim110() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ClinicalDocumentSpec_rmim110_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ClinicalDocumentSpec-rmim110");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ClinicalDocumentSpec-rmim110");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ClinicalDocumentSpec_rmim110_KO_1.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ClinicalDocumentSpec-rmim110");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ClinicalDocumentSpec_rmim110_KO_2.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ClinicalDocumentSpec-rmim110");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ClinicalDocumentSpec_rmim110_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-ClinicalDocumentSpec-rmim110");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ClinicalDocumentSpec-rmim110");
		Assert.assertFalse(nn != null);
	}

	@Test
	// the footnoteref SHALL reference a footnote in the same document (RMIM-113)
	public void test_rmim113() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ClinicalDocumentSpec_rmim113_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ClinicalDocumentSpec-rmim113");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ClinicalDocumentSpec-rmim113");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ClinicalDocumentSpec_rmim113_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ClinicalDocumentSpec-rmim113");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// Ordered list style SHOULD be used only on the element list, with listType=ordered (RMIM-114)
	public void test_rmim114() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ClinicalDocumentSpec_rmim114_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ClinicalDocumentSpec-rmim114");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "WARNING", "cdabasic-ClinicalDocumentSpec-rmim114");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ClinicalDocumentSpec_rmim114_KO.xml");
		nn = CommonUtil.selectNotification(res, "WARNING", "cdabasic-ClinicalDocumentSpec-rmim114");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// Unordered list style SHOULD be used only on the element list, with listType=unordered (RMIM-115)
	public void test_rmim115() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ClinicalDocumentSpec_rmim115_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ClinicalDocumentSpec-rmim115");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "WARNING", "cdabasic-ClinicalDocumentSpec-rmim115");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ClinicalDocumentSpec_rmim115_KO.xml");
		nn = CommonUtil.selectNotification(res, "WARNING", "cdabasic-ClinicalDocumentSpec-rmim115");
		Assert.assertTrue(nn != null);//
	}


}		
