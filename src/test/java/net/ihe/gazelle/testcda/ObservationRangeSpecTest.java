package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class ObservationRangeSpecTest {
	@Test
	// Numeric values SHALL NOT be communicated in ObservationRange/value as a simple String (RMIM-038)
	public void test_rmim038() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationRangeSpec_rmim038_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ObservationRangeSpec-rmim038");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ObservationRangeSpec-rmim038");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationRangeSpec_rmim038_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ObservationRangeSpec-rmim038");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// CS datatype SHALL NOT be used in value element, because only datatypes with (RMIM-039)
	public void test_rmim039() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationRangeSpec_rmim039_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ObservationRangeSpec-rmim039");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ObservationRangeSpec-rmim039");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationRangeSpec_rmim039_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ObservationRangeSpec-rmim039");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// interpretationCode values SHALL be from the valueSet ObservationInterpretation (RMIM-040)
	public void test_rmim040() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationRangeSpec_rmim040_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ObservationRangeSpec-rmim040");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ObservationRangeSpec-rmim040");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ObservationRangeSpec_rmim040_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ObservationRangeSpec-rmim040");
		Assert.assertTrue(nn != null);//
	}


}		
