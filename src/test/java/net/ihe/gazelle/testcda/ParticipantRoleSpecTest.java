package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class ParticipantRoleSpecTest {
	@Test
	// ParticipantRole SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-092, RIM-011)
	public void test_rmim092_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ParticipantRoleSpec_rmim092_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ParticipantRoleSpec-rmim092_1");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ParticipantRoleSpec_rmim092_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ParticipantRoleSpec-rmim092_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ParticipantRoleSpec_rmim092_1_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-ParticipantRoleSpec-rmim092_1");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The ids elements of ParticipantRole SHALL be distinct (RMIM-092, RIM-012)
	public void test_rmim092_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ParticipantRoleSpec_rmim092_2_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ParticipantRoleSpec-rmim092_2");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ParticipantRoleSpec_rmim092_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ParticipantRoleSpec-rmim092_2");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// ParticipantRole SHALL NOT have addr element with nullFlavor, if there are other addr (RMIM-092, RIM-016)
	public void test_rmim092_3() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ParticipantRoleSpec_rmim092_3_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ParticipantRoleSpec-rmim092_3");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ParticipantRoleSpec_rmim092_3_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ParticipantRoleSpec-rmim092_3");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ParticipantRoleSpec_rmim092_3_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-ParticipantRoleSpec-rmim092_3");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The addr elements of ParticipantRole SHALL be distinct (RMIM-092, RIM-017)
	public void test_rmim092_4() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ParticipantRoleSpec_rmim092_4_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ParticipantRoleSpec-rmim092_4");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ParticipantRoleSpec_rmim092_4_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ParticipantRoleSpec-rmim092_4");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// ParticipantRole SHALL NOT have telecom element with nullFlavor, if there are other telecom elements which are not null (RMIM-092, RIM-018)
	public void test_rmim092_5() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ParticipantRoleSpec_rmim092_5_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ParticipantRoleSpec-rmim092_5");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ParticipantRoleSpec_rmim092_5_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ParticipantRoleSpec-rmim092_5");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ParticipantRoleSpec_rmim092_5_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-ParticipantRoleSpec-rmim092_5");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The telecom elements of ParticipantRole SHALL be distinct (RMIM-092, RIM-019)
	public void test_rmim092_6() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ParticipantRoleSpec_rmim092_6_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ParticipantRoleSpec-rmim092_6");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ParticipantRoleSpec_rmim092_6_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ParticipantRoleSpec-rmim092_6");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// If participantRole has not a null of flavor and its enclosing participant has not a null of flavor, 
	//it SHALL have a scopingEntity, or a playingEntityChoice (RMIM-093)
	public void test_rmim093() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ParticipantRoleSpec_rmim093_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-ParticipantRoleSpec-rmim093");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ParticipantRoleSpec_rmim093_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-ParticipantRoleSpec-rmim093");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ParticipantRoleSpec_rmim093_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-ParticipantRoleSpec-rmim093");
		Assert.assertTrue(nn != null);
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ParticipantRoleSpec_rmim093_OK_3.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-ParticipantRoleSpec-rmim093");
		Assert.assertTrue(nn != null);
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ParticipantRoleSpec_rmim093_OK_4.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-ParticipantRoleSpec-rmim093");
		Assert.assertTrue(nn != null);
	}


}		
