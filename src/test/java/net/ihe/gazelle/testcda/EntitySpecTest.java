package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class EntitySpecTest {
	@Test
	// Entity SHALL NOT have id element with nullFlavor, if there are other ids (RMIM-107, RIM-020)
	public void test_rmim107_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EntitySpec_rmim107_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-EntitySpec-rmim107_1");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EntitySpec_rmim107_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-EntitySpec-rmim107_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EntitySpec_rmim107_1_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-EntitySpec-rmim107_1");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The ids elements of Entity SHALL be distinct (RMIM-107, RIM-021)
	public void test_rmim107_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EntitySpec_rmim107_2_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-EntitySpec-rmim107_2");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EntitySpec_rmim107_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-EntitySpec-rmim107_2");
		Assert.assertTrue(nn != null);//
	}


}		
