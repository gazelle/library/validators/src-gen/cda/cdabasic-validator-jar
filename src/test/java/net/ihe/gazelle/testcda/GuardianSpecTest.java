package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class GuardianSpecTest {
	@Test
	// Guardian SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-081, RIM-011)
	public void test_rmim081_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/GuardianSpec_rmim081_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-GuardianSpec-rmim081_1");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/GuardianSpec_rmim081_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-GuardianSpec-rmim081_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/GuardianSpec_rmim081_1_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-GuardianSpec-rmim081_1");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The ids elements of Guardian SHALL be distinct (RMIM-081, RIM-012)
	public void test_rmim081_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/GuardianSpec_rmim081_2_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-GuardianSpec-rmim081_2");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/GuardianSpec_rmim081_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-GuardianSpec-rmim081_2");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// Gardian SHALL NOT have addr element with nullFlavor, if there are other addr (RMIM-081, RIM-016)
	public void test_rmim081_3() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/GuardianSpec_rmim081_3_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-GuardianSpec-rmim081_3");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/GuardianSpec_rmim081_3_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-GuardianSpec-rmim081_3");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/GuardianSpec_rmim081_3_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-GuardianSpec-rmim081_3");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The addr elements of Gardian SHALL be distinct (RMIM-081, RIM-017)
	public void test_rmim081_4() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/GuardianSpec_rmim081_4_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-GuardianSpec-rmim081_4");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/GuardianSpec_rmim081_4_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-GuardianSpec-rmim081_4");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// Guardian SHALL NOT have telecom element with nullFlavor, if there are other (RMIM-081, RIM-018)
	public void test_rmim081_5() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/GuardianSpec_rmim081_5_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-GuardianSpec-rmim081_5");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/GuardianSpec_rmim081_5_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-GuardianSpec-rmim081_5");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/GuardianSpec_rmim081_5_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-GuardianSpec-rmim081_5");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The telecom elements of Role SHALL be distinct (RMIM-081, RIM-018)
	public void test_rmim081_6() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/GuardianSpec_rmim081_6_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-GuardianSpec-rmim081_6");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/GuardianSpec_rmim081_6_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-GuardianSpec-rmim081_6");
		Assert.assertTrue(nn != null);//
	}


}		
