package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class SupplySpecTest {
	@Test
	// Supply SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-035, RIM-001)
	public void test_rmim035_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SupplySpec_rmim035_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-SupplySpec-rmim035_1");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SupplySpec_rmim035_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-SupplySpec-rmim035_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SupplySpec_rmim035_1_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-SupplySpec-rmim035_1");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The ids elements of Supply SHALL be distinct (RMIM-035, RIM-002)
	public void test_rmim035_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SupplySpec_rmim035_2_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-SupplySpec-rmim035_2");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SupplySpec_rmim035_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-SupplySpec-rmim035_2");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// The statusCode if presents SHALL be from the valueSet ActStatus (RMIM-035, RIM-003)
	public void test_rmim035_3() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SupplySpec_rmim035_3_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-SupplySpec-rmim035_3");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SupplySpec_rmim035_3_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-SupplySpec-rmim035_3");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// The priorityCode elements of Supply SHALL be distinct (RMIM-036)
	public void test_rmim036() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SupplySpec_rmim036_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-SupplySpec-rmim036");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SupplySpec_rmim036_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-SupplySpec-rmim036");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// Supply SHALL NOT have priorityCode element with nullFlavor, 
	//if there are other priorityCode elements which are not null (RMIM-037)
	public void test_rmim037() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SupplySpec_rmim037_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-SupplySpec-rmim037");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SupplySpec_rmim037_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-SupplySpec-rmim037");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SupplySpec_rmim037_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-SupplySpec-rmim037");
		Assert.assertTrue(nn != null);
	}


}		
