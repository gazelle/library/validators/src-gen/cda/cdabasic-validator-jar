package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class IntendedRecipientSpecTest {
	@Test
	// IntendedRecipient SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-074, RIM-011)
	public void test_rmim074_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IntendedRecipientSpec_rmim074_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-IntendedRecipientSpec-rmim074_1");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IntendedRecipientSpec_rmim074_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-IntendedRecipientSpec-rmim074_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IntendedRecipientSpec_rmim074_1_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-IntendedRecipientSpec-rmim074_1");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The ids elements of IntendedRecipient SHALL be distinct (RMIM-074, RIM-012)
	public void test_rmim074_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IntendedRecipientSpec_rmim074_2_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-IntendedRecipientSpec-rmim074_2");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IntendedRecipientSpec_rmim074_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-IntendedRecipientSpec-rmim074_2");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// IntendedRecipient SHALL NOT have addr element with nullFlavor, if there are other addr (RMIM-074, RIM-016)
	public void test_rmim074_3() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IntendedRecipientSpec_rmim074_3_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-IntendedRecipientSpec-rmim074_3");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IntendedRecipientSpec_rmim074_3_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-IntendedRecipientSpec-rmim074_3");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IntendedRecipientSpec_rmim074_3_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-IntendedRecipientSpec-rmim074_3");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The addr elements of IntendedRecipient SHALL be distinct (RMIM-074, RIM-017)
	public void test_rmim074_4() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IntendedRecipientSpec_rmim074_4_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-IntendedRecipientSpec-rmim074_4");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IntendedRecipientSpec_rmim074_4_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-IntendedRecipientSpec-rmim074_4");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// IntendedRecipient SHALL NOT have telecom element with nullFlavor, if there are other (RMIM-074, RIM-018)
	public void test_rmim074_5() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IntendedRecipientSpec_rmim074_5_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-IntendedRecipientSpec-rmim074_5");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IntendedRecipientSpec_rmim074_5_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-IntendedRecipientSpec-rmim074_5");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IntendedRecipientSpec_rmim074_5_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-IntendedRecipientSpec-rmim074_5");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The telecom elements of IntendedRecipient SHALL be distinct (RMIM-074, RIM-019)
	public void test_rmim074_6() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IntendedRecipientSpec_rmim074_6_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-IntendedRecipientSpec-rmim074_6");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IntendedRecipientSpec_rmim074_6_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-IntendedRecipientSpec-rmim074_6");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// If intendedRecipient has not a null of flavor and its enclosing informationRecipient has not a null of flavor, 
	//it SHALL have an informationRecipient or a receivedOrganization (RMIM-075)
	public void test_rmim075() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IntendedRecipientSpec_rmim075_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-IntendedRecipientSpec-rmim075");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IntendedRecipientSpec_rmim075_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-IntendedRecipientSpec-rmim075");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IntendedRecipientSpec_rmim075_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-IntendedRecipientSpec-rmim076");
		Assert.assertTrue(nn != null);
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IntendedRecipientSpec_rmim075_OK_3.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-IntendedRecipientSpec-rmim076");
		Assert.assertTrue(nn != null);
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IntendedRecipientSpec_rmim075_OK_4.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-IntendedRecipientSpec-rmim076");
		Assert.assertTrue(nn != null);
	}

	@Test
	// When intendedRecipient=HLTHCHRT, the informationRecipient SHALL NOT be provided (RMIM-076)
	public void test_rmim076() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IntendedRecipientSpec_rmim076_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-IntendedRecipientSpec-rmim076");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IntendedRecipientSpec_rmim076_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-IntendedRecipientSpec-rmim076");
		Assert.assertTrue(nn != null);//
	}


}		
