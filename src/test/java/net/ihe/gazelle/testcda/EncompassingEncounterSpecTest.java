package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class EncompassingEncounterSpecTest {
	@Test
	// EncompassingEncounter SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-061, RIM-001)
	public void test_rmim061_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EncompassingEncounterSpec_rmim061_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-EncompassingEncounterSpec-rmim061_1");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EncompassingEncounterSpec_rmim061_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-EncompassingEncounterSpec-rmim061_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EncompassingEncounterSpec_rmim061_1_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-EncompassingEncounterSpec-rmim061_1");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The ids elements of EncompassingEncounter SHALL be distinct (RMIM-061, RIM-002)
	public void test_rmim061_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EncompassingEncounterSpec_rmim061_2_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-EncompassingEncounterSpec-rmim061_2");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EncompassingEncounterSpec_rmim061_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-EncompassingEncounterSpec-rmim061_2");
		Assert.assertTrue(nn != null);//
	}


}		
