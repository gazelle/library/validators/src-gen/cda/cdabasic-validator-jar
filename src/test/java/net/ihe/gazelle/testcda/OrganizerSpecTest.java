package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class OrganizerSpecTest {
	@Test
	// Organizer SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-013, RIM-001)
	public void test_rmim013_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/OrganizerSpec_rmim013_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-OrganizerSpec-rmim013_1");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/OrganizerSpec_rmim013_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-OrganizerSpec-rmim013_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/OrganizerSpec_rmim013_1_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-OrganizerSpec-rmim013_1");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The ids elements of Organizer SHALL be distinct (RMIM-013, RIM-002)
	public void test_rmim013_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/OrganizerSpec_rmim013_2_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-OrganizerSpec-rmim013_2");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/OrganizerSpec_rmim013_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-OrganizerSpec-rmim013_2");
		Assert.assertTrue(nn != null);//
	}


}		
