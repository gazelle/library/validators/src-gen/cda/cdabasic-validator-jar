package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class PlayingEntitySpecTest {
	@Test
	// PlayingEntity SHALL NOT have name element with nullFlavor, if there are other name (RMIM-104, RIM-022)
	public void test_rmim104_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PlayingEntitySpec_rmim104_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-PlayingEntitySpec-rmim104_1");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PlayingEntitySpec_rmim104_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-PlayingEntitySpec-rmim104_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PlayingEntitySpec_rmim104_1_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-PlayingEntitySpec-rmim104_1");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The name elements of PlayingEntity SHALL be distinct (RMIM-104, RIM-023)
	public void test_rmim104_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PlayingEntitySpec_rmim104_2_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-PlayingEntitySpec-rmim104_2");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PlayingEntitySpec_rmim104_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-PlayingEntitySpec-rmim104_2");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// PlayingEntity SHALL NOT have quantity element with nullFlavor, if there are other quantity elements which are not null (RMIM-105)
	public void test_rmim105() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PlayingEntitySpec_rmim105_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-PlayingEntitySpec-rmim105");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PlayingEntitySpec_rmim105_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-PlayingEntitySpec-rmim105");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PlayingEntitySpec_rmim105_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-PlayingEntitySpec-rmim105");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The quantity elements of PlayingEntity SHALL be distinct (RMIM-106)
	public void test_rmim106() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PlayingEntitySpec_rmim106_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-PlayingEntitySpec-rmim106");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PlayingEntitySpec_rmim106_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-PlayingEntitySpec-rmim106");
		Assert.assertTrue(nn != null);//
	}


}		
