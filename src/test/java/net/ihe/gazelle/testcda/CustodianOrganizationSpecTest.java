package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class CustodianOrganizationSpecTest {
	@Test
	// CustodianOrganization SHALL NOT have id element with nullFlavor, if there are other ids (RMIM-099, RIM-020)
	public void test_rmim099_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CustodianOrganizationSpec_rmim099_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-CustodianOrganizationSpec-rmim099_1");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-CustodianOrganizationSpec-rmim099_1");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CustodianOrganizationSpec_rmim099_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-CustodianOrganizationSpec-rmim099_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CustodianOrganizationSpec_rmim099_1_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdabasic-CustodianOrganizationSpec-rmim099_2");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The ids elements of CustodianOrganization SHALL be distinct (RMIM-099, RIM-021)
	public void test_rmim099_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CustodianOrganizationSpec_rmim099_2_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-CustodianOrganizationSpec-rmim099_2");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-CustodianOrganizationSpec-rmim099_2");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CustodianOrganizationSpec_rmim099_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-CustodianOrganizationSpec-rmim099_2");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// id element SHALL be present and is mandatory (RMIM-100)
	public void test_rmim100() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CustodianOrganizationSpec_rmim100_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-CustodianOrganizationSpec-rmim100");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-CustodianOrganizationSpec-rmim100");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CustodianOrganizationSpec_rmim100_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-CustodianOrganizationSpec-rmim100");
		Assert.assertTrue(nn != null);//
	}


}		
