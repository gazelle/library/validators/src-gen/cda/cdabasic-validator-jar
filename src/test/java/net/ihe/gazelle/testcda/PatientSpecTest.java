package net.ihe.gazelle.testcda;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class PatientSpecTest {
	@Test
	// Patient SHALL NOT have name element with nullFlavor, if there are other name (RMIM-101, RIM-022)
	public void test_rmim101_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PatientSpec_rmim101_1_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-PatientSpec-rmim101_1");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PatientSpec_rmim101_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-PatientSpec-rmim101_1");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PatientSpec_rmim101_1_OK_2.xml");
		nn= CommonUtil.selectNotification(res, "note", "cdabasic-PatientSpec-rmim101_1");
		Assert.assertTrue(nn != null);
	}

	@Test
	// The name elements of Patient SHALL be distinct (RMIM-101, RIM-023)
	public void test_rmim101_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PatientSpec_rmim101_2_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-PatientSpec-rmim101_2");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PatientSpec_rmim101_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdabasic-PatientSpec-rmim101_2");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// id element SHOULD NOT be present (RMIM-102)
	public void test_rmim102() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PatientSpec_rmim102_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdabasic-PatientSpec-rmim102");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PatientSpec_rmim102_KO.xml");
		nn = CommonUtil.selectNotification(res, "WARNING", "cdabasic-PatientSpec-rmim102");
		Assert.assertTrue(nn != null);//
	}


}		
