package net.ihe.gazelle.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RimUpdater {
	
	public static void main(String[] args) throws IOException {
		updateConstraintDocumentation("/home/aboufahj/workspaceTopcased/cdabasic-model/tmp/xml/cdabasic-constraint.xml");
	}

	private static void updateConstraintDocumentation(String string) throws IOException {
		String content = readDoc(string);
		String res = content;
		Pattern pat = Pattern.compile("assertionId=\"RIM-(\\d*)\" idScheme=\"RMIM\"");
		Matcher mat = pat.matcher(content);
		while (mat.find()) {
			res = res.replace(mat.group(), "assertionId=\"RIM-" + mat.group(1) + "\" idScheme=\"RIM\"");			
		}
		
		printDoc(res, getNewNameForSaving(string));
	}

	private static String getNewNameForSaving(String string) {
		return "/home/aboufahj/workspaceTopcased/cdabasic-model/tmp/xml/cdabasic-constraint-modified.xml";
	}

	public static String readDoc(String name) throws IOException {
		BufferedReader scanner = new BufferedReader(new FileReader(name));
		StringBuilder res = new StringBuilder();
		try {
			String line = scanner.readLine();
			while (line != null) {
				res.append(line + "\n");
				line = scanner.readLine();
			}
		} finally {
			scanner.close();
		}
		return res.toString();
	}
	
	public static void printDoc(String doc, String name) throws IOException {
		FileWriter fw = new FileWriter(new File(name));
		fw.append(doc);
		fw.close();
	}
}
