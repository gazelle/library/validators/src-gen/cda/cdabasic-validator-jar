package net.ihe.gazelle.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import net.ihe.gazelle.basic.validation.BasicITSValidation;
import net.ihe.gazelle.cda.POCDMT000040ClinicalDocument;
import net.ihe.gazelle.cda.cdabasic.CDABASICPackValidator;
import net.ihe.gazelle.cda.cdadt.CDADTPackValidator;
import net.ihe.gazelle.cda.cdanblock.CDANBLOCKPackValidator;
import net.ihe.gazelle.gen.common.CommonOperations;
import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import net.ihe.gazelle.gen.common.SVSConsumer;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.MDAValidation;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validation.Warning;

public class CommonUtil {

	static {
		System.out.println("Initiating the CDA validator of the application");
		CommonOperations.setValueSetProvider(new SVSConsumer() {
			@Override
			protected String getSVSRepositoryUrl() {
				return "https://gazelle.ihe.net/SVSSimulator/rest/RetrieveValueSetForSimulator";
			}
		});
	}

	public static DetailedResult getDetailedResultFromFile(String dd) {
		POCDMT000040ClinicalDocument ss = null;
		try {
			ss = load(new FileInputStream(dd));
		} catch (FileNotFoundException e) {
			System.out.println("problem to find file: " + dd);
			return errorExtraction(ss, e);
		} catch (JAXBException e) {
			System.out.println("problem to parse file: " + dd);
			return errorExtraction(ss, e);
		}
		List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<ConstraintValidatorModule>();
		listConstraintValidatorModule.add(new CDABASICPackValidator());
		listConstraintValidatorModule.add(new CDADTPackValidator());
		listConstraintValidatorModule.add(new CDANBLOCKPackValidator());
		DetailedResult res = new DetailedResult();
		validateClinicalDocumentType(res, ss, listConstraintValidatorModule);
		try {
			String content = readDoc(dd);
			Notification not1 = BasicITSValidation.validateLangNotUsed(content);
			Notification not2 = BasicITSValidation.validateNoNamespaceSchemaLocationNotUsed(content);
			Notification not3 = BasicITSValidation.validateSchemaLocationNotUsed(content);
			res.getMDAValidation().getWarningOrErrorOrNote().add(not1);
			res.getMDAValidation().getWarningOrErrorOrNote().add(not2);
			res.getMDAValidation().getWarningOrErrorOrNote().add(not3);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return res;
	}

	private static DetailedResult errorExtraction(
			POCDMT000040ClinicalDocument ss, Exception e) {
		DetailedResult res = new DetailedResult();
		res.setMDAValidation(new MDAValidation());
		net.ihe.gazelle.validation.Error err = new Error();
		err.setIdentifiant("val");
		err.setTest("val");
		err.setDescription("problem to find file: " + ss + ". error is :" + e.getMessage());
		res.getMDAValidation().getWarningOrErrorOrNote().add(err);
		return res;
	}
	
	private static POCDMT000040ClinicalDocument load(String ss) {
		POCDMT000040ClinicalDocument pr = null;
		try {
			pr = load(new FileInputStream(ss));
		} catch (FileNotFoundException e) {
			System.out.println("problem to find file: " + ss);
		} catch (JAXBException e) {
			System.out.println("problem to parse file: " + ss);
			e.printStackTrace();
		}
		return pr;
	}

	private static POCDMT000040ClinicalDocument load(InputStream is)
			throws JAXBException {
		JAXBContext jc = JAXBContext
				.newInstance(POCDMT000040ClinicalDocument.class);
		Unmarshaller u = jc.createUnmarshaller();
		POCDMT000040ClinicalDocument mimi = (POCDMT000040ClinicalDocument) u
				.unmarshal(is);
		return mimi;
	}

	private static void validateClinicalDocumentType(DetailedResult res,
			POCDMT000040ClinicalDocument pnr,
			List<ConstraintValidatorModule> listConstraintValidatorModule) {
		List<Notification> ln = new ArrayList<Notification>();
		for (ConstraintValidatorModule cvm : listConstraintValidatorModule) {
			POCDMT000040ClinicalDocument.validateByModule(pnr,
					"/ClinicalDocument", cvm, ln);
		}
		if (res.getMDAValidation() == null)
			res.setMDAValidation(new MDAValidation());
		Collections.sort(ln, new Comparator<Notification>() {

			@Override
			public int compare(Notification o1, Notification o2) {
				if (o1 instanceof Error) return -1;
				if (o1 instanceof Warning){
					if (o2 instanceof Error) return 1;
					if (o2 instanceof Note) return -1;
				}
				if (o2 instanceof Error ) return 1;
				return 0;
			}
		});
		for (Notification notification : ln) {
			res.getMDAValidation().getWarningOrErrorOrNote().add(notification);
		}
	}

	public static Notification selectNotification(DetailedResult res,
			String type, String name) {
		Notification not = null;
		if (res != null) {
			if (res.getMDAValidation() != null) {
				if (res.getMDAValidation().getWarningOrErrorOrNote() != null) {
					for (Object obj : res.getMDAValidation()
							.getWarningOrErrorOrNote()) {
						Notification nn = (Notification) obj;
						if (nn.getIdentifiant().equals(name)) {
							if (type != null) {
								if (obj.getClass().getSimpleName()
										.equalsIgnoreCase(type)) {
									not = nn;
								}
							}
						}
					}
				}
			}
		}
		return not;
	}
	
	public static String readDoc(String name) throws IOException {
		BufferedReader scanner = new BufferedReader(new FileReader(name));
		StringBuilder res = new StringBuilder();
		try {
			String line = scanner.readLine();
			while (line != null) {
				res.append(line + "\n");
				line = scanner.readLine();
			}
		} finally {
			scanner.close();
		}
		return res.toString();
	}

}
