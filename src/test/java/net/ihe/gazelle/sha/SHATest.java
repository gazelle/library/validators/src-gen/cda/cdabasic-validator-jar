package net.ihe.gazelle.sha;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;

public class SHATest {
	
	public static void main(String[] args) {
		String ss = "Hola word";
		String calc = calc(new ByteArrayInputStream(ss.getBytes()), "SHA-1");
		System.out.println(calc);
		calc = calc(new ByteArrayInputStream(ss.getBytes()), "SHA-256");
		System.out.println(calc);
	}
	
	public static String calc(InputStream is, String sha) {
	    String output;
	    int read;
	    byte[] buffer = new byte[8192];

	    try {
	        MessageDigest digest = MessageDigest.getInstance(sha);
	        while ((read = is.read(buffer)) > 0) {
	            digest.update(buffer, 0, read);
	        }
	        byte[] hash = digest.digest();
	        BigInteger bigInt = new BigInteger(1, hash);
	        output = bigInt.toString(16);
	        while ( output.length() < 32 ) {
	            output = "0"+output;
	        }
	    } 
	    catch (Exception e) {
	        e.printStackTrace(System.err);
	        return null;
	    }

	    return output;
	}

}
