package net.ihe.gazelle.sha;

import java.net.URI;
import java.net.URISyntaxException;

public class URITest {
	
	public static void main(String[] args) throws URISyntaxException {
		URI uri = new URI("mailto:areval.com");
		System.out.println(uri.getAuthority());
		System.out.println(uri.getHost());
		System.out.println(uri.getScheme());
	}

}
