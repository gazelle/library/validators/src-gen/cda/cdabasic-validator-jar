package net.ihe.gazelle.testnblock;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class ParagraphSpecTest {
	@Test
	// if <caption> present in the <paragraph> element, it shall be done before any other character data, including whitespaces (RMIM-116)
	public void test_rmim116() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ParagraphSpec_rmim116_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdanblock-ParagraphSpec-rmim116");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ParagraphSpec_rmim116_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdanblock-ParagraphSpec-rmim116");
		Assert.assertTrue(nn != null);//
	}


}		
