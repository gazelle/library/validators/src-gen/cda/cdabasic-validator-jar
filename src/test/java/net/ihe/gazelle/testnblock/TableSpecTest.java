package net.ihe.gazelle.testnblock;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class TableSpecTest {
	@Test
	// table.border SHOULD NOT be used (RMIM-112)
	public void test_rmim112_1() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/TableSpec_rmim112_1_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdanblock-TableSpec-rmim112_1");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/TableSpec_rmim112_1_KO.xml");
		nn = CommonUtil.selectNotification(res, "WARNING", "cdanblock-TableSpec-rmim112_1");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// table.cellpading SHOULD NOT be used (RMIM-112)
	public void test_rmim112_2() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/TableSpec_rmim112_2_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdanblock-TableSpec-rmim112_2");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/TableSpec_rmim112_2_KO.xml");
		nn = CommonUtil.selectNotification(res, "WARNING", "cdanblock-TableSpec-rmim112_2");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// table.cellspacing SHOULD NOT be used (RMIM-112)
	public void test_rmim112_3() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/TableSpec_rmim112_3_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdanblock-TableSpec-rmim112_3");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/TableSpec_rmim112_3_KO.xml");
		nn = CommonUtil.selectNotification(res, "WARNING", "cdanblock-TableSpec-rmim112_3");
		Assert.assertTrue(nn != null);//
	}


}		
