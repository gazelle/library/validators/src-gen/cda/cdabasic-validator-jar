package net.ihe.gazelle.testnblock;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class LinkHtmlSpecTest {
	@Test
	// The linkHtml/name element SHOULD NOT be used (RMIM-108)
	public void test_rmim108() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/LinkHtmlSpec_rmim108_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdanblock-LinkHtmlSpec-rmim108");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/LinkHtmlSpec_rmim108_KO.xml");
		nn = CommonUtil.selectNotification(res, "WARNING", "cdanblock-LinkHtmlSpec-rmim108");
		Assert.assertTrue(nn != null);//
	}


}		
