package net.ihe.gazelle.testbasic;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.basic.validation.BasicITSValidation;
import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

public class BasicITSTest {

	// xml:lang SHALL NOT be used in datatypes
	@Test
	public void test_dtits001() {
		try {
			String cda = CommonUtil.readDoc("src/test/resources/samples/BasicITS_dtits001_OK_1.xml");
			Notification nn = BasicITSValidation
					.validateLangNotUsed(cda);
			Assert.assertTrue(nn instanceof Note);
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			String cda = CommonUtil.readDoc("src/test/resources/samples/BasicITS_dtits001_KO_1.xml");
			Notification nn = BasicITSValidation
					.validateLangNotUsed(cda);
			Assert.assertTrue(nn instanceof Error);//
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// xsi:schemaLocation SHALL NOT be used
	@Test
	public void test_dtits002() {
		try {
			String cda = CommonUtil.readDoc("src/test/resources/samples/BasicITS_dtits002_OK_1.xml");
			Notification nn = BasicITSValidation
					.validateSchemaLocationNotUsed(cda);
			Assert.assertTrue(nn instanceof Note);
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			String cda = CommonUtil.readDoc("src/test/resources/samples/BasicITS_dtits002_KO_1.xml");
			Notification nn = BasicITSValidation
					.validateSchemaLocationNotUsed(cda);
			Assert.assertTrue(nn instanceof Error);//
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// xsi:noNamespaceSchemaLocation SHALL NOT be used, for the same reason of
	// the prohibition of xsi:schemaLocation
	@Test
	public void test_dtits003() {
		try {
			String cda = CommonUtil.readDoc("src/test/resources/samples/BasicITS_dtits003_OK_1.xml");
			Notification nn = BasicITSValidation
					.validateNoNamespaceSchemaLocationNotUsed(cda);
			Assert.assertTrue(nn instanceof Note);
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			String cda = CommonUtil.readDoc("src/test/resources/samples/BasicITS_dtits003_KO_1.xml");
			Notification nn = BasicITSValidation
					.validateNoNamespaceSchemaLocationNotUsed(cda);
			Assert.assertTrue(nn instanceof Error);//
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
