package net.ihe.gazelle.testdt;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class MOSpecTest {
	@Test
	// In MO datatype, the currency unit SHALL be from ISO 4217 (CDADT-027)
	public void test_cdadt027() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/MOSpec_cdadt027_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-MOSpec-cdadt027");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdadt-MOSpec-cdadt027");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/MOSpec_cdadt027_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-MOSpec-cdadt027");
		Assert.assertTrue(nn != null);//
	}


}		
