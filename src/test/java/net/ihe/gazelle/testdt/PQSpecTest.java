package net.ihe.gazelle.testdt;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class PQSpecTest {
	@Test
	// In PQ datatype, the translation elements SHALL have distinct values (CDADT-035)
	public void test_cdadt035() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PQSpec_cdadt035_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-PQSpec-cdadt035");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdadt-PQSpec-cdadt035");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PQSpec_cdadt035_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-PQSpec-cdadt035");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// In PQ datatype, the translation elements SHALL not be null if there are translations which are not null (CDADT-036)
	public void test_cdadt036() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PQSpec_cdadt036_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-PQSpec-cdadt036");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PQSpec_cdadt036_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-PQSpec-cdadt036");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PQSpec_cdadt036_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdadt-PQSpec-cdadt036");
		Assert.assertTrue(nn != null);
	}
	
	@Test
	// In PQ datatype, the unit attribute SHALL be from UCUM code system (CDADT-036)
	public void test_cdadt037() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PQSpec_cdadt037_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-PQSpec-cdadt037");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PQSpec_cdadt037_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-PQSpec-cdadt037");
		Assert.assertTrue(nn != null);//
		
	}


}		
