package net.ihe.gazelle.testdt;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class TSSpecTest {
	@Test
	// In TS datatype, value SHALL have a valid timestamp (DTITS-017)
	public void test_dtits017() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/TSSpec_dtits017_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-TSSpec-dtits017");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/TSSpec_dtits017_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-TSSpec-dtits017");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// In TS datatype, value SHALL be present, otherwise nullFlavor shall be provided, not both (DTITS-018)
	public void test_dtits018() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/TSSpec_dtits018_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-TSSpec-dtits018");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdadt-TSSpec-dtits018");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/TSSpec_dtits018_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-TSSpec-dtits018");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/TSSpec_dtits018_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdadt-TSSpec-dtits018");
		Assert.assertTrue(nn != null);
	}


}		
