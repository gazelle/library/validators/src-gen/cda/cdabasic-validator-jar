package net.ihe.gazelle.testdt;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class CDSpecTest {
	@Test
	// codeSystem attribute SHALL be provided when codeSystemName is provided (CDADT-004)
	public void test_cdadt004() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CDSpec_cdadt004_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-CDSpec-cdadt004");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdadt-CDSpec-cdadt004");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CDSpec_cdadt004_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-CDSpec-cdadt004");
		Assert.assertTrue(nn != null);//
	}
	
	@Test
	// In CD datatype, codeSystem attribute SHALL be provided when codeSystemVersion is provided (CDADT-005)
	public void test_cdadt005() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CDSpec_cdadt005_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-CDSpec-cdadt005");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdadt-CDSpec-cdadt005");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CDSpec_cdadt005_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-CDSpec-cdadt005");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// code attribute SHALL be provided when displayName is provided (CDADT-006)
	public void test_cdadt006() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CDSpec_cdadt006_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-CDSpec-cdadt006");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdadt-CDSpec-cdadt006");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CDSpec_cdadt006_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-CDSpec-cdadt006");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// originalText attribute SHALL NOT have multimedia value, only reference or plain text form are allowed (CDADT-007)
	public void test_cdadt007() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CDSpec_cdadt007_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-CDSpec-cdadt007");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdadt-CDSpec-cdadt007");
		Assert.assertFalse(nn != null);
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CDSpec_cdadt007_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdadt-CDSpec-cdadt007");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdadt-CDSpec-cdadt007");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CDSpec_cdadt007_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-CDSpec-cdadt007");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// if the nullFlavor is not defined, the code SHALL be provided (CDADT-008)
	public void test_cdadt008() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CDSpec_cdadt008_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-CDSpec-cdadt008");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CDSpec_cdadt008_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-CDSpec-cdadt008");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CDSpec_cdadt008_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdadt-CDSpec-cdadt008");
		Assert.assertTrue(nn != null);
	}

	@Test
	// When UUID is used in codeSystem, the hexadecimal digits A-F SHALL be in upper case (CDADT-011)
	public void test_cdadt011() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CDSpec_cdadt011_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-CDSpec-cdadt011");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdadt-CDSpec-cdadt011");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CDSpec_cdadt011_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-CDSpec-cdadt011");
		Assert.assertTrue(nn != null);//
	}
	
	@Test
	// Validation of instance by a constraint : cdadt020
	// IN CD datatype, when CD/@nullFlavor= OTH , the CD/@codeSystem SHALL be present (CDADT-020)
	public void test_cdadt020() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CDSpec_cdadt020_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-CDSpec-cdadt020");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdadt-CDSpec-cdadt020");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CDSpec_cdadt020_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-CDSpec-cdadt020");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// In CD datatype, the translation elements SHALL be disctinct (CDADT-029)
	public void test_cdadt029() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CDSpec_cdadt029_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-CDSpec-cdadt029");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdadt-CDSpec-cdadt029");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CDSpec_cdadt029_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-CDSpec-cdadt029");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// In CD datatype, the translation elements SHALL not be null if there are translations which are not null (CDADT-030)
	public void test_cdadt030() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CDSpec_cdadt030_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-CDSpec-cdadt030");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdadt-CDSpec-cdadt030");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CDSpec_cdadt030_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-CDSpec-cdadt030");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CDSpec_cdadt030_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdadt-CDSpec-cdadt030");
		Assert.assertTrue(nn != null);
	}


}		
