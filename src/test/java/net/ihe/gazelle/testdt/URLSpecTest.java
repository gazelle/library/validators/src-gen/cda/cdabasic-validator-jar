package net.ihe.gazelle.testdt;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class URLSpecTest {
	@Test
	// value attribute SHALL have this form : ^(\\w+:.+|#.+)$ (CDADT-013)
	public void test_cdadt013() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/URLSpec_cdadt013_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-URLSpec-cdadt013");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/URLSpec_cdadt013_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-URLSpec-cdadt013");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// When the URL datatype is tel or fax, the structure of the litteral is specified according to the specification 
	//in the datatypes specification, paragraph 2.18.3, and where the value is defined according to this regex
	//(tel|fax):\\+?([0-9]|\\(|\\)|\\.|\\-)*$ (CDADT-014) 
	public void test_cdadt014() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/URLSpec_cdadt014_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-URLSpec-cdadt014");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdadt-URLSpec-cdadt014");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/URLSpec_cdadt014_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-URLSpec-cdadt014");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// In URL datatype, the mail SHALL be as defined in RFC2368 (DTITS-009)
	public void test_dtits009() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/URLSpec_dtits009_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-URLSpec-dtits009");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/URLSpec_dtits009_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-URLSpec-dtits009");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// In URL datatype, the http SHALL be as defined in RFC2396 (DTITS-010)
	public void test_dtits010() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/URLSpec_dtits010_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-URLSpec-dtits010");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/URLSpec_dtits010_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-URLSpec-dtits010");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// In URL datatype, the ftp SHALL be as defined in RFC1738 (DTITS-011)
	public void test_dtits011() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/URLSpec_dtits011_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-URLSpec-dtits011");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/URLSpec_dtits011_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-URLSpec-dtits011");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// In URL datatype, the file SHALL be as defined in RFC1738 (DTITS-012)
	public void test_dtits012() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/URLSpec_dtits012_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-URLSpec-dtits012");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/URLSpec_dtits012_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-URLSpec-dtits012");
		Assert.assertTrue(nn != null);//
	}

//	@Test
//	// In URL datatype, the nfs SHALL be as defined in RFC2224 (DTITS-013)
//	public void test_dtits013() {
//		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/URLSpec_dtits013_OK.xml");
//		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-URLSpec-dtits013");
//		Assert.assertTrue(nn != null);
//
//		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/URLSpec_dtits013_KO.xml");
//		nn = CommonUtil.selectNotification(res, "error", "cdadt-URLSpec-dtits013");
//		Assert.assertTrue(nn != null);//
//	}
//
//	@Test
//	// In URL datatype, the telnet SHALL be as defined in RFC1738 (DTITS-014)
//	public void test_dtits014() {
//		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/URLSpec_dtits014_OK.xml");
//		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-URLSpec-dtits014");
//		Assert.assertTrue(nn != null);
//
//		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/URLSpec_dtits014_KO.xml");
//		nn = CommonUtil.selectNotification(res, "error", "cdadt-URLSpec-dtits014");
//		Assert.assertTrue(nn != null);//
//	}
//
//	@Test
//	// In URL datatype, the modem SHALL be as defined in RFC3966 and RFC2806 (DTITS-015)
//	public void test_dtits015() {
//		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/URLSpec_dtits015_OK.xml");
//		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-URLSpec-dtits015");
//		Assert.assertTrue(nn != null);
//
//		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/URLSpec_dtits015_KO.xml");
//		nn = CommonUtil.selectNotification(res, "error", "cdadt-URLSpec-dtits015");
//		Assert.assertTrue(nn != null);//
//	}


}		
