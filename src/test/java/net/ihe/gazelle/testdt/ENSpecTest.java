package net.ihe.gazelle.testdt;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class ENSpecTest {
	@Test
	// In EN datatype, the use attribute SHALL have distinct values (CDADT-034)
	public void test_cdadt034() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ENSpec_cdadt034_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-ENSpec-cdadt034");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ENSpec_cdadt034_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-ENSpec-cdadt034");
		Assert.assertTrue(nn != null);//
	}


}		
