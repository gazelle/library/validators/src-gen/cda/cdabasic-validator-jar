package net.ihe.gazelle.testdt;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class EDSpecTest {
	@Test
	// mediaType of the ED datatype SHOULD be from known mediaTypes (CDADT-002)
	public void test_cdadt002() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EDSpec_cdadt002_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-EDSpec-cdadt002");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdadt-EDSpec-cdadt002");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EDSpec_cdadt002_KO.xml");
		nn = CommonUtil.selectNotification(res, "WARNING", "cdadt-EDSpec-cdadt002");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// if integrityCheck attribute is provided, it SHALL have a valid value according to its integrityCheckAlgorithm (CDADT-003)
	public void test_cdadt003() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EDSpec_cdadt003_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-EDSpec-cdadt003");
		Assert.assertTrue(nn != null);
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EDSpec_cdadt003_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdadt-EDSpec-cdadt003");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdadt-EDSpec-cdadt003");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EDSpec_cdadt003_KO_1.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-EDSpec-cdadt003");
		Assert.assertTrue(nn != null);
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EDSpec_cdadt003_KO_2.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-EDSpec-cdadt003");
		Assert.assertTrue(nn != null);
	}

	@Test
	// In ED datatype, if it is not null, it SHALL have a binary string or reference element (CDADT-023)
	public void test_cdadt023() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EDSpec_cdadt023_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-EDSpec-cdadt023");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdadt-EDSpec-cdadt023");
		Assert.assertFalse(nn != null);
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EDSpec_cdadt023_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdadt-EDSpec-cdadt023");
		Assert.assertTrue(nn != null);
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EDSpec_cdadt023_OK_3.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdadt-EDSpec-cdadt023");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EDSpec_cdadt023_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-EDSpec-cdadt023");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// In ED datatype, the language SHALL be as defined in RFC3066 (DTITS-006)
	public void test_dtits006() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EDSpec_dtits006_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-EDSpec-dtits006");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdadt-EDSpec-dtits006");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EDSpec_dtits006_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-EDSpec-dtits006");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// In ED datatype, when it has reference form, there shall not be white spaces before and after the reference,
	//the binary data shall not be present or shall represent the same information in the reference (DTITS-007)
	public void test_dtits007() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EDSpec_dtits007_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-EDSpec-dtits007");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "warning", "cdadt-EDSpec-dtits007");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EDSpec_dtits007_KO.xml");
		nn = CommonUtil.selectNotification(res, "warning", "cdadt-EDSpec-dtits007");
		Assert.assertTrue(nn != null);//
	}


}		
