package net.ihe.gazelle.testdt;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class IVLREALSpecTest {
	@Test
	// In IVL<REAL> datatypes, the low value SHALL be lessOrEqual the high value (CDADT-017)
	public void test_cdadt017() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IVLREALSpec_cdadt017_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-IVLREALSpec-cdadt017");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdadt-IVLREALSpec-cdadt017");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IVLREALSpec_cdadt017_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-IVLREALSpec-cdadt017");
		Assert.assertTrue(nn != null);//
	}


}		
