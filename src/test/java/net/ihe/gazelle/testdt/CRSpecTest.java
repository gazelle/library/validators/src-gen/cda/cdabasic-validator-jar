package net.ihe.gazelle.testdt;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class CRSpecTest {
	@Test
	// In CR datatype, if it is not null, value element SHALL not be null (CDADT-025)
	public void test_cdadt025() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CRSpec_cdadt025_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-CRSpec-cdadt025");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdadt-CRSpec-cdadt025");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CRSpec_cdadt025_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-CRSpec-cdadt025");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CRSpec_cdadt025_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdadt-CRSpec-cdadt025");
		Assert.assertTrue(nn != null);
	}


}		
