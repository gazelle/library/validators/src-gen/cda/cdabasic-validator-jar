package net.ihe.gazelle.testdt;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class IVLTSSpecTest {
	@Test
	// In IVL<TS> datatypes, the low value SHALL be lessOrEqual the high value (CDADT-017)
	public void test_cdadt017() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IVLTSSpec_cdadt017_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-IVLTSSpec-cdadt017");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IVLTSSpec_cdadt017_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-IVLTSSpec-cdadt017");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// In IVL<TS>, the width/@unit SHALL be from mesure of time values (CDADT-019)
	public void test_cdadt019() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IVLTSSpec_cdadt019_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-IVLTSSpec-cdadt019");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdadt-IVLTSSpec-cdadt019");
		Assert.assertFalse(nn != null);
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IVLTSSpec_cdadt019_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-IVLTSSpec-cdadt019");
		Assert.assertTrue(nn != null);//
	}


}		
