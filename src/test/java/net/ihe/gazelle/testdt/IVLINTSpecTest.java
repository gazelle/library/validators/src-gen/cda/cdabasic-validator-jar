package net.ihe.gazelle.testdt;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class IVLINTSpecTest {
	@Test
	// In IVL<INT> datatypes, the low value SHALL be lessOrEqual the high value (CDADT-017)
	public void test_cdadt017() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IVLINTSpec_cdadt017_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-IVLINTSpec-cdadt017");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IVLINTSpec_cdadt017_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-IVLINTSpec-cdadt017");
		Assert.assertTrue(nn != null);//
	}


}		
