package net.ihe.gazelle.testdt;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class ANYSpecTest {
	@Test
	// NP value of nullFlavor is not used for CDA documents (CDADT-001)
	public void test_cdadt001() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ANYSpec_cdadt001_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-ANYSpec-cdadt001");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdadt-ANYSpec-cdadt001");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ANYSpec_cdadt001_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-ANYSpec-cdadt001");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// The use of nullFlavor PINF and NINF SHALL be for datatypes QTY (CDADT-022)
	public void test_cdadt022() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ANYSpec_cdadt022_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-ANYSpec-cdadt022");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdadt-ANYSpec-cdadt022");
		Assert.assertFalse(nn != null);
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ANYSpec_cdadt022_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdadt-ANYSpec-cdadt022");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdadt-ANYSpec-cdadt022");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ANYSpec_cdadt022_KO_1.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-ANYSpec-cdadt022");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ANYSpec_cdadt022_KO_2.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-ANYSpec-cdadt022");
		Assert.assertTrue(nn != null);//
	}


}		
