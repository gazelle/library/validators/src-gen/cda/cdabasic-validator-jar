package net.ihe.gazelle.testdt;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class ADSpecTest {
	@Test
	// In AD datatype, the use attribute SHALL have disctinct values (CDADT-032)
	public void test_cdadt032() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ADSpec_cdadt032_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-ADSpec-cdadt032");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdadt-ADSpec-cdadt032");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ADSpec_cdadt032_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-ADSpec-cdadt032");
		Assert.assertTrue(nn != null);//
	}


}		
