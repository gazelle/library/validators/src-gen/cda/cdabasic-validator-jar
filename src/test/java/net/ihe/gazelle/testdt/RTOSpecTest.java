package net.ihe.gazelle.testdt;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class RTOSpecTest {
	@Test
	// In RTO datatype, the denominator SHALL NOT be zero (CDADT-016)
	public void test_cdadt016() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RTOSpec_cdadt016_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-RTOSpec-cdadt016");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdadt-RTOSpec-cdadt016");
		Assert.assertFalse(nn != null);
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RTOSpec_cdadt016_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-RTOSpec-cdadt016");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RTOSpec_cdadt016_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdadt-RTOSpec-cdadt016");
		Assert.assertTrue(nn != null);
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/RTOSpec_cdadt016_OK_3.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdadt-RTOSpec-cdadt016");
		Assert.assertTrue(nn != null);
	}


}		
