package net.ihe.gazelle.testdt;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class SCSpecTest {
	@Test
	// code SHALL NOT be given without the text (CDADT-010)
	public void test_cdadt010() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SCSpec_cdadt010_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-SCSpec-cdadt010");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/SCSpec_cdadt010_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-SCSpec-cdadt010");
		Assert.assertTrue(nn != null);//
	}


}		
