package net.ihe.gazelle.testdt;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class EIVLEventSpecTest {
	@Test
	// if the nullFlavor is not defined, the code SHALL be provided (CDADT-008)
	public void test_cdadt008() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EIVLEventSpec_cdadt008_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-EIVLEventSpec-cdadt008");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdadt-EIVLEventSpec-cdadt008");
		Assert.assertFalse(nn != null);//

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EIVLEventSpec_cdadt008_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-EIVLEventSpec-cdadt008");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/EIVLEventSpec_cdadt008_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdadt-EIVLEventSpec-cdadt008");
		Assert.assertTrue(nn != null);
	}


}		
