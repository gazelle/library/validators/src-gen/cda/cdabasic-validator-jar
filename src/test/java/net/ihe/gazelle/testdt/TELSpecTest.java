package net.ihe.gazelle.testdt;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class TELSpecTest {
	@Test
	// In TEL datatype, the use attribute SHALL have disctinct values (CDADT-031)
	public void test_cdadt031() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/TELSpec_cdadt031_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-TELSpec-cdadt031");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/TELSpec_cdadt031_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-TELSpec-cdadt031");
		Assert.assertTrue(nn != null);//
	}


}		
