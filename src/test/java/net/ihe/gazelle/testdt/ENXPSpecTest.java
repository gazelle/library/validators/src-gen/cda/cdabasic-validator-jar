package net.ihe.gazelle.testdt;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class ENXPSpecTest {
	@Test
	// In ENXP datatype, the qualifier attribute SHALL have distinct values (CDADT-033)
	public void test_cdadt033() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ENXPSpec_cdadt033_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-ENXPSpec-cdadt033");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/ENXPSpec_cdadt033_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-ENXPSpec-cdadt033");
		Assert.assertTrue(nn != null);//
	}


}		
