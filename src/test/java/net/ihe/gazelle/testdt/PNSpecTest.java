package net.ihe.gazelle.testdt;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class PNSpecTest {
	@Test
	// the qualifier ofelements of PN datatype SHALL NOT include LS as value (CDADT-015)
	public void test_cdadt015() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PNSpec_cdadt015_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-PNSpec-cdadt015");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PNSpec_cdadt015_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-PNSpec-cdadt015");
		Assert.assertTrue(nn != null);//
	}


}		
