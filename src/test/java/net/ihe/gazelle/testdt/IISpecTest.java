package net.ihe.gazelle.testdt;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class IISpecTest {
	@Test
	// When root is UUID, the hexadecimal digits A-F SHALL be in upper case (CDADT-011)
	public void test_cdadt011() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IISpec_cdadt011_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-IISpec-cdadt011");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IISpec_cdadt011_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-IISpec-cdadt011");
		Assert.assertTrue(nn != null);//
	}

	@Test
	// when nullFlavor is not defined, the root SHALL be provided (CDADT-012)
	public void test_cdadt012() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IISpec_cdadt012_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-IISpec-cdadt012");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IISpec_cdadt012_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-IISpec-cdadt012");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/IISpec_cdadt012_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdadt-IISpec-cdadt012");
		Assert.assertTrue(nn != null);
	}


}		
