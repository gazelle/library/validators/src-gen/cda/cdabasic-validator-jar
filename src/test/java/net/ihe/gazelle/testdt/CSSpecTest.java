package net.ihe.gazelle.testdt;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class CSSpecTest {
	@Test
	// if the nullFlavor is not defined, the code SHALL be provided (CDADT-009)
	public void test_cdadt009() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CSSpec_cdadt009_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-CSSpec-cdadt009");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdadt-CSSpec-cdadt009");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CSSpec_cdadt009_KO.xml");
		nn = CommonUtil.selectNotification(res, "ERROR", "cdadt-CSSpec-cdadt009");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/CSSpec_cdadt009_OK_2.xml");
		nn = CommonUtil.selectNotification(res, "note", "cdadt-CSSpec-cdadt009");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdadt-CSSpec-cdadt009");
		Assert.assertFalse(nn != null);
	}


}		
