package net.ihe.gazelle.testdt;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class TNSpecTest {
	@Test
	// In TN datatype, if it is not null, the string value SHALL be provided and not empty, even after formating it (CDADT-026)
	public void test_cdadt026() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/TNSpec_cdadt026_OK.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-TNSpec-cdadt026");
		Assert.assertTrue(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/TNSpec_cdadt026_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-TNSpec-cdadt026");
		Assert.assertTrue(nn != null);//
	}


}		
