package net.ihe.gazelle.testdt;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.util.CommonUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Notification;
public class PIVLPPDTSSpecTest {
	@Test
	// In PIVL datatype, if it is not null, it SHALL have a period (CDADT-028)
	public void test_cdadt028() {
		DetailedResult res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PIVLPPDTSSpec_cdadt028_OK_1.xml");
		Notification nn = CommonUtil.selectNotification(res, "note", "cdadt-PIVLPPDTSSpec-cdadt028");
		Assert.assertTrue(nn != null);
		nn = CommonUtil.selectNotification(res, "error", "cdadt-PIVLPPDTSSpec-cdadt028");
		Assert.assertFalse(nn != null);

		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PIVLPPDTSSpec_cdadt028_KO.xml");
		nn = CommonUtil.selectNotification(res, "error", "cdadt-PIVLPPDTSSpec-cdadt028");
		Assert.assertTrue(nn != null);//
		
		res = CommonUtil.getDetailedResultFromFile("src/test/resources/samples/PIVLPPDTSSpec_cdadt028_OK_2.xml");
		nn= CommonUtil.selectNotification(res, "note", "cdadt-PIVLPPDTSSpec-cdadt028");
		Assert.assertTrue(nn != null);
	}


}		
