package net.ihe.gazelle.basic.validation;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.MessageDigest;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.voc.CompressionAlgorithm;

public final class SHAValidation {
	
	private SHAValidation() {
		// private constructor
	}
	
	
	private static Logger log = LoggerFactory.getLogger(SHAValidation.class);
	
	public static InputStream decodeB64(String b64, CompressionAlgorithm ca){
		if (b64 == null || ca != null){
			return null;
		}
		else{
			return new ByteArrayInputStream(Base64.decodeBase64(b64.getBytes(Charset.forName("UTF-8"))));
		}
	}
	
	public static String calc(String str, String sha) {
		if (str != null) {
			ByteArrayInputStream bais = new ByteArrayInputStream(str.getBytes(Charset.forName("UTF-8")));
			return calc(bais, sha);
		}
		return null;
	}
	
	public static String calc(InputStream is, String sha) {
	    String output;
	    int read;
	    byte[] buffer = new byte[8192];

	    try {
	        MessageDigest digest = MessageDigest.getInstance(sha);
	        while ((read = is.read(buffer)) > 0) {
	            digest.update(buffer, 0, read);
	        }
	        byte[] hash = digest.digest();
	        BigInteger bigInt = new BigInteger(1, hash);
	        output = bigInt.toString(16);
	        while ( output.length() < 32 ) {
	            output = "0"+output;
	        }
	    } 
	    catch (Exception e) {
	    	log.error("Problem to calculate the sha", e);
	        return null;
	    }

	    return output;
	}

}
