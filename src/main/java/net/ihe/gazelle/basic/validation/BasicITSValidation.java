package net.ihe.gazelle.basic.validation;

import net.ihe.gazelle.gen.common.DatatypesNamespaceContext;
import net.ihe.gazelle.gen.common.XpathUtils;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

public final class BasicITSValidation {
	
	private static final String DTITS = "DTITS";
	private static final String DASH_CLINICAL_DOCUMENT = "/ClinicalDocument";
	private static final String CLINICAL_DOCUMENT = "ClinicalDocument";

	private BasicITSValidation() {
		// private constructor
	}
	private static class DataTypeExtended extends DatatypesNamespaceContext{
		@Override
		public String getNamespaceURI(String prefix) {
			if ("cda".equals(prefix)) {
				return "urn:hl7-org:v3";
			} else if ("xsi".equals(prefix)) {
				return "http://www.w3.org/2001/XMLSchema-instance";
			}
			else if ("xml".equals(prefix)) {
				return "http://www.w3.org/XML/1998/namespace";
			}
			return null;
		}

		@Override
		public String getPrefix(String namespaceURI) {
			if ("urn:hl7-org:v3".equals(namespaceURI)) {
				return "cda";
			} else if ("http://www.w3.org/2001/XMLSchema-instance".equals(namespaceURI)) {
				return "xsi";
			}
			else if ("http://www.w3.org/XML/1998/namespace".equals(namespaceURI)) {
				return "xml";
			}
			return null;
		}
	}

	public static Notification validateLangNotUsed(String cda){
		String description = "xml:lang SHALL NOT be used (DTITS-001)";
		Notification not = null;
		Boolean res = false;
		try {
			res = XpathUtils.evaluateByString(cda, CLINICAL_DOCUMENT, "//@xml:lang", new DataTypeExtended());
		} catch (Exception e) {
			res = false;
		}
		if (res){
			not = new Error();
		}
		else{
			not = new Note();
		}
		not.setDescription(description);
		not.setLocation(DASH_CLINICAL_DOCUMENT);
		not.setTest("dtits001");
		not.setIdentifiant("dtits001");
		Assertion as = new Assertion();
		as.setAssertionId("DTITS-001");
		as.setIdScheme(DTITS);
		not.getAssertions().add(as);
		return not;
	}
	
	public static Notification validateSchemaLocationNotUsed(String cda){
		String description = "xsi:schemaLocation SHALL NOT be used (DTITS-002)";
		Notification not = null;
		Boolean res = false;
		try {
			res = XpathUtils.evaluateByString(cda, CLINICAL_DOCUMENT, "//@xsi:schemaLocation", new DataTypeExtended());
		} catch (Exception e) {
			res = false;
		}
		if (res){
			not = new Error();
		}
		else{
			not = new Note();
		}
		not.setDescription(description);
		not.setLocation(DASH_CLINICAL_DOCUMENT);
		not.setTest("dtits002");
		not.setIdentifiant("dtits002");
		Assertion as = new Assertion();
		as.setAssertionId("DTITS-002");
		as.setIdScheme(DTITS);
		not.getAssertions().add(as);
		return not;
	}

	public static Notification validateNoNamespaceSchemaLocationNotUsed(String cda){
		String description = "xsi:noNamespaceSchemaLocation SHALL NOT be used, for the same reason of the prohibition of xsi:schemaLocation (DTITS-003)";
		Notification not = null;
		Boolean res = false;
		try {
			res = XpathUtils.evaluateByString(cda, CLINICAL_DOCUMENT, "//@xsi:noNamespaceSchemaLocation", new DataTypeExtended());
		} catch (Exception e) {
			res = false;
		}
		if (res){
			not = new Error();
		}
		else{
			not = new Note();
		}
		not.setDescription(description);
		not.setLocation(DASH_CLINICAL_DOCUMENT);
		not.setTest("dtits003");
		not.setIdentifiant("dtits003");
		Assertion as = new Assertion();
		as.setAssertionId("DTITS-003");
		as.setIdScheme(DTITS);
		not.getAssertions().add(as);
		return not;
	}

	
}
