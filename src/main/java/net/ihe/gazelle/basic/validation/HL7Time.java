package net.ihe.gazelle.basic.validation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class HL7Time {
	
	private HL7Time() {
		// private constructor
	}
	private static final String[] formatStrings = { "yyyyMMddHHmmss.SSSZ", "yyyyMMddHHmmss.SSS", "yyyyMMddHHmmss",
			"yyyyMMddHHmm", "yyyyMMddHH", "yyyyMMdd", "yyyyMM", "yyyy" };

	private static final List<SimpleDateFormat> simpleDateFormats = new ArrayList<SimpleDateFormat>();

	static {
		for (String st : formatStrings) {
			simpleDateFormats.add(new SimpleDateFormat(st));
		}
	}

	public static Date parse(String dateString) {
		if (dateString != null) {
			for (SimpleDateFormat simpleDateFormat : simpleDateFormats) {
				try {
					return simpleDateFormat.parse(dateString);
				} catch (ParseException e) {
				}
			}
		}
		return null;
	}

}