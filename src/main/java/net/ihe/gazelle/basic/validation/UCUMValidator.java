package net.ihe.gazelle.basic.validation;

import org.fhir.ucum.UcumEssenceService;
import org.fhir.ucum.UcumException;
import org.fhir.ucum.UcumService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pixelmed.ucum.UCUMAnalyzer;
import com.pixelmed.ucum.UCUMCanonicalStructure;
import com.pixelmed.ucum.UCUMConversionException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public final class UCUMValidator {
	
	
	private static final String ERROR_TO_PARSE_UNIT = "Error to parse unit";
	private static Logger log = LoggerFactory.getLogger(UCUMValidator.class);
	
	private UCUMValidator() {}
	
	public static boolean validateUCUM(String unit) {
		try {
			URL ucumUrl = UCUMValidator.class.getClassLoader().getResource("ucum-essence.xml");
			if(ucumUrl == null){
				return false;
			}
			InputStream ucumStream = ucumUrl.openStream();
			UcumService ucumService = new UcumEssenceService(ucumStream);
			String errors = ucumService.validate(unit);
			if (errors == null) {
				return true;
			}
		} catch (UcumException | IOException e) {
			log.info(ERROR_TO_PARSE_UNIT, e);
		}
		return false;
	}
	
	public static boolean unitsAreComparable(String unit1, String unit2) {
		if (unit1 != null && unit2 != null) {
			try {
				UCUMCanonicalStructure ac1 = UCUMAnalyzer.getCanonicalForm(unit1, 0);
				UCUMCanonicalStructure ac2 = UCUMAnalyzer.getCanonicalForm(unit2, 0);
				return ac1.equalsApartFromConstant(ac2);
			} catch (UCUMConversionException e) {
				log.info(ERROR_TO_PARSE_UNIT, e);
			}	
		}
		return false;
	}
	
	public static Double convertToUnit(Double value, String unit1, String unit2) {
		if (value != null && unit1 != null && unit2 != null) {
			try {
				double converter = UCUMAnalyzer.getConversionFactor(unit2, unit1, 0);
				if (converter > 0) {
					return value*converter;
				}
					
			} catch (UCUMConversionException e) {
				log.info(ERROR_TO_PARSE_UNIT, e);
			}
		}
		return  null;
	}
	
	/**
	 * Compare value1,unit1 with value2,unit2
	 * <pre>value1 and value2 should be convertible to double</pre>
	 * <pre>unit1 and unit2 should be comparable</pre>
	 * @param value1
	 * @param unit1
	 * @param value2
	 * @param unit2
	 * @return
	 */
	public static Boolean value1IsLowerOrEqualThanValue2(String value1, String unit1, String value2, String unit2) {
		if (value1 != null  && value2 != null) {
			Double val1 = null;
			Double val2 = null;
			try {
				val1 = Double.parseDouble(value1);
				val2 = Double.parseDouble(value2);
			}
			catch(Exception e) {
				log.info("Not able to parse double values", e);
				return true;
			}
			if (unit1 ==null && unit2 == null) {
				return val1 <= val2;
			}
			else if (unit1 == null || unit2 == null) {
				return false;
			}
			else {
				Double dd = convertToUnit(val1, unit1, unit2);
				return dd != null?dd<=val2:false;
			}
		}
		return true;
	}
	

}
