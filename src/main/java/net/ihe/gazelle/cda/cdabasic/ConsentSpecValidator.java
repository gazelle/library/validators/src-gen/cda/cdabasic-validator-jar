package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.datatypes.II;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;	



 /**
  * class :        ConsentSpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040Consent
  * 
  */
public final class ConsentSpecValidator{


    private ConsentSpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim059_1
	* Consent SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-059, RIM-001)
	*
	*/
	private static boolean _validateConsentSpec_Rmim059_1(net.ihe.gazelle.cda.POCDMT000040Consent aClass){
		java.util.ArrayList<II> result1;
		result1 = new java.util.ArrayList<II>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getId()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim059_2
	* The ids elements of Consent SHALL be distinct (RMIM-059, RIM-002)
	*
	*/
	private static boolean _validateConsentSpec_Rmim059_2(net.ihe.gazelle.cda.POCDMT000040Consent aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    java.util.ArrayList<II> result2;
			    result2 = new java.util.ArrayList<II>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (II anElement2 : aClass.getId()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim059_3
	* The statusCode if presents SHALL be from the valueSet ActStatus (RMIM-059, RIM-003)
	*
	*/
	private static boolean _validateConsentSpec_Rmim059_3(net.ihe.gazelle.cda.POCDMT000040Consent aClass){
		return (((aClass.getStatusCode() == null) || !(aClass.getStatusCode().getNullFlavor() == null)) || aClass.getStatusCode().matchesCodeToValueSet("1.3.6.1.4.1.12559.11.18.2", aClass.getStatusCode().getCode()));
		
	}

	/**
	* Validation of instance by a constraint : rmim060
	* consent.statusCode SHALL have the value 'completed' (or nothing) (RMIM-060)
	*
	*/
	private static boolean _validateConsentSpec_Rmim060(net.ihe.gazelle.cda.POCDMT000040Consent aClass){
		return (((aClass.getStatusCode() == null) || (((String) aClass.getStatusCode().getCode()) == null)) || aClass.getStatusCode().getCode().equals("completed"));
		
	}

	/**
	* Validation of class-constraint : ConsentSpec
    * Verify if an element of type ConsentSpec can be validated by ConsentSpec
	*
	*/
	public static boolean _isConsentSpec(net.ihe.gazelle.cda.POCDMT000040Consent aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   ConsentSpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040Consent
     * 
     */
    public static void _validateConsentSpec(net.ihe.gazelle.cda.POCDMT000040Consent aClass, String location, List<Notification> diagnostic) {
		if (_isConsentSpec(aClass)){
			executeCons_ConsentSpec_Rmim059_1(aClass, location, diagnostic);
			executeCons_ConsentSpec_Rmim059_2(aClass, location, diagnostic);
			executeCons_ConsentSpec_Rmim059_3(aClass, location, diagnostic);
			executeCons_ConsentSpec_Rmim060(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ConsentSpec_Rmim059_1(net.ihe.gazelle.cda.POCDMT000040Consent aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateConsentSpec_Rmim059_1(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim059_1");
		notif.setDescription("Consent SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-059, RIM-001)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ConsentSpec-rmim059_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-059;RIM-001"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_ConsentSpec_Rmim059_2(net.ihe.gazelle.cda.POCDMT000040Consent aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateConsentSpec_Rmim059_2(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim059_2");
		notif.setDescription("The ids elements of Consent SHALL be distinct (RMIM-059, RIM-002)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ConsentSpec-rmim059_2");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-059;RIM-002"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_ConsentSpec_Rmim059_3(net.ihe.gazelle.cda.POCDMT000040Consent aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateConsentSpec_Rmim059_3(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim059_3");
		notif.setDescription("The statusCode if presents SHALL be from the valueSet ActStatus (RMIM-059, RIM-003)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ConsentSpec-rmim059_3");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-059;RIM-003"));
		
		notif.setType("Vocabulary");
		diagnostic.add(notif);
	}

	private static void executeCons_ConsentSpec_Rmim060(net.ihe.gazelle.cda.POCDMT000040Consent aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateConsentSpec_Rmim060(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim060");
		notif.setDescription("consent.statusCode SHALL have the value 'completed' (or nothing) (RMIM-060)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ConsentSpec-rmim060");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-060"));
		
		notif.setType("Fixed value");
		diagnostic.add(notif);
	}

}
