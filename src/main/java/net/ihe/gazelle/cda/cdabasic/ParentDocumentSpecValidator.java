package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.datatypes.II;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;	



 /**
  * class :        ParentDocumentSpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040ParentDocument
  * 
  */
public final class ParentDocumentSpecValidator{


    private ParentDocumentSpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim053_1
	* ParentDocument SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-053, RIM-001)
	*
	*/
	private static boolean _validateParentDocumentSpec_Rmim053_1(net.ihe.gazelle.cda.POCDMT000040ParentDocument aClass){
		java.util.ArrayList<II> result1;
		result1 = new java.util.ArrayList<II>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getId()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim053_2
	* The ids elements of ParentDocument SHALL be distinct (RMIM-053, RIM-002)
	*
	*/
	private static boolean _validateParentDocumentSpec_Rmim053_2(net.ihe.gazelle.cda.POCDMT000040ParentDocument aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    java.util.ArrayList<II> result2;
			    result2 = new java.util.ArrayList<II>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (II anElement2 : aClass.getId()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim054
	* ParentDocument/text.BIN element SHALL NOT be used (RMIM-054)
	*
	*/
	private static boolean _validateParentDocumentSpec_Rmim054(net.ihe.gazelle.cda.POCDMT000040ParentDocument aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getText().getListStringValues()) {
			    if (!anElement1.equals("")) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return (((aClass.getText() == null) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getText().getListStringValues())).equals(Integer.valueOf(0))) || result1);
		
		
	}

	/**
	* Validation of class-constraint : ParentDocumentSpec
    * Verify if an element of type ParentDocumentSpec can be validated by ParentDocumentSpec
	*
	*/
	public static boolean _isParentDocumentSpec(net.ihe.gazelle.cda.POCDMT000040ParentDocument aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   ParentDocumentSpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040ParentDocument
     * 
     */
    public static void _validateParentDocumentSpec(net.ihe.gazelle.cda.POCDMT000040ParentDocument aClass, String location, List<Notification> diagnostic) {
		if (_isParentDocumentSpec(aClass)){
			executeCons_ParentDocumentSpec_Rmim053_1(aClass, location, diagnostic);
			executeCons_ParentDocumentSpec_Rmim053_2(aClass, location, diagnostic);
			executeCons_ParentDocumentSpec_Rmim054(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ParentDocumentSpec_Rmim053_1(net.ihe.gazelle.cda.POCDMT000040ParentDocument aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateParentDocumentSpec_Rmim053_1(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim053_1");
		notif.setDescription("ParentDocument SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-053, RIM-001)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ParentDocumentSpec-rmim053_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-053;RIM-001"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_ParentDocumentSpec_Rmim053_2(net.ihe.gazelle.cda.POCDMT000040ParentDocument aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateParentDocumentSpec_Rmim053_2(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim053_2");
		notif.setDescription("The ids elements of ParentDocument SHALL be distinct (RMIM-053, RIM-002)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ParentDocumentSpec-rmim053_2");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-053;RIM-002"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_ParentDocumentSpec_Rmim054(net.ihe.gazelle.cda.POCDMT000040ParentDocument aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateParentDocumentSpec_Rmim054(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim054");
		notif.setDescription("ParentDocument/text.BIN element SHALL NOT be used (RMIM-054)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ParentDocumentSpec-rmim054");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-054"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

}
