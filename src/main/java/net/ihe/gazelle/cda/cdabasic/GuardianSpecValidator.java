package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.datatypes.AD;
import net.ihe.gazelle.datatypes.II;
import net.ihe.gazelle.datatypes.TEL;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;	



 /**
  * class :        GuardianSpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040Guardian
  * 
  */
public final class GuardianSpecValidator{


    private GuardianSpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim081_1
	* Guardian SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-081, RIM-011)
	*
	*/
	private static boolean _validateGuardianSpec_Rmim081_1(net.ihe.gazelle.cda.POCDMT000040Guardian aClass){
		java.util.ArrayList<II> result1;
		result1 = new java.util.ArrayList<II>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getId()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim081_2
	* The ids elements of Guardian SHALL be distinct (RMIM-081, RIM-012)
	*
	*/
	private static boolean _validateGuardianSpec_Rmim081_2(net.ihe.gazelle.cda.POCDMT000040Guardian aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    java.util.ArrayList<II> result2;
			    result2 = new java.util.ArrayList<II>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (II anElement2 : aClass.getId()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim081_3
	* Gardian SHALL NOT have addr element with nullFlavor, if there are other addr (RMIM-081, RIM-016)
	*
	*/
	private static boolean _validateGuardianSpec_Rmim081_3(net.ihe.gazelle.cda.POCDMT000040Guardian aClass){
		java.util.ArrayList<AD> result1;
		result1 = new java.util.ArrayList<AD>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (AD anElement1 : aClass.getAddr()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAddr()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim081_4
	* The addr elements of Gardian SHALL be distinct (RMIM-081, RIM-017)
	*
	*/
	private static boolean _validateGuardianSpec_Rmim081_4(net.ihe.gazelle.cda.POCDMT000040Guardian aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (AD anElement1 : aClass.getAddr()) {
			    java.util.ArrayList<AD> result2;
			    result2 = new java.util.ArrayList<AD>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (AD anElement2 : aClass.getAddr()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim081_5
	* Guardian SHALL NOT have telecom element with nullFlavor, if there are other (RMIM-081, RIM-018)
	*
	*/
	private static boolean _validateGuardianSpec_Rmim081_5(net.ihe.gazelle.cda.POCDMT000040Guardian aClass){
		java.util.ArrayList<TEL> result1;
		result1 = new java.util.ArrayList<TEL>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (TEL anElement1 : aClass.getTelecom()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getTelecom()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim081_6
	* The telecom elements of Role SHALL be distinct (RMIM-081, RIM-018)
	*
	*/
	private static boolean _validateGuardianSpec_Rmim081_6(net.ihe.gazelle.cda.POCDMT000040Guardian aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (TEL anElement1 : aClass.getTelecom()) {
			    java.util.ArrayList<TEL> result2;
			    result2 = new java.util.ArrayList<TEL>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (TEL anElement2 : aClass.getTelecom()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of class-constraint : GuardianSpec
    * Verify if an element of type GuardianSpec can be validated by GuardianSpec
	*
	*/
	public static boolean _isGuardianSpec(net.ihe.gazelle.cda.POCDMT000040Guardian aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   GuardianSpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040Guardian
     * 
     */
    public static void _validateGuardianSpec(net.ihe.gazelle.cda.POCDMT000040Guardian aClass, String location, List<Notification> diagnostic) {
		if (_isGuardianSpec(aClass)){
			executeCons_GuardianSpec_Rmim081_1(aClass, location, diagnostic);
			executeCons_GuardianSpec_Rmim081_2(aClass, location, diagnostic);
			executeCons_GuardianSpec_Rmim081_3(aClass, location, diagnostic);
			executeCons_GuardianSpec_Rmim081_4(aClass, location, diagnostic);
			executeCons_GuardianSpec_Rmim081_5(aClass, location, diagnostic);
			executeCons_GuardianSpec_Rmim081_6(aClass, location, diagnostic);
		}
	}

	private static void executeCons_GuardianSpec_Rmim081_1(net.ihe.gazelle.cda.POCDMT000040Guardian aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateGuardianSpec_Rmim081_1(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim081_1");
		notif.setDescription("Guardian SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-081, RIM-011)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-GuardianSpec-rmim081_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-081;RIM-011"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_GuardianSpec_Rmim081_2(net.ihe.gazelle.cda.POCDMT000040Guardian aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateGuardianSpec_Rmim081_2(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim081_2");
		notif.setDescription("The ids elements of Guardian SHALL be distinct (RMIM-081, RIM-012)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-GuardianSpec-rmim081_2");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-081;RIM-012"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_GuardianSpec_Rmim081_3(net.ihe.gazelle.cda.POCDMT000040Guardian aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateGuardianSpec_Rmim081_3(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim081_3");
		notif.setDescription("Gardian SHALL NOT have addr element with nullFlavor, if there are other addr (RMIM-081, RIM-016)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-GuardianSpec-rmim081_3");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-081;RIM-016"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_GuardianSpec_Rmim081_4(net.ihe.gazelle.cda.POCDMT000040Guardian aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateGuardianSpec_Rmim081_4(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim081_4");
		notif.setDescription("The addr elements of Gardian SHALL be distinct (RMIM-081, RIM-017)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-GuardianSpec-rmim081_4");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-081;RIM-017"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_GuardianSpec_Rmim081_5(net.ihe.gazelle.cda.POCDMT000040Guardian aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateGuardianSpec_Rmim081_5(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim081_5");
		notif.setDescription("Guardian SHALL NOT have telecom element with nullFlavor, if there are other (RMIM-081, RIM-018)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-GuardianSpec-rmim081_5");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-081;RIM-018"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_GuardianSpec_Rmim081_6(net.ihe.gazelle.cda.POCDMT000040Guardian aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateGuardianSpec_Rmim081_6(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim081_6");
		notif.setDescription("The telecom elements of Role SHALL be distinct (RMIM-081, RIM-018)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-GuardianSpec-rmim081_6");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-081"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

}
