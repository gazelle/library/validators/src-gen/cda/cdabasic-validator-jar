package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.datatypes.AD;
import net.ihe.gazelle.datatypes.TEL;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;	



 /**
  * class :        RelatedSubjectSpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040RelatedSubject
  * 
  */
public final class RelatedSubjectSpecValidator{


    private RelatedSubjectSpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim087_1
	* The addr elements of RelatedSubject SHALL be distinct (RMIM-087, RIM-016)
	*
	*/
	private static boolean _validateRelatedSubjectSpec_Rmim087_1(net.ihe.gazelle.cda.POCDMT000040RelatedSubject aClass){
		java.util.ArrayList<AD> result1;
		result1 = new java.util.ArrayList<AD>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (AD anElement1 : aClass.getAddr()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAddr()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim087_2
	* The addr elements of RelatedSubject SHALL be distinct (RMIM-087, RIM-017)
	*
	*/
	private static boolean _validateRelatedSubjectSpec_Rmim087_2(net.ihe.gazelle.cda.POCDMT000040RelatedSubject aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (AD anElement1 : aClass.getAddr()) {
			    java.util.ArrayList<AD> result2;
			    result2 = new java.util.ArrayList<AD>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (AD anElement2 : aClass.getAddr()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim087_3
	* RelatedSubject SHALL NOT have telecom element with nullFlavor, if there are other telecom elements which are not null (RMIM-087, RIM-018)
	*
	*/
	private static boolean _validateRelatedSubjectSpec_Rmim087_3(net.ihe.gazelle.cda.POCDMT000040RelatedSubject aClass){
		java.util.ArrayList<TEL> result1;
		result1 = new java.util.ArrayList<TEL>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (TEL anElement1 : aClass.getTelecom()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getTelecom()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim087_4
	* The telecom elements of RelatedSubject SHALL be distinct (RMIM-087, RIM-019)
	*
	*/
	private static boolean _validateRelatedSubjectSpec_Rmim087_4(net.ihe.gazelle.cda.POCDMT000040RelatedSubject aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (TEL anElement1 : aClass.getTelecom()) {
			    java.util.ArrayList<TEL> result2;
			    result2 = new java.util.ArrayList<TEL>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (TEL anElement2 : aClass.getTelecom()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of class-constraint : RelatedSubjectSpec
    * Verify if an element of type RelatedSubjectSpec can be validated by RelatedSubjectSpec
	*
	*/
	public static boolean _isRelatedSubjectSpec(net.ihe.gazelle.cda.POCDMT000040RelatedSubject aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   RelatedSubjectSpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040RelatedSubject
     * 
     */
    public static void _validateRelatedSubjectSpec(net.ihe.gazelle.cda.POCDMT000040RelatedSubject aClass, String location, List<Notification> diagnostic) {
		if (_isRelatedSubjectSpec(aClass)){
			executeCons_RelatedSubjectSpec_Rmim087_1(aClass, location, diagnostic);
			executeCons_RelatedSubjectSpec_Rmim087_2(aClass, location, diagnostic);
			executeCons_RelatedSubjectSpec_Rmim087_3(aClass, location, diagnostic);
			executeCons_RelatedSubjectSpec_Rmim087_4(aClass, location, diagnostic);
		}
	}

	private static void executeCons_RelatedSubjectSpec_Rmim087_1(net.ihe.gazelle.cda.POCDMT000040RelatedSubject aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateRelatedSubjectSpec_Rmim087_1(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim087_1");
		notif.setDescription("The addr elements of RelatedSubject SHALL be distinct (RMIM-087, RIM-016)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-RelatedSubjectSpec-rmim087_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-087;RIM-016"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_RelatedSubjectSpec_Rmim087_2(net.ihe.gazelle.cda.POCDMT000040RelatedSubject aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateRelatedSubjectSpec_Rmim087_2(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim087_2");
		notif.setDescription("The addr elements of RelatedSubject SHALL be distinct (RMIM-087, RIM-017)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-RelatedSubjectSpec-rmim087_2");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-087;RIM-017"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_RelatedSubjectSpec_Rmim087_3(net.ihe.gazelle.cda.POCDMT000040RelatedSubject aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateRelatedSubjectSpec_Rmim087_3(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim087_3");
		notif.setDescription("RelatedSubject SHALL NOT have telecom element with nullFlavor, if there are other telecom elements which are not null (RMIM-087, RIM-018)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-RelatedSubjectSpec-rmim087_3");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-087;RIM-018"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_RelatedSubjectSpec_Rmim087_4(net.ihe.gazelle.cda.POCDMT000040RelatedSubject aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateRelatedSubjectSpec_Rmim087_4(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim087_4");
		notif.setDescription("The telecom elements of RelatedSubject SHALL be distinct (RMIM-087, RIM-019)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-RelatedSubjectSpec-rmim087_4");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-087;RIM-019"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

}
