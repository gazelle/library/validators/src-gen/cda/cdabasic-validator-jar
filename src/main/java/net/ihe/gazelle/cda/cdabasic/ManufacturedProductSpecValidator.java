package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.datatypes.II;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;	



 /**
  * class :        ManufacturedProductSpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040ManufacturedProduct
  * 
  */
public final class ManufacturedProductSpecValidator{


    private ManufacturedProductSpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim089_1
	* ManufacturedProduct SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-089, RIM-011)
	*
	*/
	private static boolean _validateManufacturedProductSpec_Rmim089_1(net.ihe.gazelle.cda.POCDMT000040ManufacturedProduct aClass){
		java.util.ArrayList<II> result1;
		result1 = new java.util.ArrayList<II>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getId()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim089_2
	* The ids elements of ManufacturedProduct SHALL be distinct (RMIM-089, RIM-012)
	*
	*/
	private static boolean _validateManufacturedProductSpec_Rmim089_2(net.ihe.gazelle.cda.POCDMT000040ManufacturedProduct aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    java.util.ArrayList<II> result2;
			    result2 = new java.util.ArrayList<II>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (II anElement2 : aClass.getId()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of class-constraint : ManufacturedProductSpec
    * Verify if an element of type ManufacturedProductSpec can be validated by ManufacturedProductSpec
	*
	*/
	public static boolean _isManufacturedProductSpec(net.ihe.gazelle.cda.POCDMT000040ManufacturedProduct aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   ManufacturedProductSpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040ManufacturedProduct
     * 
     */
    public static void _validateManufacturedProductSpec(net.ihe.gazelle.cda.POCDMT000040ManufacturedProduct aClass, String location, List<Notification> diagnostic) {
		if (_isManufacturedProductSpec(aClass)){
			executeCons_ManufacturedProductSpec_Rmim089_1(aClass, location, diagnostic);
			executeCons_ManufacturedProductSpec_Rmim089_2(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ManufacturedProductSpec_Rmim089_1(net.ihe.gazelle.cda.POCDMT000040ManufacturedProduct aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateManufacturedProductSpec_Rmim089_1(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim089_1");
		notif.setDescription("ManufacturedProduct SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-089, RIM-011)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ManufacturedProductSpec-rmim089_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-089;RIM-011"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_ManufacturedProductSpec_Rmim089_2(net.ihe.gazelle.cda.POCDMT000040ManufacturedProduct aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateManufacturedProductSpec_Rmim089_2(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim089_2");
		notif.setDescription("The ids elements of ManufacturedProduct SHALL be distinct (RMIM-089, RIM-012)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ManufacturedProductSpec-rmim089_2");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-089;RIM-012"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

}
