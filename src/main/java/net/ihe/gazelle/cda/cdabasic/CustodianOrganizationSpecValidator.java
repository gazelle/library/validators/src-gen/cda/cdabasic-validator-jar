package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.datatypes.II;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;	



 /**
  * class :        CustodianOrganizationSpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040CustodianOrganization
  * 
  */
public final class CustodianOrganizationSpecValidator{


    private CustodianOrganizationSpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim099_1
	* CustodianOrganization SHALL NOT have id element with nullFlavor, if there are other ids (RMIM-099, RIM-020)
	*
	*/
	private static boolean _validateCustodianOrganizationSpec_Rmim099_1(net.ihe.gazelle.cda.POCDMT000040CustodianOrganization aClass){
		java.util.ArrayList<II> result1;
		result1 = new java.util.ArrayList<II>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getId()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim099_2
	* The ids elements of CustodianOrganization SHALL be distinct (RMIM-099, RIM-021)
	*
	*/
	private static boolean _validateCustodianOrganizationSpec_Rmim099_2(net.ihe.gazelle.cda.POCDMT000040CustodianOrganization aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    java.util.ArrayList<II> result2;
			    result2 = new java.util.ArrayList<II>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (II anElement2 : aClass.getId()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim100
	* id element SHALL be present and is mandatory (RMIM-100)
	*
	*/
	private static boolean _validateCustodianOrganizationSpec_Rmim100(net.ihe.gazelle.cda.POCDMT000040CustodianOrganization aClass){
		Boolean result1;
		result1 = false;
		
		/* Iterator Exists: Iterate and check, if any element fulfills the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    if (!(((String) anElement1.getRoot()) == null)) {
			        result1 = true;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of class-constraint : CustodianOrganizationSpec
    * Verify if an element of type CustodianOrganizationSpec can be validated by CustodianOrganizationSpec
	*
	*/
	public static boolean _isCustodianOrganizationSpec(net.ihe.gazelle.cda.POCDMT000040CustodianOrganization aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   CustodianOrganizationSpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040CustodianOrganization
     * 
     */
    public static void _validateCustodianOrganizationSpec(net.ihe.gazelle.cda.POCDMT000040CustodianOrganization aClass, String location, List<Notification> diagnostic) {
		if (_isCustodianOrganizationSpec(aClass)){
			executeCons_CustodianOrganizationSpec_Rmim099_1(aClass, location, diagnostic);
			executeCons_CustodianOrganizationSpec_Rmim099_2(aClass, location, diagnostic);
			executeCons_CustodianOrganizationSpec_Rmim100(aClass, location, diagnostic);
		}
	}

	private static void executeCons_CustodianOrganizationSpec_Rmim099_1(net.ihe.gazelle.cda.POCDMT000040CustodianOrganization aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCustodianOrganizationSpec_Rmim099_1(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim099_1");
		notif.setDescription("CustodianOrganization SHALL NOT have id element with nullFlavor, if there are other ids (RMIM-099, RIM-020)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-CustodianOrganizationSpec-rmim099_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-099;RIM-020"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_CustodianOrganizationSpec_Rmim099_2(net.ihe.gazelle.cda.POCDMT000040CustodianOrganization aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCustodianOrganizationSpec_Rmim099_2(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim099_2");
		notif.setDescription("The ids elements of CustodianOrganization SHALL be distinct (RMIM-099, RIM-021)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-CustodianOrganizationSpec-rmim099_2");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-099;RIM-021"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_CustodianOrganizationSpec_Rmim100(net.ihe.gazelle.cda.POCDMT000040CustodianOrganization aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCustodianOrganizationSpec_Rmim100(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim100");
		notif.setDescription("id element SHALL be present and is mandatory (RMIM-100)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-CustodianOrganizationSpec-rmim100");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-100"));
		
		notif.setType("Mandatory");
		diagnostic.add(notif);
	}

}
