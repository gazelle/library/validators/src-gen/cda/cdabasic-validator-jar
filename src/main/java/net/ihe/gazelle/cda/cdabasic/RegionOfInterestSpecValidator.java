package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.cda.POCDMT000040EntryRelationship;
import net.ihe.gazelle.cda.POCDMT000040ObservationMedia;
import net.ihe.gazelle.cda.POCDMT000040Reference;
import net.ihe.gazelle.datatypes.II;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.voc.XActRelationshipEntryRelationship;	



 /**
  * class :        RegionOfInterestSpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040RegionOfInterest
  * 
  */
public final class RegionOfInterestSpecValidator{


    private RegionOfInterestSpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim023_1
	* RegionOfInterest SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-023, RIM-001)
	*
	*/
	private static boolean _validateRegionOfInterestSpec_Rmim023_1(net.ihe.gazelle.cda.POCDMT000040RegionOfInterest aClass){
		java.util.ArrayList<II> result1;
		result1 = new java.util.ArrayList<II>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getId()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim023_2
	* The ids elements of RegionOfInterest SHALL be distinct (RMIM-023, RIM-002)
	*
	*/
	private static boolean _validateRegionOfInterestSpec_Rmim023_2(net.ihe.gazelle.cda.POCDMT000040RegionOfInterest aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    java.util.ArrayList<II> result2;
			    result2 = new java.util.ArrayList<II>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (II anElement2 : aClass.getId()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim024
	* A RegionOfInterest SHALL have an entryRelationship[typeCode= SUBJ ]/observationMedia,  or it SHALL be included into an observationMedia/entryRelationship[@typeCode= SUBJ  and @inversionInd= true ],  or it SHALL include an external Observation into reference[typeCode= SUBJ ]/ExternalObservation (RMIM-024)
	*
	*/
	private static boolean _validateRegionOfInterestSpec_Rmim024(net.ihe.gazelle.cda.POCDMT000040RegionOfInterest aClass){
		java.util.ArrayList<POCDMT000040EntryRelationship> result1;
		result1 = new java.util.ArrayList<POCDMT000040EntryRelationship>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (POCDMT000040EntryRelationship anElement1 : aClass.getEntryRelationship()) {
			    if (((!(anElement1.getTypeCode() == null) && anElement1.getTypeCode().equals(XActRelationshipEntryRelationship.SUBJ)) && !(anElement1.getObservationMedia() == null))) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		java.util.ArrayList<POCDMT000040Reference> result2;
		result2 = new java.util.ArrayList<POCDMT000040Reference>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (POCDMT000040Reference anElement2 : aClass.getReference()) {
			    if (!(anElement2.getExternalObservation() == null)) {
			        result2.add(anElement2);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return (((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1) > Integer.valueOf(0)) || (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2) > Integer.valueOf(0))) || (((((((!(aClass.get__parent() == null) && (POCDMT000040EntryRelationship.class.isAssignableFrom(aClass.get__parent().getClass()))) && !(((POCDMT000040EntryRelationship) aClass.get__parent()).getTypeCode() == null)) && ((POCDMT000040EntryRelationship) aClass.get__parent()).getTypeCode().equals(XActRelationshipEntryRelationship.SUBJ)) && !(((Boolean) ((POCDMT000040EntryRelationship) aClass.get__parent()).isInversionInd()) == null)) && ((Object) ((POCDMT000040EntryRelationship) aClass.get__parent()).isInversionInd()).equals(Boolean.TRUE)) && !(((POCDMT000040EntryRelationship) aClass.get__parent()).get__parent() == null)) && (POCDMT000040ObservationMedia.class.isAssignableFrom(((POCDMT000040EntryRelationship) aClass.get__parent()).get__parent().getClass()))));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim025
	* A RegionOfInterest must reference exactly one ObservationMedia or one ExternalObservation (RMIM-025)
	*
	*/
	private static boolean _validateRegionOfInterestSpec_Rmim025(net.ihe.gazelle.cda.POCDMT000040RegionOfInterest aClass){
		java.util.ArrayList<POCDMT000040EntryRelationship> result1;
		result1 = new java.util.ArrayList<POCDMT000040EntryRelationship>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (POCDMT000040EntryRelationship anElement1 : aClass.getEntryRelationship()) {
			    if (((!(anElement1.getTypeCode() == null) && anElement1.getTypeCode().equals(XActRelationshipEntryRelationship.SUBJ)) && !(anElement1.getObservationMedia() == null))) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		java.util.ArrayList<POCDMT000040Reference> result2;
		result2 = new java.util.ArrayList<POCDMT000040Reference>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (POCDMT000040Reference anElement2 : aClass.getReference()) {
			    if (!(anElement2.getExternalObservation() == null)) {
			        result2.add(anElement2);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return (((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1) > Integer.valueOf(0)) || (((((((!(aClass.get__parent() == null) && (POCDMT000040EntryRelationship.class.isAssignableFrom(aClass.get__parent().getClass()))) && !(((POCDMT000040EntryRelationship) aClass.get__parent()).getTypeCode() == null)) && ((POCDMT000040EntryRelationship) aClass.get__parent()).getTypeCode().equals(XActRelationshipEntryRelationship.SUBJ)) && !(((Boolean) ((POCDMT000040EntryRelationship) aClass.get__parent()).isInversionInd()) == null)) && ((Object) ((POCDMT000040EntryRelationship) aClass.get__parent()).isInversionInd()).equals(Boolean.TRUE)) && !(((POCDMT000040EntryRelationship) aClass.get__parent()).get__parent() == null)) && (POCDMT000040ObservationMedia.class.isAssignableFrom(((POCDMT000040EntryRelationship) aClass.get__parent()).get__parent().getClass())))) ^ (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2) > Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim028
	* The code of RegionOfInterest if present SHALL be from ROIOverlayShape valueSet (RMIM-028)
	*
	*/
	private static boolean _validateRegionOfInterestSpec_Rmim028(net.ihe.gazelle.cda.POCDMT000040RegionOfInterest aClass){
		return (((aClass.getCode() == null) || (((String) aClass.getCode().getCode()) == null)) || aClass.getCode().matchesCodeToValueSet("1.3.6.1.4.1.12559.11.18.6", aClass.getCode().getCode()));
		
	}

	/**
	* Validation of instance by a constraint : rmim029
	* The value list SHALL contain a pair number of values (RMIM-029)
	*
	*/
	private static boolean _validateRegionOfInterestSpec_Rmim029(net.ihe.gazelle.cda.POCDMT000040RegionOfInterest aClass){
		return ((Object) (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue()) % Integer.valueOf(2))).equals(Integer.valueOf(0));
		
	}

	/**
	* Validation of instance by a constraint : rmim030
	* If the code has the value CIRCLE, the value list SHALL have 4 values (RMIM-030)
	*
	*/
	private static boolean _validateRegionOfInterestSpec_Rmim030(net.ihe.gazelle.cda.POCDMT000040RegionOfInterest aClass){
		return ((((aClass.getCode() == null) || (((String) aClass.getCode().getCode()) == null)) || !aClass.getCode().getCode().equals("CIRCLE")) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue())).equals(Integer.valueOf(4)));
		
	}

	/**
	* Validation of instance by a constraint : rmim031
	* If the code has the value ELLIPSE, the value list SHALL have 8 values (RMIM-031)
	*
	*/
	private static boolean _validateRegionOfInterestSpec_Rmim031(net.ihe.gazelle.cda.POCDMT000040RegionOfInterest aClass){
		return ((((aClass.getCode() == null) || (((String) aClass.getCode().getCode()) == null)) || !aClass.getCode().getCode().equals("ELLIPSE")) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getValue())).equals(Integer.valueOf(8)));
		
	}

	/**
	* Validation of class-constraint : RegionOfInterestSpec
    * Verify if an element of type RegionOfInterestSpec can be validated by RegionOfInterestSpec
	*
	*/
	public static boolean _isRegionOfInterestSpec(net.ihe.gazelle.cda.POCDMT000040RegionOfInterest aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   RegionOfInterestSpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040RegionOfInterest
     * 
     */
    public static void _validateRegionOfInterestSpec(net.ihe.gazelle.cda.POCDMT000040RegionOfInterest aClass, String location, List<Notification> diagnostic) {
		if (_isRegionOfInterestSpec(aClass)){
			executeCons_RegionOfInterestSpec_Rmim023_1(aClass, location, diagnostic);
			executeCons_RegionOfInterestSpec_Rmim023_2(aClass, location, diagnostic);
			executeCons_RegionOfInterestSpec_Rmim024(aClass, location, diagnostic);
			executeCons_RegionOfInterestSpec_Rmim025(aClass, location, diagnostic);
			executeCons_RegionOfInterestSpec_Rmim028(aClass, location, diagnostic);
			executeCons_RegionOfInterestSpec_Rmim029(aClass, location, diagnostic);
			executeCons_RegionOfInterestSpec_Rmim030(aClass, location, diagnostic);
			executeCons_RegionOfInterestSpec_Rmim031(aClass, location, diagnostic);
		}
	}

	private static void executeCons_RegionOfInterestSpec_Rmim023_1(net.ihe.gazelle.cda.POCDMT000040RegionOfInterest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateRegionOfInterestSpec_Rmim023_1(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim023_1");
		notif.setDescription("RegionOfInterest SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-023, RIM-001)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-RegionOfInterestSpec-rmim023_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-023;RIM-001"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_RegionOfInterestSpec_Rmim023_2(net.ihe.gazelle.cda.POCDMT000040RegionOfInterest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateRegionOfInterestSpec_Rmim023_2(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim023_2");
		notif.setDescription("The ids elements of RegionOfInterest SHALL be distinct (RMIM-023, RIM-002)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-RegionOfInterestSpec-rmim023_2");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-023;RIM-002"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_RegionOfInterestSpec_Rmim024(net.ihe.gazelle.cda.POCDMT000040RegionOfInterest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateRegionOfInterestSpec_Rmim024(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim024");
		notif.setDescription("A RegionOfInterest SHALL have an entryRelationship[typeCode= SUBJ ]/observationMedia,  or it SHALL be included into an observationMedia/entryRelationship[@typeCode= SUBJ  and @inversionInd= true ],  or it SHALL include an external Observation into reference[typeCode= SUBJ ]/ExternalObservation (RMIM-024)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-RegionOfInterestSpec-rmim024");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-024"));
		
		notif.setType("Mandatory");
		diagnostic.add(notif);
	}

	private static void executeCons_RegionOfInterestSpec_Rmim025(net.ihe.gazelle.cda.POCDMT000040RegionOfInterest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateRegionOfInterestSpec_Rmim025(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim025");
		notif.setDescription("A RegionOfInterest must reference exactly one ObservationMedia or one ExternalObservation (RMIM-025)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-RegionOfInterestSpec-rmim025");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-025"));
		
		notif.setType("Cardinality");
		diagnostic.add(notif);
	}

	private static void executeCons_RegionOfInterestSpec_Rmim028(net.ihe.gazelle.cda.POCDMT000040RegionOfInterest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateRegionOfInterestSpec_Rmim028(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim028");
		notif.setDescription("The code of RegionOfInterest if present SHALL be from ROIOverlayShape valueSet (RMIM-028)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-RegionOfInterestSpec-rmim028");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-028"));
		
		notif.setType("Vocabulary");
		diagnostic.add(notif);
	}

	private static void executeCons_RegionOfInterestSpec_Rmim029(net.ihe.gazelle.cda.POCDMT000040RegionOfInterest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateRegionOfInterestSpec_Rmim029(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim029");
		notif.setDescription("The value list SHALL contain a pair number of values (RMIM-029)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-RegionOfInterestSpec-rmim029");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-029"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

	private static void executeCons_RegionOfInterestSpec_Rmim030(net.ihe.gazelle.cda.POCDMT000040RegionOfInterest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateRegionOfInterestSpec_Rmim030(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim030");
		notif.setDescription("If the code has the value CIRCLE, the value list SHALL have 4 values (RMIM-030)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-RegionOfInterestSpec-rmim030");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-030"));
		
		notif.setType("Cardinality");
		diagnostic.add(notif);
	}

	private static void executeCons_RegionOfInterestSpec_Rmim031(net.ihe.gazelle.cda.POCDMT000040RegionOfInterest aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateRegionOfInterestSpec_Rmim031(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim031");
		notif.setDescription("If the code has the value ELLIPSE, the value list SHALL have 8 values (RMIM-031)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-RegionOfInterestSpec-rmim031");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-031"));
		
		notif.setType("Cardinality");
		diagnostic.add(notif);
	}

}
