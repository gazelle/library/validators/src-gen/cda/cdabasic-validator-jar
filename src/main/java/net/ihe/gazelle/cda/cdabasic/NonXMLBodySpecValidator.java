package net.ihe.gazelle.cda.cdabasic;

import java.nio.charset.Charset;
import java.util.List;

import net.ihe.gazelle.basic.validation.SHAValidation;
import net.ihe.gazelle.datatypes.BinaryDataEncoding;
import net.ihe.gazelle.datatypes.ED;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.voc.IntegrityCheckAlgorithm;
import net.ihe.gazelle.xmltools.xsd.XMLValidator;	



 /**
  * class :        NonXMLBodySpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040NonXMLBody
  * 
  */
public final class NonXMLBodySpecValidator{


    private NonXMLBodySpecValidator() {}

public static Boolean validateTextIsNotXML(ED ed1) {
	// Start of user code for method validateTextIsNotXML
	if (ed1 != null){
				List<String> ed1Strings = ed1.getListStringValues();
				String ed1String = "";
				if (ed1Strings != null){
					for (String string : ed1Strings) {
						ed1String = ed1String + string;
					}
				}
	
				String sha = "SHA-1";
				if (ed1.getIntegrityCheckAlgorithm() != null){
					if (ed1.getIntegrityCheckAlgorithm() == IntegrityCheckAlgorithm.SHA256){
						sha = "SHA-256";
					}
				}
	
				if (ed1.getRepresentation() != null && ed1.getRepresentation() == BinaryDataEncoding.B64){
					if (ed1.getCompression() != null){
						// TODO
						ed1String = null;
					}
					else{
						java.io.InputStream ed1is = SHAValidation.decodeB64(ed1String, null);
						List<net.ihe.gazelle.xmltools.xsd.ValidationException> ex = XMLValidator.validate(ed1is);
						if (ex != null && ex.size()>0) return true;
						return false;
					}
				}
				else{
					List<net.ihe.gazelle.xmltools.xsd.ValidationException> ex = XMLValidator.validate(new java.io.ByteArrayInputStream(ed1String.getBytes(Charset.forName("UTF-8"))));
					if (ex != null && ex.size()>0) return true;
					return false;
				}
			}
			return true;
	// End of user code
}


	/**
	* Validation of instance by a constraint : rmim047
	* The languageCode shall be from the valueSet HumanLanguage (RMIM-047)
	*
	*/
	private static boolean _validateNonXMLBodySpec_Rmim047(net.ihe.gazelle.cda.POCDMT000040NonXMLBody aClass){
		return (((aClass.getLanguageCode() == null) || !(aClass.getLanguageCode().getNullFlavor() == null)) || (!(((String) aClass.getLanguageCode().getCode()) == null) && aClass.getLanguageCode().matches(aClass.getLanguageCode().getCode(), "\\w+(\\-\\w+)?")));
		
	}

	/**
	* Validation of instance by a constraint : rmim048
	* The content of text element SHALL be in other form than XML form (RMIM-048)
	*
	*/
	private static boolean _validateNonXMLBodySpec_Rmim048(net.ihe.gazelle.cda.POCDMT000040NonXMLBody aClass){
		java.util.ArrayList<String> collection1;
		collection1 = new java.util.ArrayList<String>();
		
		collection1.add("application/soap+xml");
		collection1.add("text/xml");
		collection1.add("application/xml");
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : collection1) {
			    if (!!anElement1.equals(aClass.getText().getMediaType())) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((aClass.getText() == null) || (((((String) aClass.getText().getMediaType()) == null) || result1) && NonXMLBodySpecValidator.validateTextIsNotXML(aClass.getText())));
		
		
	}

	/**
	* Validation of class-constraint : NonXMLBodySpec
    * Verify if an element of type NonXMLBodySpec can be validated by NonXMLBodySpec
	*
	*/
	public static boolean _isNonXMLBodySpec(net.ihe.gazelle.cda.POCDMT000040NonXMLBody aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   NonXMLBodySpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040NonXMLBody
     * 
     */
    public static void _validateNonXMLBodySpec(net.ihe.gazelle.cda.POCDMT000040NonXMLBody aClass, String location, List<Notification> diagnostic) {
		if (_isNonXMLBodySpec(aClass)){
			executeCons_NonXMLBodySpec_Rmim047(aClass, location, diagnostic);
			executeCons_NonXMLBodySpec_Rmim048(aClass, location, diagnostic);
		}
	}

	private static void executeCons_NonXMLBodySpec_Rmim047(net.ihe.gazelle.cda.POCDMT000040NonXMLBody aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateNonXMLBodySpec_Rmim047(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim047");
		notif.setDescription("The languageCode shall be from the valueSet HumanLanguage (RMIM-047)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-NonXMLBodySpec-rmim047");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-047"));
		
		notif.setType("Vocabulary");
		diagnostic.add(notif);
	}

	private static void executeCons_NonXMLBodySpec_Rmim048(net.ihe.gazelle.cda.POCDMT000040NonXMLBody aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateNonXMLBodySpec_Rmim048(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim048");
		notif.setDescription("The content of text element SHALL be in other form than XML form (RMIM-048)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-NonXMLBodySpec-rmim048");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-048"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

}
