package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;	



 /**
  * class :        AuthorSpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040Author
  * 
  */
public final class AuthorSpecValidator{


    private AuthorSpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim078
	* If assignedAuthor has not a null of flavor and its enclosing author has not a null of flavor,  it SHALL have an assignedAuthoringDevice or assignedPerson or a representedOrganization element (RMIM-078)
	*
	*/
	private static boolean _validateAuthorSpec_Rmim078(net.ihe.gazelle.cda.POCDMT000040Author aClass){
		return (((!(aClass.getNullFlavor() == null) || (aClass.getAssignedAuthor() == null)) || !(aClass.getAssignedAuthor().getNullFlavor() == null)) || ((!(aClass.getAssignedAuthor().getAssignedAuthoringDevice() == null) || !(aClass.getAssignedAuthor().getAssignedPerson() == null)) || !(aClass.getAssignedAuthor().getRepresentedOrganization() == null)));
		
	}

	/**
	* Validation of class-constraint : AuthorSpec
    * Verify if an element of type AuthorSpec can be validated by AuthorSpec
	*
	*/
	public static boolean _isAuthorSpec(net.ihe.gazelle.cda.POCDMT000040Author aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   AuthorSpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040Author
     * 
     */
    public static void _validateAuthorSpec(net.ihe.gazelle.cda.POCDMT000040Author aClass, String location, List<Notification> diagnostic) {
		if (_isAuthorSpec(aClass)){
			executeCons_AuthorSpec_Rmim078(aClass, location, diagnostic);
		}
	}

	private static void executeCons_AuthorSpec_Rmim078(net.ihe.gazelle.cda.POCDMT000040Author aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAuthorSpec_Rmim078(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim078");
		notif.setDescription("If assignedAuthor has not a null of flavor and its enclosing author has not a null of flavor,  it SHALL have an assignedAuthoringDevice or assignedPerson or a representedOrganization element (RMIM-078)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-AuthorSpec-rmim078");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-078"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

}
