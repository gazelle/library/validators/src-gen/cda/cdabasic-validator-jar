package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validation.Warning;	



 /**
  * class :        AuthoringDeviceSpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040AuthoringDevice
  * 
  */
public final class AuthoringDeviceSpecValidator{


    private AuthoringDeviceSpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim098
	* asMaintainedEntity SHOULD NOT be used (RMIM-098)
	*
	*/
	private static boolean _validateAuthoringDeviceSpec_Rmim098(net.ihe.gazelle.cda.POCDMT000040AuthoringDevice aClass){
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAsMaintainedEntity())).equals(Integer.valueOf(0));
		
	}

	/**
	* Validation of class-constraint : AuthoringDeviceSpec
    * Verify if an element of type AuthoringDeviceSpec can be validated by AuthoringDeviceSpec
	*
	*/
	public static boolean _isAuthoringDeviceSpec(net.ihe.gazelle.cda.POCDMT000040AuthoringDevice aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   AuthoringDeviceSpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040AuthoringDevice
     * 
     */
    public static void _validateAuthoringDeviceSpec(net.ihe.gazelle.cda.POCDMT000040AuthoringDevice aClass, String location, List<Notification> diagnostic) {
		if (_isAuthoringDeviceSpec(aClass)){
			executeCons_AuthoringDeviceSpec_Rmim098(aClass, location, diagnostic);
		}
	}

	private static void executeCons_AuthoringDeviceSpec_Rmim098(net.ihe.gazelle.cda.POCDMT000040AuthoringDevice aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAuthoringDeviceSpec_Rmim098(aClass) )){
				notif = new Warning();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim098");
		notif.setDescription("asMaintainedEntity SHOULD NOT be used (RMIM-098)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-AuthoringDeviceSpec-rmim098");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-098"));
		
		notif.setType("Fixed value");
		diagnostic.add(notif);
	}

}
