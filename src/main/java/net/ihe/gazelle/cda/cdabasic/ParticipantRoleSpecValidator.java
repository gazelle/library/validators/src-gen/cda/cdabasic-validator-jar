package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.cda.POCDMT000040Participant2;
import net.ihe.gazelle.datatypes.AD;
import net.ihe.gazelle.datatypes.II;
import net.ihe.gazelle.datatypes.TEL;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;	



 /**
  * class :        ParticipantRoleSpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040ParticipantRole
  * 
  */
public final class ParticipantRoleSpecValidator{


    private ParticipantRoleSpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim092_1
	* ParticipantRole SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-092, RIM-011)
	*
	*/
	private static boolean _validateParticipantRoleSpec_Rmim092_1(net.ihe.gazelle.cda.POCDMT000040ParticipantRole aClass){
		java.util.ArrayList<II> result1;
		result1 = new java.util.ArrayList<II>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getId()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim092_2
	* The ids elements of ParticipantRole SHALL be distinct (RMIM-092, RIM-012)
	*
	*/
	private static boolean _validateParticipantRoleSpec_Rmim092_2(net.ihe.gazelle.cda.POCDMT000040ParticipantRole aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    java.util.ArrayList<II> result2;
			    result2 = new java.util.ArrayList<II>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (II anElement2 : aClass.getId()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim092_3
	* ParticipantRole SHALL NOT have addr element with nullFlavor, if there are other addr (RMIM-092, RIM-016)
	*
	*/
	private static boolean _validateParticipantRoleSpec_Rmim092_3(net.ihe.gazelle.cda.POCDMT000040ParticipantRole aClass){
		java.util.ArrayList<AD> result1;
		result1 = new java.util.ArrayList<AD>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (AD anElement1 : aClass.getAddr()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAddr()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim092_4
	* The addr elements of ParticipantRole SHALL be distinct (RMIM-092, RIM-017)
	*
	*/
	private static boolean _validateParticipantRoleSpec_Rmim092_4(net.ihe.gazelle.cda.POCDMT000040ParticipantRole aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (AD anElement1 : aClass.getAddr()) {
			    java.util.ArrayList<AD> result2;
			    result2 = new java.util.ArrayList<AD>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (AD anElement2 : aClass.getAddr()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim092_5
	* ParticipantRole SHALL NOT have telecom element with nullFlavor, if there are other telecom elements which are not null (RMIM-092, RIM-018)
	*
	*/
	private static boolean _validateParticipantRoleSpec_Rmim092_5(net.ihe.gazelle.cda.POCDMT000040ParticipantRole aClass){
		java.util.ArrayList<TEL> result1;
		result1 = new java.util.ArrayList<TEL>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (TEL anElement1 : aClass.getTelecom()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getTelecom()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim092_6
	* The telecom elements of ParticipantRole SHALL be distinct (RMIM-092, RIM-019)
	*
	*/
	private static boolean _validateParticipantRoleSpec_Rmim092_6(net.ihe.gazelle.cda.POCDMT000040ParticipantRole aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (TEL anElement1 : aClass.getTelecom()) {
			    java.util.ArrayList<TEL> result2;
			    result2 = new java.util.ArrayList<TEL>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (TEL anElement2 : aClass.getTelecom()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim093
	* If participantRole has not a null of flavor and its enclosing participant has not a null of flavor,  it SHALL have a scopingEntity, or a playingEntityChoice (RMIM-093)
	*
	*/
	private static boolean _validateParticipantRoleSpec_Rmim093(net.ihe.gazelle.cda.POCDMT000040ParticipantRole aClass){
		return ((((((!(aClass.getNullFlavor() == null) || (aClass.get__parent() == null)) || !(POCDMT000040Participant2.class.isAssignableFrom(aClass.get__parent().getClass()))) || !(((POCDMT000040Participant2) aClass.get__parent()).getNullFlavor() == null)) || !(aClass.getScopingEntity() == null)) || !(aClass.getPlayingEntity() == null)) || !(aClass.getPlayingDevice() == null));
		
	}

	/**
	* Validation of class-constraint : ParticipantRoleSpec
    * Verify if an element of type ParticipantRoleSpec can be validated by ParticipantRoleSpec
	*
	*/
	public static boolean _isParticipantRoleSpec(net.ihe.gazelle.cda.POCDMT000040ParticipantRole aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   ParticipantRoleSpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040ParticipantRole
     * 
     */
    public static void _validateParticipantRoleSpec(net.ihe.gazelle.cda.POCDMT000040ParticipantRole aClass, String location, List<Notification> diagnostic) {
		if (_isParticipantRoleSpec(aClass)){
			executeCons_ParticipantRoleSpec_Rmim092_1(aClass, location, diagnostic);
			executeCons_ParticipantRoleSpec_Rmim092_2(aClass, location, diagnostic);
			executeCons_ParticipantRoleSpec_Rmim092_3(aClass, location, diagnostic);
			executeCons_ParticipantRoleSpec_Rmim092_4(aClass, location, diagnostic);
			executeCons_ParticipantRoleSpec_Rmim092_5(aClass, location, diagnostic);
			executeCons_ParticipantRoleSpec_Rmim092_6(aClass, location, diagnostic);
			executeCons_ParticipantRoleSpec_Rmim093(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ParticipantRoleSpec_Rmim092_1(net.ihe.gazelle.cda.POCDMT000040ParticipantRole aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateParticipantRoleSpec_Rmim092_1(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim092_1");
		notif.setDescription("ParticipantRole SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-092, RIM-011)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ParticipantRoleSpec-rmim092_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-092;RIM-011"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_ParticipantRoleSpec_Rmim092_2(net.ihe.gazelle.cda.POCDMT000040ParticipantRole aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateParticipantRoleSpec_Rmim092_2(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim092_2");
		notif.setDescription("The ids elements of ParticipantRole SHALL be distinct (RMIM-092, RIM-012)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ParticipantRoleSpec-rmim092_2");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-092;RIM-012"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_ParticipantRoleSpec_Rmim092_3(net.ihe.gazelle.cda.POCDMT000040ParticipantRole aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateParticipantRoleSpec_Rmim092_3(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim092_3");
		notif.setDescription("ParticipantRole SHALL NOT have addr element with nullFlavor, if there are other addr (RMIM-092, RIM-016)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ParticipantRoleSpec-rmim092_3");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-092;RIM-016"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_ParticipantRoleSpec_Rmim092_4(net.ihe.gazelle.cda.POCDMT000040ParticipantRole aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateParticipantRoleSpec_Rmim092_4(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim092_4");
		notif.setDescription("The addr elements of ParticipantRole SHALL be distinct (RMIM-092, RIM-017)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ParticipantRoleSpec-rmim092_4");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-092;RIM-017"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_ParticipantRoleSpec_Rmim092_5(net.ihe.gazelle.cda.POCDMT000040ParticipantRole aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateParticipantRoleSpec_Rmim092_5(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim092_5");
		notif.setDescription("ParticipantRole SHALL NOT have telecom element with nullFlavor, if there are other telecom elements which are not null (RMIM-092, RIM-018)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ParticipantRoleSpec-rmim092_5");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-092"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_ParticipantRoleSpec_Rmim092_6(net.ihe.gazelle.cda.POCDMT000040ParticipantRole aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateParticipantRoleSpec_Rmim092_6(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim092_6");
		notif.setDescription("The telecom elements of ParticipantRole SHALL be distinct (RMIM-092, RIM-019)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ParticipantRoleSpec-rmim092_6");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-092"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_ParticipantRoleSpec_Rmim093(net.ihe.gazelle.cda.POCDMT000040ParticipantRole aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateParticipantRoleSpec_Rmim093(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim093");
		notif.setDescription("If participantRole has not a null of flavor and its enclosing participant has not a null of flavor,  it SHALL have a scopingEntity, or a playingEntityChoice (RMIM-093)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ParticipantRoleSpec-rmim093");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-093"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

}
