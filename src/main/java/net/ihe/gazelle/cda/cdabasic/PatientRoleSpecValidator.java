package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.cda.POCDMT000040RecordTarget;
import net.ihe.gazelle.datatypes.AD;
import net.ihe.gazelle.datatypes.II;
import net.ihe.gazelle.datatypes.TEL;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;	



 /**
  * class :        PatientRoleSpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040PatientRole
  * 
  */
public final class PatientRoleSpecValidator{


    private PatientRoleSpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim079_1
	* PatientRole SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-079, RIM-011)
	*
	*/
	private static boolean _validatePatientRoleSpec_Rmim079_1(net.ihe.gazelle.cda.POCDMT000040PatientRole aClass){
		java.util.ArrayList<II> result1;
		result1 = new java.util.ArrayList<II>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getId()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim079_2
	* The ids elements of PatientRole SHALL be distinct (RMIM-079, RIM-012)
	*
	*/
	private static boolean _validatePatientRoleSpec_Rmim079_2(net.ihe.gazelle.cda.POCDMT000040PatientRole aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    java.util.ArrayList<II> result2;
			    result2 = new java.util.ArrayList<II>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (II anElement2 : aClass.getId()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim079_3
	* PatientRole SHALL NOT have addr element with nullFlavor, if there are other addr (RMIM-079, RIM-016)
	*
	*/
	private static boolean _validatePatientRoleSpec_Rmim079_3(net.ihe.gazelle.cda.POCDMT000040PatientRole aClass){
		java.util.ArrayList<AD> result1;
		result1 = new java.util.ArrayList<AD>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (AD anElement1 : aClass.getAddr()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAddr()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim079_4
	* The addr elements of PatientRole SHALL be distinct (RMIM-079, RIM-017)
	*
	*/
	private static boolean _validatePatientRoleSpec_Rmim079_4(net.ihe.gazelle.cda.POCDMT000040PatientRole aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (AD anElement1 : aClass.getAddr()) {
			    java.util.ArrayList<AD> result2;
			    result2 = new java.util.ArrayList<AD>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (AD anElement2 : aClass.getAddr()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim079_5
	* PatientRole SHALL NOT have telecom element with nullFlavor, if there are other telecom elements which are not null (RMIM-079, RIM-018)
	*
	*/
	private static boolean _validatePatientRoleSpec_Rmim079_5(net.ihe.gazelle.cda.POCDMT000040PatientRole aClass){
		java.util.ArrayList<TEL> result1;
		result1 = new java.util.ArrayList<TEL>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (TEL anElement1 : aClass.getTelecom()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getTelecom()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim079_6
	* The telecom elements of PatientRole SHALL be distinct (RMIM-079, RIM-019)
	*
	*/
	private static boolean _validatePatientRoleSpec_Rmim079_6(net.ihe.gazelle.cda.POCDMT000040PatientRole aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (TEL anElement1 : aClass.getTelecom()) {
			    java.util.ArrayList<TEL> result2;
			    result2 = new java.util.ArrayList<TEL>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (TEL anElement2 : aClass.getTelecom()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim080
	* If patientRole has not a null of flavor and its enclosing recordTarget has not a null of flavor,  it SHALL have a patient entity, or a providerOrganization (RMIM-080)
	*
	*/
	private static boolean _validatePatientRoleSpec_Rmim080(net.ihe.gazelle.cda.POCDMT000040PatientRole aClass){
		return (((((!(aClass.getNullFlavor() == null) || (aClass.get__parent() == null)) || !(POCDMT000040RecordTarget.class.isAssignableFrom(aClass.get__parent().getClass()))) || !(((POCDMT000040RecordTarget) aClass.get__parent()).getNullFlavor() == null)) || !(aClass.getPatient() == null)) || !(aClass.getProviderOrganization() == null));
		
	}

	/**
	* Validation of class-constraint : PatientRoleSpec
    * Verify if an element of type PatientRoleSpec can be validated by PatientRoleSpec
	*
	*/
	public static boolean _isPatientRoleSpec(net.ihe.gazelle.cda.POCDMT000040PatientRole aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   PatientRoleSpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040PatientRole
     * 
     */
    public static void _validatePatientRoleSpec(net.ihe.gazelle.cda.POCDMT000040PatientRole aClass, String location, List<Notification> diagnostic) {
		if (_isPatientRoleSpec(aClass)){
			executeCons_PatientRoleSpec_Rmim079_1(aClass, location, diagnostic);
			executeCons_PatientRoleSpec_Rmim079_2(aClass, location, diagnostic);
			executeCons_PatientRoleSpec_Rmim079_3(aClass, location, diagnostic);
			executeCons_PatientRoleSpec_Rmim079_4(aClass, location, diagnostic);
			executeCons_PatientRoleSpec_Rmim079_5(aClass, location, diagnostic);
			executeCons_PatientRoleSpec_Rmim079_6(aClass, location, diagnostic);
			executeCons_PatientRoleSpec_Rmim080(aClass, location, diagnostic);
		}
	}

	private static void executeCons_PatientRoleSpec_Rmim079_1(net.ihe.gazelle.cda.POCDMT000040PatientRole aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validatePatientRoleSpec_Rmim079_1(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim079_1");
		notif.setDescription("PatientRole SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-079, RIM-011)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-PatientRoleSpec-rmim079_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-079;RIM-011"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_PatientRoleSpec_Rmim079_2(net.ihe.gazelle.cda.POCDMT000040PatientRole aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validatePatientRoleSpec_Rmim079_2(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim079_2");
		notif.setDescription("The ids elements of PatientRole SHALL be distinct (RMIM-079, RIM-012)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-PatientRoleSpec-rmim079_2");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-079;RIM-012"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_PatientRoleSpec_Rmim079_3(net.ihe.gazelle.cda.POCDMT000040PatientRole aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validatePatientRoleSpec_Rmim079_3(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim079_3");
		notif.setDescription("PatientRole SHALL NOT have addr element with nullFlavor, if there are other addr (RMIM-079, RIM-016)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-PatientRoleSpec-rmim079_3");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-079;RIM-016"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_PatientRoleSpec_Rmim079_4(net.ihe.gazelle.cda.POCDMT000040PatientRole aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validatePatientRoleSpec_Rmim079_4(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim079_4");
		notif.setDescription("The addr elements of PatientRole SHALL be distinct (RMIM-079, RIM-017)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-PatientRoleSpec-rmim079_4");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-079;RIM-017"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_PatientRoleSpec_Rmim079_5(net.ihe.gazelle.cda.POCDMT000040PatientRole aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validatePatientRoleSpec_Rmim079_5(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim079_5");
		notif.setDescription("PatientRole SHALL NOT have telecom element with nullFlavor, if there are other telecom elements which are not null (RMIM-079, RIM-018)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-PatientRoleSpec-rmim079_5");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-079"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_PatientRoleSpec_Rmim079_6(net.ihe.gazelle.cda.POCDMT000040PatientRole aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validatePatientRoleSpec_Rmim079_6(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim079_6");
		notif.setDescription("The telecom elements of PatientRole SHALL be distinct (RMIM-079, RIM-019)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-PatientRoleSpec-rmim079_6");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-079"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_PatientRoleSpec_Rmim080(net.ihe.gazelle.cda.POCDMT000040PatientRole aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validatePatientRoleSpec_Rmim080(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim080");
		notif.setDescription("If patientRole has not a null of flavor and its enclosing recordTarget has not a null of flavor,  it SHALL have a patient entity, or a providerOrganization (RMIM-080)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-PatientRoleSpec-rmim080");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-080"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

}
