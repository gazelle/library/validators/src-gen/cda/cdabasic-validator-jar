package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.datatypes.CD;
import net.ihe.gazelle.datatypes.II;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;	



 /**
  * class :        SubstanceAdministrationSpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040SubstanceAdministration
  * 
  */
public final class SubstanceAdministrationSpecValidator{


    private SubstanceAdministrationSpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim032_1
	* SubstanceAdministration SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-032, RIM-001)
	*
	*/
	private static boolean _validateSubstanceAdministrationSpec_Rmim032_1(net.ihe.gazelle.cda.POCDMT000040SubstanceAdministration aClass){
		java.util.ArrayList<II> result1;
		result1 = new java.util.ArrayList<II>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getId()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim032_2
	* The ids elements of SubstanceAdministration SHALL be distinct (RMIM-032, RIM-002)
	*
	*/
	private static boolean _validateSubstanceAdministrationSpec_Rmim032_2(net.ihe.gazelle.cda.POCDMT000040SubstanceAdministration aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    java.util.ArrayList<II> result2;
			    result2 = new java.util.ArrayList<II>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (II anElement2 : aClass.getId()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim032_3
	* The statusCode if presents SHALL be from the valueSet ActStatus (RMIM-032, RIM-003)
	*
	*/
	private static boolean _validateSubstanceAdministrationSpec_Rmim032_3(net.ihe.gazelle.cda.POCDMT000040SubstanceAdministration aClass){
		return (((aClass.getStatusCode() == null) || (((String) aClass.getStatusCode().getCode()) == null)) || aClass.getStatusCode().matchesCodeToValueSet("1.3.6.1.4.1.12559.11.18.2", aClass.getStatusCode().getCode()));
		
	}

	/**
	* Validation of instance by a constraint : rmim033
	* The approachSiteCode elements of SubstanceAdministration SHALL be distinct (RMIM-033)
	*
	*/
	private static boolean _validateSubstanceAdministrationSpec_Rmim033(net.ihe.gazelle.cda.POCDMT000040SubstanceAdministration aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (CD anElement1 : aClass.getApproachSiteCode()) {
			    java.util.ArrayList<CD> result2;
			    result2 = new java.util.ArrayList<CD>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (CD anElement2 : aClass.getApproachSiteCode()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim034
	* SubstanceAdministration SHALL NOT have approachSiteCode element with nullFlavor,  if there are other approachSiteCode elements which are not null (RMIM-034)
	*
	*/
	private static boolean _validateSubstanceAdministrationSpec_Rmim034(net.ihe.gazelle.cda.POCDMT000040SubstanceAdministration aClass){
		java.util.ArrayList<CD> result1;
		result1 = new java.util.ArrayList<CD>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (CD anElement1 : aClass.getApproachSiteCode()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getApproachSiteCode()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of class-constraint : SubstanceAdministrationSpec
    * Verify if an element of type SubstanceAdministrationSpec can be validated by SubstanceAdministrationSpec
	*
	*/
	public static boolean _isSubstanceAdministrationSpec(net.ihe.gazelle.cda.POCDMT000040SubstanceAdministration aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   SubstanceAdministrationSpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040SubstanceAdministration
     * 
     */
    public static void _validateSubstanceAdministrationSpec(net.ihe.gazelle.cda.POCDMT000040SubstanceAdministration aClass, String location, List<Notification> diagnostic) {
		if (_isSubstanceAdministrationSpec(aClass)){
			executeCons_SubstanceAdministrationSpec_Rmim032_1(aClass, location, diagnostic);
			executeCons_SubstanceAdministrationSpec_Rmim032_2(aClass, location, diagnostic);
			executeCons_SubstanceAdministrationSpec_Rmim032_3(aClass, location, diagnostic);
			executeCons_SubstanceAdministrationSpec_Rmim033(aClass, location, diagnostic);
			executeCons_SubstanceAdministrationSpec_Rmim034(aClass, location, diagnostic);
		}
	}

	private static void executeCons_SubstanceAdministrationSpec_Rmim032_1(net.ihe.gazelle.cda.POCDMT000040SubstanceAdministration aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateSubstanceAdministrationSpec_Rmim032_1(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim032_1");
		notif.setDescription("SubstanceAdministration SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-032, RIM-001)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-SubstanceAdministrationSpec-rmim032_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-032;RIM-001"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_SubstanceAdministrationSpec_Rmim032_2(net.ihe.gazelle.cda.POCDMT000040SubstanceAdministration aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateSubstanceAdministrationSpec_Rmim032_2(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim032_2");
		notif.setDescription("The ids elements of SubstanceAdministration SHALL be distinct (RMIM-032, RIM-002)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-SubstanceAdministrationSpec-rmim032_2");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-032;RIM-002"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_SubstanceAdministrationSpec_Rmim032_3(net.ihe.gazelle.cda.POCDMT000040SubstanceAdministration aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateSubstanceAdministrationSpec_Rmim032_3(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim032_3");
		notif.setDescription("The statusCode if presents SHALL be from the valueSet ActStatus (RMIM-032, RIM-003)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-SubstanceAdministrationSpec-rmim032_3");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-032"));
		
		notif.setType("Vocabulary");
		diagnostic.add(notif);
	}

	private static void executeCons_SubstanceAdministrationSpec_Rmim033(net.ihe.gazelle.cda.POCDMT000040SubstanceAdministration aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateSubstanceAdministrationSpec_Rmim033(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim033");
		notif.setDescription("The approachSiteCode elements of SubstanceAdministration SHALL be distinct (RMIM-033)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-SubstanceAdministrationSpec-rmim033");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-033"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_SubstanceAdministrationSpec_Rmim034(net.ihe.gazelle.cda.POCDMT000040SubstanceAdministration aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateSubstanceAdministrationSpec_Rmim034(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim034");
		notif.setDescription("SubstanceAdministration SHALL NOT have approachSiteCode element with nullFlavor,  if there are other approachSiteCode elements which are not null (RMIM-034)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-SubstanceAdministrationSpec-rmim034");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-034"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

}
