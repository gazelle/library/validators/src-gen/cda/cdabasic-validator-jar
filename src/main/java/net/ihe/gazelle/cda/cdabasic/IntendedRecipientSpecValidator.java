package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.cda.POCDMT000040InformationRecipient;
import net.ihe.gazelle.datatypes.AD;
import net.ihe.gazelle.datatypes.II;
import net.ihe.gazelle.datatypes.TEL;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.voc.XInformationRecipientRole;	



 /**
  * class :        IntendedRecipientSpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040IntendedRecipient
  * 
  */
public final class IntendedRecipientSpecValidator{


    private IntendedRecipientSpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim074_1
	* IntendedRecipient SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-074, RIM-011)
	*
	*/
	private static boolean _validateIntendedRecipientSpec_Rmim074_1(net.ihe.gazelle.cda.POCDMT000040IntendedRecipient aClass){
		java.util.ArrayList<II> result1;
		result1 = new java.util.ArrayList<II>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getId()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim074_2
	* The ids elements of IntendedRecipient SHALL be distinct (RMIM-074, RIM-012)
	*
	*/
	private static boolean _validateIntendedRecipientSpec_Rmim074_2(net.ihe.gazelle.cda.POCDMT000040IntendedRecipient aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    java.util.ArrayList<II> result2;
			    result2 = new java.util.ArrayList<II>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (II anElement2 : aClass.getId()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim074_3
	* IntendedRecipient SHALL NOT have addr element with nullFlavor, if there are other addr (RMIM-074, RIM-016)
	*
	*/
	private static boolean _validateIntendedRecipientSpec_Rmim074_3(net.ihe.gazelle.cda.POCDMT000040IntendedRecipient aClass){
		java.util.ArrayList<AD> result1;
		result1 = new java.util.ArrayList<AD>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (AD anElement1 : aClass.getAddr()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAddr()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim074_4
	* The addr elements of IntendedRecipient SHALL be distinct (RMIM-074, RIM-017)
	*
	*/
	private static boolean _validateIntendedRecipientSpec_Rmim074_4(net.ihe.gazelle.cda.POCDMT000040IntendedRecipient aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (AD anElement1 : aClass.getAddr()) {
			    java.util.ArrayList<AD> result2;
			    result2 = new java.util.ArrayList<AD>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (AD anElement2 : aClass.getAddr()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim074_5
	* IntendedRecipient SHALL NOT have telecom element with nullFlavor, if there are other (RMIM-074, RIM-018)
	*
	*/
	private static boolean _validateIntendedRecipientSpec_Rmim074_5(net.ihe.gazelle.cda.POCDMT000040IntendedRecipient aClass){
		java.util.ArrayList<TEL> result1;
		result1 = new java.util.ArrayList<TEL>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (TEL anElement1 : aClass.getTelecom()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getTelecom()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim074_6
	* The telecom elements of IntendedRecipient SHALL be distinct (RMIM-074, RIM-019)
	*
	*/
	private static boolean _validateIntendedRecipientSpec_Rmim074_6(net.ihe.gazelle.cda.POCDMT000040IntendedRecipient aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (TEL anElement1 : aClass.getTelecom()) {
			    java.util.ArrayList<TEL> result2;
			    result2 = new java.util.ArrayList<TEL>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (TEL anElement2 : aClass.getTelecom()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim075
	* If intendedRecipient has not a null of flavor and its enclosing informationRecipient has not a null of flavor,  it SHALL have an informationRecipient or a receivedOrganization (RMIM-075)
	*
	*/
	private static boolean _validateIntendedRecipientSpec_Rmim075(net.ihe.gazelle.cda.POCDMT000040IntendedRecipient aClass){
		return (((((!(aClass.getNullFlavor() == null) || (aClass.get__parent() == null)) || !(POCDMT000040InformationRecipient.class.isAssignableFrom(aClass.get__parent().getClass()))) || !(((POCDMT000040InformationRecipient) aClass.get__parent()).getNullFlavor() == null)) || !(aClass.getInformationRecipient() == null)) || !(aClass.getReceivedOrganization() == null));
		
	}

	/**
	* Validation of instance by a constraint : rmim076
	* When intendedRecipient=HLTHCHRT, the informationRecipient SHALL NOT be provided (RMIM-076)
	*
	*/
	private static boolean _validateIntendedRecipientSpec_Rmim076(net.ihe.gazelle.cda.POCDMT000040IntendedRecipient aClass){
		return (((aClass.getClassCode() == null) || !aClass.getClassCode().equals(XInformationRecipientRole.HLTHCHRT)) || (aClass.getInformationRecipient() == null));
		
	}

	/**
	* Validation of class-constraint : IntendedRecipientSpec
    * Verify if an element of type IntendedRecipientSpec can be validated by IntendedRecipientSpec
	*
	*/
	public static boolean _isIntendedRecipientSpec(net.ihe.gazelle.cda.POCDMT000040IntendedRecipient aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   IntendedRecipientSpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040IntendedRecipient
     * 
     */
    public static void _validateIntendedRecipientSpec(net.ihe.gazelle.cda.POCDMT000040IntendedRecipient aClass, String location, List<Notification> diagnostic) {
		if (_isIntendedRecipientSpec(aClass)){
			executeCons_IntendedRecipientSpec_Rmim074_1(aClass, location, diagnostic);
			executeCons_IntendedRecipientSpec_Rmim074_2(aClass, location, diagnostic);
			executeCons_IntendedRecipientSpec_Rmim074_3(aClass, location, diagnostic);
			executeCons_IntendedRecipientSpec_Rmim074_4(aClass, location, diagnostic);
			executeCons_IntendedRecipientSpec_Rmim074_5(aClass, location, diagnostic);
			executeCons_IntendedRecipientSpec_Rmim074_6(aClass, location, diagnostic);
			executeCons_IntendedRecipientSpec_Rmim075(aClass, location, diagnostic);
			executeCons_IntendedRecipientSpec_Rmim076(aClass, location, diagnostic);
		}
	}

	private static void executeCons_IntendedRecipientSpec_Rmim074_1(net.ihe.gazelle.cda.POCDMT000040IntendedRecipient aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateIntendedRecipientSpec_Rmim074_1(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim074_1");
		notif.setDescription("IntendedRecipient SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-074, RIM-011)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-IntendedRecipientSpec-rmim074_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-074;RIM-011"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_IntendedRecipientSpec_Rmim074_2(net.ihe.gazelle.cda.POCDMT000040IntendedRecipient aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateIntendedRecipientSpec_Rmim074_2(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim074_2");
		notif.setDescription("The ids elements of IntendedRecipient SHALL be distinct (RMIM-074, RIM-012)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-IntendedRecipientSpec-rmim074_2");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-074;RIM-012"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_IntendedRecipientSpec_Rmim074_3(net.ihe.gazelle.cda.POCDMT000040IntendedRecipient aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateIntendedRecipientSpec_Rmim074_3(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim074_3");
		notif.setDescription("IntendedRecipient SHALL NOT have addr element with nullFlavor, if there are other addr (RMIM-074, RIM-016)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-IntendedRecipientSpec-rmim074_3");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-074;RIM-016"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_IntendedRecipientSpec_Rmim074_4(net.ihe.gazelle.cda.POCDMT000040IntendedRecipient aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateIntendedRecipientSpec_Rmim074_4(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim074_4");
		notif.setDescription("The addr elements of IntendedRecipient SHALL be distinct (RMIM-074, RIM-017)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-IntendedRecipientSpec-rmim074_4");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-074;RIM-017"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_IntendedRecipientSpec_Rmim074_5(net.ihe.gazelle.cda.POCDMT000040IntendedRecipient aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateIntendedRecipientSpec_Rmim074_5(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim074_5");
		notif.setDescription("IntendedRecipient SHALL NOT have telecom element with nullFlavor, if there are other (RMIM-074, RIM-018)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-IntendedRecipientSpec-rmim074_5");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-074"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_IntendedRecipientSpec_Rmim074_6(net.ihe.gazelle.cda.POCDMT000040IntendedRecipient aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateIntendedRecipientSpec_Rmim074_6(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim074_6");
		notif.setDescription("The telecom elements of IntendedRecipient SHALL be distinct (RMIM-074, RIM-019)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-IntendedRecipientSpec-rmim074_6");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-074"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_IntendedRecipientSpec_Rmim075(net.ihe.gazelle.cda.POCDMT000040IntendedRecipient aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateIntendedRecipientSpec_Rmim075(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim075");
		notif.setDescription("If intendedRecipient has not a null of flavor and its enclosing informationRecipient has not a null of flavor,  it SHALL have an informationRecipient or a receivedOrganization (RMIM-075)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-IntendedRecipientSpec-rmim075");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-075"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_IntendedRecipientSpec_Rmim076(net.ihe.gazelle.cda.POCDMT000040IntendedRecipient aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateIntendedRecipientSpec_Rmim076(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim076");
		notif.setDescription("When intendedRecipient=HLTHCHRT, the informationRecipient SHALL NOT be provided (RMIM-076)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-IntendedRecipientSpec-rmim076");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-076"));
		
		notif.setType("Cardinality");
		diagnostic.add(notif);
	}

}
