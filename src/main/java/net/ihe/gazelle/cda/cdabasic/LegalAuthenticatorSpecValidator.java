package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validation.Warning;	



 /**
  * class :        LegalAuthenticatorSpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040LegalAuthenticator
  * 
  */
public final class LegalAuthenticatorSpecValidator{


    private LegalAuthenticatorSpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim069_1
	* The signatureCode elements of LegalAuthenticator SHALL be from the valueSet ParticipationSignature (RMIM-069, RIM-010)
	*
	*/
	private static boolean _validateLegalAuthenticatorSpec_Rmim069_1(net.ihe.gazelle.cda.POCDMT000040LegalAuthenticator aClass){
		return (((aClass.getSignatureCode() == null) || !(aClass.getSignatureCode().getNullFlavor() == null)) || aClass.getSignatureCode().matchesCodeToValueSet("1.3.6.1.4.1.12559.11.18.4", aClass.getSignatureCode().getCode()));
		
	}

	/**
	* Validation of instance by a constraint : rmim070
	* The legalAuthenticator.signatureCode elements SHOULD NOT have the value X (RMIM-070)
	*
	*/
	private static boolean _validateLegalAuthenticatorSpec_Rmim070(net.ihe.gazelle.cda.POCDMT000040LegalAuthenticator aClass){
		return (((aClass.getSignatureCode() == null) || (((String) aClass.getSignatureCode().getCode()) == null)) || !aClass.getSignatureCode().getCode().equals("X"));
		
	}

	/**
	* Validation of class-constraint : LegalAuthenticatorSpec
    * Verify if an element of type LegalAuthenticatorSpec can be validated by LegalAuthenticatorSpec
	*
	*/
	public static boolean _isLegalAuthenticatorSpec(net.ihe.gazelle.cda.POCDMT000040LegalAuthenticator aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   LegalAuthenticatorSpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040LegalAuthenticator
     * 
     */
    public static void _validateLegalAuthenticatorSpec(net.ihe.gazelle.cda.POCDMT000040LegalAuthenticator aClass, String location, List<Notification> diagnostic) {
		if (_isLegalAuthenticatorSpec(aClass)){
			executeCons_LegalAuthenticatorSpec_Rmim069_1(aClass, location, diagnostic);
			executeCons_LegalAuthenticatorSpec_Rmim070(aClass, location, diagnostic);
		}
	}

	private static void executeCons_LegalAuthenticatorSpec_Rmim069_1(net.ihe.gazelle.cda.POCDMT000040LegalAuthenticator aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateLegalAuthenticatorSpec_Rmim069_1(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim069_1");
		notif.setDescription("The signatureCode elements of LegalAuthenticator SHALL be from the valueSet ParticipationSignature (RMIM-069, RIM-010)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-LegalAuthenticatorSpec-rmim069_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-069;RIM-010"));
		
		notif.setType("Vocabulary");
		diagnostic.add(notif);
	}

	private static void executeCons_LegalAuthenticatorSpec_Rmim070(net.ihe.gazelle.cda.POCDMT000040LegalAuthenticator aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateLegalAuthenticatorSpec_Rmim070(aClass) )){
				notif = new Warning();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim070");
		notif.setDescription("The legalAuthenticator.signatureCode elements SHOULD NOT have the value X (RMIM-070)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-LegalAuthenticatorSpec-rmim070");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-070"));
		
		notif.setType("Fixed value");
		diagnostic.add(notif);
	}

}
