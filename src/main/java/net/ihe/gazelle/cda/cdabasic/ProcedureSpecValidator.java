package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.datatypes.CD;
import net.ihe.gazelle.datatypes.CE;
import net.ihe.gazelle.datatypes.II;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;	



 /**
  * class :        ProcedureSpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040Procedure
  * 
  */
public final class ProcedureSpecValidator{


    private ProcedureSpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim014_1
	* Procedure SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-014, RIM-001)
	*
	*/
	private static boolean _validateProcedureSpec_Rmim014_1(net.ihe.gazelle.cda.POCDMT000040Procedure aClass){
		java.util.ArrayList<II> result1;
		result1 = new java.util.ArrayList<II>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getId()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim014_2
	* The ids elements of Procedure SHALL be distinct (RMIM-014, RIM-002)
	*
	*/
	private static boolean _validateProcedureSpec_Rmim014_2(net.ihe.gazelle.cda.POCDMT000040Procedure aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    java.util.ArrayList<II> result2;
			    result2 = new java.util.ArrayList<II>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (II anElement2 : aClass.getId()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim014_3
	* The statusCode if presents SHALL be from the valueSet ActStatus (RMIM-014, RIM-003)
	*
	*/
	private static boolean _validateProcedureSpec_Rmim014_3(net.ihe.gazelle.cda.POCDMT000040Procedure aClass){
		return (((aClass.getStatusCode() == null) || (((String) aClass.getStatusCode().getCode()) == null)) || aClass.getStatusCode().matchesCodeToValueSet("1.3.6.1.4.1.12559.11.18.2", aClass.getStatusCode().getCode()));
		
	}

	/**
	* Validation of instance by a constraint : rmim015
	* The languageCode shall be from the valueSet HumanLanguage (RMIM-015)
	*
	*/
	private static boolean _validateProcedureSpec_Rmim015(net.ihe.gazelle.cda.POCDMT000040Procedure aClass){
		return (((aClass.getLanguageCode() == null) || !(aClass.getLanguageCode().getNullFlavor() == null)) || (!(((String) aClass.getLanguageCode().getCode()) == null) && aClass.getLanguageCode().matches(aClass.getLanguageCode().getCode(), "\\w+(\\-\\w+)?")));
		
	}

	/**
	* Validation of instance by a constraint : rmim017
	* Procedure SHALL NOT have methodCode element with nullFlavor,  if there are other methodCode elements which are not null (RMIM-017)
	*
	*/
	private static boolean _validateProcedureSpec_Rmim017(net.ihe.gazelle.cda.POCDMT000040Procedure aClass){
		java.util.ArrayList<CE> result1;
		result1 = new java.util.ArrayList<CE>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (CE anElement1 : aClass.getMethodCode()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getMethodCode()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim018
	* The methodCode elements of Procedure SHALL be distinct (RMIM-018)
	*
	*/
	private static boolean _validateProcedureSpec_Rmim018(net.ihe.gazelle.cda.POCDMT000040Procedure aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (CE anElement1 : aClass.getMethodCode()) {
			    java.util.ArrayList<CE> result2;
			    result2 = new java.util.ArrayList<CE>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (CE anElement2 : aClass.getMethodCode()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim019
	* Procedure SHALL NOT have approachSiteCode element with nullFlavor,  if there are other approachSiteCode elements which are not null (RMIM-019)
	*
	*/
	private static boolean _validateProcedureSpec_Rmim019(net.ihe.gazelle.cda.POCDMT000040Procedure aClass){
		java.util.ArrayList<CD> result1;
		result1 = new java.util.ArrayList<CD>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (CD anElement1 : aClass.getApproachSiteCode()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getApproachSiteCode()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim020
	* The approachSiteCode elements of Procedure SHALL be distinct (RMIM-020)
	*
	*/
	private static boolean _validateProcedureSpec_Rmim020(net.ihe.gazelle.cda.POCDMT000040Procedure aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (CD anElement1 : aClass.getApproachSiteCode()) {
			    java.util.ArrayList<CD> result2;
			    result2 = new java.util.ArrayList<CD>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (CD anElement2 : aClass.getApproachSiteCode()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim021
	* Procedure SHALL NOT have targetSiteCode element with nullFlavor,  if there are other targetSiteCode elements which are not null (RMIM-021)
	*
	*/
	private static boolean _validateProcedureSpec_Rmim021(net.ihe.gazelle.cda.POCDMT000040Procedure aClass){
		java.util.ArrayList<CD> result1;
		result1 = new java.util.ArrayList<CD>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (CD anElement1 : aClass.getTargetSiteCode()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getTargetSiteCode()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim022
	* The targetSiteCode elements of Procedure SHALL be distinct (RMIM-022)
	*
	*/
	private static boolean _validateProcedureSpec_Rmim022(net.ihe.gazelle.cda.POCDMT000040Procedure aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (CD anElement1 : aClass.getTargetSiteCode()) {
			    java.util.ArrayList<CD> result2;
			    result2 = new java.util.ArrayList<CD>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (CD anElement2 : aClass.getTargetSiteCode()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of class-constraint : ProcedureSpec
    * Verify if an element of type ProcedureSpec can be validated by ProcedureSpec
	*
	*/
	public static boolean _isProcedureSpec(net.ihe.gazelle.cda.POCDMT000040Procedure aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   ProcedureSpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040Procedure
     * 
     */
    public static void _validateProcedureSpec(net.ihe.gazelle.cda.POCDMT000040Procedure aClass, String location, List<Notification> diagnostic) {
		if (_isProcedureSpec(aClass)){
			executeCons_ProcedureSpec_Rmim014_1(aClass, location, diagnostic);
			executeCons_ProcedureSpec_Rmim014_2(aClass, location, diagnostic);
			executeCons_ProcedureSpec_Rmim014_3(aClass, location, diagnostic);
			executeCons_ProcedureSpec_Rmim015(aClass, location, diagnostic);
			executeCons_ProcedureSpec_Rmim017(aClass, location, diagnostic);
			executeCons_ProcedureSpec_Rmim018(aClass, location, diagnostic);
			executeCons_ProcedureSpec_Rmim019(aClass, location, diagnostic);
			executeCons_ProcedureSpec_Rmim020(aClass, location, diagnostic);
			executeCons_ProcedureSpec_Rmim021(aClass, location, diagnostic);
			executeCons_ProcedureSpec_Rmim022(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ProcedureSpec_Rmim014_1(net.ihe.gazelle.cda.POCDMT000040Procedure aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateProcedureSpec_Rmim014_1(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim014_1");
		notif.setDescription("Procedure SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-014, RIM-001)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ProcedureSpec-rmim014_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-014;RIM-001"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_ProcedureSpec_Rmim014_2(net.ihe.gazelle.cda.POCDMT000040Procedure aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateProcedureSpec_Rmim014_2(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim014_2");
		notif.setDescription("The ids elements of Procedure SHALL be distinct (RMIM-014, RIM-002)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ProcedureSpec-rmim014_2");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-014;RIM-002"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_ProcedureSpec_Rmim014_3(net.ihe.gazelle.cda.POCDMT000040Procedure aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateProcedureSpec_Rmim014_3(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim014_3");
		notif.setDescription("The statusCode if presents SHALL be from the valueSet ActStatus (RMIM-014, RIM-003)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ProcedureSpec-rmim014_3");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-014;RIM-003"));
		
		notif.setType("Vocabulary");
		diagnostic.add(notif);
	}

	private static void executeCons_ProcedureSpec_Rmim015(net.ihe.gazelle.cda.POCDMT000040Procedure aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateProcedureSpec_Rmim015(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim015");
		notif.setDescription("The languageCode shall be from the valueSet HumanLanguage (RMIM-015)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ProcedureSpec-rmim015");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-015"));
		
		notif.setType("Vocabulary");
		diagnostic.add(notif);
	}

	private static void executeCons_ProcedureSpec_Rmim017(net.ihe.gazelle.cda.POCDMT000040Procedure aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateProcedureSpec_Rmim017(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim017");
		notif.setDescription("Procedure SHALL NOT have methodCode element with nullFlavor,  if there are other methodCode elements which are not null (RMIM-017)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ProcedureSpec-rmim017");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-017"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_ProcedureSpec_Rmim018(net.ihe.gazelle.cda.POCDMT000040Procedure aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateProcedureSpec_Rmim018(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim018");
		notif.setDescription("The methodCode elements of Procedure SHALL be distinct (RMIM-018)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ProcedureSpec-rmim018");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-018"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_ProcedureSpec_Rmim019(net.ihe.gazelle.cda.POCDMT000040Procedure aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateProcedureSpec_Rmim019(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim019");
		notif.setDescription("Procedure SHALL NOT have approachSiteCode element with nullFlavor,  if there are other approachSiteCode elements which are not null (RMIM-019)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ProcedureSpec-rmim019");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-019"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_ProcedureSpec_Rmim020(net.ihe.gazelle.cda.POCDMT000040Procedure aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateProcedureSpec_Rmim020(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim020");
		notif.setDescription("The approachSiteCode elements of Procedure SHALL be distinct (RMIM-020)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ProcedureSpec-rmim020");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-020"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_ProcedureSpec_Rmim021(net.ihe.gazelle.cda.POCDMT000040Procedure aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateProcedureSpec_Rmim021(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim021");
		notif.setDescription("Procedure SHALL NOT have targetSiteCode element with nullFlavor,  if there are other targetSiteCode elements which are not null (RMIM-021)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ProcedureSpec-rmim021");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-021"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_ProcedureSpec_Rmim022(net.ihe.gazelle.cda.POCDMT000040Procedure aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateProcedureSpec_Rmim022(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim022");
		notif.setDescription("The targetSiteCode elements of Procedure SHALL be distinct (RMIM-022)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ProcedureSpec-rmim022");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-022"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

}
