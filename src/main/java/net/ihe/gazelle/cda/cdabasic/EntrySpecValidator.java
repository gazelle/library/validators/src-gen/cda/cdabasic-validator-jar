package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.cda.POCDMT000040Section;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.voc.XActRelationshipEntry;	



 /**
  * class :        EntrySpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040Entry
  * 
  */
public final class EntrySpecValidator{


    private EntrySpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim066
	* When entry@typeCode=DRIV, the section containing the entry SHALL contain a text element (RMIM-066)
	*
	*/
	private static boolean _validateEntrySpec_Rmim066(net.ihe.gazelle.cda.POCDMT000040Entry aClass){
		return ((((aClass.getTypeCode() == null) || !aClass.getTypeCode().equals(XActRelationshipEntry.DRIV)) || (aClass.get__parent() == null)) || ((POCDMT000040Section.class.isAssignableFrom(aClass.get__parent().getClass())) && !(((POCDMT000040Section) aClass.get__parent()).getText() == null)));
		
	}

	/**
	* Validation of class-constraint : EntrySpec
    * Verify if an element of type EntrySpec can be validated by EntrySpec
	*
	*/
	public static boolean _isEntrySpec(net.ihe.gazelle.cda.POCDMT000040Entry aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   EntrySpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040Entry
     * 
     */
    public static void _validateEntrySpec(net.ihe.gazelle.cda.POCDMT000040Entry aClass, String location, List<Notification> diagnostic) {
		if (_isEntrySpec(aClass)){
			executeCons_EntrySpec_Rmim066(aClass, location, diagnostic);
		}
	}

	private static void executeCons_EntrySpec_Rmim066(net.ihe.gazelle.cda.POCDMT000040Entry aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateEntrySpec_Rmim066(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim066");
		notif.setDescription("When entry@typeCode=DRIV, the section containing the entry SHALL contain a text element (RMIM-066)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-EntrySpec-rmim066");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-066"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

}
