package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.datatypes.CE;
import net.ihe.gazelle.datatypes.II;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;	



 /**
  * class :        SupplySpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040Supply
  * 
  */
public final class SupplySpecValidator{


    private SupplySpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim035_1
	* Supply SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-035, RIM-001)
	*
	*/
	private static boolean _validateSupplySpec_Rmim035_1(net.ihe.gazelle.cda.POCDMT000040Supply aClass){
		java.util.ArrayList<II> result1;
		result1 = new java.util.ArrayList<II>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getId()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim035_2
	* The ids elements of Supply SHALL be distinct (RMIM-035, RIM-002)
	*
	*/
	private static boolean _validateSupplySpec_Rmim035_2(net.ihe.gazelle.cda.POCDMT000040Supply aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    java.util.ArrayList<II> result2;
			    result2 = new java.util.ArrayList<II>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (II anElement2 : aClass.getId()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim035_3
	* The statusCode if presents SHALL be from the valueSet ActStatus (RMIM-035, RIM-003)
	*
	*/
	private static boolean _validateSupplySpec_Rmim035_3(net.ihe.gazelle.cda.POCDMT000040Supply aClass){
		return (((aClass.getStatusCode() == null) || (((String) aClass.getStatusCode().getCode()) == null)) || aClass.getStatusCode().matchesCodeToValueSet("1.3.6.1.4.1.12559.11.18.2", aClass.getStatusCode().getCode()));
		
	}

	/**
	* Validation of instance by a constraint : rmim036
	* The priorityCode elements of Supply SHALL be distinct (RMIM-036)
	*
	*/
	private static boolean _validateSupplySpec_Rmim036(net.ihe.gazelle.cda.POCDMT000040Supply aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (CE anElement1 : aClass.getPriorityCode()) {
			    java.util.ArrayList<CE> result2;
			    result2 = new java.util.ArrayList<CE>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (CE anElement2 : aClass.getPriorityCode()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim037
	* Supply SHALL NOT have priorityCode element with nullFlavor,  if there are other priorityCode elements which are not null (RMIM-037)
	*
	*/
	private static boolean _validateSupplySpec_Rmim037(net.ihe.gazelle.cda.POCDMT000040Supply aClass){
		java.util.ArrayList<CE> result1;
		result1 = new java.util.ArrayList<CE>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (CE anElement1 : aClass.getPriorityCode()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getPriorityCode()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of class-constraint : SupplySpec
    * Verify if an element of type SupplySpec can be validated by SupplySpec
	*
	*/
	public static boolean _isSupplySpec(net.ihe.gazelle.cda.POCDMT000040Supply aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   SupplySpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040Supply
     * 
     */
    public static void _validateSupplySpec(net.ihe.gazelle.cda.POCDMT000040Supply aClass, String location, List<Notification> diagnostic) {
		if (_isSupplySpec(aClass)){
			executeCons_SupplySpec_Rmim035_1(aClass, location, diagnostic);
			executeCons_SupplySpec_Rmim035_2(aClass, location, diagnostic);
			executeCons_SupplySpec_Rmim035_3(aClass, location, diagnostic);
			executeCons_SupplySpec_Rmim036(aClass, location, diagnostic);
			executeCons_SupplySpec_Rmim037(aClass, location, diagnostic);
		}
	}

	private static void executeCons_SupplySpec_Rmim035_1(net.ihe.gazelle.cda.POCDMT000040Supply aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateSupplySpec_Rmim035_1(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim035_1");
		notif.setDescription("Supply SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-035, RIM-001)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-SupplySpec-rmim035_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-035;RIM-001"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_SupplySpec_Rmim035_2(net.ihe.gazelle.cda.POCDMT000040Supply aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateSupplySpec_Rmim035_2(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim035_2");
		notif.setDescription("The ids elements of Supply SHALL be distinct (RMIM-035, RIM-002)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-SupplySpec-rmim035_2");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-035;RIM-002"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_SupplySpec_Rmim035_3(net.ihe.gazelle.cda.POCDMT000040Supply aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateSupplySpec_Rmim035_3(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim035_3");
		notif.setDescription("The statusCode if presents SHALL be from the valueSet ActStatus (RMIM-035, RIM-003)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-SupplySpec-rmim035_3");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-035;RIM-003"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_SupplySpec_Rmim036(net.ihe.gazelle.cda.POCDMT000040Supply aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateSupplySpec_Rmim036(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim036");
		notif.setDescription("The priorityCode elements of Supply SHALL be distinct (RMIM-036)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-SupplySpec-rmim036");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-036"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_SupplySpec_Rmim037(net.ihe.gazelle.cda.POCDMT000040Supply aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateSupplySpec_Rmim037(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim037");
		notif.setDescription("Supply SHALL NOT have priorityCode element with nullFlavor,  if there are other priorityCode elements which are not null (RMIM-037)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-SupplySpec-rmim037");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-037"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

}
