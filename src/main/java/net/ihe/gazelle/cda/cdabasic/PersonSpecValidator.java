package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.datatypes.PN;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;	



 /**
  * class :        PersonSpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040Person
  * 
  */
public final class PersonSpecValidator{


    private PersonSpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim097_1
	* Person SHALL NOT have name element with nullFlavor, if there are other name (RMIM-097, RIM-022)
	*
	*/
	private static boolean _validatePersonSpec_Rmim097_1(net.ihe.gazelle.cda.POCDMT000040Person aClass){
		java.util.ArrayList<PN> result1;
		result1 = new java.util.ArrayList<PN>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (PN anElement1 : aClass.getName()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getName()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim097_2
	* The name elements of Person SHALL be distinct (RMIM-097, RIM-023)
	*
	*/
	private static boolean _validatePersonSpec_Rmim097_2(net.ihe.gazelle.cda.POCDMT000040Person aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (PN anElement1 : aClass.getName()) {
			    java.util.ArrayList<PN> result2;
			    result2 = new java.util.ArrayList<PN>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (PN anElement2 : aClass.getName()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of class-constraint : PersonSpec
    * Verify if an element of type PersonSpec can be validated by PersonSpec
	*
	*/
	public static boolean _isPersonSpec(net.ihe.gazelle.cda.POCDMT000040Person aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   PersonSpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040Person
     * 
     */
    public static void _validatePersonSpec(net.ihe.gazelle.cda.POCDMT000040Person aClass, String location, List<Notification> diagnostic) {
		if (_isPersonSpec(aClass)){
			executeCons_PersonSpec_Rmim097_1(aClass, location, diagnostic);
			executeCons_PersonSpec_Rmim097_2(aClass, location, diagnostic);
		}
	}

	private static void executeCons_PersonSpec_Rmim097_1(net.ihe.gazelle.cda.POCDMT000040Person aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validatePersonSpec_Rmim097_1(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim097_1");
		notif.setDescription("Person SHALL NOT have name element with nullFlavor, if there are other name (RMIM-097, RIM-022)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-PersonSpec-rmim097_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-097;RIM-022"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_PersonSpec_Rmim097_2(net.ihe.gazelle.cda.POCDMT000040Person aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validatePersonSpec_Rmim097_2(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim097_2");
		notif.setDescription("The name elements of Person SHALL be distinct (RMIM-097, RIM-023)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-PersonSpec-rmim097_2");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-097;RIM-023"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

}
