package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.datatypes.ANY;
import net.ihe.gazelle.datatypes.CS;
import net.ihe.gazelle.datatypes.ST;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;	



 /**
  * class :        CriterionSpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040Criterion
  * 
  */
public final class CriterionSpecValidator{


    private CriterionSpecValidator() {}

public static Boolean validateValueIsNotNumeric(ANY value) {
	// Start of user code for method validateValueIsNotNumeric
	if (value instanceof ST){
		ST st = (ST)value;
		List<String> lst = st.getListStringValues();
		if (lst.size()>1){
			return true;
		}
		if (lst.size()==1){
			try{
				Double dl = Double.valueOf(lst.get(0));
				return false;
			}
			catch(Exception e){
				return true;
			}
		}
	}
	return true;
	// End of user code
}


	/**
	* Validation of instance by a constraint : rmim041
	* Numeric values SHALL NOT be communicated in Criterion/value as a simple String (RMIM-041)
	*
	*/
	private static boolean _validateCriterionSpec_Rmim041(net.ihe.gazelle.cda.POCDMT000040Criterion aClass){
		return (((aClass.getValue() == null) || !aClass.getValue().getClass().getCanonicalName().equals(ST.class.getCanonicalName())) || CriterionSpecValidator.validateValueIsNotNumeric(aClass.getValue()));
		
	}

	/**
	* Validation of instance by a constraint : rmim042
	* CS datatype SHALL NOT be used in value element, because only datatypes with (RMIM-042)
	*
	*/
	private static boolean _validateCriterionSpec_Rmim042(net.ihe.gazelle.cda.POCDMT000040Criterion aClass){
		return ((aClass.getValue() == null) || !(CS.class.isAssignableFrom(aClass.getValue().getClass())));
		
	}

	/**
	* Validation of class-constraint : CriterionSpec
    * Verify if an element of type CriterionSpec can be validated by CriterionSpec
	*
	*/
	public static boolean _isCriterionSpec(net.ihe.gazelle.cda.POCDMT000040Criterion aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   CriterionSpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040Criterion
     * 
     */
    public static void _validateCriterionSpec(net.ihe.gazelle.cda.POCDMT000040Criterion aClass, String location, List<Notification> diagnostic) {
		if (_isCriterionSpec(aClass)){
			executeCons_CriterionSpec_Rmim041(aClass, location, diagnostic);
			executeCons_CriterionSpec_Rmim042(aClass, location, diagnostic);
		}
	}

	private static void executeCons_CriterionSpec_Rmim041(net.ihe.gazelle.cda.POCDMT000040Criterion aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCriterionSpec_Rmim041(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim041");
		notif.setDescription("Numeric values SHALL NOT be communicated in Criterion/value as a simple String (RMIM-041)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-CriterionSpec-rmim041");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-041"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

	private static void executeCons_CriterionSpec_Rmim042(net.ihe.gazelle.cda.POCDMT000040Criterion aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCriterionSpec_Rmim042(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim042");
		notif.setDescription("CS datatype SHALL NOT be used in value element, because only datatypes with (RMIM-042)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-CriterionSpec-rmim042");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-042"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

}
