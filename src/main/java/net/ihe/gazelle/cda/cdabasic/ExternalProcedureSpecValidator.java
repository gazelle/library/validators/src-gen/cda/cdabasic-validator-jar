package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.datatypes.II;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;	



 /**
  * class :        ExternalProcedureSpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040ExternalProcedure
  * 
  */
public final class ExternalProcedureSpecValidator{


    private ExternalProcedureSpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim045_1
	* ExternalProcedure SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-045, RIM-001)
	*
	*/
	private static boolean _validateExternalProcedureSpec_Rmim045_1(net.ihe.gazelle.cda.POCDMT000040ExternalProcedure aClass){
		java.util.ArrayList<II> result1;
		result1 = new java.util.ArrayList<II>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getId()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim045_2
	* The ids elements of ExternalProcedure SHALL be distinct (RMIM-045, RIM-002)
	*
	*/
	private static boolean _validateExternalProcedureSpec_Rmim045_2(net.ihe.gazelle.cda.POCDMT000040ExternalProcedure aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    java.util.ArrayList<II> result2;
			    result2 = new java.util.ArrayList<II>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (II anElement2 : aClass.getId()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of class-constraint : ExternalProcedureSpec
    * Verify if an element of type ExternalProcedureSpec can be validated by ExternalProcedureSpec
	*
	*/
	public static boolean _isExternalProcedureSpec(net.ihe.gazelle.cda.POCDMT000040ExternalProcedure aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   ExternalProcedureSpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040ExternalProcedure
     * 
     */
    public static void _validateExternalProcedureSpec(net.ihe.gazelle.cda.POCDMT000040ExternalProcedure aClass, String location, List<Notification> diagnostic) {
		if (_isExternalProcedureSpec(aClass)){
			executeCons_ExternalProcedureSpec_Rmim045_1(aClass, location, diagnostic);
			executeCons_ExternalProcedureSpec_Rmim045_2(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ExternalProcedureSpec_Rmim045_1(net.ihe.gazelle.cda.POCDMT000040ExternalProcedure aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateExternalProcedureSpec_Rmim045_1(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim045_1");
		notif.setDescription("ExternalProcedure SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-045, RIM-001)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ExternalProcedureSpec-rmim045_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-045;RIM-001"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_ExternalProcedureSpec_Rmim045_2(net.ihe.gazelle.cda.POCDMT000040ExternalProcedure aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateExternalProcedureSpec_Rmim045_2(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim045_2");
		notif.setDescription("The ids elements of ExternalProcedure SHALL be distinct (RMIM-045, RIM-002)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ExternalProcedureSpec-rmim045_2");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-045;RIM-002"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

}
