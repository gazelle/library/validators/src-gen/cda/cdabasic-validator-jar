package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.datatypes.AD;
import net.ihe.gazelle.datatypes.II;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;	



 /**
  * class :        AssignedAuthorSpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040AssignedAuthor
  * 
  */
public final class AssignedAuthorSpecValidator{


    private AssignedAuthorSpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim077_1
	* AssignedAuthor SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-077, RIM-011)
	*
	*/
	private static boolean _validateAssignedAuthorSpec_Rmim077_1(net.ihe.gazelle.cda.POCDMT000040AssignedAuthor aClass){
		java.util.ArrayList<II> result1;
		result1 = new java.util.ArrayList<II>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getId()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim077_2
	* The ids elements of AssignedAuthor SHALL be distinct (RMIM-077, RIM-012)
	*
	*/
	private static boolean _validateAssignedAuthorSpec_Rmim077_2(net.ihe.gazelle.cda.POCDMT000040AssignedAuthor aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    java.util.ArrayList<II> result2;
			    result2 = new java.util.ArrayList<II>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (II anElement2 : aClass.getId()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim077_3
	* AssignedAuthor SHALL NOT have addr element with nullFlavor, if there are other addr (RMIM-077, RIM-016)
	*
	*/
	private static boolean _validateAssignedAuthorSpec_Rmim077_3(net.ihe.gazelle.cda.POCDMT000040AssignedAuthor aClass){
		java.util.ArrayList<AD> result1;
		result1 = new java.util.ArrayList<AD>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (AD anElement1 : aClass.getAddr()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAddr()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim077_4
	* The addr elements of AssignedAuthor SHALL be distinct (RMIM-077, RIM-017)
	*
	*/
	private static boolean _validateAssignedAuthorSpec_Rmim077_4(net.ihe.gazelle.cda.POCDMT000040AssignedAuthor aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (AD anElement1 : aClass.getAddr()) {
			    java.util.ArrayList<AD> result2;
			    result2 = new java.util.ArrayList<AD>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (AD anElement2 : aClass.getAddr()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of class-constraint : AssignedAuthorSpec
    * Verify if an element of type AssignedAuthorSpec can be validated by AssignedAuthorSpec
	*
	*/
	public static boolean _isAssignedAuthorSpec(net.ihe.gazelle.cda.POCDMT000040AssignedAuthor aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   AssignedAuthorSpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040AssignedAuthor
     * 
     */
    public static void _validateAssignedAuthorSpec(net.ihe.gazelle.cda.POCDMT000040AssignedAuthor aClass, String location, List<Notification> diagnostic) {
		if (_isAssignedAuthorSpec(aClass)){
			executeCons_AssignedAuthorSpec_Rmim077_1(aClass, location, diagnostic);
			executeCons_AssignedAuthorSpec_Rmim077_2(aClass, location, diagnostic);
			executeCons_AssignedAuthorSpec_Rmim077_3(aClass, location, diagnostic);
			executeCons_AssignedAuthorSpec_Rmim077_4(aClass, location, diagnostic);
		}
	}

	private static void executeCons_AssignedAuthorSpec_Rmim077_1(net.ihe.gazelle.cda.POCDMT000040AssignedAuthor aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAssignedAuthorSpec_Rmim077_1(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim077_1");
		notif.setDescription("AssignedAuthor SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-077, RIM-011)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-AssignedAuthorSpec-rmim077_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-077;RIM-011"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_AssignedAuthorSpec_Rmim077_2(net.ihe.gazelle.cda.POCDMT000040AssignedAuthor aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAssignedAuthorSpec_Rmim077_2(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim077_2");
		notif.setDescription("The ids elements of AssignedAuthor SHALL be distinct (RMIM-077, RIM-012)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-AssignedAuthorSpec-rmim077_2");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-077;RIM-012"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_AssignedAuthorSpec_Rmim077_3(net.ihe.gazelle.cda.POCDMT000040AssignedAuthor aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAssignedAuthorSpec_Rmim077_3(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim077_3");
		notif.setDescription("AssignedAuthor SHALL NOT have addr element with nullFlavor, if there are other addr (RMIM-077, RIM-016)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-AssignedAuthorSpec-rmim077_3");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-077;RIM-016"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_AssignedAuthorSpec_Rmim077_4(net.ihe.gazelle.cda.POCDMT000040AssignedAuthor aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAssignedAuthorSpec_Rmim077_4(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim077_4");
		notif.setDescription("The addr elements of AssignedAuthor SHALL be distinct (RMIM-077, RIM-017)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-AssignedAuthorSpec-rmim077_4");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-077;RIM-017"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

}
