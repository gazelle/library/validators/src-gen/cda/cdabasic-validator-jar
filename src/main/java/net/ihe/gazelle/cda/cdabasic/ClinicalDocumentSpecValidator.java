package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.cda.POCDMT000040RelatedDocument;
import net.ihe.gazelle.cda.POCDMT000040Section;
import net.ihe.gazelle.datatypes.II;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.voc.XActRelationshipDocument;	



 /**
  * class :        ClinicalDocumentSpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040ClinicalDocument
  * 
  */
public final class ClinicalDocumentSpecValidator{


    private ClinicalDocumentSpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim026
	* if regionOfInterest is target of a renderMultimedia then it shall have exactly one ObservationMedia and not an ExternalObservation (RMIM-026)
	*
	*/
	private static boolean _validateClinicalDocumentSpec_Rmim026(net.ihe.gazelle.cda.POCDMT000040ClinicalDocument aClass){
		return aClass.validateByXPATHV2("every $regionOfInterest in //cda:regionOfInterest satisfies ( not( some $ref in data(//cda:renderMultiMedia/@referencedObject) satisfies $ref = data($regionOfInterest/@ID)) or boolean($regionOfInterest/cda:entryRelationship/cda:observationMedia) or boolean(name($regionOfInterest/parent::node()/parent::node())=\u0027observationMedia\u0027))");
		
	}

	/**
	* Validation of instance by a constraint : rmim051
	* The Section.id element is unique into the CDA Document (RMIM-051)
	*
	*/
	private static boolean _validateClinicalDocumentSpec_Rmim051(net.ihe.gazelle.cda.POCDMT000040ClinicalDocument aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (POCDMT000040Section anElement1 : aClass.getAllSections()) {
			    java.util.ArrayList<POCDMT000040Section> result2;
			    result2 = new java.util.ArrayList<POCDMT000040Section>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (POCDMT000040Section anElement2 : aClass.getAllSections()) {
			    	    if ((!(anElement2.getId() == null) && anElement2.getId().cdaEquals(anElement1.getId()))) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!(((anElement1.getId() == null) || !(anElement1.getId().getNullFlavor() == null)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1)))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim055
	* ParentDocument/id SHALL be different from the id of ClinicalDocument/id,  when the typeCode=RPLC or typeCode=APND (RMIM-055)
	*
	*/
	private static boolean _validateClinicalDocumentSpec_Rmim055(net.ihe.gazelle.cda.POCDMT000040ClinicalDocument aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (POCDMT000040RelatedDocument anElement1 : aClass.getRelatedDocument()) {
			    Boolean result2;
			    result2 = true;
			    
			    /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
			    try{
			    	for (II anElement2 : anElement1.getParentDocument().getId()) {
			    	    if (!!anElement2.cdaEquals(aClass.getId())) {
			    	        result2 = false;
			    	        break;
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((((anElement1.getTypeCode() == null) || anElement1.getTypeCode().equals(XActRelationshipDocument.XFRM)) || (anElement1.getParentDocument() == null)) || result2)) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim056_1
	* All documents in a chain of replacement SHALL have the same setId (RMIM-056)
	*
	*/
	private static boolean _validateClinicalDocumentSpec_Rmim056_1(net.ihe.gazelle.cda.POCDMT000040ClinicalDocument aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (POCDMT000040RelatedDocument anElement1 : aClass.getRelatedDocument()) {
			    if (!(((((anElement1.getTypeCode() == null) || !anElement1.getTypeCode().equals(XActRelationshipDocument.RPLC)) || (anElement1.getParentDocument() == null)) || (anElement1.getParentDocument().getSetId() == null)) || anElement1.getParentDocument().getSetId().cdaEquals(aClass.getSetId()))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim056_2
	* All documents in a chain of replacement SHALL have a versionNumber incrementing (RMIM-056)
	*
	*/
	private static boolean _validateClinicalDocumentSpec_Rmim056_2(net.ihe.gazelle.cda.POCDMT000040ClinicalDocument aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (POCDMT000040RelatedDocument anElement1 : aClass.getRelatedDocument()) {
			    if (!((((((anElement1.getTypeCode() == null) || !anElement1.getTypeCode().equals(XActRelationshipDocument.RPLC)) || (anElement1.getParentDocument() == null)) || (anElement1.getParentDocument().getVersionNumber() == null)) || (((Integer) anElement1.getParentDocument().getVersionNumber().getValue()) == null)) || (aClass.getVersionNumber().getValue() > anElement1.getParentDocument().getVersionNumber().getValue()))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return (((aClass.getVersionNumber() == null) || (((Integer) aClass.getVersionNumber().getValue()) == null)) || result1);
		
		
	}

	/**
	* Validation of instance by a constraint : rmim063
	* The languageCode shall be from the valueSet HumanLanguage (RMIM-063)
	*
	*/
	private static boolean _validateClinicalDocumentSpec_Rmim063(net.ihe.gazelle.cda.POCDMT000040ClinicalDocument aClass){
		return (((aClass.getLanguageCode() == null) || !(aClass.getLanguageCode().getNullFlavor() == null)) || (!(((String) aClass.getLanguageCode().getCode()) == null) && aClass.getLanguageCode().matches(aClass.getLanguageCode().getCode(), "\\w+(\\-\\w+)?")));
		
	}

	/**
	* Validation of instance by a constraint : rmim064
	* copyTime SHOULD NOT be used (4.2.1.9, [1]) (RMIM-064)
	*
	*/
	private static boolean _validateClinicalDocumentSpec_Rmim064(net.ihe.gazelle.cda.POCDMT000040ClinicalDocument aClass){
		return (aClass.getCopyTime() == null);
		
	}

	/**
	* Validation of instance by a constraint : rmim065
	* Only these combination of relatedDocument MAY be present : (APND), (RPLC), (XFR), (XFRM, RPLC), (XFRM, APND) (RMIM-065)
	*
	*/
	private static boolean _validateClinicalDocumentSpec_Rmim065(net.ihe.gazelle.cda.POCDMT000040ClinicalDocument aClass){
		java.util.ArrayList<POCDMT000040RelatedDocument> result1;
		result1 = new java.util.ArrayList<POCDMT000040RelatedDocument>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (POCDMT000040RelatedDocument anElement1 : aClass.getRelatedDocument()) {
			    if ((!(anElement1.getTypeCode() == null) && anElement1.getTypeCode().equals(XActRelationshipDocument.XFRM))) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getRelatedDocument()) < Integer.valueOf(2)) || (((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getRelatedDocument())).equals(Integer.valueOf(2)) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(1))));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim109
	* renderMultimedia@referencedObject SHALL have a value of ObservationMultimedia or RegionOfInterest within the same document (RMIM-109)
	*
	*/
	private static boolean _validateClinicalDocumentSpec_Rmim109(net.ihe.gazelle.cda.POCDMT000040ClinicalDocument aClass){
		return aClass.validateByXPATHV2("every $referencedObject in data(//cda:renderMultiMedia/@referencedObject) satisfies ((some $om in //cda:observationMedia satisfies $om/@ID=$referencedObject) or    (some $om in //cda:regionOfInterest satisfies $om/@ID=$referencedObject))");
		
	}

	/**
	* Validation of instance by a constraint : rmim110
	* renderMultimedia SHALL refer to a single observationMedia, or to multiple regionOfInterest (RMIM-110)
	*
	*/
	private static boolean _validateClinicalDocumentSpec_Rmim110(net.ihe.gazelle.cda.POCDMT000040ClinicalDocument aClass){
		return aClass.validateByXPATHV2("every $referencedObject in data(//cda:renderMultiMedia/@referencedObject) satisfies(count(tokenize(string($referencedObject),\u0027\\s+\u0027))<2 or (every $ref in tokenize(string($referencedObject),\u0027\\s+\u0027) satisfies(some $om in //cda:regionOfInterest satisfies $om/@ID=$ref)))");
		
	}

	/**
	* Validation of instance by a constraint : rmim113
	* the footnoteref SHALL reference a footnote in the same document (RMIM-113)
	*
	*/
	private static boolean _validateClinicalDocumentSpec_Rmim113(net.ihe.gazelle.cda.POCDMT000040ClinicalDocument aClass){
		return aClass.validateByXPATHV2("every $fnr in data(//cda:footnoteRef/@IDREF) satisfies (some $om in //cda:footnote satisfies $om/@ID=$fnr)");
		
	}

	/**
	* Validation of instance by a constraint : rmim114
	* Ordered list style SHOULD be used only on the element list, with listType=ordered (RMIM-114)
	*
	*/
	private static boolean _validateClinicalDocumentSpec_Rmim114(net.ihe.gazelle.cda.POCDMT000040ClinicalDocument aClass){
		return aClass.validateByXPATHV2("every $nblock in //.[@styleCode] satisfies((every $ref in tokenize(string($nblock/@styleCode),\u0027\\s+\u0027) satisfies((data($ref)!=\u0027Arabic\u0027) and (data($ref)!=\u0027LittleRoman\u0027) and (data($ref)!=\u0027BigRoman\u0027) and (data($ref)!=\u0027LittleAlpha\u0027) and (data($ref)!=\u0027BigAlpha\u0027))) or ($nblock/@listType=\u0027ordered\u0027 and name($nblock)=\u0027list\u0027))");
		
	}

	/**
	* Validation of instance by a constraint : rmim115
	* Unordered list style SHOULD be used only on the element list, with listType=unordered (RMIM-115)
	*
	*/
	private static boolean _validateClinicalDocumentSpec_Rmim115(net.ihe.gazelle.cda.POCDMT000040ClinicalDocument aClass){
		return aClass.validateByXPATHV2("every $nblock in //.[@styleCode] satisfies ((every $ref in tokenize(string($nblock/@styleCode),\u0027\\s+\u0027) satisfies((data($ref)!=\u0027Disc\u0027) and (data($ref)!=\u0027Circle\u0027) and (data($ref)!=\u0027Square\u0027))) or (name($nblock)=\u0027list\u0027 and ($nblock/@listType=\u0027unordered\u0027 or not($nblock/@listType))))");
		
	}

	/**
	* Validation of class-constraint : ClinicalDocumentSpec
    * Verify if an element of type ClinicalDocumentSpec can be validated by ClinicalDocumentSpec
	*
	*/
	public static boolean _isClinicalDocumentSpec(net.ihe.gazelle.cda.POCDMT000040ClinicalDocument aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   ClinicalDocumentSpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040ClinicalDocument
     * 
     */
    public static void _validateClinicalDocumentSpec(net.ihe.gazelle.cda.POCDMT000040ClinicalDocument aClass, String location, List<Notification> diagnostic) {
		if (_isClinicalDocumentSpec(aClass)){
			executeCons_ClinicalDocumentSpec_Rmim026(aClass, location, diagnostic);
			executeCons_ClinicalDocumentSpec_Rmim051(aClass, location, diagnostic);
			executeCons_ClinicalDocumentSpec_Rmim055(aClass, location, diagnostic);
			executeCons_ClinicalDocumentSpec_Rmim056_1(aClass, location, diagnostic);
			executeCons_ClinicalDocumentSpec_Rmim056_2(aClass, location, diagnostic);
			executeCons_ClinicalDocumentSpec_Rmim063(aClass, location, diagnostic);
			executeCons_ClinicalDocumentSpec_Rmim064(aClass, location, diagnostic);
			executeCons_ClinicalDocumentSpec_Rmim065(aClass, location, diagnostic);
			executeCons_ClinicalDocumentSpec_Rmim109(aClass, location, diagnostic);
			executeCons_ClinicalDocumentSpec_Rmim110(aClass, location, diagnostic);
			executeCons_ClinicalDocumentSpec_Rmim113(aClass, location, diagnostic);
			executeCons_ClinicalDocumentSpec_Rmim114(aClass, location, diagnostic);
			executeCons_ClinicalDocumentSpec_Rmim115(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ClinicalDocumentSpec_Rmim026(net.ihe.gazelle.cda.POCDMT000040ClinicalDocument aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateClinicalDocumentSpec_Rmim026(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim026");
		notif.setDescription("if regionOfInterest is target of a renderMultimedia then it shall have exactly one ObservationMedia and not an ExternalObservation (RMIM-026)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ClinicalDocumentSpec-rmim026");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-026"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_ClinicalDocumentSpec_Rmim051(net.ihe.gazelle.cda.POCDMT000040ClinicalDocument aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateClinicalDocumentSpec_Rmim051(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim051");
		notif.setDescription("The Section.id element is unique into the CDA Document (RMIM-051)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ClinicalDocumentSpec-rmim051");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-051"));
		
		notif.setType("Cardinality");
		diagnostic.add(notif);
	}

	private static void executeCons_ClinicalDocumentSpec_Rmim055(net.ihe.gazelle.cda.POCDMT000040ClinicalDocument aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateClinicalDocumentSpec_Rmim055(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim055");
		notif.setDescription("ParentDocument/id SHALL be different from the id of ClinicalDocument/id,  when the typeCode=RPLC or typeCode=APND (RMIM-055)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ClinicalDocumentSpec-rmim055");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-055"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_ClinicalDocumentSpec_Rmim056_1(net.ihe.gazelle.cda.POCDMT000040ClinicalDocument aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateClinicalDocumentSpec_Rmim056_1(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim056_1");
		notif.setDescription("All documents in a chain of replacement SHALL have the same setId (RMIM-056)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ClinicalDocumentSpec-rmim056_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-056"));
		
		notif.setType("Fixed value");
		diagnostic.add(notif);
	}

	private static void executeCons_ClinicalDocumentSpec_Rmim056_2(net.ihe.gazelle.cda.POCDMT000040ClinicalDocument aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateClinicalDocumentSpec_Rmim056_2(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim056_2");
		notif.setDescription("All documents in a chain of replacement SHALL have a versionNumber incrementing (RMIM-056)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ClinicalDocumentSpec-rmim056_2");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-056"));
		
		notif.setType("Fixed value");
		diagnostic.add(notif);
	}

	private static void executeCons_ClinicalDocumentSpec_Rmim063(net.ihe.gazelle.cda.POCDMT000040ClinicalDocument aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateClinicalDocumentSpec_Rmim063(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim063");
		notif.setDescription("The languageCode shall be from the valueSet HumanLanguage (RMIM-063)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ClinicalDocumentSpec-rmim063");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-063"));
		
		notif.setType("Vocabulary");
		diagnostic.add(notif);
	}

	private static void executeCons_ClinicalDocumentSpec_Rmim064(net.ihe.gazelle.cda.POCDMT000040ClinicalDocument aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateClinicalDocumentSpec_Rmim064(aClass) )){
				notif = new Warning();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim064");
		notif.setDescription("copyTime SHOULD NOT be used (4.2.1.9, [1]) (RMIM-064)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ClinicalDocumentSpec-rmim064");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-064"));
		
		notif.setType("Fixed value");
		diagnostic.add(notif);
	}

	private static void executeCons_ClinicalDocumentSpec_Rmim065(net.ihe.gazelle.cda.POCDMT000040ClinicalDocument aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateClinicalDocumentSpec_Rmim065(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim065");
		notif.setDescription("Only these combination of relatedDocument MAY be present : (APND), (RPLC), (XFR), (XFRM, RPLC), (XFRM, APND) (RMIM-065)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ClinicalDocumentSpec-rmim065");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-065"));
		
		notif.setType("Fixed value");
		diagnostic.add(notif);
	}

	private static void executeCons_ClinicalDocumentSpec_Rmim109(net.ihe.gazelle.cda.POCDMT000040ClinicalDocument aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateClinicalDocumentSpec_Rmim109(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim109");
		notif.setDescription("renderMultimedia@referencedObject SHALL have a value of ObservationMultimedia or RegionOfInterest within the same document (RMIM-109)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ClinicalDocumentSpec-rmim109");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-109"));
		
		notif.setType("Fixed value");
		diagnostic.add(notif);
	}

	private static void executeCons_ClinicalDocumentSpec_Rmim110(net.ihe.gazelle.cda.POCDMT000040ClinicalDocument aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateClinicalDocumentSpec_Rmim110(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim110");
		notif.setDescription("renderMultimedia SHALL refer to a single observationMedia, or to multiple regionOfInterest (RMIM-110)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ClinicalDocumentSpec-rmim110");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-110"));
		
		notif.setType("Cardinality");
		diagnostic.add(notif);
	}

	private static void executeCons_ClinicalDocumentSpec_Rmim113(net.ihe.gazelle.cda.POCDMT000040ClinicalDocument aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateClinicalDocumentSpec_Rmim113(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim113");
		notif.setDescription("the footnoteref SHALL reference a footnote in the same document (RMIM-113)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ClinicalDocumentSpec-rmim113");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-113"));
		
		notif.setType("Mandatory");
		diagnostic.add(notif);
	}

	private static void executeCons_ClinicalDocumentSpec_Rmim114(net.ihe.gazelle.cda.POCDMT000040ClinicalDocument aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateClinicalDocumentSpec_Rmim114(aClass) )){
				notif = new Warning();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim114");
		notif.setDescription("Ordered list style SHOULD be used only on the element list, with listType=ordered (RMIM-114)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ClinicalDocumentSpec-rmim114");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-114"));
		
		notif.setType("Fixed value");
		diagnostic.add(notif);
	}

	private static void executeCons_ClinicalDocumentSpec_Rmim115(net.ihe.gazelle.cda.POCDMT000040ClinicalDocument aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateClinicalDocumentSpec_Rmim115(aClass) )){
				notif = new Warning();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim115");
		notif.setDescription("Unordered list style SHOULD be used only on the element list, with listType=unordered (RMIM-115)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ClinicalDocumentSpec-rmim115");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-115"));
		
		notif.setType("Fixed value");
		diagnostic.add(notif);
	}

}
