package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.datatypes.II;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;	



 /**
  * class :        ActSpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040Act
  * 
  */
public final class ActSpecValidator{


    private ActSpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim001_1
	* Act SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-001, RIM-001)
	*
	*/
	private static boolean _validateActSpec_Rmim001_1(net.ihe.gazelle.cda.POCDMT000040Act aClass){
		java.util.ArrayList<II> result1;
		result1 = new java.util.ArrayList<II>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getId()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim001_2
	* The ids elements of Act SHALL be distinct (RMIM-001, RIM-002)
	*
	*/
	private static boolean _validateActSpec_Rmim001_2(net.ihe.gazelle.cda.POCDMT000040Act aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    java.util.ArrayList<II> result2;
			    result2 = new java.util.ArrayList<II>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (II anElement2 : aClass.getId()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim001_3
	* The statusCode if presents SHALL be from the valueSet ActStatus (RMIM-001, RIM-003)
	*
	*/
	private static boolean _validateActSpec_Rmim001_3(net.ihe.gazelle.cda.POCDMT000040Act aClass){
		return (((aClass.getStatusCode() == null) || (((String) aClass.getStatusCode().getCode()) == null)) || aClass.getStatusCode().matchesCodeToValueSet("1.3.6.1.4.1.12559.11.18.2", aClass.getStatusCode().getCode()));
		
	}

	/**
	* Validation of instance by a constraint : rmim002
	* The languageCode shall be from the valueSet HumanLanguage (RMIM-002)
	*
	*/
	private static boolean _validateActSpec_Rmim002(net.ihe.gazelle.cda.POCDMT000040Act aClass){
		return (((aClass.getLanguageCode() == null) || !(aClass.getLanguageCode().getNullFlavor() == null)) || (!(((String) aClass.getLanguageCode().getCode()) == null) && aClass.getLanguageCode().matches(aClass.getLanguageCode().getCode(), "\\w+(\\-\\w+)?")));
		
	}

	/**
	* Validation of class-constraint : ActSpec
    * Verify if an element of type ActSpec can be validated by ActSpec
	*
	*/
	public static boolean _isActSpec(net.ihe.gazelle.cda.POCDMT000040Act aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   ActSpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040Act
     * 
     */
    public static void _validateActSpec(net.ihe.gazelle.cda.POCDMT000040Act aClass, String location, List<Notification> diagnostic) {
		if (_isActSpec(aClass)){
			executeCons_ActSpec_Rmim001_1(aClass, location, diagnostic);
			executeCons_ActSpec_Rmim001_2(aClass, location, diagnostic);
			executeCons_ActSpec_Rmim001_3(aClass, location, diagnostic);
			executeCons_ActSpec_Rmim002(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ActSpec_Rmim001_1(net.ihe.gazelle.cda.POCDMT000040Act aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateActSpec_Rmim001_1(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim001_1");
		notif.setDescription("Act SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-001, RIM-001)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ActSpec-rmim001_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-001;RIM-001"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_ActSpec_Rmim001_2(net.ihe.gazelle.cda.POCDMT000040Act aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateActSpec_Rmim001_2(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim001_2");
		notif.setDescription("The ids elements of Act SHALL be distinct (RMIM-001, RIM-002)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ActSpec-rmim001_2");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-001;RIM-002"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_ActSpec_Rmim001_3(net.ihe.gazelle.cda.POCDMT000040Act aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateActSpec_Rmim001_3(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim001_3");
		notif.setDescription("The statusCode if presents SHALL be from the valueSet ActStatus (RMIM-001, RIM-003)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ActSpec-rmim001_3");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-001;RIM-003"));
		
		notif.setType("Vocabulary");
		diagnostic.add(notif);
	}

	private static void executeCons_ActSpec_Rmim002(net.ihe.gazelle.cda.POCDMT000040Act aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateActSpec_Rmim002(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim002");
		notif.setDescription("The languageCode shall be from the valueSet HumanLanguage (RMIM-002)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ActSpec-rmim002");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-002"));
		
		notif.setType("Vocabulary");
		diagnostic.add(notif);
	}

}
