package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;	



 /**
  * class :        StructuredBodySpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040StructuredBody
  * 
  */
public final class StructuredBodySpecValidator{


    private StructuredBodySpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim049
	* The languageCode shall be from the valueSet HumanLanguage (RMIM-049)
	*
	*/
	private static boolean _validateStructuredBodySpec_Rmim049(net.ihe.gazelle.cda.POCDMT000040StructuredBody aClass){
		return (((aClass.getLanguageCode() == null) || !(aClass.getLanguageCode().getNullFlavor() == null)) || (!(((String) aClass.getLanguageCode().getCode()) == null) && aClass.getLanguageCode().matches(aClass.getLanguageCode().getCode(), "\\w+(\\-\\w+)?")));
		
	}

	/**
	* Validation of class-constraint : StructuredBodySpec
    * Verify if an element of type StructuredBodySpec can be validated by StructuredBodySpec
	*
	*/
	public static boolean _isStructuredBodySpec(net.ihe.gazelle.cda.POCDMT000040StructuredBody aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   StructuredBodySpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040StructuredBody
     * 
     */
    public static void _validateStructuredBodySpec(net.ihe.gazelle.cda.POCDMT000040StructuredBody aClass, String location, List<Notification> diagnostic) {
		if (_isStructuredBodySpec(aClass)){
			executeCons_StructuredBodySpec_Rmim049(aClass, location, diagnostic);
		}
	}

	private static void executeCons_StructuredBodySpec_Rmim049(net.ihe.gazelle.cda.POCDMT000040StructuredBody aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateStructuredBodySpec_Rmim049(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim049");
		notif.setDescription("The languageCode shall be from the valueSet HumanLanguage (RMIM-049)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-StructuredBodySpec-rmim049");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-049"));
		
		notif.setType("Vocabulary");
		diagnostic.add(notif);
	}

}
