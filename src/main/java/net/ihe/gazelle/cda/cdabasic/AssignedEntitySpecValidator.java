package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.cda.POCDMT000040Authenticator;
import net.ihe.gazelle.cda.POCDMT000040DataEnterer;
import net.ihe.gazelle.cda.POCDMT000040EncounterParticipant;
import net.ihe.gazelle.cda.POCDMT000040Informant12;
import net.ihe.gazelle.cda.POCDMT000040LegalAuthenticator;
import net.ihe.gazelle.cda.POCDMT000040Performer1;
import net.ihe.gazelle.cda.POCDMT000040Performer2;
import net.ihe.gazelle.cda.POCDMT000040ResponsibleParty;
import net.ihe.gazelle.datatypes.AD;
import net.ihe.gazelle.datatypes.II;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;	



 /**
  * class :        AssignedEntitySpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040AssignedEntity
  * 
  */
public final class AssignedEntitySpecValidator{


    private AssignedEntitySpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim072_1
	* AssignedEntity SHALL NOT have id element with nullFlavor, if there are other ids element (RMIM-072, RIM-011)
	*
	*/
	private static boolean _validateAssignedEntitySpec_Rmim072_1(net.ihe.gazelle.cda.POCDMT000040AssignedEntity aClass){
		java.util.ArrayList<II> result1;
		result1 = new java.util.ArrayList<II>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getId()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim072_2
	* The ids elements of AssignedEntity SHALL be distinct (RMIM-072, RIM-012)
	*
	*/
	private static boolean _validateAssignedEntitySpec_Rmim072_2(net.ihe.gazelle.cda.POCDMT000040AssignedEntity aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    java.util.ArrayList<II> result2;
			    result2 = new java.util.ArrayList<II>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (II anElement2 : aClass.getId()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim072_3
	* AssignedEntity SHALL NOT have addr element with nullFlavor, if there are other addr (RMIM-072, RIM-016)
	*
	*/
	private static boolean _validateAssignedEntitySpec_Rmim072_3(net.ihe.gazelle.cda.POCDMT000040AssignedEntity aClass){
		java.util.ArrayList<AD> result1;
		result1 = new java.util.ArrayList<AD>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (AD anElement1 : aClass.getAddr()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAddr()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim072_4
	* The addr elements of AssignedEntity SHALL be distinct (RMIM-072, RIM-017)
	*
	*/
	private static boolean _validateAssignedEntitySpec_Rmim072_4(net.ihe.gazelle.cda.POCDMT000040AssignedEntity aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (AD anElement1 : aClass.getAddr()) {
			    java.util.ArrayList<AD> result2;
			    result2 = new java.util.ArrayList<AD>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (AD anElement2 : aClass.getAddr()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim073
	* If assignedEntity has not a null of flavor and its enclosing participation has not a null of flavor,  it SHALL have an assignedPerson or a representedOrganization(RMIM-073)
	*
	*/
	private static boolean _validateAssignedEntitySpec_Rmim073(net.ihe.gazelle.cda.POCDMT000040AssignedEntity aClass){
		return (((((((((!(aClass.getNullFlavor() == null) || ((POCDMT000040Authenticator.class.isAssignableFrom(aClass.get__parent().getClass())) && !(((POCDMT000040Authenticator) aClass.get__parent()).getNullFlavor() == null))) || ((POCDMT000040LegalAuthenticator.class.isAssignableFrom(aClass.get__parent().getClass())) && !(((POCDMT000040LegalAuthenticator) aClass.get__parent()).getNullFlavor() == null))) || ((POCDMT000040DataEnterer.class.isAssignableFrom(aClass.get__parent().getClass())) && !(((POCDMT000040DataEnterer) aClass.get__parent()).getNullFlavor() == null))) || ((POCDMT000040Informant12.class.isAssignableFrom(aClass.get__parent().getClass())) && !(((POCDMT000040Informant12) aClass.get__parent()).getNullFlavor() == null))) || ((POCDMT000040Performer1.class.isAssignableFrom(aClass.get__parent().getClass())) && !(((POCDMT000040Performer1) aClass.get__parent()).getNullFlavor() == null))) || ((POCDMT000040Performer2.class.isAssignableFrom(aClass.get__parent().getClass())) && !(((POCDMT000040Performer2) aClass.get__parent()).getNullFlavor() == null))) || ((POCDMT000040ResponsibleParty.class.isAssignableFrom(aClass.get__parent().getClass())) && !(((POCDMT000040ResponsibleParty) aClass.get__parent()).getNullFlavor() == null))) || ((POCDMT000040EncounterParticipant.class.isAssignableFrom(aClass.get__parent().getClass())) && !(((POCDMT000040EncounterParticipant) aClass.get__parent()).getNullFlavor() == null))) || (!(aClass.getAssignedPerson() == null) || !(aClass.getRepresentedOrganization() == null)));
		
	}

	/**
	* Validation of class-constraint : AssignedEntitySpec
    * Verify if an element of type AssignedEntitySpec can be validated by AssignedEntitySpec
	*
	*/
	public static boolean _isAssignedEntitySpec(net.ihe.gazelle.cda.POCDMT000040AssignedEntity aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   AssignedEntitySpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040AssignedEntity
     * 
     */
    public static void _validateAssignedEntitySpec(net.ihe.gazelle.cda.POCDMT000040AssignedEntity aClass, String location, List<Notification> diagnostic) {
		if (_isAssignedEntitySpec(aClass)){
			executeCons_AssignedEntitySpec_Rmim072_1(aClass, location, diagnostic);
			executeCons_AssignedEntitySpec_Rmim072_2(aClass, location, diagnostic);
			executeCons_AssignedEntitySpec_Rmim072_3(aClass, location, diagnostic);
			executeCons_AssignedEntitySpec_Rmim072_4(aClass, location, diagnostic);
			executeCons_AssignedEntitySpec_Rmim073(aClass, location, diagnostic);
		}
	}

	private static void executeCons_AssignedEntitySpec_Rmim072_1(net.ihe.gazelle.cda.POCDMT000040AssignedEntity aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAssignedEntitySpec_Rmim072_1(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim072_1");
		notif.setDescription("AssignedEntity SHALL NOT have id element with nullFlavor, if there are other ids element (RMIM-072, RIM-011)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-AssignedEntitySpec-rmim072_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-072;RIM-011"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_AssignedEntitySpec_Rmim072_2(net.ihe.gazelle.cda.POCDMT000040AssignedEntity aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAssignedEntitySpec_Rmim072_2(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim072_2");
		notif.setDescription("The ids elements of AssignedEntity SHALL be distinct (RMIM-072, RIM-012)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-AssignedEntitySpec-rmim072_2");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-072;RIM-012"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_AssignedEntitySpec_Rmim072_3(net.ihe.gazelle.cda.POCDMT000040AssignedEntity aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAssignedEntitySpec_Rmim072_3(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim072_3");
		notif.setDescription("AssignedEntity SHALL NOT have addr element with nullFlavor, if there are other addr (RMIM-072, RIM-016)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-AssignedEntitySpec-rmim072_3");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-072;RIM-016"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_AssignedEntitySpec_Rmim072_4(net.ihe.gazelle.cda.POCDMT000040AssignedEntity aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAssignedEntitySpec_Rmim072_4(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim072_4");
		notif.setDescription("The addr elements of AssignedEntity SHALL be distinct (RMIM-072, RIM-017)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-AssignedEntitySpec-rmim072_4");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-072;RIM-017"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_AssignedEntitySpec_Rmim073(net.ihe.gazelle.cda.POCDMT000040AssignedEntity aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAssignedEntitySpec_Rmim073(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim073");
		notif.setDescription("If assignedEntity has not a null of flavor and its enclosing participation has not a null of flavor,  it SHALL have an assignedPerson or a representedOrganization(RMIM-073)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-AssignedEntitySpec-rmim073");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-073"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

}
