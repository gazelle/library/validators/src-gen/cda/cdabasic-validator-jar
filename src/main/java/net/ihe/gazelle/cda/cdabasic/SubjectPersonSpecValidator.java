package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.datatypes.PN;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;	



 /**
  * class :        SubjectPersonSpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040SubjectPerson
  * 
  */
public final class SubjectPersonSpecValidator{


    private SubjectPersonSpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim103_1
	* SubjectPerson SHALL NOT have name element with nullFlavor, if there are other name (RMIM-103, RIM-022)
	*
	*/
	private static boolean _validateSubjectPersonSpec_Rmim103_1(net.ihe.gazelle.cda.POCDMT000040SubjectPerson aClass){
		java.util.ArrayList<PN> result1;
		result1 = new java.util.ArrayList<PN>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (PN anElement1 : aClass.getName()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getName()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim103_2
	* The name elements of SubjectPerson SHALL be distinct (RMIM-103, RIM-023)
	*
	*/
	private static boolean _validateSubjectPersonSpec_Rmim103_2(net.ihe.gazelle.cda.POCDMT000040SubjectPerson aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (PN anElement1 : aClass.getName()) {
			    java.util.ArrayList<PN> result2;
			    result2 = new java.util.ArrayList<PN>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (PN anElement2 : aClass.getName()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of class-constraint : SubjectPersonSpec
    * Verify if an element of type SubjectPersonSpec can be validated by SubjectPersonSpec
	*
	*/
	public static boolean _isSubjectPersonSpec(net.ihe.gazelle.cda.POCDMT000040SubjectPerson aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   SubjectPersonSpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040SubjectPerson
     * 
     */
    public static void _validateSubjectPersonSpec(net.ihe.gazelle.cda.POCDMT000040SubjectPerson aClass, String location, List<Notification> diagnostic) {
		if (_isSubjectPersonSpec(aClass)){
			executeCons_SubjectPersonSpec_Rmim103_1(aClass, location, diagnostic);
			executeCons_SubjectPersonSpec_Rmim103_2(aClass, location, diagnostic);
		}
	}

	private static void executeCons_SubjectPersonSpec_Rmim103_1(net.ihe.gazelle.cda.POCDMT000040SubjectPerson aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateSubjectPersonSpec_Rmim103_1(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim103_1");
		notif.setDescription("SubjectPerson SHALL NOT have name element with nullFlavor, if there are other name (RMIM-103, RIM-022)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-SubjectPersonSpec-rmim103_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-103;RIM-022"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_SubjectPersonSpec_Rmim103_2(net.ihe.gazelle.cda.POCDMT000040SubjectPerson aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateSubjectPersonSpec_Rmim103_2(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim103_2");
		notif.setDescription("The name elements of SubjectPerson SHALL be distinct (RMIM-103, RIM-023)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-SubjectPersonSpec-rmim103_2");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-103;RIM-023"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

}
