package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.datatypes.ANY;
import net.ihe.gazelle.datatypes.CS;
import net.ihe.gazelle.datatypes.ST;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;	



 /**
  * class :        ObservationRangeSpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040ObservationRange
  * 
  */
public final class ObservationRangeSpecValidator{


    private ObservationRangeSpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim038
	* Numeric values SHALL NOT be communicated in ObservationRange/value as a simple String (RMIM-038)
	*
	*/
	private static boolean _validateObservationRangeSpec_Rmim038(net.ihe.gazelle.cda.POCDMT000040ObservationRange aClass){
		return (((aClass.getValue() == null) || !aClass.getValue().getClass().getCanonicalName().equals(ST.class.getCanonicalName())) || CriterionSpecValidator.validateValueIsNotNumeric(aClass.getValue()));
		
	}

	/**
	* Validation of instance by a constraint : rmim039
	* CS datatype SHALL NOT be used in value element, because only datatypes with (RMIM-039)
	*
	*/
	private static boolean _validateObservationRangeSpec_Rmim039(net.ihe.gazelle.cda.POCDMT000040ObservationRange aClass){
		java.util.Set<ANY> result2;
		result2 = new java.util.HashSet<ANY>();
		if (aClass.getValue() != null) {
			result2.add(aClass.getValue());
		}
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (ANY anElement1 : result2) {
			    if (!!(CS.class.isAssignableFrom(anElement1.getClass()))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim040
	* interpretationCode values SHALL be from the valueSet ObservationInterpretation (RMIM-040)
	*
	*/
	private static boolean _validateObservationRangeSpec_Rmim040(net.ihe.gazelle.cda.POCDMT000040ObservationRange aClass){
		return (((aClass.getInterpretationCode() == null) || (((String) aClass.getInterpretationCode().getCode()) == null)) || aClass.getInterpretationCode().matchesValueSet("1.3.6.1.4.1.12559.11.18.5", aClass.getInterpretationCode().getCode(), aClass.getInterpretationCode().getCodeSystem(), null, null));
		
	}

	/**
	* Validation of class-constraint : ObservationRangeSpec
    * Verify if an element of type ObservationRangeSpec can be validated by ObservationRangeSpec
	*
	*/
	public static boolean _isObservationRangeSpec(net.ihe.gazelle.cda.POCDMT000040ObservationRange aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   ObservationRangeSpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040ObservationRange
     * 
     */
    public static void _validateObservationRangeSpec(net.ihe.gazelle.cda.POCDMT000040ObservationRange aClass, String location, List<Notification> diagnostic) {
		if (_isObservationRangeSpec(aClass)){
			executeCons_ObservationRangeSpec_Rmim038(aClass, location, diagnostic);
			executeCons_ObservationRangeSpec_Rmim039(aClass, location, diagnostic);
			executeCons_ObservationRangeSpec_Rmim040(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ObservationRangeSpec_Rmim038(net.ihe.gazelle.cda.POCDMT000040ObservationRange aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateObservationRangeSpec_Rmim038(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim038");
		notif.setDescription("Numeric values SHALL NOT be communicated in ObservationRange/value as a simple String (RMIM-038)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ObservationRangeSpec-rmim038");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-038"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

	private static void executeCons_ObservationRangeSpec_Rmim039(net.ihe.gazelle.cda.POCDMT000040ObservationRange aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateObservationRangeSpec_Rmim039(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim039");
		notif.setDescription("CS datatype SHALL NOT be used in value element, because only datatypes with (RMIM-039)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ObservationRangeSpec-rmim039");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-039"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

	private static void executeCons_ObservationRangeSpec_Rmim040(net.ihe.gazelle.cda.POCDMT000040ObservationRange aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateObservationRangeSpec_Rmim040(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim040");
		notif.setDescription("interpretationCode values SHALL be from the valueSet ObservationInterpretation (RMIM-040)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ObservationRangeSpec-rmim040");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-040"));
		
		notif.setType("Vocabulary");
		diagnostic.add(notif);
	}

}
