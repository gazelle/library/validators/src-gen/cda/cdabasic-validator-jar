package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.datatypes.PN;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validation.Warning;	



 /**
  * class :        PatientSpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040Patient
  * 
  */
public final class PatientSpecValidator{


    private PatientSpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim101_1
	* Patient SHALL NOT have name element with nullFlavor, if there are other name (RMIM-101, RIM-022)
	*
	*/
	private static boolean _validatePatientSpec_Rmim101_1(net.ihe.gazelle.cda.POCDMT000040Patient aClass){
		java.util.ArrayList<PN> result1;
		result1 = new java.util.ArrayList<PN>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (PN anElement1 : aClass.getName()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getName()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim101_2
	* The name elements of Patient SHALL be distinct (RMIM-101, RIM-023)
	*
	*/
	private static boolean _validatePatientSpec_Rmim101_2(net.ihe.gazelle.cda.POCDMT000040Patient aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (PN anElement1 : aClass.getName()) {
			    java.util.ArrayList<PN> result2;
			    result2 = new java.util.ArrayList<PN>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (PN anElement2 : aClass.getName()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim102
	* id element SHOULD NOT be present (RMIM-102)
	*
	*/
	private static boolean _validatePatientSpec_Rmim102(net.ihe.gazelle.cda.POCDMT000040Patient aClass){
		return (aClass.getId() == null);
		
	}

	/**
	* Validation of class-constraint : PatientSpec
    * Verify if an element of type PatientSpec can be validated by PatientSpec
	*
	*/
	public static boolean _isPatientSpec(net.ihe.gazelle.cda.POCDMT000040Patient aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   PatientSpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040Patient
     * 
     */
    public static void _validatePatientSpec(net.ihe.gazelle.cda.POCDMT000040Patient aClass, String location, List<Notification> diagnostic) {
		if (_isPatientSpec(aClass)){
			executeCons_PatientSpec_Rmim101_1(aClass, location, diagnostic);
			executeCons_PatientSpec_Rmim101_2(aClass, location, diagnostic);
			executeCons_PatientSpec_Rmim102(aClass, location, diagnostic);
		}
	}

	private static void executeCons_PatientSpec_Rmim101_1(net.ihe.gazelle.cda.POCDMT000040Patient aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validatePatientSpec_Rmim101_1(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim101_1");
		notif.setDescription("Patient SHALL NOT have name element with nullFlavor, if there are other name (RMIM-101, RIM-022)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-PatientSpec-rmim101_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-101;RIM-022"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_PatientSpec_Rmim101_2(net.ihe.gazelle.cda.POCDMT000040Patient aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validatePatientSpec_Rmim101_2(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim101_2");
		notif.setDescription("The name elements of Patient SHALL be distinct (RMIM-101, RIM-023)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-PatientSpec-rmim101_2");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-101;RIM-023"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_PatientSpec_Rmim102(net.ihe.gazelle.cda.POCDMT000040Patient aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validatePatientSpec_Rmim102(aClass) )){
				notif = new Warning();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim102");
		notif.setDescription("id element SHOULD NOT be present (RMIM-102)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-PatientSpec-rmim102");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-102"));
		
		notif.setType("Cardinality");
		diagnostic.add(notif);
	}

}
