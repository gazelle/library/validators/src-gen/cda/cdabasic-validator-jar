package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.datatypes.II;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;	



 /**
  * class :        SpecimenRoleSpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040SpecimenRole
  * 
  */
public final class SpecimenRoleSpecValidator{


    private SpecimenRoleSpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim091_1
	* SpecimenRole SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-091, RIM-011)
	*
	*/
	private static boolean _validateSpecimenRoleSpec_Rmim091_1(net.ihe.gazelle.cda.POCDMT000040SpecimenRole aClass){
		java.util.ArrayList<II> result1;
		result1 = new java.util.ArrayList<II>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getId()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim091_2
	* The ids elements of SpecimenRole SHALL be distinct (RMIM-091, RIM-012)
	*
	*/
	private static boolean _validateSpecimenRoleSpec_Rmim091_2(net.ihe.gazelle.cda.POCDMT000040SpecimenRole aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    java.util.ArrayList<II> result2;
			    result2 = new java.util.ArrayList<II>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (II anElement2 : aClass.getId()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of class-constraint : SpecimenRoleSpec
    * Verify if an element of type SpecimenRoleSpec can be validated by SpecimenRoleSpec
	*
	*/
	public static boolean _isSpecimenRoleSpec(net.ihe.gazelle.cda.POCDMT000040SpecimenRole aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   SpecimenRoleSpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040SpecimenRole
     * 
     */
    public static void _validateSpecimenRoleSpec(net.ihe.gazelle.cda.POCDMT000040SpecimenRole aClass, String location, List<Notification> diagnostic) {
		if (_isSpecimenRoleSpec(aClass)){
			executeCons_SpecimenRoleSpec_Rmim091_1(aClass, location, diagnostic);
			executeCons_SpecimenRoleSpec_Rmim091_2(aClass, location, diagnostic);
		}
	}

	private static void executeCons_SpecimenRoleSpec_Rmim091_1(net.ihe.gazelle.cda.POCDMT000040SpecimenRole aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateSpecimenRoleSpec_Rmim091_1(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim091_1");
		notif.setDescription("SpecimenRole SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-091, RIM-011)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-SpecimenRoleSpec-rmim091_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-091;RIM-011"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_SpecimenRoleSpec_Rmim091_2(net.ihe.gazelle.cda.POCDMT000040SpecimenRole aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateSpecimenRoleSpec_Rmim091_2(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim091_2");
		notif.setDescription("The ids elements of SpecimenRole SHALL be distinct (RMIM-091, RIM-012)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-SpecimenRoleSpec-rmim091_2");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-091;RIM-012"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

}
