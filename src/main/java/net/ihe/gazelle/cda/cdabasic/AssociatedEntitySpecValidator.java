package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.cda.POCDMT000040Participant1;
import net.ihe.gazelle.datatypes.AD;
import net.ihe.gazelle.datatypes.II;
import net.ihe.gazelle.datatypes.TEL;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;	



 /**
  * class :        AssociatedEntitySpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040AssociatedEntity
  * 
  */
public final class AssociatedEntitySpecValidator{


    private AssociatedEntitySpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim083_1
	* AssociatedEntity SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-083, RIM-011)
	*
	*/
	private static boolean _validateAssociatedEntitySpec_Rmim083_1(net.ihe.gazelle.cda.POCDMT000040AssociatedEntity aClass){
		java.util.ArrayList<II> result1;
		result1 = new java.util.ArrayList<II>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getId()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim083_2
	* The ids elements of AssociatedEntity SHALL be distinct (RMIM-083, RIM-012)
	*
	*/
	private static boolean _validateAssociatedEntitySpec_Rmim083_2(net.ihe.gazelle.cda.POCDMT000040AssociatedEntity aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    java.util.ArrayList<II> result2;
			    result2 = new java.util.ArrayList<II>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (II anElement2 : aClass.getId()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim083_3
	* AssociatedEntity SHALL NOT have addr element with nullFlavor, if there are other addr (RMIM-083, RIM-016)
	*
	*/
	private static boolean _validateAssociatedEntitySpec_Rmim083_3(net.ihe.gazelle.cda.POCDMT000040AssociatedEntity aClass){
		java.util.ArrayList<AD> result1;
		result1 = new java.util.ArrayList<AD>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (AD anElement1 : aClass.getAddr()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAddr()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim083_4
	* The addr elements of AssociatedEntity SHALL be distinct  (RMIM-083, RIM-017)
	*
	*/
	private static boolean _validateAssociatedEntitySpec_Rmim083_4(net.ihe.gazelle.cda.POCDMT000040AssociatedEntity aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (AD anElement1 : aClass.getAddr()) {
			    java.util.ArrayList<AD> result2;
			    result2 = new java.util.ArrayList<AD>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (AD anElement2 : aClass.getAddr()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim083_5
	* AssociatedEntity SHALL NOT have telecom element with nullFlavor, if there are other  (RMIM-083, RIM-018)
	*
	*/
	private static boolean _validateAssociatedEntitySpec_Rmim083_5(net.ihe.gazelle.cda.POCDMT000040AssociatedEntity aClass){
		java.util.ArrayList<TEL> result1;
		result1 = new java.util.ArrayList<TEL>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (TEL anElement1 : aClass.getTelecom()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getTelecom()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim083_6
	* The telecom elements of AssociatedEntity SHALL be distinct  (RMIM-083, RIM-019)
	*
	*/
	private static boolean _validateAssociatedEntitySpec_Rmim083_6(net.ihe.gazelle.cda.POCDMT000040AssociatedEntity aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (TEL anElement1 : aClass.getTelecom()) {
			    java.util.ArrayList<TEL> result2;
			    result2 = new java.util.ArrayList<TEL>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (TEL anElement2 : aClass.getTelecom()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim084
	* If associatedEntity has not a null of flavor and its enclosing participant has not a null of flavor,  it SHALL have an associatedPerson, or a scopingOrganization (RMIM-084)
	*
	*/
	private static boolean _validateAssociatedEntitySpec_Rmim084(net.ihe.gazelle.cda.POCDMT000040AssociatedEntity aClass){
		return (((((!(aClass.getNullFlavor() == null) || (aClass.get__parent() == null)) || !(POCDMT000040Participant1.class.isAssignableFrom(aClass.get__parent().getClass()))) || !(((POCDMT000040Participant1) aClass.get__parent()).getNullFlavor() == null)) || !(aClass.getAssociatedPerson() == null)) || !(aClass.getScopingOrganization() == null));
		
	}

	/**
	* Validation of class-constraint : AssociatedEntitySpec
    * Verify if an element of type AssociatedEntitySpec can be validated by AssociatedEntitySpec
	*
	*/
	public static boolean _isAssociatedEntitySpec(net.ihe.gazelle.cda.POCDMT000040AssociatedEntity aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   AssociatedEntitySpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040AssociatedEntity
     * 
     */
    public static void _validateAssociatedEntitySpec(net.ihe.gazelle.cda.POCDMT000040AssociatedEntity aClass, String location, List<Notification> diagnostic) {
		if (_isAssociatedEntitySpec(aClass)){
			executeCons_AssociatedEntitySpec_Rmim083_1(aClass, location, diagnostic);
			executeCons_AssociatedEntitySpec_Rmim083_2(aClass, location, diagnostic);
			executeCons_AssociatedEntitySpec_Rmim083_3(aClass, location, diagnostic);
			executeCons_AssociatedEntitySpec_Rmim083_4(aClass, location, diagnostic);
			executeCons_AssociatedEntitySpec_Rmim083_5(aClass, location, diagnostic);
			executeCons_AssociatedEntitySpec_Rmim083_6(aClass, location, diagnostic);
			executeCons_AssociatedEntitySpec_Rmim084(aClass, location, diagnostic);
		}
	}

	private static void executeCons_AssociatedEntitySpec_Rmim083_1(net.ihe.gazelle.cda.POCDMT000040AssociatedEntity aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAssociatedEntitySpec_Rmim083_1(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim083_1");
		notif.setDescription("AssociatedEntity SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-083, RIM-011)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-AssociatedEntitySpec-rmim083_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-083;RIM-011"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_AssociatedEntitySpec_Rmim083_2(net.ihe.gazelle.cda.POCDMT000040AssociatedEntity aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAssociatedEntitySpec_Rmim083_2(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim083_2");
		notif.setDescription("The ids elements of AssociatedEntity SHALL be distinct (RMIM-083, RIM-012)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-AssociatedEntitySpec-rmim083_2");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-083;RIM-012"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_AssociatedEntitySpec_Rmim083_3(net.ihe.gazelle.cda.POCDMT000040AssociatedEntity aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAssociatedEntitySpec_Rmim083_3(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim083_3");
		notif.setDescription("AssociatedEntity SHALL NOT have addr element with nullFlavor, if there are other addr (RMIM-083, RIM-016)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-AssociatedEntitySpec-rmim083_3");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-083;RIM-016"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_AssociatedEntitySpec_Rmim083_4(net.ihe.gazelle.cda.POCDMT000040AssociatedEntity aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAssociatedEntitySpec_Rmim083_4(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim083_4");
		notif.setDescription("The addr elements of AssociatedEntity SHALL be distinct  (RMIM-083, RIM-017)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-AssociatedEntitySpec-rmim083_4");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-083;RIM-017"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_AssociatedEntitySpec_Rmim083_5(net.ihe.gazelle.cda.POCDMT000040AssociatedEntity aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAssociatedEntitySpec_Rmim083_5(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim083_5");
		notif.setDescription("AssociatedEntity SHALL NOT have telecom element with nullFlavor, if there are other  (RMIM-083, RIM-018)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-AssociatedEntitySpec-rmim083_5");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-083;RIM-018"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_AssociatedEntitySpec_Rmim083_6(net.ihe.gazelle.cda.POCDMT000040AssociatedEntity aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAssociatedEntitySpec_Rmim083_6(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim083_6");
		notif.setDescription("The telecom elements of AssociatedEntity SHALL be distinct  (RMIM-083, RIM-019)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-AssociatedEntitySpec-rmim083_6");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-083;RIM-019"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_AssociatedEntitySpec_Rmim084(net.ihe.gazelle.cda.POCDMT000040AssociatedEntity aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAssociatedEntitySpec_Rmim084(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim084");
		notif.setDescription("If associatedEntity has not a null of flavor and its enclosing participant has not a null of flavor,  it SHALL have an associatedPerson, or a scopingOrganization (RMIM-084)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-AssociatedEntitySpec-rmim084");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-084"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

}
