package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.datatypes.PN;
import net.ihe.gazelle.datatypes.PQ;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;	



 /**
  * class :        PlayingEntitySpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040PlayingEntity
  * 
  */
public final class PlayingEntitySpecValidator{


    private PlayingEntitySpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim104_1
	* PlayingEntity SHALL NOT have name element with nullFlavor, if there are other name (RMIM-104, RIM-022)
	*
	*/
	private static boolean _validatePlayingEntitySpec_Rmim104_1(net.ihe.gazelle.cda.POCDMT000040PlayingEntity aClass){
		java.util.ArrayList<PN> result1;
		result1 = new java.util.ArrayList<PN>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (PN anElement1 : aClass.getName()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getName()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim104_2
	* The name elements of PlayingEntity SHALL be distinct (RMIM-104, RIM-023)
	*
	*/
	private static boolean _validatePlayingEntitySpec_Rmim104_2(net.ihe.gazelle.cda.POCDMT000040PlayingEntity aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (PN anElement1 : aClass.getName()) {
			    java.util.ArrayList<PN> result2;
			    result2 = new java.util.ArrayList<PN>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (PN anElement2 : aClass.getName()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim105
	* PlayingEntity SHALL NOT have quantity element with nullFlavor, if there are other quantity elements which are not null (RMIM-105)
	*
	*/
	private static boolean _validatePlayingEntitySpec_Rmim105(net.ihe.gazelle.cda.POCDMT000040PlayingEntity aClass){
		java.util.ArrayList<PQ> result1;
		result1 = new java.util.ArrayList<PQ>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (PQ anElement1 : aClass.getQuantity()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getQuantity()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim106
	* The quantity elements of PlayingEntity SHALL be distinct (RMIM-106)
	*
	*/
	private static boolean _validatePlayingEntitySpec_Rmim106(net.ihe.gazelle.cda.POCDMT000040PlayingEntity aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (PQ anElement1 : aClass.getQuantity()) {
			    java.util.ArrayList<PQ> result2;
			    result2 = new java.util.ArrayList<PQ>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (PQ anElement2 : aClass.getQuantity()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of class-constraint : PlayingEntitySpec
    * Verify if an element of type PlayingEntitySpec can be validated by PlayingEntitySpec
	*
	*/
	public static boolean _isPlayingEntitySpec(net.ihe.gazelle.cda.POCDMT000040PlayingEntity aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   PlayingEntitySpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040PlayingEntity
     * 
     */
    public static void _validatePlayingEntitySpec(net.ihe.gazelle.cda.POCDMT000040PlayingEntity aClass, String location, List<Notification> diagnostic) {
		if (_isPlayingEntitySpec(aClass)){
			executeCons_PlayingEntitySpec_Rmim104_1(aClass, location, diagnostic);
			executeCons_PlayingEntitySpec_Rmim104_2(aClass, location, diagnostic);
			executeCons_PlayingEntitySpec_Rmim105(aClass, location, diagnostic);
			executeCons_PlayingEntitySpec_Rmim106(aClass, location, diagnostic);
		}
	}

	private static void executeCons_PlayingEntitySpec_Rmim104_1(net.ihe.gazelle.cda.POCDMT000040PlayingEntity aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validatePlayingEntitySpec_Rmim104_1(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim104_1");
		notif.setDescription("PlayingEntity SHALL NOT have name element with nullFlavor, if there are other name (RMIM-104, RIM-022)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-PlayingEntitySpec-rmim104_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-104;RIM-022"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_PlayingEntitySpec_Rmim104_2(net.ihe.gazelle.cda.POCDMT000040PlayingEntity aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validatePlayingEntitySpec_Rmim104_2(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim104_2");
		notif.setDescription("The name elements of PlayingEntity SHALL be distinct (RMIM-104, RIM-023)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-PlayingEntitySpec-rmim104_2");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-104;RIM-023"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_PlayingEntitySpec_Rmim105(net.ihe.gazelle.cda.POCDMT000040PlayingEntity aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validatePlayingEntitySpec_Rmim105(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim105");
		notif.setDescription("PlayingEntity SHALL NOT have quantity element with nullFlavor, if there are other quantity elements which are not null (RMIM-105)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-PlayingEntitySpec-rmim105");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-105"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_PlayingEntitySpec_Rmim106(net.ihe.gazelle.cda.POCDMT000040PlayingEntity aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validatePlayingEntitySpec_Rmim106(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim106");
		notif.setDescription("The quantity elements of PlayingEntity SHALL be distinct (RMIM-106)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-PlayingEntitySpec-rmim106");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-106"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

}
