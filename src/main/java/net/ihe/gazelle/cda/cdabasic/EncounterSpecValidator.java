package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.datatypes.II;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;	



 /**
  * class :        EncounterSpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040Encounter
  * 
  */
public final class EncounterSpecValidator{


    private EncounterSpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim003_1
	* Encounter SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-003, RIM-001)
	*
	*/
	private static boolean _validateEncounterSpec_Rmim003_1(net.ihe.gazelle.cda.POCDMT000040Encounter aClass){
		java.util.ArrayList<II> result1;
		result1 = new java.util.ArrayList<II>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getId()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim003_2
	* The ids elements of Encounter SHALL be distinct (RMIM-003, RIM-002)
	*
	*/
	private static boolean _validateEncounterSpec_Rmim003_2(net.ihe.gazelle.cda.POCDMT000040Encounter aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    java.util.ArrayList<II> result2;
			    result2 = new java.util.ArrayList<II>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (II anElement2 : aClass.getId()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim003_3
	* The statusCode if presents SHALL be from the valueSet ActStatus (RMIM-003, RIM-003)
	*
	*/
	private static boolean _validateEncounterSpec_Rmim003_3(net.ihe.gazelle.cda.POCDMT000040Encounter aClass){
		return (((aClass.getStatusCode() == null) || !(aClass.getStatusCode().getNullFlavor() == null)) || aClass.getStatusCode().matchesCodeToValueSet("1.3.6.1.4.1.12559.11.18.2", aClass.getStatusCode().getCode()));
		
	}

	/**
	* Validation of class-constraint : EncounterSpec
    * Verify if an element of type EncounterSpec can be validated by EncounterSpec
	*
	*/
	public static boolean _isEncounterSpec(net.ihe.gazelle.cda.POCDMT000040Encounter aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   EncounterSpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040Encounter
     * 
     */
    public static void _validateEncounterSpec(net.ihe.gazelle.cda.POCDMT000040Encounter aClass, String location, List<Notification> diagnostic) {
		if (_isEncounterSpec(aClass)){
			executeCons_EncounterSpec_Rmim003_1(aClass, location, diagnostic);
			executeCons_EncounterSpec_Rmim003_2(aClass, location, diagnostic);
			executeCons_EncounterSpec_Rmim003_3(aClass, location, diagnostic);
		}
	}

	private static void executeCons_EncounterSpec_Rmim003_1(net.ihe.gazelle.cda.POCDMT000040Encounter aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateEncounterSpec_Rmim003_1(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim003_1");
		notif.setDescription("Encounter SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-003, RIM-001)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-EncounterSpec-rmim003_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-003;RIM-001"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_EncounterSpec_Rmim003_2(net.ihe.gazelle.cda.POCDMT000040Encounter aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateEncounterSpec_Rmim003_2(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim003_2");
		notif.setDescription("The ids elements of Encounter SHALL be distinct (RMIM-003, RIM-002)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-EncounterSpec-rmim003_2");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-003;RIM-002"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_EncounterSpec_Rmim003_3(net.ihe.gazelle.cda.POCDMT000040Encounter aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateEncounterSpec_Rmim003_3(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim003_3");
		notif.setDescription("The statusCode if presents SHALL be from the valueSet ActStatus (RMIM-003, RIM-003)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-EncounterSpec-rmim003_3");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-003;RIM-003"));
		
		notif.setType("Vocabulary");
		diagnostic.add(notif);
	}

}
