package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.datatypes.II;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;	



 /**
  * class :        OrganizationPartOfSpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040OrganizationPartOf
  * 
  */
public final class OrganizationPartOfSpecValidator{


    private OrganizationPartOfSpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim071_1
	* OrganizationPartOf SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-071, RIM-011)
	*
	*/
	private static boolean _validateOrganizationPartOfSpec_Rmim071_1(net.ihe.gazelle.cda.POCDMT000040OrganizationPartOf aClass){
		java.util.ArrayList<II> result1;
		result1 = new java.util.ArrayList<II>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getId()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim071_2
	* The ids elements of OrganizationPartOf SHALL be distinct (RMIM-071, RIM-012)
	*
	*/
	private static boolean _validateOrganizationPartOfSpec_Rmim071_2(net.ihe.gazelle.cda.POCDMT000040OrganizationPartOf aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    java.util.ArrayList<II> result2;
			    result2 = new java.util.ArrayList<II>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (II anElement2 : aClass.getId()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim071_3
	* The statusCode if presents SHALL be from the valueSet RoleStatus (RMIM-071, RIM-013)
	*
	*/
	private static boolean _validateOrganizationPartOfSpec_Rmim071_3(net.ihe.gazelle.cda.POCDMT000040OrganizationPartOf aClass){
		return (((aClass.getStatusCode() == null) || (((String) aClass.getStatusCode().getCode()) == null)) || aClass.getStatusCode().matchesCodeToValueSet("1.3.6.1.4.1.12559.11.18.1", aClass.getStatusCode().getCode()));
		
	}

	/**
	* Validation of class-constraint : OrganizationPartOfSpec
    * Verify if an element of type OrganizationPartOfSpec can be validated by OrganizationPartOfSpec
	*
	*/
	public static boolean _isOrganizationPartOfSpec(net.ihe.gazelle.cda.POCDMT000040OrganizationPartOf aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   OrganizationPartOfSpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040OrganizationPartOf
     * 
     */
    public static void _validateOrganizationPartOfSpec(net.ihe.gazelle.cda.POCDMT000040OrganizationPartOf aClass, String location, List<Notification> diagnostic) {
		if (_isOrganizationPartOfSpec(aClass)){
			executeCons_OrganizationPartOfSpec_Rmim071_1(aClass, location, diagnostic);
			executeCons_OrganizationPartOfSpec_Rmim071_2(aClass, location, diagnostic);
			executeCons_OrganizationPartOfSpec_Rmim071_3(aClass, location, diagnostic);
		}
	}

	private static void executeCons_OrganizationPartOfSpec_Rmim071_1(net.ihe.gazelle.cda.POCDMT000040OrganizationPartOf aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateOrganizationPartOfSpec_Rmim071_1(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim071_1");
		notif.setDescription("OrganizationPartOf SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-071, RIM-011)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-OrganizationPartOfSpec-rmim071_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-071;RIM-011"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_OrganizationPartOfSpec_Rmim071_2(net.ihe.gazelle.cda.POCDMT000040OrganizationPartOf aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateOrganizationPartOfSpec_Rmim071_2(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim071_2");
		notif.setDescription("The ids elements of OrganizationPartOf SHALL be distinct (RMIM-071, RIM-012)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-OrganizationPartOfSpec-rmim071_2");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-071;RIM-012"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_OrganizationPartOfSpec_Rmim071_3(net.ihe.gazelle.cda.POCDMT000040OrganizationPartOf aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateOrganizationPartOfSpec_Rmim071_3(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim071_3");
		notif.setDescription("The statusCode if presents SHALL be from the valueSet RoleStatus (RMIM-071, RIM-013)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-OrganizationPartOfSpec-rmim071_3");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-071;RIM-013"));
		
		notif.setType("Vocabulary");
		diagnostic.add(notif);
	}

}
