package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.datatypes.AD;
import net.ihe.gazelle.datatypes.II;
import net.ihe.gazelle.datatypes.ON;
import net.ihe.gazelle.datatypes.TEL;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;	



 /**
  * class :        OrganizationSpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040Organization
  * 
  */
public final class OrganizationSpecValidator{


    private OrganizationSpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim094_1
	* Organization SHALL NOT have id element with nullFlavor, if there are other ids (RMIM-094, RIM-020)
	*
	*/
	private static boolean _validateOrganizationSpec_Rmim094_1(net.ihe.gazelle.cda.POCDMT000040Organization aClass){
		java.util.ArrayList<II> result1;
		result1 = new java.util.ArrayList<II>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getId()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim094_2
	* The ids elements of Organization SHALL be distinct (RMIM-094, RIM-021)
	*
	*/
	private static boolean _validateOrganizationSpec_Rmim094_2(net.ihe.gazelle.cda.POCDMT000040Organization aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    java.util.ArrayList<II> result2;
			    result2 = new java.util.ArrayList<II>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (II anElement2 : aClass.getId()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim094_3
	* Organization SHALL NOT have name element with nullFlavor, if there are other name (RMIM-094, RIM-022)
	*
	*/
	private static boolean _validateOrganizationSpec_Rmim094_3(net.ihe.gazelle.cda.POCDMT000040Organization aClass){
		java.util.ArrayList<ON> result1;
		result1 = new java.util.ArrayList<ON>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (ON anElement1 : aClass.getName()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getName()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim094_4
	* The name elements of Organizarion SHALL be distinct (RMIM-094, RIM-023)
	*
	*/
	private static boolean _validateOrganizationSpec_Rmim094_4(net.ihe.gazelle.cda.POCDMT000040Organization aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (ON anElement1 : aClass.getName()) {
			    java.util.ArrayList<ON> result2;
			    result2 = new java.util.ArrayList<ON>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (ON anElement2 : aClass.getName()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim094_5
	* Organization SHALL NOT have telecom element with nullFlavor,  if there are other telecom elements which are not null (RMIM-094, RIM-024)
	*
	*/
	private static boolean _validateOrganizationSpec_Rmim094_5(net.ihe.gazelle.cda.POCDMT000040Organization aClass){
		java.util.ArrayList<TEL> result1;
		result1 = new java.util.ArrayList<TEL>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (TEL anElement1 : aClass.getTelecom()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getTelecom()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim094_6
	* The telecom elements of Entity SHALL be distinct (RMIM-094, RIM-025)
	*
	*/
	private static boolean _validateOrganizationSpec_Rmim094_6(net.ihe.gazelle.cda.POCDMT000040Organization aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (TEL anElement1 : aClass.getTelecom()) {
			    java.util.ArrayList<TEL> result2;
			    result2 = new java.util.ArrayList<TEL>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (TEL anElement2 : aClass.getTelecom()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim095
	* Organization SHALL NOT have addr element with nullFlavor, if there are other (RMIM-095)
	*
	*/
	private static boolean _validateOrganizationSpec_Rmim095(net.ihe.gazelle.cda.POCDMT000040Organization aClass){
		java.util.ArrayList<AD> result1;
		result1 = new java.util.ArrayList<AD>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (AD anElement1 : aClass.getAddr()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAddr()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim096
	* The addr elements of Organization SHALL be distinct (RMIM-096)
	*
	*/
	private static boolean _validateOrganizationSpec_Rmim096(net.ihe.gazelle.cda.POCDMT000040Organization aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (AD anElement1 : aClass.getAddr()) {
			    java.util.ArrayList<AD> result2;
			    result2 = new java.util.ArrayList<AD>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (AD anElement2 : aClass.getAddr()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of class-constraint : OrganizationSpec
    * Verify if an element of type OrganizationSpec can be validated by OrganizationSpec
	*
	*/
	public static boolean _isOrganizationSpec(net.ihe.gazelle.cda.POCDMT000040Organization aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   OrganizationSpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040Organization
     * 
     */
    public static void _validateOrganizationSpec(net.ihe.gazelle.cda.POCDMT000040Organization aClass, String location, List<Notification> diagnostic) {
		if (_isOrganizationSpec(aClass)){
			executeCons_OrganizationSpec_Rmim094_1(aClass, location, diagnostic);
			executeCons_OrganizationSpec_Rmim094_2(aClass, location, diagnostic);
			executeCons_OrganizationSpec_Rmim094_3(aClass, location, diagnostic);
			executeCons_OrganizationSpec_Rmim094_4(aClass, location, diagnostic);
			executeCons_OrganizationSpec_Rmim094_5(aClass, location, diagnostic);
			executeCons_OrganizationSpec_Rmim094_6(aClass, location, diagnostic);
			executeCons_OrganizationSpec_Rmim095(aClass, location, diagnostic);
			executeCons_OrganizationSpec_Rmim096(aClass, location, diagnostic);
		}
	}

	private static void executeCons_OrganizationSpec_Rmim094_1(net.ihe.gazelle.cda.POCDMT000040Organization aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateOrganizationSpec_Rmim094_1(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim094_1");
		notif.setDescription("Organization SHALL NOT have id element with nullFlavor, if there are other ids (RMIM-094, RIM-020)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-OrganizationSpec-rmim094_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-094;RIM-020"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_OrganizationSpec_Rmim094_2(net.ihe.gazelle.cda.POCDMT000040Organization aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateOrganizationSpec_Rmim094_2(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim094_2");
		notif.setDescription("The ids elements of Organization SHALL be distinct (RMIM-094, RIM-021)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-OrganizationSpec-rmim094_2");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-094;RIM-021"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_OrganizationSpec_Rmim094_3(net.ihe.gazelle.cda.POCDMT000040Organization aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateOrganizationSpec_Rmim094_3(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim094_3");
		notif.setDescription("Organization SHALL NOT have name element with nullFlavor, if there are other name (RMIM-094, RIM-022)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-OrganizationSpec-rmim094_3");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-094;RIM-022"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_OrganizationSpec_Rmim094_4(net.ihe.gazelle.cda.POCDMT000040Organization aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateOrganizationSpec_Rmim094_4(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim094_4");
		notif.setDescription("The name elements of Organizarion SHALL be distinct (RMIM-094, RIM-023)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-OrganizationSpec-rmim094_4");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-094;RIM-023"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_OrganizationSpec_Rmim094_5(net.ihe.gazelle.cda.POCDMT000040Organization aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateOrganizationSpec_Rmim094_5(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim094_5");
		notif.setDescription("Organization SHALL NOT have telecom element with nullFlavor,  if there are other telecom elements which are not null (RMIM-094, RIM-024)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-OrganizationSpec-rmim094_5");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-094"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_OrganizationSpec_Rmim094_6(net.ihe.gazelle.cda.POCDMT000040Organization aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateOrganizationSpec_Rmim094_6(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim094_6");
		notif.setDescription("The telecom elements of Entity SHALL be distinct (RMIM-094, RIM-025)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-OrganizationSpec-rmim094_6");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-094"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_OrganizationSpec_Rmim095(net.ihe.gazelle.cda.POCDMT000040Organization aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateOrganizationSpec_Rmim095(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim095");
		notif.setDescription("Organization SHALL NOT have addr element with nullFlavor, if there are other (RMIM-095)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-OrganizationSpec-rmim095");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-095"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_OrganizationSpec_Rmim096(net.ihe.gazelle.cda.POCDMT000040Organization aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateOrganizationSpec_Rmim096(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim096");
		notif.setDescription("The addr elements of Organization SHALL be distinct (RMIM-096)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-OrganizationSpec-rmim096");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-096"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

}
