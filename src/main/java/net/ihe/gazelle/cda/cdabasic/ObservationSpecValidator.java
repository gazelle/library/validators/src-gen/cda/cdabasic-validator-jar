package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.datatypes.ANY;
import net.ihe.gazelle.datatypes.CD;
import net.ihe.gazelle.datatypes.CE;
import net.ihe.gazelle.datatypes.CS;
import net.ihe.gazelle.datatypes.II;
import net.ihe.gazelle.datatypes.ST;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;	



 /**
  * class :        ObservationSpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040Observation
  * 
  */
public final class ObservationSpecValidator{


    private ObservationSpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim004_1
	* Observation SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-004, RIM-001)
	*
	*/
	private static boolean _validateObservationSpec_Rmim004_1(net.ihe.gazelle.cda.POCDMT000040Observation aClass){
		java.util.ArrayList<II> result1;
		result1 = new java.util.ArrayList<II>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getId()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim004_2
	* The ids elements of Observation SHALL be distinct (RMIM-004, RIM-002)
	*
	*/
	private static boolean _validateObservationSpec_Rmim004_2(net.ihe.gazelle.cda.POCDMT000040Observation aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    java.util.ArrayList<II> result2;
			    result2 = new java.util.ArrayList<II>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (II anElement2 : aClass.getId()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim004_3
	* The statusCode if presents SHALL be from the valueSet ActStatus (RMIM-004, RIM-003)
	*
	*/
	private static boolean _validateObservationSpec_Rmim004_3(net.ihe.gazelle.cda.POCDMT000040Observation aClass){
		return (((aClass.getStatusCode() == null) || (((String) aClass.getStatusCode().getCode()) == null)) || aClass.getStatusCode().matchesCodeToValueSet("1.3.6.1.4.1.12559.11.18.2", aClass.getStatusCode().getCode()));
		
	}

	/**
	* Validation of instance by a constraint : rmim004_4
	* Observation SHALL NOT have interpretationCode element with nullFlavor,  if there are other interpretationCode elements which are not null (RMIM-004, RIM-004)
	*
	*/
	private static boolean _validateObservationSpec_Rmim004_4(net.ihe.gazelle.cda.POCDMT000040Observation aClass){
		java.util.ArrayList<CE> result1;
		result1 = new java.util.ArrayList<CE>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (CE anElement1 : aClass.getInterpretationCode()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getInterpretationCode()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim004_5
	* The interpretationCode elements of Act SHALL be distinct (RMIM-004, RIM-005)
	*
	*/
	private static boolean _validateObservationSpec_Rmim004_5(net.ihe.gazelle.cda.POCDMT000040Observation aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (CE anElement1 : aClass.getInterpretationCode()) {
			    java.util.ArrayList<CE> result2;
			    result2 = new java.util.ArrayList<CE>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (CE anElement2 : aClass.getInterpretationCode()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim004_6
	* Observation SHALL NOT have methodCode element with nullFlavor,  if there are other methodCode elements which are not null   (RMIM-004, RIM-006)
	*
	*/
	private static boolean _validateObservationSpec_Rmim004_6(net.ihe.gazelle.cda.POCDMT000040Observation aClass){
		java.util.ArrayList<CE> result1;
		result1 = new java.util.ArrayList<CE>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (CE anElement1 : aClass.getMethodCode()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getMethodCode()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim004_7
	* The methodCode elements of Act SHALL be distinct  (RMIM-004, RIM-007)
	*
	*/
	private static boolean _validateObservationSpec_Rmim004_7(net.ihe.gazelle.cda.POCDMT000040Observation aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (CE anElement1 : aClass.getMethodCode()) {
			    java.util.ArrayList<CE> result2;
			    result2 = new java.util.ArrayList<CE>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (CE anElement2 : aClass.getMethodCode()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim004_8
	* Observation SHALL NOT have targetSiteCode element with nullFlavor, if there are other targetSiteCode elements which are not null   (RMIM-004, RIM-008)
	*
	*/
	private static boolean _validateObservationSpec_Rmim004_8(net.ihe.gazelle.cda.POCDMT000040Observation aClass){
		java.util.ArrayList<CD> result1;
		result1 = new java.util.ArrayList<CD>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (CD anElement1 : aClass.getTargetSiteCode()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getTargetSiteCode()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim004_9
	* The targetSiteCode elements of Act SHALL be distinct  (RMIM-004, RIM-009)
	*
	*/
	private static boolean _validateObservationSpec_Rmim004_9(net.ihe.gazelle.cda.POCDMT000040Observation aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (CD anElement1 : aClass.getTargetSiteCode()) {
			    java.util.ArrayList<CD> result2;
			    result2 = new java.util.ArrayList<CD>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (CD anElement2 : aClass.getTargetSiteCode()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim005
	* The languageCode shall be from the valueSet HumanLanguage (RMIM-005)
	*
	*/
	private static boolean _validateObservationSpec_Rmim005(net.ihe.gazelle.cda.POCDMT000040Observation aClass){
		return (((aClass.getLanguageCode() == null) || !(aClass.getLanguageCode().getNullFlavor() == null)) || (!(((String) aClass.getLanguageCode().getCode()) == null) && aClass.getLanguageCode().matches(aClass.getLanguageCode().getCode(), "\\w+(\\-\\w+)?")));
		
	}

	/**
	* Validation of instance by a constraint : rmim009
	* Numeric values SHALL NOT be communicated in Observation/value as a simple String (RMIM-009)
	*
	*/
	private static boolean _validateObservationSpec_Rmim009(net.ihe.gazelle.cda.POCDMT000040Observation aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (ANY anElement1 : aClass.getValue()) {
			    if (!(!anElement1.getClass().getCanonicalName().equals(ST.class.getCanonicalName()) || CriterionSpecValidator.validateValueIsNotNumeric(anElement1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim010
	* CS datatype SHALL NOT be used in value element, because only datatypes with (RMIM-010)
	*
	*/
	private static boolean _validateObservationSpec_Rmim010(net.ihe.gazelle.cda.POCDMT000040Observation aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (ANY anElement1 : aClass.getValue()) {
			    if (!!(CS.class.isAssignableFrom(anElement1.getClass()))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim011
	* interpretationCode values SHALL be from the valueSet ObservationInterpretation (RMIM-011)
	*
	*/
	private static boolean _validateObservationSpec_Rmim011(net.ihe.gazelle.cda.POCDMT000040Observation aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (CE anElement1 : aClass.getInterpretationCode()) {
			    if (!((((String) anElement1.getCode()) == null) || anElement1.matchesValueSet("1.3.6.1.4.1.12559.11.18.5", anElement1.getCode(), anElement1.getCodeSystem(), null, null))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of class-constraint : ObservationSpec
    * Verify if an element of type ObservationSpec can be validated by ObservationSpec
	*
	*/
	public static boolean _isObservationSpec(net.ihe.gazelle.cda.POCDMT000040Observation aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   ObservationSpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040Observation
     * 
     */
    public static void _validateObservationSpec(net.ihe.gazelle.cda.POCDMT000040Observation aClass, String location, List<Notification> diagnostic) {
		if (_isObservationSpec(aClass)){
			executeCons_ObservationSpec_Rmim004_1(aClass, location, diagnostic);
			executeCons_ObservationSpec_Rmim004_2(aClass, location, diagnostic);
			executeCons_ObservationSpec_Rmim004_3(aClass, location, diagnostic);
			executeCons_ObservationSpec_Rmim004_4(aClass, location, diagnostic);
			executeCons_ObservationSpec_Rmim004_5(aClass, location, diagnostic);
			executeCons_ObservationSpec_Rmim004_6(aClass, location, diagnostic);
			executeCons_ObservationSpec_Rmim004_7(aClass, location, diagnostic);
			executeCons_ObservationSpec_Rmim004_8(aClass, location, diagnostic);
			executeCons_ObservationSpec_Rmim004_9(aClass, location, diagnostic);
			executeCons_ObservationSpec_Rmim005(aClass, location, diagnostic);
			executeCons_ObservationSpec_Rmim009(aClass, location, diagnostic);
			executeCons_ObservationSpec_Rmim010(aClass, location, diagnostic);
			executeCons_ObservationSpec_Rmim011(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ObservationSpec_Rmim004_1(net.ihe.gazelle.cda.POCDMT000040Observation aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateObservationSpec_Rmim004_1(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim004_1");
		notif.setDescription("Observation SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-004, RIM-001)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ObservationSpec-rmim004_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-004;RIM-001"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_ObservationSpec_Rmim004_2(net.ihe.gazelle.cda.POCDMT000040Observation aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateObservationSpec_Rmim004_2(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim004_2");
		notif.setDescription("The ids elements of Observation SHALL be distinct (RMIM-004, RIM-002)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ObservationSpec-rmim004_2");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-004;RIM-002"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_ObservationSpec_Rmim004_3(net.ihe.gazelle.cda.POCDMT000040Observation aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateObservationSpec_Rmim004_3(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim004_3");
		notif.setDescription("The statusCode if presents SHALL be from the valueSet ActStatus (RMIM-004, RIM-003)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ObservationSpec-rmim004_3");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-004;RIM-003"));
		
		notif.setType("Vocabulary");
		diagnostic.add(notif);
	}

	private static void executeCons_ObservationSpec_Rmim004_4(net.ihe.gazelle.cda.POCDMT000040Observation aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateObservationSpec_Rmim004_4(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim004_4");
		notif.setDescription("Observation SHALL NOT have interpretationCode element with nullFlavor,  if there are other interpretationCode elements which are not null (RMIM-004, RIM-004)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ObservationSpec-rmim004_4");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-004;RIM-004"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_ObservationSpec_Rmim004_5(net.ihe.gazelle.cda.POCDMT000040Observation aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateObservationSpec_Rmim004_5(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim004_5");
		notif.setDescription("The interpretationCode elements of Act SHALL be distinct (RMIM-004, RIM-005)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ObservationSpec-rmim004_5");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-004"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_ObservationSpec_Rmim004_6(net.ihe.gazelle.cda.POCDMT000040Observation aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateObservationSpec_Rmim004_6(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim004_6");
		notif.setDescription("Observation SHALL NOT have methodCode element with nullFlavor,  if there are other methodCode elements which are not null   (RMIM-004, RIM-006)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ObservationSpec-rmim004_6");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-004"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_ObservationSpec_Rmim004_7(net.ihe.gazelle.cda.POCDMT000040Observation aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateObservationSpec_Rmim004_7(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim004_7");
		notif.setDescription("The methodCode elements of Act SHALL be distinct  (RMIM-004, RIM-007)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ObservationSpec-rmim004_7");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-004"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_ObservationSpec_Rmim004_8(net.ihe.gazelle.cda.POCDMT000040Observation aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateObservationSpec_Rmim004_8(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim004_8");
		notif.setDescription("Observation SHALL NOT have targetSiteCode element with nullFlavor, if there are other targetSiteCode elements which are not null   (RMIM-004, RIM-008)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ObservationSpec-rmim004_8");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-004"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_ObservationSpec_Rmim004_9(net.ihe.gazelle.cda.POCDMT000040Observation aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateObservationSpec_Rmim004_9(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim004_9");
		notif.setDescription("The targetSiteCode elements of Act SHALL be distinct  (RMIM-004, RIM-009)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ObservationSpec-rmim004_9");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-004"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_ObservationSpec_Rmim005(net.ihe.gazelle.cda.POCDMT000040Observation aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateObservationSpec_Rmim005(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim005");
		notif.setDescription("The languageCode shall be from the valueSet HumanLanguage (RMIM-005)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ObservationSpec-rmim005");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-005"));
		
		notif.setType("Vocabulary");
		diagnostic.add(notif);
	}

	private static void executeCons_ObservationSpec_Rmim009(net.ihe.gazelle.cda.POCDMT000040Observation aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateObservationSpec_Rmim009(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim009");
		notif.setDescription("Numeric values SHALL NOT be communicated in Observation/value as a simple String (RMIM-009)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ObservationSpec-rmim009");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-009"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

	private static void executeCons_ObservationSpec_Rmim010(net.ihe.gazelle.cda.POCDMT000040Observation aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateObservationSpec_Rmim010(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim010");
		notif.setDescription("CS datatype SHALL NOT be used in value element, because only datatypes with (RMIM-010)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ObservationSpec-rmim010");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-010"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

	private static void executeCons_ObservationSpec_Rmim011(net.ihe.gazelle.cda.POCDMT000040Observation aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateObservationSpec_Rmim011(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim011");
		notif.setDescription("interpretationCode values SHALL be from the valueSet ObservationInterpretation (RMIM-011)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-ObservationSpec-rmim011");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-011"));
		
		notif.setType("Vocabulary");
		diagnostic.add(notif);
	}

}
