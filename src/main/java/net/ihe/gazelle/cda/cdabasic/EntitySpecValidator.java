package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.datatypes.II;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;	



 /**
  * class :        EntitySpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040Entity
  * 
  */
public final class EntitySpecValidator{


    private EntitySpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim107_1
	* Entity SHALL NOT have id element with nullFlavor, if there are other ids (RMIM-107, RIM-020)
	*
	*/
	private static boolean _validateEntitySpec_Rmim107_1(net.ihe.gazelle.cda.POCDMT000040Entity aClass){
		java.util.ArrayList<II> result1;
		result1 = new java.util.ArrayList<II>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getId()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim107_2
	* The ids elements of Entity SHALL be distinct (RMIM-107, RIM-021)
	*
	*/
	private static boolean _validateEntitySpec_Rmim107_2(net.ihe.gazelle.cda.POCDMT000040Entity aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    java.util.ArrayList<II> result2;
			    result2 = new java.util.ArrayList<II>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (II anElement2 : aClass.getId()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of class-constraint : EntitySpec
    * Verify if an element of type EntitySpec can be validated by EntitySpec
	*
	*/
	public static boolean _isEntitySpec(net.ihe.gazelle.cda.POCDMT000040Entity aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   EntitySpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040Entity
     * 
     */
    public static void _validateEntitySpec(net.ihe.gazelle.cda.POCDMT000040Entity aClass, String location, List<Notification> diagnostic) {
		if (_isEntitySpec(aClass)){
			executeCons_EntitySpec_Rmim107_1(aClass, location, diagnostic);
			executeCons_EntitySpec_Rmim107_2(aClass, location, diagnostic);
		}
	}

	private static void executeCons_EntitySpec_Rmim107_1(net.ihe.gazelle.cda.POCDMT000040Entity aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateEntitySpec_Rmim107_1(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim107_1");
		notif.setDescription("Entity SHALL NOT have id element with nullFlavor, if there are other ids (RMIM-107, RIM-020)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-EntitySpec-rmim107_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-107;RIM-020"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_EntitySpec_Rmim107_2(net.ihe.gazelle.cda.POCDMT000040Entity aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateEntitySpec_Rmim107_2(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim107_2");
		notif.setDescription("The ids elements of Entity SHALL be distinct (RMIM-107, RIM-021)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-EntitySpec-rmim107_2");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-107;RIM-021"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

}
