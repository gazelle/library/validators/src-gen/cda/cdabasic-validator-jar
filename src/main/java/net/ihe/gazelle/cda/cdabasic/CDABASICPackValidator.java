package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import net.ihe.gazelle.validation.Notification;	



public class CDABASICPackValidator implements ConstraintValidatorModule{


    /**
     * Create a new ObjectValidator that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public CDABASICPackValidator() {}
    


	/**
	* Validation of instance of an object
	*
	*/
	public void validate(Object obj, String location, List<Notification> diagnostic){

		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040Act){
			net.ihe.gazelle.cda.POCDMT000040Act aClass = ( net.ihe.gazelle.cda.POCDMT000040Act)obj;
			ActSpecValidator._validateActSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040AssignedAuthor){
			net.ihe.gazelle.cda.POCDMT000040AssignedAuthor aClass = ( net.ihe.gazelle.cda.POCDMT000040AssignedAuthor)obj;
			AssignedAuthorSpecValidator._validateAssignedAuthorSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040AssignedEntity){
			net.ihe.gazelle.cda.POCDMT000040AssignedEntity aClass = ( net.ihe.gazelle.cda.POCDMT000040AssignedEntity)obj;
			AssignedEntitySpecValidator._validateAssignedEntitySpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040AssociatedEntity){
			net.ihe.gazelle.cda.POCDMT000040AssociatedEntity aClass = ( net.ihe.gazelle.cda.POCDMT000040AssociatedEntity)obj;
			AssociatedEntitySpecValidator._validateAssociatedEntitySpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040Authenticator){
			net.ihe.gazelle.cda.POCDMT000040Authenticator aClass = ( net.ihe.gazelle.cda.POCDMT000040Authenticator)obj;
			AuthenticatorSpecValidator._validateAuthenticatorSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040Author){
			net.ihe.gazelle.cda.POCDMT000040Author aClass = ( net.ihe.gazelle.cda.POCDMT000040Author)obj;
			AuthorSpecValidator._validateAuthorSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040AuthoringDevice){
			net.ihe.gazelle.cda.POCDMT000040AuthoringDevice aClass = ( net.ihe.gazelle.cda.POCDMT000040AuthoringDevice)obj;
			AuthoringDeviceSpecValidator._validateAuthoringDeviceSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040ClinicalDocument){
			net.ihe.gazelle.cda.POCDMT000040ClinicalDocument aClass = ( net.ihe.gazelle.cda.POCDMT000040ClinicalDocument)obj;
			ClinicalDocumentSpecValidator._validateClinicalDocumentSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040Consent){
			net.ihe.gazelle.cda.POCDMT000040Consent aClass = ( net.ihe.gazelle.cda.POCDMT000040Consent)obj;
			ConsentSpecValidator._validateConsentSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040Criterion){
			net.ihe.gazelle.cda.POCDMT000040Criterion aClass = ( net.ihe.gazelle.cda.POCDMT000040Criterion)obj;
			CriterionSpecValidator._validateCriterionSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040CustodianOrganization){
			net.ihe.gazelle.cda.POCDMT000040CustodianOrganization aClass = ( net.ihe.gazelle.cda.POCDMT000040CustodianOrganization)obj;
			CustodianOrganizationSpecValidator._validateCustodianOrganizationSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040EncompassingEncounter){
			net.ihe.gazelle.cda.POCDMT000040EncompassingEncounter aClass = ( net.ihe.gazelle.cda.POCDMT000040EncompassingEncounter)obj;
			EncompassingEncounterSpecValidator._validateEncompassingEncounterSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040Encounter){
			net.ihe.gazelle.cda.POCDMT000040Encounter aClass = ( net.ihe.gazelle.cda.POCDMT000040Encounter)obj;
			EncounterSpecValidator._validateEncounterSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040Entity){
			net.ihe.gazelle.cda.POCDMT000040Entity aClass = ( net.ihe.gazelle.cda.POCDMT000040Entity)obj;
			EntitySpecValidator._validateEntitySpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040Entry){
			net.ihe.gazelle.cda.POCDMT000040Entry aClass = ( net.ihe.gazelle.cda.POCDMT000040Entry)obj;
			EntrySpecValidator._validateEntrySpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040ExternalAct){
			net.ihe.gazelle.cda.POCDMT000040ExternalAct aClass = ( net.ihe.gazelle.cda.POCDMT000040ExternalAct)obj;
			ExternalActSpecValidator._validateExternalActSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040ExternalDocument){
			net.ihe.gazelle.cda.POCDMT000040ExternalDocument aClass = ( net.ihe.gazelle.cda.POCDMT000040ExternalDocument)obj;
			ExternalDocumentSpecValidator._validateExternalDocumentSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040ExternalObservation){
			net.ihe.gazelle.cda.POCDMT000040ExternalObservation aClass = ( net.ihe.gazelle.cda.POCDMT000040ExternalObservation)obj;
			ExternalObservationSpecValidator._validateExternalObservationSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040ExternalProcedure){
			net.ihe.gazelle.cda.POCDMT000040ExternalProcedure aClass = ( net.ihe.gazelle.cda.POCDMT000040ExternalProcedure)obj;
			ExternalProcedureSpecValidator._validateExternalProcedureSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040Guardian){
			net.ihe.gazelle.cda.POCDMT000040Guardian aClass = ( net.ihe.gazelle.cda.POCDMT000040Guardian)obj;
			GuardianSpecValidator._validateGuardianSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040HealthCareFacility){
			net.ihe.gazelle.cda.POCDMT000040HealthCareFacility aClass = ( net.ihe.gazelle.cda.POCDMT000040HealthCareFacility)obj;
			HealthCareFacilitySpecValidator._validateHealthCareFacilitySpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040IntendedRecipient){
			net.ihe.gazelle.cda.POCDMT000040IntendedRecipient aClass = ( net.ihe.gazelle.cda.POCDMT000040IntendedRecipient)obj;
			IntendedRecipientSpecValidator._validateIntendedRecipientSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040LegalAuthenticator){
			net.ihe.gazelle.cda.POCDMT000040LegalAuthenticator aClass = ( net.ihe.gazelle.cda.POCDMT000040LegalAuthenticator)obj;
			LegalAuthenticatorSpecValidator._validateLegalAuthenticatorSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040ManufacturedProduct){
			net.ihe.gazelle.cda.POCDMT000040ManufacturedProduct aClass = ( net.ihe.gazelle.cda.POCDMT000040ManufacturedProduct)obj;
			ManufacturedProductSpecValidator._validateManufacturedProductSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040NonXMLBody){
			net.ihe.gazelle.cda.POCDMT000040NonXMLBody aClass = ( net.ihe.gazelle.cda.POCDMT000040NonXMLBody)obj;
			NonXMLBodySpecValidator._validateNonXMLBodySpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040ObservationMedia){
			net.ihe.gazelle.cda.POCDMT000040ObservationMedia aClass = ( net.ihe.gazelle.cda.POCDMT000040ObservationMedia)obj;
			ObservationMediaSpecValidator._validateObservationMediaSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040ObservationRange){
			net.ihe.gazelle.cda.POCDMT000040ObservationRange aClass = ( net.ihe.gazelle.cda.POCDMT000040ObservationRange)obj;
			ObservationRangeSpecValidator._validateObservationRangeSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040Observation){
			net.ihe.gazelle.cda.POCDMT000040Observation aClass = ( net.ihe.gazelle.cda.POCDMT000040Observation)obj;
			ObservationSpecValidator._validateObservationSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040Order){
			net.ihe.gazelle.cda.POCDMT000040Order aClass = ( net.ihe.gazelle.cda.POCDMT000040Order)obj;
			OrderSpecValidator._validateOrderSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040OrganizationPartOf){
			net.ihe.gazelle.cda.POCDMT000040OrganizationPartOf aClass = ( net.ihe.gazelle.cda.POCDMT000040OrganizationPartOf)obj;
			OrganizationPartOfSpecValidator._validateOrganizationPartOfSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040Organization){
			net.ihe.gazelle.cda.POCDMT000040Organization aClass = ( net.ihe.gazelle.cda.POCDMT000040Organization)obj;
			OrganizationSpecValidator._validateOrganizationSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040Organizer){
			net.ihe.gazelle.cda.POCDMT000040Organizer aClass = ( net.ihe.gazelle.cda.POCDMT000040Organizer)obj;
			OrganizerSpecValidator._validateOrganizerSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040ParentDocument){
			net.ihe.gazelle.cda.POCDMT000040ParentDocument aClass = ( net.ihe.gazelle.cda.POCDMT000040ParentDocument)obj;
			ParentDocumentSpecValidator._validateParentDocumentSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040ParticipantRole){
			net.ihe.gazelle.cda.POCDMT000040ParticipantRole aClass = ( net.ihe.gazelle.cda.POCDMT000040ParticipantRole)obj;
			ParticipantRoleSpecValidator._validateParticipantRoleSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040PatientRole){
			net.ihe.gazelle.cda.POCDMT000040PatientRole aClass = ( net.ihe.gazelle.cda.POCDMT000040PatientRole)obj;
			PatientRoleSpecValidator._validatePatientRoleSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040Patient){
			net.ihe.gazelle.cda.POCDMT000040Patient aClass = ( net.ihe.gazelle.cda.POCDMT000040Patient)obj;
			PatientSpecValidator._validatePatientSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040Person){
			net.ihe.gazelle.cda.POCDMT000040Person aClass = ( net.ihe.gazelle.cda.POCDMT000040Person)obj;
			PersonSpecValidator._validatePersonSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040PlayingEntity){
			net.ihe.gazelle.cda.POCDMT000040PlayingEntity aClass = ( net.ihe.gazelle.cda.POCDMT000040PlayingEntity)obj;
			PlayingEntitySpecValidator._validatePlayingEntitySpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040Procedure){
			net.ihe.gazelle.cda.POCDMT000040Procedure aClass = ( net.ihe.gazelle.cda.POCDMT000040Procedure)obj;
			ProcedureSpecValidator._validateProcedureSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040RegionOfInterest){
			net.ihe.gazelle.cda.POCDMT000040RegionOfInterest aClass = ( net.ihe.gazelle.cda.POCDMT000040RegionOfInterest)obj;
			RegionOfInterestSpecValidator._validateRegionOfInterestSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040RelatedEntity){
			net.ihe.gazelle.cda.POCDMT000040RelatedEntity aClass = ( net.ihe.gazelle.cda.POCDMT000040RelatedEntity)obj;
			RelatedEntitySpecValidator._validateRelatedEntitySpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040RelatedSubject){
			net.ihe.gazelle.cda.POCDMT000040RelatedSubject aClass = ( net.ihe.gazelle.cda.POCDMT000040RelatedSubject)obj;
			RelatedSubjectSpecValidator._validateRelatedSubjectSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040Section){
			net.ihe.gazelle.cda.POCDMT000040Section aClass = ( net.ihe.gazelle.cda.POCDMT000040Section)obj;
			SectionSpecValidator._validateSectionSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040ServiceEvent){
			net.ihe.gazelle.cda.POCDMT000040ServiceEvent aClass = ( net.ihe.gazelle.cda.POCDMT000040ServiceEvent)obj;
			ServiceEventSpecValidator._validateServiceEventSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040SpecimenRole){
			net.ihe.gazelle.cda.POCDMT000040SpecimenRole aClass = ( net.ihe.gazelle.cda.POCDMT000040SpecimenRole)obj;
			SpecimenRoleSpecValidator._validateSpecimenRoleSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040StructuredBody){
			net.ihe.gazelle.cda.POCDMT000040StructuredBody aClass = ( net.ihe.gazelle.cda.POCDMT000040StructuredBody)obj;
			StructuredBodySpecValidator._validateStructuredBodySpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040SubjectPerson){
			net.ihe.gazelle.cda.POCDMT000040SubjectPerson aClass = ( net.ihe.gazelle.cda.POCDMT000040SubjectPerson)obj;
			SubjectPersonSpecValidator._validateSubjectPersonSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040SubstanceAdministration){
			net.ihe.gazelle.cda.POCDMT000040SubstanceAdministration aClass = ( net.ihe.gazelle.cda.POCDMT000040SubstanceAdministration)obj;
			SubstanceAdministrationSpecValidator._validateSubstanceAdministrationSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.cda.POCDMT000040Supply){
			net.ihe.gazelle.cda.POCDMT000040Supply aClass = ( net.ihe.gazelle.cda.POCDMT000040Supply)obj;
			SupplySpecValidator._validateSupplySpec(aClass, location, diagnostic);
		}
	
	}

}

