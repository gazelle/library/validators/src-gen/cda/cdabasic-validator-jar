package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validation.Warning;	



 /**
  * class :        AuthenticatorSpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040Authenticator
  * 
  */
public final class AuthenticatorSpecValidator{


    private AuthenticatorSpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim067_1
	* The signatureCode elements of Authenticator SHALL be from the valueSet ParticipationSignature (RMIM-067, RIM-010)
	*
	*/
	private static boolean _validateAuthenticatorSpec_Rmim067_1(net.ihe.gazelle.cda.POCDMT000040Authenticator aClass){
		return (((aClass.getSignatureCode() == null) || !(aClass.getSignatureCode().getNullFlavor() == null)) || aClass.getSignatureCode().matchesCodeToValueSet("1.3.6.1.4.1.12559.11.18.4", aClass.getSignatureCode().getCode()));
		
	}

	/**
	* Validation of instance by a constraint : rmim068
	* The authenticator.signatureCode elements SHOULD NOT have the value X (RMIM-068)
	*
	*/
	private static boolean _validateAuthenticatorSpec_Rmim068(net.ihe.gazelle.cda.POCDMT000040Authenticator aClass){
		return (((aClass.getSignatureCode() == null) || (((String) aClass.getSignatureCode().getCode()) == null)) || !aClass.getSignatureCode().getCode().equals("X"));
		
	}

	/**
	* Validation of class-constraint : AuthenticatorSpec
    * Verify if an element of type AuthenticatorSpec can be validated by AuthenticatorSpec
	*
	*/
	public static boolean _isAuthenticatorSpec(net.ihe.gazelle.cda.POCDMT000040Authenticator aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   AuthenticatorSpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040Authenticator
     * 
     */
    public static void _validateAuthenticatorSpec(net.ihe.gazelle.cda.POCDMT000040Authenticator aClass, String location, List<Notification> diagnostic) {
		if (_isAuthenticatorSpec(aClass)){
			executeCons_AuthenticatorSpec_Rmim067_1(aClass, location, diagnostic);
			executeCons_AuthenticatorSpec_Rmim068(aClass, location, diagnostic);
		}
	}

	private static void executeCons_AuthenticatorSpec_Rmim067_1(net.ihe.gazelle.cda.POCDMT000040Authenticator aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAuthenticatorSpec_Rmim067_1(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim067_1");
		notif.setDescription("The signatureCode elements of Authenticator SHALL be from the valueSet ParticipationSignature (RMIM-067, RIM-010)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-AuthenticatorSpec-rmim067_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-067;RIM-010"));
		
		notif.setType("Vocabulary");
		diagnostic.add(notif);
	}

	private static void executeCons_AuthenticatorSpec_Rmim068(net.ihe.gazelle.cda.POCDMT000040Authenticator aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAuthenticatorSpec_Rmim068(aClass) )){
				notif = new Warning();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim068");
		notif.setDescription("The authenticator.signatureCode elements SHOULD NOT have the value X (RMIM-068)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-AuthenticatorSpec-rmim068");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-068"));
		
		notif.setType("Fixed value");
		diagnostic.add(notif);
	}

}
