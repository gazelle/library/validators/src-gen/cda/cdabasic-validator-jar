package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.cda.POCDMT000040Component5;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validation.Warning;	



 /**
  * class :        SectionSpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040Section
  * 
  */
public final class SectionSpecValidator{


    private SectionSpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim050
	* The languageCode shall be from the valueSet HumanLanguage (RMIM-050)
	*
	*/
	private static boolean _validateSectionSpec_Rmim050(net.ihe.gazelle.cda.POCDMT000040Section aClass){
		return (((aClass.getLanguageCode() == null) || !(aClass.getLanguageCode().getNullFlavor() == null)) || (!(((String) aClass.getLanguageCode().getCode()) == null) && aClass.getLanguageCode().matches(aClass.getLanguageCode().getCode(), "\\w+(\\-\\w+)?")));
		
	}

	/**
	* Validation of instance by a constraint : rmim052
	* Section.text SHALL be specified, as it is a required element (RMIM-052)
	*
	*/
	private static boolean _validateSectionSpec_Rmim052(net.ihe.gazelle.cda.POCDMT000040Section aClass){
		Boolean result1;
		result1 = false;
		
		/* Iterator Exists: Iterate and check, if any element fulfills the condition. */
		try{
			for (POCDMT000040Component5 anElement1 : aClass.getComponent()) {
			    if ((!(anElement1.getSection() == null) && !(anElement1.getSection().getText() == null))) {
			        result1 = true;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return (!(aClass.getText() == null) || result1);
		
		
	}

	/**
	* Validation of class-constraint : SectionSpec
    * Verify if an element of type SectionSpec can be validated by SectionSpec
	*
	*/
	public static boolean _isSectionSpec(net.ihe.gazelle.cda.POCDMT000040Section aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   SectionSpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040Section
     * 
     */
    public static void _validateSectionSpec(net.ihe.gazelle.cda.POCDMT000040Section aClass, String location, List<Notification> diagnostic) {
		if (_isSectionSpec(aClass)){
			executeCons_SectionSpec_Rmim050(aClass, location, diagnostic);
			executeCons_SectionSpec_Rmim052(aClass, location, diagnostic);
		}
	}

	private static void executeCons_SectionSpec_Rmim050(net.ihe.gazelle.cda.POCDMT000040Section aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateSectionSpec_Rmim050(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim050");
		notif.setDescription("The languageCode shall be from the valueSet HumanLanguage (RMIM-050)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-SectionSpec-rmim050");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-050"));
		
		notif.setType("Vocabulary");
		diagnostic.add(notif);
	}

	private static void executeCons_SectionSpec_Rmim052(net.ihe.gazelle.cda.POCDMT000040Section aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateSectionSpec_Rmim052(aClass) )){
				notif = new Warning();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim052");
		notif.setDescription("Section.text SHALL be specified, as it is a required element (RMIM-052)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-SectionSpec-rmim052");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-052"));
		
		notif.setType("Mandatory");
		diagnostic.add(notif);
	}

}
