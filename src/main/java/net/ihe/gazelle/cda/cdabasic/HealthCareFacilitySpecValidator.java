package net.ihe.gazelle.cda.cdabasic;

import java.util.List;

import net.ihe.gazelle.cda.POCDMT000040Location;
import net.ihe.gazelle.datatypes.II;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;	



 /**
  * class :        HealthCareFacilitySpec
  * package :   cdabasic
  * Constraint Spec Class
  * class of test : POCDMT000040HealthCareFacility
  * 
  */
public final class HealthCareFacilitySpecValidator{


    private HealthCareFacilitySpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim085_1
	* HealthCareFacility SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-085, RIM-011)
	*
	*/
	private static boolean _validateHealthCareFacilitySpec_Rmim085_1(net.ihe.gazelle.cda.POCDMT000040HealthCareFacility aClass){
		java.util.ArrayList<II> result1;
		result1 = new java.util.ArrayList<II>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getId()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : rmim085_2
	* The ids elements of HealthCareFacility SHALL be distinct (RMIM-085, RIM-012)
	*
	*/
	private static boolean _validateHealthCareFacilitySpec_Rmim085_2(net.ihe.gazelle.cda.POCDMT000040HealthCareFacility aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    java.util.ArrayList<II> result2;
			    result2 = new java.util.ArrayList<II>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (II anElement2 : aClass.getId()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : rmim086
	* If healthCareFacility has not a null of flavor and its enclosing location has not a null of flavor,  it SHALL have a location or a serviceProviderOrganization (RMIM-086)
	*
	*/
	private static boolean _validateHealthCareFacilitySpec_Rmim086(net.ihe.gazelle.cda.POCDMT000040HealthCareFacility aClass){
		return (((((!(aClass.getNullFlavor() == null) || (aClass.get__parent() == null)) || !(POCDMT000040Location.class.isAssignableFrom(aClass.get__parent().getClass()))) || !(((POCDMT000040Location) aClass.get__parent()).getNullFlavor() == null)) || !(aClass.getLocation() == null)) || !(aClass.getServiceProviderOrganization() == null));
		
	}

	/**
	* Validation of class-constraint : HealthCareFacilitySpec
    * Verify if an element of type HealthCareFacilitySpec can be validated by HealthCareFacilitySpec
	*
	*/
	public static boolean _isHealthCareFacilitySpec(net.ihe.gazelle.cda.POCDMT000040HealthCareFacility aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   HealthCareFacilitySpec
     * class ::  net.ihe.gazelle.cda.POCDMT000040HealthCareFacility
     * 
     */
    public static void _validateHealthCareFacilitySpec(net.ihe.gazelle.cda.POCDMT000040HealthCareFacility aClass, String location, List<Notification> diagnostic) {
		if (_isHealthCareFacilitySpec(aClass)){
			executeCons_HealthCareFacilitySpec_Rmim085_1(aClass, location, diagnostic);
			executeCons_HealthCareFacilitySpec_Rmim085_2(aClass, location, diagnostic);
			executeCons_HealthCareFacilitySpec_Rmim086(aClass, location, diagnostic);
		}
	}

	private static void executeCons_HealthCareFacilitySpec_Rmim085_1(net.ihe.gazelle.cda.POCDMT000040HealthCareFacility aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateHealthCareFacilitySpec_Rmim085_1(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim085_1");
		notif.setDescription("HealthCareFacility SHALL NOT have id element with nullFlavor, if there are other ids elements (RMIM-085, RIM-011)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-HealthCareFacilitySpec-rmim085_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-085;RIM-011"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_HealthCareFacilitySpec_Rmim085_2(net.ihe.gazelle.cda.POCDMT000040HealthCareFacility aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateHealthCareFacilitySpec_Rmim085_2(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim085_2");
		notif.setDescription("The ids elements of HealthCareFacility SHALL be distinct (RMIM-085, RIM-012)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-HealthCareFacilitySpec-rmim085_2");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-085;RIM-012"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_HealthCareFacilitySpec_Rmim086(net.ihe.gazelle.cda.POCDMT000040HealthCareFacility aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateHealthCareFacilitySpec_Rmim086(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim086");
		notif.setDescription("If healthCareFacility has not a null of flavor and its enclosing location has not a null of flavor,  it SHALL have a location or a serviceProviderOrganization (RMIM-086)");
		notif.setLocation(location);
		notif.setIdentifiant("cdabasic-HealthCareFacilitySpec-rmim086");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-086"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

}
