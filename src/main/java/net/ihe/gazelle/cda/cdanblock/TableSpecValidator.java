package net.ihe.gazelle.cda.cdanblock;

import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validation.Warning;





 /**
  * class :        TableSpec
  * package :   cdanblock
  * Constraint Spec Class
  * class of test : StrucDocTable
  * 
  */
public final class TableSpecValidator{


    private TableSpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim112_1
	* table.border SHOULD NOT be used (RMIM-112)
	*
	*/
	private static boolean _validateTableSpec_Rmim112_1(net.ihe.gazelle.nblock.StrucDocTable aClass){
		return (((String) aClass.getBorder()) == null);
		
	}

	/**
	* Validation of instance by a constraint : rmim112_2
	* table.cellpading SHOULD NOT be used (RMIM-112)
	*
	*/
	private static boolean _validateTableSpec_Rmim112_2(net.ihe.gazelle.nblock.StrucDocTable aClass){
		return (((String) aClass.getCellpadding()) == null);
		
	}

	/**
	* Validation of instance by a constraint : rmim112_3
	* table.cellspacing SHOULD NOT be used (RMIM-112)
	*
	*/
	private static boolean _validateTableSpec_Rmim112_3(net.ihe.gazelle.nblock.StrucDocTable aClass){
		return (((String) aClass.getCellspacing()) == null);
		
	}

	/**
	* Validation of class-constraint : TableSpec
    * Verify if an element of type TableSpec can be validated by TableSpec
	*
	*/
	public static boolean _isTableSpec(net.ihe.gazelle.nblock.StrucDocTable aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   TableSpec
     * class ::  net.ihe.gazelle.nblock.StrucDocTable
     * 
     */
    public static void _validateTableSpec(net.ihe.gazelle.nblock.StrucDocTable aClass, String location, List<Notification> diagnostic) {
		if (_isTableSpec(aClass)){
			executeCons_TableSpec_Rmim112_1(aClass, location, diagnostic);
			executeCons_TableSpec_Rmim112_2(aClass, location, diagnostic);
			executeCons_TableSpec_Rmim112_3(aClass, location, diagnostic);
		}
	}

	private static void executeCons_TableSpec_Rmim112_1(net.ihe.gazelle.nblock.StrucDocTable aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateTableSpec_Rmim112_1(aClass) )){
				notif = new Warning();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim112_1");
		notif.setDescription("table.border SHOULD NOT be used (RMIM-112)");
		notif.setLocation(location);
		notif.setIdentifiant("cdanblock-TableSpec-rmim112_1");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-112"));
		
		notif.setType("Fixed value");
		diagnostic.add(notif);
	}

	private static void executeCons_TableSpec_Rmim112_2(net.ihe.gazelle.nblock.StrucDocTable aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateTableSpec_Rmim112_2(aClass) )){
				notif = new Warning();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim112_2");
		notif.setDescription("table.cellpading SHOULD NOT be used (RMIM-112)");
		notif.setLocation(location);
		notif.setIdentifiant("cdanblock-TableSpec-rmim112_2");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-112"));
		
		notif.setType("Fixed value");
		diagnostic.add(notif);
	}

	private static void executeCons_TableSpec_Rmim112_3(net.ihe.gazelle.nblock.StrucDocTable aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateTableSpec_Rmim112_3(aClass) )){
				notif = new Warning();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim112_3");
		notif.setDescription("table.cellspacing SHOULD NOT be used (RMIM-112)");
		notif.setLocation(location);
		notif.setIdentifiant("cdanblock-TableSpec-rmim112_3");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-112"));
		
		notif.setType("Fixed value");
		diagnostic.add(notif);
	}

}
