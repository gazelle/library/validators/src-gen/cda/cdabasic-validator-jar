package net.ihe.gazelle.cda.cdanblock;

import java.util.List;

import net.ihe.gazelle.nblock.StrucDocParagraph;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;





 /**
  * class :        ParagraphSpec
  * package :   cdanblock
  * Constraint Spec Class
  * class of test : StrucDocParagraph
  * 
  */
public final class ParagraphSpecValidator{


    private ParagraphSpecValidator() {}

public static Boolean validateCaptionIsFirst(StrucDocParagraph parag) {
	// Start of user code for method validateCaptionIsFirst
	if (parag.getCaption() != null){
				boolean res = false;
				java.io.Serializable obj  =parag.getMixed().get(0);
				if (obj instanceof javax.xml.bind.JAXBElement){
					if (((javax.xml.bind.JAXBElement<?>)obj).getName().getLocalPart().equals("caption")){
						return true;
					}
				}
				return res;
			}
			return true;
	// End of user code
}


	/**
	* Validation of instance by a constraint : rmim116
	* if <caption> present in the <paragraph> element, it shall be done before any other character data, including whitespaces (RMIM-116)
	*
	*/
	private static boolean _validateParagraphSpec_Rmim116(net.ihe.gazelle.nblock.StrucDocParagraph aClass){
		return ((aClass.getCaption() == null) || ParagraphSpecValidator.validateCaptionIsFirst(aClass));
		
	}

	/**
	* Validation of class-constraint : ParagraphSpec
    * Verify if an element of type ParagraphSpec can be validated by ParagraphSpec
	*
	*/
	public static boolean _isParagraphSpec(net.ihe.gazelle.nblock.StrucDocParagraph aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   ParagraphSpec
     * class ::  net.ihe.gazelle.nblock.StrucDocParagraph
     * 
     */
    public static void _validateParagraphSpec(net.ihe.gazelle.nblock.StrucDocParagraph aClass, String location, List<Notification> diagnostic) {
		if (_isParagraphSpec(aClass)){
			executeCons_ParagraphSpec_Rmim116(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ParagraphSpec_Rmim116(net.ihe.gazelle.nblock.StrucDocParagraph aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateParagraphSpec_Rmim116(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim116");
		notif.setDescription("if <caption> present in the <paragraph> element, it shall be done before any other character data, including whitespaces (RMIM-116)");
		notif.setLocation(location);
		notif.setIdentifiant("cdanblock-ParagraphSpec-rmim116");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-116"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

}
