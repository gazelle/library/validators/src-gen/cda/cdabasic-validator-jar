package net.ihe.gazelle.cda.cdanblock;

import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validation.Warning;





 /**
  * class :        LinkHtmlSpec
  * package :   cdanblock
  * Constraint Spec Class
  * class of test : StrucDocLinkHtml
  * 
  */
public final class LinkHtmlSpecValidator{


    private LinkHtmlSpecValidator() {}



	/**
	* Validation of instance by a constraint : rmim108
	* The linkHtml/name element SHOULD NOT be used (RMIM-108)
	*
	*/
	private static boolean _validateLinkHtmlSpec_Rmim108(net.ihe.gazelle.nblock.StrucDocLinkHtml aClass){
		return (((String) aClass.getName()) == null);
		
	}

	/**
	* Validation of class-constraint : LinkHtmlSpec
    * Verify if an element of type LinkHtmlSpec can be validated by LinkHtmlSpec
	*
	*/
	public static boolean _isLinkHtmlSpec(net.ihe.gazelle.nblock.StrucDocLinkHtml aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   LinkHtmlSpec
     * class ::  net.ihe.gazelle.nblock.StrucDocLinkHtml
     * 
     */
    public static void _validateLinkHtmlSpec(net.ihe.gazelle.nblock.StrucDocLinkHtml aClass, String location, List<Notification> diagnostic) {
		if (_isLinkHtmlSpec(aClass)){
			executeCons_LinkHtmlSpec_Rmim108(aClass, location, diagnostic);
		}
	}

	private static void executeCons_LinkHtmlSpec_Rmim108(net.ihe.gazelle.nblock.StrucDocLinkHtml aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateLinkHtmlSpec_Rmim108(aClass) )){
				notif = new Warning();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("rmim108");
		notif.setDescription("The linkHtml/name element SHOULD NOT be used (RMIM-108)");
		notif.setLocation(location);
		notif.setIdentifiant("cdanblock-LinkHtmlSpec-rmim108");
		notif.getAssertions().add(new Assertion("RMIM","RMIM-108"));
		
		notif.setType("Fixed value");
		diagnostic.add(notif);
	}

}
