package net.ihe.gazelle.cda.cdanblock;

import java.util.List;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import net.ihe.gazelle.validation.Notification;





public class CDANBLOCKPackValidator implements ConstraintValidatorModule{


    /**
     * Create a new ObjectValidator that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public CDANBLOCKPackValidator() {}
    


	/**
	* Validation of instance of an object
	*
	*/
	public void validate(Object obj, String location, List<Notification> diagnostic){

		if (obj instanceof net.ihe.gazelle.nblock.StrucDocLinkHtml){
			net.ihe.gazelle.nblock.StrucDocLinkHtml aClass = ( net.ihe.gazelle.nblock.StrucDocLinkHtml)obj;
			LinkHtmlSpecValidator._validateLinkHtmlSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.nblock.StrucDocParagraph){
			net.ihe.gazelle.nblock.StrucDocParagraph aClass = ( net.ihe.gazelle.nblock.StrucDocParagraph)obj;
			ParagraphSpecValidator._validateParagraphSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.nblock.StrucDocTable){
			net.ihe.gazelle.nblock.StrucDocTable aClass = ( net.ihe.gazelle.nblock.StrucDocTable)obj;
			TableSpecValidator._validateTableSpec(aClass, location, diagnostic);
		}
	
	}

}

