package net.ihe.gazelle.cda.cdadt;

import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;


 /**
  * class :        PIVLTSSpec
  * package :   cdadt
  * Constraint Spec Class
  * class of test : PIVLTS
  * 
  */
public final class PIVLTSSpecValidator{


    private PIVLTSSpecValidator() {}

public static Boolean unitIsMeasureOfTime(String unit) {
	// Start of user code for method unitIsMeasureOfTime
	return net.ihe.gazelle.basic.validation.UCUMValidator.unitsAreComparable(unit, "s");
	// End of user code
}


	/**
	* Validation of instance by a constraint : cdadt028
	* In PIVL datatype, if it is not null, it SHALL have a period (CDADT-028)
	*
	*/
	private static boolean _validatePIVLTSSpec_Cdadt028(net.ihe.gazelle.datatypes.PIVLTS aClass){
		return (!(aClass.getNullFlavor() == null) ^ !(aClass.getPeriod() == null));
		
	}

	/**
	* Validation of instance by a constraint : dtits016
	* In PIVL_TS datatype, unit attribute of period element SHALL have a value from the time units (DTITS-016)
	*
	*/
	private static boolean _validatePIVLTSSpec_Dtits016(net.ihe.gazelle.datatypes.PIVLTS aClass){
		return (((aClass.getPeriod() == null) || (((String) aClass.getPeriod().getUnit()) == null)) || PIVLTSSpecValidator.unitIsMeasureOfTime(aClass.getPeriod().getUnit()));
		
	}

	/**
	* Validation of class-constraint : PIVLTSSpec
    * Verify if an element of type PIVLTSSpec can be validated by PIVLTSSpec
	*
	*/
	public static boolean _isPIVLTSSpec(net.ihe.gazelle.datatypes.PIVLTS aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   PIVLTSSpec
     * class ::  net.ihe.gazelle.datatypes.PIVLTS
     * 
     */
    public static void _validatePIVLTSSpec(net.ihe.gazelle.datatypes.PIVLTS aClass, String location, List<Notification> diagnostic) {
		if (_isPIVLTSSpec(aClass)){
			executeCons_PIVLTSSpec_Cdadt028(aClass, location, diagnostic);
			executeCons_PIVLTSSpec_Dtits016(aClass, location, diagnostic);
		}
	}

	private static void executeCons_PIVLTSSpec_Cdadt028(net.ihe.gazelle.datatypes.PIVLTS aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validatePIVLTSSpec_Cdadt028(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt028");
		notif.setDescription("In PIVL datatype, if it is not null, it SHALL have a period (CDADT-028)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-PIVLTSSpec-cdadt028");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-028"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

	private static void executeCons_PIVLTSSpec_Dtits016(net.ihe.gazelle.datatypes.PIVLTS aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validatePIVLTSSpec_Dtits016(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("dtits016");
		notif.setDescription("In PIVL_TS datatype, unit attribute of period element SHALL have a value from the time units (DTITS-016)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-PIVLTSSpec-dtits016");
		notif.getAssertions().add(new Assertion("DTITS","DTITS-016"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

}
