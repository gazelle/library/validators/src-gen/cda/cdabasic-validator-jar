package net.ihe.gazelle.cda.cdadt;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;


 /**
  * class :        ADSpec
  * package :   cdadt
  * Constraint Spec Class
  * class of test : AD
  * 
  */
public final class ADSpecValidator{


    private ADSpecValidator() {}

public static Boolean verifyUsesDistinct(String uses) {
	// Start of user code for method verifyUsesDistinct
	if (uses == null || uses.trim().length()== 0){
			return true;
		}
		String[] listUse = uses.split(" ");
		HashSet<String> hs = new HashSet<String>(Arrays.asList(listUse));
		return listUse.length == hs.size();
	// End of user code
}


	/**
	* Validation of instance by a constraint : cdadt032
	* In AD datatype, the use attribute SHALL have disctinct values (CDADT-032)
	*
	*/
	private static boolean _validateADSpec_Cdadt032(net.ihe.gazelle.datatypes.AD aClass){
		return ADSpecValidator.verifyUsesDistinct(aClass.getUse());
		
	}

	/**
	* Validation of class-constraint : ADSpec
    * Verify if an element of type ADSpec can be validated by ADSpec
	*
	*/
	public static boolean _isADSpec(net.ihe.gazelle.datatypes.AD aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   ADSpec
     * class ::  net.ihe.gazelle.datatypes.AD
     * 
     */
    public static void _validateADSpec(net.ihe.gazelle.datatypes.AD aClass, String location, List<Notification> diagnostic) {
		if (_isADSpec(aClass)){
			executeCons_ADSpec_Cdadt032(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ADSpec_Cdadt032(net.ihe.gazelle.datatypes.AD aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateADSpec_Cdadt032(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt032");
		notif.setDescription("In AD datatype, the use attribute SHALL have disctinct values (CDADT-032)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-ADSpec-cdadt032");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-032"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

}
