package net.ihe.gazelle.cda.cdadt;

import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;


 /**
  * class :        SCSpec
  * package :   cdadt
  * Constraint Spec Class
  * class of test : SC
  * 
  */
public final class SCSpecValidator{


    private SCSpecValidator() {}



	/**
	* Validation of instance by a constraint : cdadt010
	* code SHALL NOT be given without the text (CDADT-010)
	*
	*/
	private static boolean _validateSCSpec_Cdadt010(net.ihe.gazelle.datatypes.SC aClass){
		Boolean result1;
		result1 = false;
		
		/* Iterator Exists: Iterate and check, if any element fulfills the condition. */
		try{
			for (String anElement1 : aClass.getListStringValues()) {
			    if (!anElement1.equals("")) {
			        result1 = true;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((((String) aClass.getCode()) == null) || result1);
		
		
	}

	/**
	* Validation of class-constraint : SCSpec
    * Verify if an element of type SCSpec can be validated by SCSpec
	*
	*/
	public static boolean _isSCSpec(net.ihe.gazelle.datatypes.SC aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   SCSpec
     * class ::  net.ihe.gazelle.datatypes.SC
     * 
     */
    public static void _validateSCSpec(net.ihe.gazelle.datatypes.SC aClass, String location, List<Notification> diagnostic) {
		if (_isSCSpec(aClass)){
			executeCons_SCSpec_Cdadt010(aClass, location, diagnostic);
		}
	}

	private static void executeCons_SCSpec_Cdadt010(net.ihe.gazelle.datatypes.SC aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateSCSpec_Cdadt010(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt010");
		notif.setDescription("code SHALL NOT be given without the text (CDADT-010)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-SCSpec-cdadt010");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-010"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

}
