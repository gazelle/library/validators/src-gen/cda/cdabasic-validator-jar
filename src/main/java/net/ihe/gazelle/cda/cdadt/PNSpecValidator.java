package net.ihe.gazelle.cda.cdadt;

import java.util.List;

import net.ihe.gazelle.datatypes.EnFamily;
import net.ihe.gazelle.datatypes.EnGiven;
import net.ihe.gazelle.datatypes.EnPrefix;
import net.ihe.gazelle.datatypes.EnSuffix;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;


 /**
  * class :        PNSpec
  * package :   cdadt
  * Constraint Spec Class
  * class of test : PN
  * 
  */
public final class PNSpecValidator{


    private PNSpecValidator() {}



	/**
	* Validation of instance by a constraint : cdadt015
	* the qualifier ofelements of PN datatype SHALL NOT include LS as value (CDADT-015)
	*
	*/
	private static boolean _validatePNSpec_Cdadt015(net.ihe.gazelle.datatypes.PN aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (EnFamily anElement1 : aClass.getFamily()) {
			    java.util.ArrayList<String> result2;
			    result2 = new java.util.ArrayList<String>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (String anElement2 : anElement1.getQualifier()) {
			    	    if (anElement2.equals("LS")) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(0))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		Boolean result3;
		result3 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (EnGiven anElement3 : aClass.getGiven()) {
			    java.util.ArrayList<String> result4;
			    result4 = new java.util.ArrayList<String>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (String anElement4 : anElement3.getQualifier()) {
			    	    if (anElement4.equals("LS")) {
			    	        result4.add(anElement4);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result4)).equals(Integer.valueOf(0))) {
			        result3 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		Boolean result5;
		result5 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (EnPrefix anElement5 : aClass.getPrefix()) {
			    java.util.ArrayList<String> result6;
			    result6 = new java.util.ArrayList<String>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (String anElement6 : anElement5.getQualifier()) {
			    	    if (anElement6.equals("LS")) {
			    	        result6.add(anElement6);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result6)).equals(Integer.valueOf(0))) {
			        result5 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		Boolean result7;
		result7 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (EnSuffix anElement7 : aClass.getSuffix()) {
			    java.util.ArrayList<String> result8;
			    result8 = new java.util.ArrayList<String>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (String anElement8 : anElement7.getQualifier()) {
			    	    if (anElement8.equals("LS")) {
			    	        result8.add(anElement8);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result8)).equals(Integer.valueOf(0))) {
			        result7 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return (((result1 && result3) && result5) && result7);
		
		
	}

	/**
	* Validation of class-constraint : PNSpec
    * Verify if an element of type PNSpec can be validated by PNSpec
	*
	*/
	public static boolean _isPNSpec(net.ihe.gazelle.datatypes.PN aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   PNSpec
     * class ::  net.ihe.gazelle.datatypes.PN
     * 
     */
    public static void _validatePNSpec(net.ihe.gazelle.datatypes.PN aClass, String location, List<Notification> diagnostic) {
		if (_isPNSpec(aClass)){
			executeCons_PNSpec_Cdadt015(aClass, location, diagnostic);
		}
	}

	private static void executeCons_PNSpec_Cdadt015(net.ihe.gazelle.datatypes.PN aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validatePNSpec_Cdadt015(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt015");
		notif.setDescription("the qualifier ofelements of PN datatype SHALL NOT include LS as value (CDADT-015)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-PNSpec-cdadt015");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-015"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

}
