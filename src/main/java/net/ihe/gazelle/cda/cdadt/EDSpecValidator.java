package net.ihe.gazelle.cda.cdadt;

import java.util.List;

import net.ihe.gazelle.basic.validation.SHAValidation;
import net.ihe.gazelle.datatypes.BinaryDataEncoding;
import net.ihe.gazelle.datatypes.ED;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.voc.IntegrityCheckAlgorithm;


 /**
  * class :        EDSpec
  * package :   cdadt
  * Constraint Spec Class
  * class of test : ED
  * 
  */
public final class EDSpecValidator{


    private EDSpecValidator() {}

public static Boolean validateIntegrityCheck(ED ed1) {
	// Start of user code for method validateIntegrityCheck
	if (ed1 != null){
				if (ed1.getIntegrityCheck() != null){
					List<String> ed1Strings = ed1.getListStringValues();
					String ed1String = "";
					if (ed1Strings != null){
						for (String string : ed1Strings) {
							ed1String = ed1String + string;
						}
					}
					
					String sha = "SHA-1";
					if (ed1.getIntegrityCheckAlgorithm() != null){
						if (ed1.getIntegrityCheckAlgorithm() == IntegrityCheckAlgorithm.SHA256){
							sha = "SHA-256";
						}
					}
					
					if (ed1.getRepresentation() != null && ed1.getRepresentation() == BinaryDataEncoding.B64){
						if (ed1.getCompression() != null){
							// TODO
						}
						else{
							 java.io.InputStream ed1is = SHAValidation.decodeB64(ed1String, null);
							String calcul = SHAValidation.calc(ed1is, sha);
							return ed1.getIntegrityCheck().toLowerCase().equals(calcul.toLowerCase());
						}
					}
					else{
						String calcul = SHAValidation.calc(ed1String, sha);
						return ed1.getIntegrityCheck().toLowerCase().equals(calcul.toLowerCase());
					}
				}
			}
			return true;
	// End of user code
}


	/**
	* Validation of instance by a constraint : cdadt002
	* mediaType of the ED datatype SHOULD be from known mediaTypes (CDADT-002)
	*
	*/
	private static boolean _validateEDSpec_Cdadt002(net.ihe.gazelle.datatypes.ED aClass){
		return ((((String) aClass.getMediaType()) == null) || aClass.matchesCodeToValueSet("1.3.6.1.4.1.12559.11.18.10", aClass.getMediaType()));
		
	}

	/**
	* Validation of instance by a constraint : cdadt003
	* if integrityCheck attribute is provided, it SHALL have a valid value according to its integrityCheckAlgorithm (CDADT-003)
	*
	*/
	private static boolean _validateEDSpec_Cdadt003(net.ihe.gazelle.datatypes.ED aClass){
		return ((((String) aClass.getIntegrityCheck()) == null) || EDSpecValidator.validateIntegrityCheck(aClass));
		
	}

	/**
	* Validation of instance by a constraint : cdadt023
	* In ED datatype, if it is not null, it SHALL have a binary string or reference element (CDADT-023)
	*
	*/
	private static boolean _validateEDSpec_Cdadt023(net.ihe.gazelle.datatypes.ED aClass){
		Boolean result1;
		result1 = false;
		
		/* Iterator Exists: Iterate and check, if any element fulfills the condition. */
		try{
			for (String anElement1 : aClass.getListStringValues()) {
			    if ((!(((String) anElement1) == null) && !anElement1.equals(""))) {
			        result1 = true;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((!(aClass.getNullFlavor() == null) || !(aClass.getReference() == null)) || result1);
		
		
	}

	/**
	* Validation of instance by a constraint : dtits006
	* In ED datatype, the language SHALL be as defined in RFC3066 (DTITS-006)
	*
	*/
	private static boolean _validateEDSpec_Dtits006(net.ihe.gazelle.datatypes.ED aClass){
		return ((((String) aClass.getLanguage()) == null) || aClass.matches(aClass.getLanguage(), "[a-zA-Z]{2}\\-[a-zA-Z]{2}"));
		
	}

	/**
	* Validation of instance by a constraint : dtits007
	* In ED datatype, when it has reference form, there shall not be white spaces before and after the reference, the binary data shall not be present or shall represent the same information in the reference (DTITS-007)
	*
	*/
	private static boolean _validateEDSpec_Dtits007(net.ihe.gazelle.datatypes.ED aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getListStringValues()) {
			    if (!anElement1.equals("")) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return (((aClass.getReference() == null) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getListStringValues())).equals(Integer.valueOf(0))) || result1);
		
		
	}

	/**
	* Validation of class-constraint : EDSpec
    * Verify if an element of type EDSpec can be validated by EDSpec
	*
	*/
	public static boolean _isEDSpec(net.ihe.gazelle.datatypes.ED aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   EDSpec
     * class ::  net.ihe.gazelle.datatypes.ED
     * 
     */
    public static void _validateEDSpec(net.ihe.gazelle.datatypes.ED aClass, String location, List<Notification> diagnostic) {
		if (_isEDSpec(aClass)){
			executeCons_EDSpec_Cdadt002(aClass, location, diagnostic);
			executeCons_EDSpec_Cdadt003(aClass, location, diagnostic);
			executeCons_EDSpec_Cdadt023(aClass, location, diagnostic);
			executeCons_EDSpec_Dtits006(aClass, location, diagnostic);
			executeCons_EDSpec_Dtits007(aClass, location, diagnostic);
		}
	}

	private static void executeCons_EDSpec_Cdadt002(net.ihe.gazelle.datatypes.ED aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateEDSpec_Cdadt002(aClass) )){
				notif = new Warning();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt002");
		notif.setDescription("mediaType of the ED datatype SHOULD be from known mediaTypes (CDADT-002)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-EDSpec-cdadt002");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-002"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

	private static void executeCons_EDSpec_Cdadt003(net.ihe.gazelle.datatypes.ED aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateEDSpec_Cdadt003(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt003");
		notif.setDescription("if integrityCheck attribute is provided, it SHALL have a valid value according to its integrityCheckAlgorithm (CDADT-003)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-EDSpec-cdadt003");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-003"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_EDSpec_Cdadt023(net.ihe.gazelle.datatypes.ED aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateEDSpec_Cdadt023(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt023");
		notif.setDescription("In ED datatype, if it is not null, it SHALL have a binary string or reference element (CDADT-023)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-EDSpec-cdadt023");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-023"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

	private static void executeCons_EDSpec_Dtits006(net.ihe.gazelle.datatypes.ED aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateEDSpec_Dtits006(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("dtits006");
		notif.setDescription("In ED datatype, the language SHALL be as defined in RFC3066 (DTITS-006)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-EDSpec-dtits006");
		notif.getAssertions().add(new Assertion("DTITS","DTITS-006"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

	private static void executeCons_EDSpec_Dtits007(net.ihe.gazelle.datatypes.ED aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateEDSpec_Dtits007(aClass) )){
				notif = new Warning();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("dtits007");
		notif.setDescription("In ED datatype, when it has reference form, there shall not be white spaces before and after the reference, the binary data shall not be present or shall represent the same information in the reference (DTITS-007)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-EDSpec-dtits007");
		notif.getAssertions().add(new Assertion("DTITS","DTITS-007"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

}
