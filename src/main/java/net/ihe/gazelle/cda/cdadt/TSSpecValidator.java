package net.ihe.gazelle.cda.cdadt;

import java.util.List;

import net.ihe.gazelle.datatypes.EIVLPPDTS;
import net.ihe.gazelle.datatypes.EIVLTS;
import net.ihe.gazelle.datatypes.IVLPPDTS;
import net.ihe.gazelle.datatypes.IVLTS;
import net.ihe.gazelle.datatypes.IVXBPPDTS;
import net.ihe.gazelle.datatypes.IVXBTS;
import net.ihe.gazelle.datatypes.PIVLPPDTS;
import net.ihe.gazelle.datatypes.PIVLTS;
import net.ihe.gazelle.datatypes.PPDTS;
import net.ihe.gazelle.datatypes.SXCMPPDTS;
import net.ihe.gazelle.datatypes.SXCMTS;
import net.ihe.gazelle.datatypes.SXPRTS;
import net.ihe.gazelle.datatypes.UVPTS;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;


 /**
  * class :        TSSpec
  * package :   cdadt
  * Constraint Spec Class
  * class of test : TS
  * 
  */
public final class TSSpecValidator{


    private TSSpecValidator() {}



	/**
	* Validation of instance by a constraint : dtits017
	* In TS datatype, value SHALL have a valid timestamp (DTITS-017)
	*
	*/
	private static boolean _validateTSSpec_Dtits017(net.ihe.gazelle.datatypes.TS aClass){
		return ((((String) aClass.getValue()) == null) || aClass.matches(aClass.getValue(), "(18|19|20|21)\\d\\d((0[1-9]|1[012])((0[1-9]|[12][0-9]|3[01])(([0-1]\\d|[2][0-3])(([0-5]\\d)(([0-5]\\d)((\\.[0-9]+)?)?)?)?)?)?((\\+|\\-)[0-9]{1,4})?)?"));
		
	}

	/**
	* Validation of instance by a constraint : dtits018
	* In TS datatype, value SHALL be present, otherwise nullFlavor shall be provided, not both (DTITS-018)
	*
	*/
	private static boolean _validateTSSpec_Dtits018(net.ihe.gazelle.datatypes.TS aClass){
		return ((((((((((((((IVXBTS.class.isAssignableFrom(aClass.getClass())) || (PPDTS.class.isAssignableFrom(aClass.getClass()))) || (SXCMTS.class.isAssignableFrom(aClass.getClass()))) || (UVPTS.class.isAssignableFrom(aClass.getClass()))) || (IVXBPPDTS.class.isAssignableFrom(aClass.getClass()))) || (SXCMPPDTS.class.isAssignableFrom(aClass.getClass()))) || (EIVLTS.class.isAssignableFrom(aClass.getClass()))) || (IVLTS.class.isAssignableFrom(aClass.getClass()))) || (PIVLTS.class.isAssignableFrom(aClass.getClass()))) || (SXPRTS.class.isAssignableFrom(aClass.getClass()))) || (EIVLPPDTS.class.isAssignableFrom(aClass.getClass()))) || (IVLPPDTS.class.isAssignableFrom(aClass.getClass()))) || (PIVLPPDTS.class.isAssignableFrom(aClass.getClass()))) || (!(((String) aClass.getValue()) == null) ^ !(aClass.getNullFlavor() == null)));
		
	}

	/**
	* Validation of class-constraint : TSSpec
    * Verify if an element of type TSSpec can be validated by TSSpec
	*
	*/
	public static boolean _isTSSpec(net.ihe.gazelle.datatypes.TS aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   TSSpec
     * class ::  net.ihe.gazelle.datatypes.TS
     * 
     */
    public static void _validateTSSpec(net.ihe.gazelle.datatypes.TS aClass, String location, List<Notification> diagnostic) {
		if (_isTSSpec(aClass)){
			executeCons_TSSpec_Dtits017(aClass, location, diagnostic);
			executeCons_TSSpec_Dtits018(aClass, location, diagnostic);
		}
	}

	private static void executeCons_TSSpec_Dtits017(net.ihe.gazelle.datatypes.TS aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateTSSpec_Dtits017(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("dtits017");
		notif.setDescription("In TS datatype, value SHALL have a valid timestamp (DTITS-017)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-TSSpec-dtits017");
		notif.getAssertions().add(new Assertion("DTITS","DTITS-017"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

	private static void executeCons_TSSpec_Dtits018(net.ihe.gazelle.datatypes.TS aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateTSSpec_Dtits018(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("dtits018");
		notif.setDescription("In TS datatype, value SHALL be present, otherwise nullFlavor shall be provided, not both (DTITS-018)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-TSSpec-dtits018");
		notif.getAssertions().add(new Assertion("DTITS","DTITS-018"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

}
