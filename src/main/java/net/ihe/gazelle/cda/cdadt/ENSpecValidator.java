package net.ihe.gazelle.cda.cdadt;

import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;


 /**
  * class :        ENSpec
  * package :   cdadt
  * Constraint Spec Class
  * class of test : EN
  * 
  */
public final class ENSpecValidator{


    private ENSpecValidator() {}



	/**
	* Validation of instance by a constraint : cdadt034
	* In EN datatype, the use attribute SHALL have distinct values (CDADT-034)
	*
	*/
	private static boolean _validateENSpec_Cdadt034(net.ihe.gazelle.datatypes.EN aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getUse()) {
			    java.util.ArrayList<String> result2;
			    result2 = new java.util.ArrayList<String>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (String anElement2 : aClass.getUse()) {
			    	    if (anElement2.equals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of class-constraint : ENSpec
    * Verify if an element of type ENSpec can be validated by ENSpec
	*
	*/
	public static boolean _isENSpec(net.ihe.gazelle.datatypes.EN aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   ENSpec
     * class ::  net.ihe.gazelle.datatypes.EN
     * 
     */
    public static void _validateENSpec(net.ihe.gazelle.datatypes.EN aClass, String location, List<Notification> diagnostic) {
		if (_isENSpec(aClass)){
			executeCons_ENSpec_Cdadt034(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ENSpec_Cdadt034(net.ihe.gazelle.datatypes.EN aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateENSpec_Cdadt034(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt034");
		notif.setDescription("In EN datatype, the use attribute SHALL have distinct values (CDADT-034)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-ENSpec-cdadt034");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-034"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

}
