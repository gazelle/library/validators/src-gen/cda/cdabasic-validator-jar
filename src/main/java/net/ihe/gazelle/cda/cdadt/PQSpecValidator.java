package net.ihe.gazelle.cda.cdadt;

import java.util.List;

import net.ihe.gazelle.datatypes.PQR;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;


 /**
  * class :        PQSpec
  * package :   cdadt
  * Constraint Spec Class
  * class of test : PQ
  * 
  */
public final class PQSpecValidator{


    private PQSpecValidator() {}

public static Boolean validateUnitBasedOnUCUM(String unit) {
	// Start of user code for method validateUnitBasedOnUCUM
	return net.ihe.gazelle.basic.validation.UCUMValidator.validateUCUM(unit);
	// End of user code
}


	/**
	* Validation of instance by a constraint : cdadt035
	* In PQ datatype, the translation elements SHALL have distinct values (CDADT-035)
	*
	*/
	private static boolean _validatePQSpec_Cdadt035(net.ihe.gazelle.datatypes.PQ aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (PQR anElement1 : aClass.getTranslation()) {
			    java.util.ArrayList<PQR> result2;
			    result2 = new java.util.ArrayList<PQR>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (PQR anElement2 : aClass.getTranslation()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : cdadt036
	* In PQ datatype, the translation elements SHALL not be null if there are translations which are not null (CDADT-036)
	*
	*/
	private static boolean _validatePQSpec_Cdadt036(net.ihe.gazelle.datatypes.PQ aClass){
		java.util.ArrayList<PQR> result1;
		result1 = new java.util.ArrayList<PQR>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (PQR anElement1 : aClass.getTranslation()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getTranslation()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of instance by a constraint : cdadt037
	* In PQ datatype, the unit attribute SHALL be from UCUM code system (CDADT-037)
	*
	*/
	private static boolean _validatePQSpec_Cdadt037(net.ihe.gazelle.datatypes.PQ aClass){
		return ((((String) aClass.getUnit()) == null) || PQSpecValidator.validateUnitBasedOnUCUM(aClass.getUnit()));
		
	}

	/**
	* Validation of class-constraint : PQSpec
    * Verify if an element of type PQSpec can be validated by PQSpec
	*
	*/
	public static boolean _isPQSpec(net.ihe.gazelle.datatypes.PQ aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   PQSpec
     * class ::  net.ihe.gazelle.datatypes.PQ
     * 
     */
    public static void _validatePQSpec(net.ihe.gazelle.datatypes.PQ aClass, String location, List<Notification> diagnostic) {
		if (_isPQSpec(aClass)){
			executeCons_PQSpec_Cdadt035(aClass, location, diagnostic);
			executeCons_PQSpec_Cdadt036(aClass, location, diagnostic);
			executeCons_PQSpec_Cdadt037(aClass, location, diagnostic);
		}
	}

	private static void executeCons_PQSpec_Cdadt035(net.ihe.gazelle.datatypes.PQ aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validatePQSpec_Cdadt035(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt035");
		notif.setDescription("In PQ datatype, the translation elements SHALL have distinct values (CDADT-035)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-PQSpec-cdadt035");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-035"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

	private static void executeCons_PQSpec_Cdadt036(net.ihe.gazelle.datatypes.PQ aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validatePQSpec_Cdadt036(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt036");
		notif.setDescription("In PQ datatype, the translation elements SHALL not be null if there are translations which are not null (CDADT-036)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-PQSpec-cdadt036");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-036"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

	private static void executeCons_PQSpec_Cdadt037(net.ihe.gazelle.datatypes.PQ aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validatePQSpec_Cdadt037(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt037");
		notif.setDescription("In PQ datatype, the unit attribute SHALL be from UCUM code system (CDADT-037)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-PQSpec-cdadt037");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-037"));
		
		notif.setType("Vocabulary");
		diagnostic.add(notif);
	}

}
