package net.ihe.gazelle.cda.cdadt;

import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;


 /**
  * class :        TNSpec
  * package :   cdadt
  * Constraint Spec Class
  * class of test : TN
  * 
  */
public final class TNSpecValidator{


    private TNSpecValidator() {}



	/**
	* Validation of instance by a constraint : cdadt026
	* In TN datatype, if it is not null, the string value SHALL be provided and not empty, even after formating it (CDADT-026)
	*
	*/
	private static boolean _validateTNSpec_Cdadt026(net.ihe.gazelle.datatypes.TN aClass){
		Boolean result1;
		result1 = false;
		
		/* Iterator Exists: Iterate and check, if any element fulfills the condition. */
		try{
			for (String anElement1 : aClass.getListStringValues()) {
			    if ((!(((String) anElement1) == null) && !anElement1.equals(""))) {
			        result1 = true;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return (!(aClass.getNullFlavor() == null) || result1);
		
		
	}

	/**
	* Validation of class-constraint : TNSpec
    * Verify if an element of type TNSpec can be validated by TNSpec
	*
	*/
	public static boolean _isTNSpec(net.ihe.gazelle.datatypes.TN aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   TNSpec
     * class ::  net.ihe.gazelle.datatypes.TN
     * 
     */
    public static void _validateTNSpec(net.ihe.gazelle.datatypes.TN aClass, String location, List<Notification> diagnostic) {
		if (_isTNSpec(aClass)){
			executeCons_TNSpec_Cdadt026(aClass, location, diagnostic);
		}
	}

	private static void executeCons_TNSpec_Cdadt026(net.ihe.gazelle.datatypes.TN aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateTNSpec_Cdadt026(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt026");
		notif.setDescription("In TN datatype, if it is not null, the string value SHALL be provided and not empty, even after formating it (CDADT-026)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-TNSpec-cdadt026");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-026"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

}
