package net.ihe.gazelle.cda.cdadt;

import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;


 /**
  * class :        URLSpec
  * package :   cdadt
  * Constraint Spec Class
  * class of test : URL
  * 
  */
public final class URLSpecValidator{


    private URLSpecValidator() {}



	/**
	* Validation of instance by a constraint : cdadt013
	* value attribute SHALL have this form : ^(\\w+:.+|#.+)$ (CDADT-013)
	*
	*/
	private static boolean _validateURLSpec_Cdadt013(net.ihe.gazelle.datatypes.URL aClass){
		return ((((String) aClass.getValue()) == null) || aClass.matches(aClass.getValue(), "^(\\w+:.+|#.+)$"));
		
	}

	/**
	* Validation of instance by a constraint : cdadt014
	* When the URL datatype is tel or fax, the structure of the litteral is specified according to the specification  in the datatypes specification, paragraph 2.18.3, and where the value is defined according to this regex (tel|fax):\\+?([0-9]|\\(|\\)|\\.|\\-)*$ (CDADT-014) 
	*
	*/
	private static boolean _validateURLSpec_Cdadt014(net.ihe.gazelle.datatypes.URL aClass){
		return (((((String) aClass.getValue()) == null) || !aClass.matches(aClass.getValue(), "^(tel|fax):.*$")) || aClass.matches(aClass.getValue(), "(tel|fax):\\+?([0-9]|\\(|\\)|\\.|\\-)*$"));
		
	}

	/**
	* Validation of instance by a constraint : dtits009
	* In URL datatype, the mail SHALL be as defined in RFC2368 (DTITS-009)
	*
	*/
	private static boolean _validateURLSpec_Dtits009(net.ihe.gazelle.datatypes.URL aClass){
		return (((((String) aClass.getValue()) == null) || !aClass.matches(aClass.getValue(), "^mailto:.*$")) || aClass.matches(aClass.getValue(), ".+@.+"));
		
	}

	/**
	* Validation of instance by a constraint : dtits010
	* In URL datatype, the http SHALL be as defined in RFC2396 (DTITS-010)
	*
	*/
	private static boolean _validateURLSpec_Dtits010(net.ihe.gazelle.datatypes.URL aClass){
		return (((((String) aClass.getValue()) == null) || !aClass.matches(aClass.getValue(), "^http:.*$")) || aClass.matches(aClass.getValue(), "^http\\:\\/\\/.+$"));
		
	}

	/**
	* Validation of instance by a constraint : dtits011
	* In URL datatype, the ftp SHALL be as defined in RFC1738 (DTITS-011)
	*
	*/
	private static boolean _validateURLSpec_Dtits011(net.ihe.gazelle.datatypes.URL aClass){
		return (((((String) aClass.getValue()) == null) || !aClass.matches(aClass.getValue(), "^ftp:.*$")) || aClass.matches(aClass.getValue(), "^ftp\\:\\/\\/.+$"));
		
	}

	/**
	* Validation of instance by a constraint : dtits012
	* In URL datatype, the file SHALL be as defined in RFC1738 (DTITS-012)
	*
	*/
	private static boolean _validateURLSpec_Dtits012(net.ihe.gazelle.datatypes.URL aClass){
		return (((((String) aClass.getValue()) == null) || !aClass.matches(aClass.getValue(), "^file:.*$")) || aClass.matches(aClass.getValue(), "^file\\:\\/\\/.+$"));
		
	}

	/**
	* Validation of class-constraint : URLSpec
    * Verify if an element of type URLSpec can be validated by URLSpec
	*
	*/
	public static boolean _isURLSpec(net.ihe.gazelle.datatypes.URL aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   URLSpec
     * class ::  net.ihe.gazelle.datatypes.URL
     * 
     */
    public static void _validateURLSpec(net.ihe.gazelle.datatypes.URL aClass, String location, List<Notification> diagnostic) {
		if (_isURLSpec(aClass)){
			executeCons_URLSpec_Cdadt013(aClass, location, diagnostic);
			executeCons_URLSpec_Cdadt014(aClass, location, diagnostic);
			executeCons_URLSpec_Dtits009(aClass, location, diagnostic);
			executeCons_URLSpec_Dtits010(aClass, location, diagnostic);
			executeCons_URLSpec_Dtits011(aClass, location, diagnostic);
			executeCons_URLSpec_Dtits012(aClass, location, diagnostic);
		}
	}

	private static void executeCons_URLSpec_Cdadt013(net.ihe.gazelle.datatypes.URL aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateURLSpec_Cdadt013(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt013");
		notif.setDescription("value attribute SHALL have this form : ^(\\w+:.+|#.+)$ (CDADT-013)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-URLSpec-cdadt013");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-013"));
		
		notif.setType("Fixed value");
		diagnostic.add(notif);
	}

	private static void executeCons_URLSpec_Cdadt014(net.ihe.gazelle.datatypes.URL aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateURLSpec_Cdadt014(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt014");
		notif.setDescription("When the URL datatype is tel or fax, the structure of the litteral is specified according to the specification  in the datatypes specification, paragraph 2.18.3, and where the value is defined according to this regex (tel|fax):\\+?([0-9]|\\(|\\)|\\.|\\-)*$ (CDADT-014)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-URLSpec-cdadt014");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-014"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

	private static void executeCons_URLSpec_Dtits009(net.ihe.gazelle.datatypes.URL aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateURLSpec_Dtits009(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("dtits009");
		notif.setDescription("In URL datatype, the mail SHALL be as defined in RFC2368 (DTITS-009)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-URLSpec-dtits009");
		notif.getAssertions().add(new Assertion("DTITS","DTITS-009"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

	private static void executeCons_URLSpec_Dtits010(net.ihe.gazelle.datatypes.URL aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateURLSpec_Dtits010(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("dtits010");
		notif.setDescription("In URL datatype, the http SHALL be as defined in RFC2396 (DTITS-010)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-URLSpec-dtits010");
		notif.getAssertions().add(new Assertion("DTITS","DTITS-010"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

	private static void executeCons_URLSpec_Dtits011(net.ihe.gazelle.datatypes.URL aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateURLSpec_Dtits011(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("dtits011");
		notif.setDescription("In URL datatype, the ftp SHALL be as defined in RFC1738 (DTITS-011)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-URLSpec-dtits011");
		notif.getAssertions().add(new Assertion("DTITS","DTITS-011"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

	private static void executeCons_URLSpec_Dtits012(net.ihe.gazelle.datatypes.URL aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateURLSpec_Dtits012(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("dtits012");
		notif.setDescription("In URL datatype, the file SHALL be as defined in RFC1738 (DTITS-012)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-URLSpec-dtits012");
		notif.getAssertions().add(new Assertion("DTITS","DTITS-012"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

}
