package net.ihe.gazelle.cda.cdadt;

import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;


 /**
  * class :        EIVLEventSpec
  * package :   cdadt
  * Constraint Spec Class
  * class of test : EIVLEvent
  * 
  */
public final class EIVLEventSpecValidator{


    private EIVLEventSpecValidator() {}



	/**
	* Validation of instance by a constraint : cdadt008
	* if the nullFlavor is not defined, the code SHALL be provided (CDADT-008)
	*
	*/
	private static boolean _validateEIVLEventSpec_Cdadt008(net.ihe.gazelle.datatypes.EIVLEvent aClass){
		return (!(aClass.getNullFlavor() == null) ^ !(((String) aClass.getCode()) == null));
		
	}

	/**
	* Validation of class-constraint : EIVLEventSpec
    * Verify if an element of type EIVLEventSpec can be validated by EIVLEventSpec
	*
	*/
	public static boolean _isEIVLEventSpec(net.ihe.gazelle.datatypes.EIVLEvent aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   EIVLEventSpec
     * class ::  net.ihe.gazelle.datatypes.EIVLEvent
     * 
     */
    public static void _validateEIVLEventSpec(net.ihe.gazelle.datatypes.EIVLEvent aClass, String location, List<Notification> diagnostic) {
		if (_isEIVLEventSpec(aClass)){
			executeCons_EIVLEventSpec_Cdadt008(aClass, location, diagnostic);
		}
	}

	private static void executeCons_EIVLEventSpec_Cdadt008(net.ihe.gazelle.datatypes.EIVLEvent aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateEIVLEventSpec_Cdadt008(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt008");
		notif.setDescription("if the nullFlavor is not defined, the code SHALL be provided (CDADT-008)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-EIVLEventSpec-cdadt008");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-008"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

}
