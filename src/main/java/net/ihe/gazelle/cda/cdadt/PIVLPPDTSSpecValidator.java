package net.ihe.gazelle.cda.cdadt;

import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;


 /**
  * class :        PIVLPPDTSSpec
  * package :   cdadt
  * Constraint Spec Class
  * class of test : PIVLPPDTS
  * 
  */
public final class PIVLPPDTSSpecValidator{


    private PIVLPPDTSSpecValidator() {}



	/**
	* Validation of instance by a constraint : cdadt028
	* In PIVL datatype, if it is not null, it SHALL have a period (CDADT-028)
	*
	*/
	private static boolean _validatePIVLPPDTSSpec_Cdadt028(net.ihe.gazelle.datatypes.PIVLPPDTS aClass){
		return (!(aClass.getNullFlavor() == null) ^ !(aClass.getPeriod() == null));
		
	}

	/**
	* Validation of class-constraint : PIVLPPDTSSpec
    * Verify if an element of type PIVLPPDTSSpec can be validated by PIVLPPDTSSpec
	*
	*/
	public static boolean _isPIVLPPDTSSpec(net.ihe.gazelle.datatypes.PIVLPPDTS aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   PIVLPPDTSSpec
     * class ::  net.ihe.gazelle.datatypes.PIVLPPDTS
     * 
     */
    public static void _validatePIVLPPDTSSpec(net.ihe.gazelle.datatypes.PIVLPPDTS aClass, String location, List<Notification> diagnostic) {
		if (_isPIVLPPDTSSpec(aClass)){
			executeCons_PIVLPPDTSSpec_Cdadt028(aClass, location, diagnostic);
		}
	}

	private static void executeCons_PIVLPPDTSSpec_Cdadt028(net.ihe.gazelle.datatypes.PIVLPPDTS aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validatePIVLPPDTSSpec_Cdadt028(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt028");
		notif.setDescription("In PIVL datatype, if it is not null, it SHALL have a period (CDADT-028)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-PIVLPPDTSSpec-cdadt028");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-028"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

}
