package net.ihe.gazelle.cda.cdadt;

import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;


 /**
  * class :        CRSpec
  * package :   cdadt
  * Constraint Spec Class
  * class of test : CR
  * 
  */
public final class CRSpecValidator{


    private CRSpecValidator() {}



	/**
	* Validation of instance by a constraint : cdadt025
	* In CR datatype, if it is not null, value element SHALL not be null (CDADT-025)
	*
	*/
	private static boolean _validateCRSpec_Cdadt025(net.ihe.gazelle.datatypes.CR aClass){
		return (!(aClass.getValue() == null) ^ !(aClass.getNullFlavor() == null));
		
	}

	/**
	* Validation of class-constraint : CRSpec
    * Verify if an element of type CRSpec can be validated by CRSpec
	*
	*/
	public static boolean _isCRSpec(net.ihe.gazelle.datatypes.CR aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   CRSpec
     * class ::  net.ihe.gazelle.datatypes.CR
     * 
     */
    public static void _validateCRSpec(net.ihe.gazelle.datatypes.CR aClass, String location, List<Notification> diagnostic) {
		if (_isCRSpec(aClass)){
			executeCons_CRSpec_Cdadt025(aClass, location, diagnostic);
		}
	}

	private static void executeCons_CRSpec_Cdadt025(net.ihe.gazelle.datatypes.CR aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCRSpec_Cdadt025(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt025");
		notif.setDescription("In CR datatype, if it is not null, value element SHALL not be null (CDADT-025)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-CRSpec-cdadt025");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-025"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

}
