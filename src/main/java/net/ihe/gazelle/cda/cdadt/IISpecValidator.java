package net.ihe.gazelle.cda.cdadt;

import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;


 /**
  * class :        IISpec
  * package :   cdadt
  * Constraint Spec Class
  * class of test : II
  * 
  */
public final class IISpecValidator{


    private IISpecValidator() {}



	/**
	* Validation of instance by a constraint : cdadt011
	* When root is UUID, the hexadecimal digits A-F SHALL be in upper case (CDADT-011)
	*
	*/
	private static boolean _validateIISpec_Cdadt011(net.ihe.gazelle.datatypes.II aClass){
		return (((((String) aClass.getRoot()) == null) || !aClass.matches(aClass.getRoot(), "[0-9a-zA-Z]{8}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{12}")) || aClass.matches(aClass.getRoot(), "[0-9A-Z]{8}-[0-9A-Z]{4}-[0-9A-Z]{4}-[0-9A-Z]{4}-[0-9A-Z]{12}"));
		
	}

	/**
	* Validation of instance by a constraint : cdadt012
	* when nullFlavor is not defined, the root SHALL be provided (CDADT-012)
	*
	*/
	private static boolean _validateIISpec_Cdadt012(net.ihe.gazelle.datatypes.II aClass){
		return (!(aClass.getNullFlavor() == null) || !(((String) aClass.getRoot()) == null));
		
	}

	/**
	* Validation of class-constraint : IISpec
    * Verify if an element of type IISpec can be validated by IISpec
	*
	*/
	public static boolean _isIISpec(net.ihe.gazelle.datatypes.II aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   IISpec
     * class ::  net.ihe.gazelle.datatypes.II
     * 
     */
    public static void _validateIISpec(net.ihe.gazelle.datatypes.II aClass, String location, List<Notification> diagnostic) {
		if (_isIISpec(aClass)){
			executeCons_IISpec_Cdadt011(aClass, location, diagnostic);
			executeCons_IISpec_Cdadt012(aClass, location, diagnostic);
		}
	}

	private static void executeCons_IISpec_Cdadt011(net.ihe.gazelle.datatypes.II aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateIISpec_Cdadt011(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt011");
		notif.setDescription("When root is UUID, the hexadecimal digits A-F SHALL be in upper case (CDADT-011)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-IISpec-cdadt011");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-011"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_IISpec_Cdadt012(net.ihe.gazelle.datatypes.II aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateIISpec_Cdadt012(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt012");
		notif.setDescription("when nullFlavor is not defined, the root SHALL be provided (CDADT-012)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-IISpec-cdadt012");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-012"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

}
