package net.ihe.gazelle.cda.cdadt;

import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;


 /**
  * class :        IVLTSSpec
  * package :   cdadt
  * Constraint Spec Class
  * class of test : IVLTS
  * 
  */
public final class IVLTSSpecValidator{


    private IVLTSSpecValidator() {}

public static Boolean compareTime(String low, String high) {
	// Start of user code for method compareTime
	java.util.Date dtl = net.ihe.gazelle.basic.validation.HL7Time.parse(low);
			java.util.Date dth = net.ihe.gazelle.basic.validation.HL7Time.parse(high);
			if (dtl == null || dth == null) return true;
			return dtl.before(dth) || dtl.equals(dth);
	// End of user code
}
public static Boolean unitIsMeasureOfTime(String unit) {
	// Start of user code for method unitIsMeasureOfTime
	return net.ihe.gazelle.basic.validation.UCUMValidator.unitsAreComparable(unit, "s");
	// End of user code
}


	/**
	* Validation of instance by a constraint : cdadt017
	* In IVL<TS> datatypes, the low value SHALL be lessOrEqual the high value (CDADT-017)
	*
	*/
	private static boolean _validateIVLTSSpec_Cdadt017(net.ihe.gazelle.datatypes.IVLTS aClass){
		return (((((aClass.getLow() == null) || (aClass.getHigh() == null)) || (((String) aClass.getLow().getValue()) == null)) || (((String) aClass.getLow().getValue()) == null)) || IVLTSSpecValidator.compareTime(aClass.getLow().getValue(), aClass.getHigh().getValue()));
		
	}

	/**
	* Validation of instance by a constraint : cdadt019
	* In IVL<TS>, the width/@unit SHALL be from mesure of time values (CDADT-019)
	*
	*/
	private static boolean _validateIVLTSSpec_Cdadt019(net.ihe.gazelle.datatypes.IVLTS aClass){
		return (((aClass.getWidth() == null) || (((String) aClass.getWidth().getUnit()) == null)) || IVLTSSpecValidator.unitIsMeasureOfTime(aClass.getWidth().getUnit()));
		
	}

	/**
	* Validation of class-constraint : IVLTSSpec
    * Verify if an element of type IVLTSSpec can be validated by IVLTSSpec
	*
	*/
	public static boolean _isIVLTSSpec(net.ihe.gazelle.datatypes.IVLTS aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   IVLTSSpec
     * class ::  net.ihe.gazelle.datatypes.IVLTS
     * 
     */
    public static void _validateIVLTSSpec(net.ihe.gazelle.datatypes.IVLTS aClass, String location, List<Notification> diagnostic) {
		if (_isIVLTSSpec(aClass)){
			executeCons_IVLTSSpec_Cdadt017(aClass, location, diagnostic);
			executeCons_IVLTSSpec_Cdadt019(aClass, location, diagnostic);
		}
	}

	private static void executeCons_IVLTSSpec_Cdadt017(net.ihe.gazelle.datatypes.IVLTS aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateIVLTSSpec_Cdadt017(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt017");
		notif.setDescription("In IVL<TS> datatypes, the low value SHALL be lessOrEqual the high value (CDADT-017)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-IVLTSSpec-cdadt017");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-017"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

	private static void executeCons_IVLTSSpec_Cdadt019(net.ihe.gazelle.datatypes.IVLTS aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateIVLTSSpec_Cdadt019(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt019");
		notif.setDescription("In IVL<TS>, the width/@unit SHALL be from mesure of time values (CDADT-019)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-IVLTSSpec-cdadt019");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-019"));
		
		notif.setType("Fixed value");
		diagnostic.add(notif);
	}

}
