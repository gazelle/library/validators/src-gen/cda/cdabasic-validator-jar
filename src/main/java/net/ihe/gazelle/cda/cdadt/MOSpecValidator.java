package net.ihe.gazelle.cda.cdadt;

import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;


 /**
  * class :        MOSpec
  * package :   cdadt
  * Constraint Spec Class
  * class of test : MO
  * 
  */
public final class MOSpecValidator{


    private MOSpecValidator() {}



	/**
	* Validation of instance by a constraint : cdadt027
	* In MO datatype, the currency unit SHALL be from ISO 4217 (CDADT-027)
	*
	*/
	private static boolean _validateMOSpec_Cdadt027(net.ihe.gazelle.datatypes.MO aClass){
		return ((((String) aClass.getCurrency()) == null) || aClass.matchesCodeToValueSet("1.3.6.1.4.1.12559.11.18.8", aClass.getCurrency()));
		
	}

	/**
	* Validation of class-constraint : MOSpec
    * Verify if an element of type MOSpec can be validated by MOSpec
	*
	*/
	public static boolean _isMOSpec(net.ihe.gazelle.datatypes.MO aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   MOSpec
     * class ::  net.ihe.gazelle.datatypes.MO
     * 
     */
    public static void _validateMOSpec(net.ihe.gazelle.datatypes.MO aClass, String location, List<Notification> diagnostic) {
		if (_isMOSpec(aClass)){
			executeCons_MOSpec_Cdadt027(aClass, location, diagnostic);
		}
	}

	private static void executeCons_MOSpec_Cdadt027(net.ihe.gazelle.datatypes.MO aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateMOSpec_Cdadt027(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt027");
		notif.setDescription("In MO datatype, the currency unit SHALL be from ISO 4217 (CDADT-027)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-MOSpec-cdadt027");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-027"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

}
