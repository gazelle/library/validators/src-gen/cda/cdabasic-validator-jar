package net.ihe.gazelle.cda.cdadt;

import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;


 /**
  * class :        ENXPSpec
  * package :   cdadt
  * Constraint Spec Class
  * class of test : ENXP
  * 
  */
public final class ENXPSpecValidator{


    private ENXPSpecValidator() {}



	/**
	* Validation of instance by a constraint : cdadt033
	* In ENXP datatype, the qualifier attribute SHALL have distinct values (CDADT-033)
	*
	*/
	private static boolean _validateENXPSpec_Cdadt033(net.ihe.gazelle.datatypes.ENXP aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getQualifier()) {
			    java.util.ArrayList<String> result2;
			    result2 = new java.util.ArrayList<String>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (String anElement2 : aClass.getQualifier()) {
			    	    if (anElement2.equals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of class-constraint : ENXPSpec
    * Verify if an element of type ENXPSpec can be validated by ENXPSpec
	*
	*/
	public static boolean _isENXPSpec(net.ihe.gazelle.datatypes.ENXP aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   ENXPSpec
     * class ::  net.ihe.gazelle.datatypes.ENXP
     * 
     */
    public static void _validateENXPSpec(net.ihe.gazelle.datatypes.ENXP aClass, String location, List<Notification> diagnostic) {
		if (_isENXPSpec(aClass)){
			executeCons_ENXPSpec_Cdadt033(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ENXPSpec_Cdadt033(net.ihe.gazelle.datatypes.ENXP aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateENXPSpec_Cdadt033(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt033");
		notif.setDescription("In ENXP datatype, the qualifier attribute SHALL have distinct values (CDADT-033)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-ENXPSpec-cdadt033");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-033"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

}
