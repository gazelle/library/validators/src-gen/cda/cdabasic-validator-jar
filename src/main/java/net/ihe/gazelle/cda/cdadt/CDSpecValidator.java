package net.ihe.gazelle.cda.cdadt;

import java.util.List;

import net.ihe.gazelle.datatypes.CD;
import net.ihe.gazelle.datatypes.CS;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.voc.NullFlavor;


 /**
  * class :        CDSpec
  * package :   cdadt
  * Template Class
  * Template identifier : 
  * Class of test : CD
  * 
  */
public final class CDSpecValidator{


    private CDSpecValidator() {}



	/**
	* Validation of instance by a constraint : cdadt004
	* codeSystem attribute SHALL be provided when codeSystemName is provided (CDADT-004)
	*
	*/
	private static boolean _validateCDSpec_Cdadt004(net.ihe.gazelle.datatypes.CD aClass){
		return ((((String) aClass.getCodeSystemName()) == null) || !(((String) aClass.getCodeSystem()) == null));
		
	}

	/**
	* Validation of instance by a constraint : cdadt005
	* In CD datatype, codeSystem attribute SHALL be provided when codeSystemVersion is provided (CDADT-005)
	*
	*/
	private static boolean _validateCDSpec_Cdadt005(net.ihe.gazelle.datatypes.CD aClass){
		return ((((String) aClass.getCodeSystemVersion()) == null) || !(((String) aClass.getCodeSystem()) == null));
		
	}

	/**
	* Validation of instance by a constraint : cdadt006
	* code attribute SHALL be provided when displayName is provided (CDADT-006)
	*
	*/
	private static boolean _validateCDSpec_Cdadt006(net.ihe.gazelle.datatypes.CD aClass){
		return ((((String) aClass.getDisplayName()) == null) || !(((String) aClass.getCode()) == null));
		
	}

	/**
	* Validation of instance by a constraint : cdadt007
	* originalText attribute SHALL NOT have multimedia value, only reference or plain text form are allowed (CDADT-007)
	*
	*/
	private static boolean _validateCDSpec_Cdadt007(net.ihe.gazelle.datatypes.CD aClass){
		return (((aClass.getOriginalText() == null) || (((String) aClass.getOriginalText().getMediaType()) == null)) || ((((!aClass.getOriginalText().getMediaType().equals("application/dicom") && !aClass.getOriginalText().getMediaType().equals("application/pdf")) && !aClass.getOriginalText().getMediaType().equals("image/gif")) && !aClass.getOriginalText().getMediaType().equals("image/jpeg")) && !aClass.getOriginalText().getMediaType().equals("image/tiff")));
		
	}

	/**
	* Validation of instance by a constraint : cdadt008
	* if the nullFlavor is not defined, the code SHALL be provided (CDADT-008)
	*
	*/
	private static boolean _validateCDSpec_Cdadt008(net.ihe.gazelle.datatypes.CD aClass){
		return (!(aClass.getNullFlavor() == null) || !(((String) aClass.getCode()) == null));
		
	}

	/**
	* Validation of instance by a constraint : cdadt011
	* When UUID is used in codeSystem, the hexadecimal digits A-F SHALL be in upper case (CDADT-011)
	*
	*/
	private static boolean _validateCDSpec_Cdadt011(net.ihe.gazelle.datatypes.CD aClass){
		return (((((String) aClass.getCodeSystem()) == null) || !aClass.matches(aClass.getCodeSystem(), "[0-9a-zA-Z]{8}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{12}")) || aClass.matches(aClass.getCodeSystem(), "[0-9A-Z]{8}-[0-9A-Z]{4}-[0-9A-Z]{4}-[0-9A-Z]{4}-[0-9A-Z]{12}"));
		
	}

	/**
	* Validation of instance by a constraint : cdadt020
	* IN CD datatype, when CD/@nullFlavor= OTH , the CD/@codeSystem SHALL be present (CDADT-020)
	*
	*/
	private static boolean _validateCDSpec_Cdadt020(net.ihe.gazelle.datatypes.CD aClass){
		return (((aClass.getNullFlavor() == null) || !aClass.getNullFlavor().equals(NullFlavor.OTH)) || !(((String) aClass.getCodeSystem()) == null));
		
	}

	/**
	* Validation of instance by a constraint : cdadt029
	* In CD datatype, the translation elements SHALL be disctinct (CDADT-029)
	*
	*/
	private static boolean _validateCDSpec_Cdadt029(net.ihe.gazelle.datatypes.CD aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (CD anElement1 : aClass.getTranslation()) {
			    java.util.ArrayList<CD> result2;
			    result2 = new java.util.ArrayList<CD>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (CD anElement2 : aClass.getTranslation()) {
			    	    if (anElement2.cdaEquals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getTranslation()) < Integer.valueOf(2)) || result1);
		
		
	}

	/**
	* Validation of instance by a constraint : cdadt030
	* In CD datatype, the translation elements SHALL not be null if there are translations which are not null (CDADT-030)
	*
	*/
	private static boolean _validateCDSpec_Cdadt030(net.ihe.gazelle.datatypes.CD aClass){
		java.util.ArrayList<CD> result1;
		result1 = new java.util.ArrayList<CD>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (CD anElement1 : aClass.getTranslation()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getTranslation()) < Integer.valueOf(2)) || ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(Integer.valueOf(0)));
		
		
	}

	/**
	* Validation of template-constraint by a constraint : CDSpec
    * Verify if an element can be token as a Template of type CDSpec
	*
	*/
	private static boolean _isCDSpecTemplate(net.ihe.gazelle.datatypes.CD aClass){
		return !(CS.class.isAssignableFrom(aClass.getClass()));
				
	}
	/**
	* Validation of class-constraint : CDSpec
    * Verify if an element of type CDSpec can be validated by CDSpec
	*
	*/
	public static boolean _isCDSpec(net.ihe.gazelle.datatypes.CD aClass){
		return _isCDSpecTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   CDSpec
     * class ::  net.ihe.gazelle.datatypes.CD
     * 
     */
    public static void _validateCDSpec(net.ihe.gazelle.datatypes.CD aClass, String location, List<Notification> diagnostic) {
		if (_isCDSpec(aClass)){
			executeCons_CDSpec_Cdadt004(aClass, location, diagnostic);
			executeCons_CDSpec_Cdadt005(aClass, location, diagnostic);
			executeCons_CDSpec_Cdadt006(aClass, location, diagnostic);
			executeCons_CDSpec_Cdadt007(aClass, location, diagnostic);
			executeCons_CDSpec_Cdadt008(aClass, location, diagnostic);
			executeCons_CDSpec_Cdadt011(aClass, location, diagnostic);
			executeCons_CDSpec_Cdadt020(aClass, location, diagnostic);
			executeCons_CDSpec_Cdadt029(aClass, location, diagnostic);
			executeCons_CDSpec_Cdadt030(aClass, location, diagnostic);
		}
	}

	private static void executeCons_CDSpec_Cdadt004(net.ihe.gazelle.datatypes.CD aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCDSpec_Cdadt004(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt004");
		notif.setDescription("codeSystem attribute SHALL be provided when codeSystemName is provided (CDADT-004)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-CDSpec-cdadt004");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-004"));
		
		notif.setType("Mandatory");
		diagnostic.add(notif);
	}

	private static void executeCons_CDSpec_Cdadt005(net.ihe.gazelle.datatypes.CD aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCDSpec_Cdadt005(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt005");
		notif.setDescription("In CD datatype, codeSystem attribute SHALL be provided when codeSystemVersion is provided (CDADT-005)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-CDSpec-cdadt005");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-005"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

	private static void executeCons_CDSpec_Cdadt006(net.ihe.gazelle.datatypes.CD aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCDSpec_Cdadt006(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt006");
		notif.setDescription("code attribute SHALL be provided when displayName is provided (CDADT-006)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-CDSpec-cdadt006");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-006"));
		
		notif.setType("Mandatory");
		diagnostic.add(notif);
	}

	private static void executeCons_CDSpec_Cdadt007(net.ihe.gazelle.datatypes.CD aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCDSpec_Cdadt007(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt007");
		notif.setDescription("originalText attribute SHALL NOT have multimedia value, only reference or plain text form are allowed (CDADT-007)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-CDSpec-cdadt007");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-007"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

	private static void executeCons_CDSpec_Cdadt008(net.ihe.gazelle.datatypes.CD aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCDSpec_Cdadt008(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt008");
		notif.setDescription("if the nullFlavor is not defined, the code SHALL be provided (CDADT-008)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-CDSpec-cdadt008");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-008"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_CDSpec_Cdadt011(net.ihe.gazelle.datatypes.CD aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCDSpec_Cdadt011(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt011");
		notif.setDescription("When UUID is used in codeSystem, the hexadecimal digits A-F SHALL be in upper case (CDADT-011)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-CDSpec-cdadt011");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-011"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_CDSpec_Cdadt020(net.ihe.gazelle.datatypes.CD aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCDSpec_Cdadt020(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt020");
		notif.setDescription("IN CD datatype, when CD/@nullFlavor= OTH , the CD/@codeSystem SHALL be present (CDADT-020)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-CDSpec-cdadt020");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-020"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

	private static void executeCons_CDSpec_Cdadt029(net.ihe.gazelle.datatypes.CD aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCDSpec_Cdadt029(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt029");
		notif.setDescription("In CD datatype, the translation elements SHALL be disctinct (CDADT-029)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-CDSpec-cdadt029");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-029"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

	private static void executeCons_CDSpec_Cdadt030(net.ihe.gazelle.datatypes.CD aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCDSpec_Cdadt030(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt030");
		notif.setDescription("In CD datatype, the translation elements SHALL not be null if there are translations which are not null (CDADT-030)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-CDSpec-cdadt030");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-030"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

}
