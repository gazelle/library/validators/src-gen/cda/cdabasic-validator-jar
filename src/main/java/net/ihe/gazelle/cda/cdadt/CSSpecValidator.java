package net.ihe.gazelle.cda.cdadt;

import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;


 /**
  * class :        CSSpec
  * package :   cdadt
  * Constraint Spec Class
  * class of test : CS
  * 
  */
public final class CSSpecValidator{


    private CSSpecValidator() {}



	/**
	* Validation of instance by a constraint : cdadt009
	* if the nullFlavor is not defined, the code SHALL be provided (CDADT-009)
	*
	*/
	private static boolean _validateCSSpec_Cdadt009(net.ihe.gazelle.datatypes.CS aClass){
		return (!(aClass.getNullFlavor() == null) || !(((String) aClass.getCode()) == null));
		
	}

	/**
	* Validation of class-constraint : CSSpec
    * Verify if an element of type CSSpec can be validated by CSSpec
	*
	*/
	public static boolean _isCSSpec(net.ihe.gazelle.datatypes.CS aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   CSSpec
     * class ::  net.ihe.gazelle.datatypes.CS
     * 
     */
    public static void _validateCSSpec(net.ihe.gazelle.datatypes.CS aClass, String location, List<Notification> diagnostic) {
		if (_isCSSpec(aClass)){
			executeCons_CSSpec_Cdadt009(aClass, location, diagnostic);
		}
	}

	private static void executeCons_CSSpec_Cdadt009(net.ihe.gazelle.datatypes.CS aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCSSpec_Cdadt009(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt009");
		notif.setDescription("if the nullFlavor is not defined, the code SHALL be provided (CDADT-009)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-CSSpec-cdadt009");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-009"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

}
