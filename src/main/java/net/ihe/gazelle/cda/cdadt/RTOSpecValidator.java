package net.ihe.gazelle.cda.cdadt;

import java.util.List;

import net.ihe.gazelle.datatypes.INT;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;


 /**
  * class :        RTOSpec
  * package :   cdadt
  * Constraint Spec Class
  * class of test : RTO
  * 
  */
public final class RTOSpecValidator{


    private RTOSpecValidator() {}



	/**
	* Validation of instance by a constraint : cdadt016
	* In RTO datatype, the denominator SHALL NOT be zero (CDADT-016)
	*
	*/
	private static boolean _validateRTOSpec_Cdadt016(net.ihe.gazelle.datatypes.RTO aClass){
		return (!(aClass.getNullFlavor() == null) || ((!(aClass.getDenominator() == null) && (aClass.getDenominator().getNullFlavor() == null)) && (!(INT.class.isAssignableFrom(aClass.getDenominator().getClass())) || (!(((Integer) ((INT) aClass.getDenominator()).getValue()) == null) && !((Object) ((INT) aClass.getDenominator()).getValue()).equals(Integer.valueOf(0))))));
		
	}

	/**
	* Validation of class-constraint : RTOSpec
    * Verify if an element of type RTOSpec can be validated by RTOSpec
	*
	*/
	public static boolean _isRTOSpec(net.ihe.gazelle.datatypes.RTO aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   RTOSpec
     * class ::  net.ihe.gazelle.datatypes.RTO
     * 
     */
    public static void _validateRTOSpec(net.ihe.gazelle.datatypes.RTO aClass, String location, List<Notification> diagnostic) {
		if (_isRTOSpec(aClass)){
			executeCons_RTOSpec_Cdadt016(aClass, location, diagnostic);
		}
	}

	private static void executeCons_RTOSpec_Cdadt016(net.ihe.gazelle.datatypes.RTO aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateRTOSpec_Cdadt016(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt016");
		notif.setDescription("In RTO datatype, the denominator SHALL NOT be zero (CDADT-016)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-RTOSpec-cdadt016");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-016"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

}
