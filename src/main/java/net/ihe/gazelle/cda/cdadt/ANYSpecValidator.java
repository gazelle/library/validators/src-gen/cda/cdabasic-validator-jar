package net.ihe.gazelle.cda.cdadt;

import java.util.List;

import net.ihe.gazelle.datatypes.QTY;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.voc.NullFlavor;


 /**
  * class :        ANYSpec
  * package :   cdadt
  * Constraint Spec Class
  * class of test : ANY
  * 
  */
public final class ANYSpecValidator{


    private ANYSpecValidator() {}



	/**
	* Validation of instance by a constraint : cdadt001
	* NP value of nullFlavor is not used for CDA documents (CDADT-001)
	*
	*/
	private static boolean _validateANYSpec_Cdadt001(net.ihe.gazelle.datatypes.ANY aClass){
		return ((aClass.getNullFlavor() == null) || !aClass.getNullFlavor().equals(NullFlavor.NP));
		
	}

	/**
	* Validation of instance by a constraint : cdadt022
	* The use of nullFlavor PINF and NINF SHALL be for datatypes QTY (CDADT-022)
	*
	*/
	private static boolean _validateANYSpec_Cdadt022(net.ihe.gazelle.datatypes.ANY aClass){
		return (((aClass.getNullFlavor() == null) || (!aClass.getNullFlavor().equals(NullFlavor.NINF) && !aClass.getNullFlavor().equals(NullFlavor.PINF))) || (QTY.class.isAssignableFrom(aClass.getClass())));
		
	}

	/**
	* Validation of class-constraint : ANYSpec
    * Verify if an element of type ANYSpec can be validated by ANYSpec
	*
	*/
	public static boolean _isANYSpec(net.ihe.gazelle.datatypes.ANY aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   ANYSpec
     * class ::  net.ihe.gazelle.datatypes.ANY
     * 
     */
    public static void _validateANYSpec(net.ihe.gazelle.datatypes.ANY aClass, String location, List<Notification> diagnostic) {
		if (_isANYSpec(aClass)){
			executeCons_ANYSpec_Cdadt001(aClass, location, diagnostic);
			executeCons_ANYSpec_Cdadt022(aClass, location, diagnostic);
		}
	}

	private static void executeCons_ANYSpec_Cdadt001(net.ihe.gazelle.datatypes.ANY aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateANYSpec_Cdadt001(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt001");
		notif.setDescription("NP value of nullFlavor is not used for CDA documents (CDADT-001)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-ANYSpec-cdadt001");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-001"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

	private static void executeCons_ANYSpec_Cdadt022(net.ihe.gazelle.datatypes.ANY aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateANYSpec_Cdadt022(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt022");
		notif.setDescription("The use of nullFlavor PINF and NINF SHALL be for datatypes QTY (CDADT-022)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-ANYSpec-cdadt022");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-022"));
		
		notif.setType("Context");
		diagnostic.add(notif);
	}

}
