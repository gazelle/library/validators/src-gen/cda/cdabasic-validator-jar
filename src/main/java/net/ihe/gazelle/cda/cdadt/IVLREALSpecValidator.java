package net.ihe.gazelle.cda.cdadt;

import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;


 /**
  * class :        IVLREALSpec
  * package :   cdadt
  * Constraint Spec Class
  * class of test : IVLREAL
  * 
  */
public final class IVLREALSpecValidator{


    private IVLREALSpecValidator() {}



	/**
	* Validation of instance by a constraint : cdadt017
	* In IVL<REAL> datatypes, the low value SHALL be lessOrEqual the high value (CDADT-017)
	*
	*/
	private static boolean _validateIVLREALSpec_Cdadt017(net.ihe.gazelle.datatypes.IVLREAL aClass){
		return (((((aClass.getLow() == null) || (aClass.getHigh() == null)) || (aClass.getLow().getValue() == null)) || (aClass.getHigh().getValue() == null)) || IVLPQSpecValidator.real1IsLowerThanReal2(aClass.getLow().getValue(), aClass.getHigh().getValue()));
		
	}

	/**
	* Validation of class-constraint : IVLREALSpec
    * Verify if an element of type IVLREALSpec can be validated by IVLREALSpec
	*
	*/
	public static boolean _isIVLREALSpec(net.ihe.gazelle.datatypes.IVLREAL aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   IVLREALSpec
     * class ::  net.ihe.gazelle.datatypes.IVLREAL
     * 
     */
    public static void _validateIVLREALSpec(net.ihe.gazelle.datatypes.IVLREAL aClass, String location, List<Notification> diagnostic) {
		if (_isIVLREALSpec(aClass)){
			executeCons_IVLREALSpec_Cdadt017(aClass, location, diagnostic);
		}
	}

	private static void executeCons_IVLREALSpec_Cdadt017(net.ihe.gazelle.datatypes.IVLREAL aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateIVLREALSpec_Cdadt017(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt017");
		notif.setDescription("In IVL<REAL> datatypes, the low value SHALL be lessOrEqual the high value (CDADT-017)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-IVLREALSpec-cdadt017");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-017"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

}
