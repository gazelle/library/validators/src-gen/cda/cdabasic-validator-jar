package net.ihe.gazelle.cda.cdadt;

import java.util.List;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import net.ihe.gazelle.validation.Notification;


public class CDADTPackValidator implements ConstraintValidatorModule{


    /**
     * Create a new ObjectValidator that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public CDADTPackValidator() {}
    


	/**
	* Validation of instance of an object
	*
	*/
	public void validate(Object obj, String location, List<Notification> diagnostic){

		if (obj instanceof net.ihe.gazelle.datatypes.AD){
			net.ihe.gazelle.datatypes.AD aClass = ( net.ihe.gazelle.datatypes.AD)obj;
			ADSpecValidator._validateADSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.datatypes.ANY){
			net.ihe.gazelle.datatypes.ANY aClass = ( net.ihe.gazelle.datatypes.ANY)obj;
			ANYSpecValidator._validateANYSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.datatypes.CD){
			net.ihe.gazelle.datatypes.CD aClass = ( net.ihe.gazelle.datatypes.CD)obj;
			CDSpecValidator._validateCDSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.datatypes.CR){
			net.ihe.gazelle.datatypes.CR aClass = ( net.ihe.gazelle.datatypes.CR)obj;
			CRSpecValidator._validateCRSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.datatypes.CS){
			net.ihe.gazelle.datatypes.CS aClass = ( net.ihe.gazelle.datatypes.CS)obj;
			CSSpecValidator._validateCSSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.datatypes.ED){
			net.ihe.gazelle.datatypes.ED aClass = ( net.ihe.gazelle.datatypes.ED)obj;
			EDSpecValidator._validateEDSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.datatypes.EIVLEvent){
			net.ihe.gazelle.datatypes.EIVLEvent aClass = ( net.ihe.gazelle.datatypes.EIVLEvent)obj;
			EIVLEventSpecValidator._validateEIVLEventSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.datatypes.EN){
			net.ihe.gazelle.datatypes.EN aClass = ( net.ihe.gazelle.datatypes.EN)obj;
			ENSpecValidator._validateENSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.datatypes.ENXP){
			net.ihe.gazelle.datatypes.ENXP aClass = ( net.ihe.gazelle.datatypes.ENXP)obj;
			ENXPSpecValidator._validateENXPSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.datatypes.II){
			net.ihe.gazelle.datatypes.II aClass = ( net.ihe.gazelle.datatypes.II)obj;
			IISpecValidator._validateIISpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.datatypes.IVLINT){
			net.ihe.gazelle.datatypes.IVLINT aClass = ( net.ihe.gazelle.datatypes.IVLINT)obj;
			IVLINTSpecValidator._validateIVLINTSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.datatypes.IVLPQ){
			net.ihe.gazelle.datatypes.IVLPQ aClass = ( net.ihe.gazelle.datatypes.IVLPQ)obj;
			IVLPQSpecValidator._validateIVLPQSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.datatypes.IVLREAL){
			net.ihe.gazelle.datatypes.IVLREAL aClass = ( net.ihe.gazelle.datatypes.IVLREAL)obj;
			IVLREALSpecValidator._validateIVLREALSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.datatypes.IVLTS){
			net.ihe.gazelle.datatypes.IVLTS aClass = ( net.ihe.gazelle.datatypes.IVLTS)obj;
			IVLTSSpecValidator._validateIVLTSSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.datatypes.MO){
			net.ihe.gazelle.datatypes.MO aClass = ( net.ihe.gazelle.datatypes.MO)obj;
			MOSpecValidator._validateMOSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.datatypes.PIVLPPDTS){
			net.ihe.gazelle.datatypes.PIVLPPDTS aClass = ( net.ihe.gazelle.datatypes.PIVLPPDTS)obj;
			PIVLPPDTSSpecValidator._validatePIVLPPDTSSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.datatypes.PIVLTS){
			net.ihe.gazelle.datatypes.PIVLTS aClass = ( net.ihe.gazelle.datatypes.PIVLTS)obj;
			PIVLTSSpecValidator._validatePIVLTSSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.datatypes.PN){
			net.ihe.gazelle.datatypes.PN aClass = ( net.ihe.gazelle.datatypes.PN)obj;
			PNSpecValidator._validatePNSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.datatypes.PQ){
			net.ihe.gazelle.datatypes.PQ aClass = ( net.ihe.gazelle.datatypes.PQ)obj;
			PQSpecValidator._validatePQSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.datatypes.RTO){
			net.ihe.gazelle.datatypes.RTO aClass = ( net.ihe.gazelle.datatypes.RTO)obj;
			RTOSpecValidator._validateRTOSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.datatypes.SC){
			net.ihe.gazelle.datatypes.SC aClass = ( net.ihe.gazelle.datatypes.SC)obj;
			SCSpecValidator._validateSCSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.datatypes.TEL){
			net.ihe.gazelle.datatypes.TEL aClass = ( net.ihe.gazelle.datatypes.TEL)obj;
			TELSpecValidator._validateTELSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.datatypes.TN){
			net.ihe.gazelle.datatypes.TN aClass = ( net.ihe.gazelle.datatypes.TN)obj;
			TNSpecValidator._validateTNSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.datatypes.TS){
			net.ihe.gazelle.datatypes.TS aClass = ( net.ihe.gazelle.datatypes.TS)obj;
			TSSpecValidator._validateTSSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.datatypes.URL){
			net.ihe.gazelle.datatypes.URL aClass = ( net.ihe.gazelle.datatypes.URL)obj;
			URLSpecValidator._validateURLSpec(aClass, location, diagnostic);
		}
	
	}

}

