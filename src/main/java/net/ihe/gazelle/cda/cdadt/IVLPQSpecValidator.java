package net.ihe.gazelle.cda.cdadt;

import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;


 /**
  * class :        IVLPQSpec
  * package :   cdadt
  * Constraint Spec Class
  * class of test : IVLPQ
  * 
  */
public final class IVLPQSpecValidator{


    private IVLPQSpecValidator() {}

public static Boolean real1IsLowerThanReal2(Double real1, Double real2) {
	// Start of user code for method real1IsLowerThanReal2
	if (real1 != null && real2 != null){
			return real1 <= real2;
		}
		return true;
	// End of user code
}
public static Boolean unitsAreComparables(String unit1, String unit2) {
	// Start of user code for method unitsAreComparables
	return net.ihe.gazelle.basic.validation.UCUMValidator.unitsAreComparable(unit1, unit2);
	// End of user code
}
public static Boolean value1IsLowerOrEqualThanValue2(Double value1, String unit1, Double value2, String unit2) {
	// Start of user code for method value1IsLowerOrEqualThanValue2
	if (value1 != null && value2 != null) {
			String val1 = String.valueOf(value1);
			String val2 = String.valueOf(value2);
			return net.ihe.gazelle.basic.validation.UCUMValidator.value1IsLowerOrEqualThanValue2(val1, unit1, val2, unit2);
		}
		return true;
	// End of user code
}


	/**
	* Validation of instance by a constraint : cdadt017
	* In IVL<PQ> datatype, the low value SHALL be lessOrEqual the high value (CDADT-017)
	*
	*/
	private static boolean _validateIVLPQSpec_Cdadt017(net.ihe.gazelle.datatypes.IVLPQ aClass){
		return (((((((aClass.getLow() == null) || (aClass.getHigh() == null)) || (aClass.getLow().getValue() == null)) || (aClass.getHigh().getValue() == null)) || ((((String) aClass.getLow().getUnit()) == null) ^ (((String) aClass.getHigh().getUnit()) == null))) || ((((String) aClass.getLow().getUnit()) == null) || (((String) aClass.getHigh().getUnit()) == null))) || IVLPQSpecValidator.value1IsLowerOrEqualThanValue2(aClass.getLow().getValue(), aClass.getLow().getUnit(), aClass.getHigh().getValue(), aClass.getHigh().getUnit()));
		
	}

	/**
	* Validation of instance by a constraint : cdadt018
	* In IVL<PQ> datatype, the low and high values SHALL use comparable units (CDADT-018)
	*
	*/
	private static boolean _validateIVLPQSpec_Cdadt018(net.ihe.gazelle.datatypes.IVLPQ aClass){
		return (((((aClass.getLow() == null) || (((String) aClass.getLow().getUnit()) == null)) || (aClass.getHigh() == null)) || (((String) aClass.getHigh().getUnit()) == null)) || IVLPQSpecValidator.unitsAreComparables(aClass.getLow().getUnit(), aClass.getHigh().getUnit()));
		
	}

	/**
	* Validation of class-constraint : IVLPQSpec
    * Verify if an element of type IVLPQSpec can be validated by IVLPQSpec
	*
	*/
	public static boolean _isIVLPQSpec(net.ihe.gazelle.datatypes.IVLPQ aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   IVLPQSpec
     * class ::  net.ihe.gazelle.datatypes.IVLPQ
     * 
     */
    public static void _validateIVLPQSpec(net.ihe.gazelle.datatypes.IVLPQ aClass, String location, List<Notification> diagnostic) {
		if (_isIVLPQSpec(aClass)){
			executeCons_IVLPQSpec_Cdadt017(aClass, location, diagnostic);
			executeCons_IVLPQSpec_Cdadt018(aClass, location, diagnostic);
		}
	}

	private static void executeCons_IVLPQSpec_Cdadt017(net.ihe.gazelle.datatypes.IVLPQ aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateIVLPQSpec_Cdadt017(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt017");
		notif.setDescription("In IVL<PQ> datatype, the low value SHALL be lessOrEqual the high value (CDADT-017)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-IVLPQSpec-cdadt017");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-017"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

	private static void executeCons_IVLPQSpec_Cdadt018(net.ihe.gazelle.datatypes.IVLPQ aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateIVLPQSpec_Cdadt018(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt018");
		notif.setDescription("In IVL<PQ> datatype, the low and high values SHALL use comparable units (CDADT-018)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-IVLPQSpec-cdadt018");
		
		diagnostic.add(notif);
	}

}
