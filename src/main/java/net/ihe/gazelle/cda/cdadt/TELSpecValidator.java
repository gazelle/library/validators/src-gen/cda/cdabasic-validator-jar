package net.ihe.gazelle.cda.cdadt;

import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;


 /**
  * class :        TELSpec
  * package :   cdadt
  * Constraint Spec Class
  * class of test : TEL
  * 
  */
public final class TELSpecValidator{


    private TELSpecValidator() {}



	/**
	* Validation of instance by a constraint : cdadt031
	* In TEL datatype, the use attribute SHALL have disctinct values (CDADT-031)
	*
	*/
	private static boolean _validateTELSpec_Cdadt031(net.ihe.gazelle.datatypes.TEL aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (String anElement1 : aClass.getUse()) {
			    java.util.ArrayList<String> result2;
			    result2 = new java.util.ArrayList<String>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (String anElement2 : aClass.getUse()) {
			    	    if (anElement2.equals(anElement1)) {
			    	        result2.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2)).equals(Integer.valueOf(1))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of class-constraint : TELSpec
    * Verify if an element of type TELSpec can be validated by TELSpec
	*
	*/
	public static boolean _isTELSpec(net.ihe.gazelle.datatypes.TEL aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   TELSpec
     * class ::  net.ihe.gazelle.datatypes.TEL
     * 
     */
    public static void _validateTELSpec(net.ihe.gazelle.datatypes.TEL aClass, String location, List<Notification> diagnostic) {
		if (_isTELSpec(aClass)){
			executeCons_TELSpec_Cdadt031(aClass, location, diagnostic);
		}
	}

	private static void executeCons_TELSpec_Cdadt031(net.ihe.gazelle.datatypes.TEL aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateTELSpec_Cdadt031(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt031");
		notif.setDescription("In TEL datatype, the use attribute SHALL have disctinct values (CDADT-031)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-TELSpec-cdadt031");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-031"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

}
