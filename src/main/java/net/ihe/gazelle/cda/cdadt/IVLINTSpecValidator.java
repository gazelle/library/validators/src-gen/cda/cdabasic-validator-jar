package net.ihe.gazelle.cda.cdadt;

import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;


 /**
  * class :        IVLINTSpec
  * package :   cdadt
  * Constraint Spec Class
  * class of test : IVLINT
  * 
  */
public final class IVLINTSpecValidator{


    private IVLINTSpecValidator() {}



	/**
	* Validation of instance by a constraint : cdadt017
	* In IVL<INT> datatypes, the low value SHALL be lessOrEqual the high value (CDADT-017)
	*
	*/
	private static boolean _validateIVLINTSpec_Cdadt017(net.ihe.gazelle.datatypes.IVLINT aClass){
		return (((((aClass.getLow() == null) || (aClass.getHigh() == null)) || (((Integer) aClass.getLow().getValue()) == null)) || (((Integer) aClass.getHigh().getValue()) == null)) || (aClass.getLow().getValue() <= aClass.getHigh().getValue()));
		
	}

	/**
	* Validation of class-constraint : IVLINTSpec
    * Verify if an element of type IVLINTSpec can be validated by IVLINTSpec
	*
	*/
	public static boolean _isIVLINTSpec(net.ihe.gazelle.datatypes.IVLINT aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   IVLINTSpec
     * class ::  net.ihe.gazelle.datatypes.IVLINT
     * 
     */
    public static void _validateIVLINTSpec(net.ihe.gazelle.datatypes.IVLINT aClass, String location, List<Notification> diagnostic) {
		if (_isIVLINTSpec(aClass)){
			executeCons_IVLINTSpec_Cdadt017(aClass, location, diagnostic);
		}
	}

	private static void executeCons_IVLINTSpec_Cdadt017(net.ihe.gazelle.datatypes.IVLINT aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateIVLINTSpec_Cdadt017(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Note();
		}
		notif.setTest("cdadt017");
		notif.setDescription("In IVL<INT> datatypes, the low value SHALL be lessOrEqual the high value (CDADT-017)");
		notif.setLocation(location);
		notif.setIdentifiant("cdadt-IVLINTSpec-cdadt017");
		notif.getAssertions().add(new Assertion("CDADT","CDADT-017"));
		
		notif.setType("Datatype");
		diagnostic.add(notif);
	}

}
